import { CommonModule } from "@angular/common";
import {
    ApplicationRef, Component, ComponentFactoryResolver, ComponentRef, Injectable, Injector, NgModule,
    ReflectiveInjector,
    ViewContainerRef
} from "@angular/core";
import { IPageBuilderEvent } from "coreContracts/pageBuilder/IPageBuilder";
import {
    IComponentMetadataRegistry,
    IPageBuilderComponentRegistry, IRenderablePageElement
} from "coreContracts/pageBuilder/IPageBuilderComponentRegistry";
import { IPageBuilderFactory, IPageComponentDefenitionParameters } from "coreContracts/pageBuilder/IPageBuilderFactory";
import { Observable } from "rxjs/Observable";
import { ReplaySubject } from "rxjs/ReplaySubject";
import { Subject } from "rxjs/Subject";


// 1: define Renderable components

// ---------------------------------- Simple data list ------------------------
//Common
abstract class WhenElementsChange{
    public abstract whenElementChange(): Observable<Array<string>>;
}

//View component
@Component({
    template: '<span><span>data list:</span><div *ngFor="let el of dataList | async">{{el}}</div></span>'
})
class SimpleDataListComponent {
    public dataList: Observable<Array<string>>;
    constructor(
        whenElementsChange: WhenElementsChange
    ) {
        this.dataList = whenElementsChange.whenElementChange();
    }
}

//Host Service
class SimpleDataListComponentHost implements IRenderablePageElement{
    private elementsModel: Subject<Array<string>> = new ReplaySubject(1);
    constructor(
        private cfr: ComponentFactoryResolver
    ) {
        this.elementsModel.next([]);
    }

    private whenElementsChange: WhenElementsChange = {
        whenElementChange: () => this.elementsModel
    };

    public render(viewContainer: ViewContainerRef, injector: Injector): Observable<ComponentRef<any>> {
        const cmpInst = this.cfr.resolveComponentFactory(SimpleDataListComponent);
        const cmpInjector = ReflectiveInjector.resolveAndCreate(
            [{
                provide: WhenElementsChange,
                useValue: this.whenElementsChange
            }],
            injector
        );
        const cmpRef = viewContainer
            .createComponent(
                cmpInst,
                0,
                cmpInjector
            );
        // hack for fix bug
        this.elementsModel.delay(0).subscribe( n => cmpRef.changeDetectorRef.detectChanges())
        return Observable.of(cmpRef);
    }

    public publishEvent(event: IPageBuilderEvent): IRenderablePageElement {
        this
            .elementsModel
            .first()
            .subscribe( next => {
                next.push(event.message);
                this.elementsModel.next(next);
            });
        return this;
    }
}

// Factory wrapper for page builder
@Injectable()
class SimpleDataList implements IComponentMetadataRegistry{
    constructor(
        private cfr: ComponentFactoryResolver
    ) {}

    public componentAlias: string = 'simpleDataList';
    public factoryMethod: (parameters: IPageComponentDefenitionParameters) => IRenderablePageElement =
        parameters => {
            return new SimpleDataListComponentHost(this.cfr);
        }
}

// --------------------------------- Simple form -----------------------------

//Common
abstract class WhenSavePublisher{
    public abstract publish(value: string);
}

//View component
@Component({
    template: '<input type="text" (change)="whenChangeInput.next($event)"><button (click)="whenSave.next()">save</button>'
})
class SimpleFormComponent{
    public whenChangeInput: Subject<Event> = new ReplaySubject(1);
    public whenSave: Subject<null> = new Subject();

    constructor(
        whenSavePublisher: WhenSavePublisher
    ) {
        this
            .whenSave
            .withLatestFrom(this
                .whenChangeInput
                .map( next => next.target['value'])
                .startWith(''))
            .map(([saveEvent, saveData]) => saveData)
            .subscribe( (value) => {
                whenSavePublisher.publish(value);
            });
    }
}

// Host Service
class SimpleFormComponentHost implements IRenderablePageElement{
    constructor(
        private cfr: ComponentFactoryResolver
    ) {}
    private readonly whenSavePublisherSubject = new Subject();
    private readonly whenSavePublisher: WhenSavePublisher = {
        publish: value => this.whenSavePublisherSubject.next(value)
    };
    public render(viewContainer: ViewContainerRef, injector: Injector): Observable<ComponentRef<any>> {
        const cmpInst = this.cfr.resolveComponentFactory(SimpleFormComponent);
        const cmpInjector = ReflectiveInjector.resolveAndCreate(
            [{
                provide: WhenSavePublisher,
                useValue: this.whenSavePublisher
            }],
            injector
        );
        const cmpRef = viewContainer
            .createComponent(
                cmpInst,
                0,
                cmpInjector
            );
        return Observable.of(cmpRef);
    }

    public listenEvents(): Observable<IPageBuilderEvent> {
        return this.whenSavePublisherSubject.asObservable().map(next => ({
            event: 'publishSaveData',
            message: next
        }));
    }
}

// Factory wrapper for page builder
@Injectable()
class SimpleForm implements IComponentMetadataRegistry{
    constructor(
        private cfr: ComponentFactoryResolver
    ) {}

    public componentAlias: string = 'simpleForm';
    public factoryMethod: (parameters: IPageComponentDefenitionParameters) => IRenderablePageElement =
        parameters => {
            return new SimpleFormComponentHost(this.cfr);
        }
}

// 2: bootstrap angular application

// -------------------- bootstrap app --------------------------

@NgModule({
    imports: [
        CommonModule
    ],
    providers: [
        SimpleForm,
        SimpleDataList
    ],
    declarations: [
        SimpleFormComponent,
        SimpleDataListComponent
    ],
    entryComponents: [
        SimpleFormComponent,
        SimpleDataListComponent
    ]
})
class MainAppModule {
    constructor(
        applicationRef: ApplicationRef,
        pageBuilderComponentRegistry: IPageBuilderComponentRegistry,
        pageBuilderFactory: IPageBuilderFactory,
        injector: Injector,
        simpleForm: SimpleForm,
        simpleDataList: SimpleDataList
    ) {
        // 3: register new renderable components for this application
        pageBuilderComponentRegistry
            .registerComponent(simpleForm)
            .registerComponent(simpleDataList);

        // 4: composite exists elements
        const pbInst = pageBuilderFactory.create({
            component: 'grid',
            parameters: {
                rows: '[[100]]',
                repeat: 'repeat',
                rowsCount: 3,
                aliases: '{"firstRow": [0,0], "secondRow": [1, 0], "thirdRow": [2, 0]}'
            },
            bindExternalComponent: {
                firstRow: {
                    component: 'simpleForm'
                },
                secondRow: {
                    component: 'simpleDataList',
                    componentAlias: 'dataList'
                },
                thirdRow: {
                    component: 'simpleForm'
                }
            }
        });

        // 5: await when angular app ready for start
        applicationRef
            .isStable
            .filter(n => n)
            .first()
            .subscribe( n => {
                // 6: render current sheme
                pbInst
                    .render(applicationRef.components[0].instance.viewContainerRef, injector)
                    // 7: hackable await when all nodes are rendered
                    .delay(1e3)
                    .subscribe(n => {
                        // 8: add mediate signals logick
                        Observable
                            .merge(
                                // hackable subscription to private sandboxes, becouse public not ready
                                ...pbInst['anonymousSandBoxes'].filter(el => el['output']).map( el => el['output']))
                            .filter( next => next['event'] === 'publishSaveData')
                            .subscribe( next => {
                                pbInst.publishEvent({
                                    event: 'addElement',
                                    message: next['message'],
                                    targetComponentAliases: ['dataList']
                                });
                            });
                    });
            } );
    }
}

export { MainAppModule };