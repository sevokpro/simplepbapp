import { Observable } from "rxjs";

interface IDateTimeFormat {
    dateFormat: 'YYYY.MM.DD';
    dateDelimiter: '.' | '/';
    time?: 'HH:MM:SS' | 'HH:MM';
}

abstract class ILocalizationLoader {
    public abstract loadAvailableLocalizations(): Observable<Array<string>>;
    public abstract loadLocalization(locale: string): Observable<Map<string, string>>;
    public abstract getDefaultLocale(): Observable<string>;
}

abstract class ILocalization {
    public abstract getCurrentLocale(): Observable<string>;
    public abstract changeLocale(newLocale: string): Observable<'ok'>;
    public abstract getLocaleList(): Observable<Array<string>>;

    public abstract localizeString(stringVal: string): Observable<string>;
    public abstract localizeNumber(numberVal: number): Observable<number>;
    public abstract localizeIsoDateTime(stringVal: string, format: IDateTimeFormat): Observable<string>;
}

export { IDateTimeFormat, ILocalization, ILocalizationLoader };