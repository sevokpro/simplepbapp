import { IGrid } from "coreContracts/grid/IGrid";
import { IGridParameters } from "coreContracts/grid/IGridParameters";

abstract class IGridFactory{
    public abstract create(params: IGridParameters): IGrid;
}

export {IGridFactory};