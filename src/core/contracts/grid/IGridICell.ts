interface IGridCell {
    row: number;
    column: number;
}

export {IGridCell};