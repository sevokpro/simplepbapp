import { IGridColumn } from "coreContracts/grid/IGridColumn";

type IGridColumnCollection = Array<IGridColumn>;

export {IGridColumnCollection};