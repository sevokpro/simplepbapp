import { ComponentRef, Injector, ViewContainerRef } from "@angular/core";
import { IGridCell } from "coreContracts/grid/IGridICell";
import { Observable } from "rxjs/Observable";

abstract class IGrid{
    public abstract getCell(cell: IGridCell): Observable<ViewContainerRef>;
    public abstract getCellByAlias(alias: string): Observable<ViewContainerRef>;
    public abstract render(viewContainerRef: ViewContainerRef, injector: Injector): ComponentRef<any>;
}

export {IGrid};