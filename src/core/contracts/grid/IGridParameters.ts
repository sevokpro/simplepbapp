import { GridRepeatMode } from "coreContracts/grid/GridRepeatMode";
import { IGridColumnCollection } from "coreContracts/grid/IGridColumnCollection";
import { IGridCell } from "coreContracts/grid/IGridICell";

type IGridRowsCollection = Array<IGridColumnCollection>;

interface IGridParameters{
    rows: IGridRowsCollection;
    rowsCount?: number;
    rowsRepeatMode?: GridRepeatMode;
    aliases?: Map<string, IGridCell>;
}

export {IGridParameters, IGridRowsCollection};