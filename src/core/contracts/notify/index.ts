import { Observable } from "rxjs";

interface IPosition {
    horizontal: HorizontalPosition;
    vertical: VerticalPosition;
}

enum VerticalPosition {
    top,
    middle,
    bottom
}

enum HorizontalPosition {
    right,
    middle,
    left
}

interface INotifyCtrl {
    hide(): Observable<void>;
    show(): Observable<void>;
    destroy();

    prolong(duration: number);
    updateConfig(config: INotifyConfig);
}

interface INotifyConfig {
    duration?: number;
    position?: IPosition;
}

abstract class INotify {
    public abstract success(message: string, config?: INotifyConfig): Observable<INotifyCtrl>;
    public abstract warn(message: string, config?: INotifyConfig): Observable<INotifyCtrl>;
    public abstract info(message: string, config?: INotifyConfig): Observable<INotifyCtrl>;
    public abstract error(message: string, config?: INotifyConfig): Observable<INotifyCtrl>;
}

export {
    INotify,
    INotifyCtrl,
    INotifyConfig,
    IPosition,
    VerticalPosition,
    HorizontalPosition
};