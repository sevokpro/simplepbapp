interface ILoggerMessage {
    target: 'server' | 'console';
    stackTrace: Error;
    message: string;
    logObject?: object;
}

export { ILoggerMessage };