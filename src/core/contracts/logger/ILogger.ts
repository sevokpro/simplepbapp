import { ILoggerMessage } from "./ILoggerMessage";

abstract class ILogger {
    public abstract log(message: ILoggerMessage): Promise<'ok'>;
    public abstract warn(message: ILoggerMessage): Promise<'ok'>;
    public abstract error(message: ILoggerMessage): Promise<'ok'>;
}

export { ILogger };