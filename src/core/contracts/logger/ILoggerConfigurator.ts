import { ILoggerConfig } from "./ILoggerConfig";

abstract class ILoggerConfigurator {
    public abstract setConfig(config: ILoggerConfig);
    public abstract getConfig(): ILoggerConfig;
}

export { ILoggerConfigurator };