interface ILoggerConfig {
    enableConsoleLog: boolean;
    enableServerLog: boolean;
    duplicateConsoleToServer: boolean;
}

export { ILoggerConfig };