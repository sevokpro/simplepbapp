import { IPageBuilder } from "coreContracts/pageBuilder/IPageBuilder";
import { IPageComponentDefenition } from "coreContracts/pageBuilder/IPageBuilderFactory";

abstract class IPageBuilderConstructor extends IPageBuilder{
    public abstract exportSheme(): IPageComponentDefenition;
}

export {IPageBuilderConstructor};