import { IPageBuilderEvent } from "coreContracts/pageBuilder/IPageBuilder";

abstract class IPageBuilderSwitchContainerActivateContainerEvent implements IPageBuilderEvent{
    public abstract event: string;
    public abstract targetComponentAliases?: Array<string>;
    public abstract message?: string;
    public abstract containerName: string;
}

abstract class IPageBuilderSwitchContainerEventsFactory{
    public abstract activateContainer(containerName: string, targetAlias: string): IPageBuilderSwitchContainerActivateContainerEvent;
}

export {IPageBuilderSwitchContainerEventsFactory, IPageBuilderSwitchContainerActivateContainerEvent};