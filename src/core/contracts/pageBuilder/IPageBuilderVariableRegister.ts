import { Observable } from "rxjs/Observable";

type IVariableAttributes = {
    [attributeKey: string]: any
};

type IVariableAttributesMetadata = {
    [attributeKey: string]: {
        required: boolean,
        defaultValue?: any
    }
};

interface IPageBuilderVariableMetadata{
    alias: string;
    attributesMetadata?: IVariableAttributesMetadata;
    value: (attributes?: IVariableAttributes) => Observable<any>;
}

abstract class IPageBuilderVariableRegistry{
    public abstract registerVariable(constantMetadata: IPageBuilderVariableMetadata): IPageBuilderVariableRegistry;
    public abstract getRegistryVariableMetadata(alias: string): IPageBuilderVariableMetadata;
}

export {IPageBuilderVariableMetadata, IPageBuilderVariableRegistry, IVariableAttributes, IVariableAttributesMetadata};