import { Injector, ViewContainerRef } from "@angular/core";
import { Observable } from "rxjs/Observable";

interface IListenEvent{
    componentAlias: string;
    event: string;
    message: any;
}

interface IPageBuilderEvent{
    event: string;
    targetComponentAliases?: Array<string>;
    message?: any;
}

abstract class IPageBuilder{
    public abstract render(viewContainer: ViewContainerRef, injector: Injector): any;
    public abstract subscribeOnEvents(): Observable<IListenEvent>;
    public abstract publishEvent(message: IPageBuilderEvent): IPageBuilder;
}

export {IPageBuilder, IListenEvent, IPageBuilderEvent};