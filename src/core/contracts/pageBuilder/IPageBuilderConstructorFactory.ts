import { IPageBuilderConstructor } from "coreContracts/pageBuilder/IPageBuilderConstructor";
import { IPageComponentDefenition } from "coreContracts/pageBuilder/IPageBuilderFactory";

abstract class IPageBuilderConstructorFactory{
    public abstract create(pageCreateParameters: IPageComponentDefenition): IPageBuilderConstructor;
}

export {IPageBuilderConstructorFactory};