import { ComponentRef, Injector, ViewContainerRef } from "@angular/core";
import { IPageBuilderEvent } from "coreContracts/pageBuilder/IPageBuilder";
import { IPageComponentDefenitionParameters } from "coreContracts/pageBuilder/IPageBuilderFactory";
import { Observable } from "rxjs/Observable";

interface IRenderablePageElement{
    render(viewContainer: ViewContainerRef, injector: Injector): Observable<ComponentRef<any>>;
    getExposeRef?(refAlias: string): Observable<ViewContainerRef>;

    getExposeRefAliases?(): Observable<Array<string>>;

    publishEvent?(event: IPageBuilderEvent): IRenderablePageElement;
    listenEvents?(): Observable<IPageBuilderEvent>;
}

type IPageBuilderParametersMetadataAvailableTypes = 'string' | 'number' | 'json' | 'date';

interface IParametersMetadata{
    [parametrAlias: string]: {
        type: IPageBuilderParametersMetadataAvailableTypes,
        required: boolean,
        defaultValue?: any,
        verboseName?: string,
        description?: string,
        example?: {
            verbose: string,
            exampleValue: any
        }
    };
}

interface IComponentMetadataRegistry{
    componentAlias: string;
    factoryMethod: (parameters: IPageComponentDefenitionParameters) => IRenderablePageElement;
    parametersMetadata?: IParametersMetadata;
}

abstract class IPageBuilderComponentRegistry{
    public abstract registerComponent(metadata: IComponentMetadataRegistry): IPageBuilderComponentRegistry;
    public abstract getComponentMetadata(componentAlias: string): IComponentMetadataRegistry;
    public abstract getRegisteredComponents(): Array<string>;
}

export {IPageBuilderComponentRegistry, IComponentMetadataRegistry, IRenderablePageElement, IParametersMetadata, IPageBuilderParametersMetadataAvailableTypes};