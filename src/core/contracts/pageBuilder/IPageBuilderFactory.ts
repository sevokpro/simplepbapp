import { IPageBuilder } from "coreContracts/pageBuilder/IPageBuilder";

type IPageComponentDefenitionParameters = {[parametrAlias: string]: any};

interface IPageComponentDefenition{
    component: string;
    componentAlias?: string;
    parameters?: IPageComponentDefenitionParameters;
    bindExternalComponent?: {
        [refAlias: string]: IPageComponentDefenition
    };
}

abstract class IPageBuilderFactory{
    public abstract create(pageCreateParameters: IPageComponentDefenition): IPageBuilder;
}

export {IPageComponentDefenitionParameters, IPageComponentDefenition, IPageBuilderFactory};