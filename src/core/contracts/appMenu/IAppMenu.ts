import { Injector, ViewContainerRef } from "@angular/core";
import { INode } from "coreContracts/appMenu/IAppMenuNode";
import { Observable } from "rxjs/Observable";
import { WindowUnits } from "../../modules/window/windowUnits";

abstract class IAppMenu {
    public abstract loadNodes(nodes: Array<INode>);
    public abstract selectActiveNode(nodeUuid: string);
    public abstract render(viewRef: ViewContainerRef, injector: Injector);

    public abstract minimize();
    public abstract maximize();
    public abstract toggle();

    public abstract whenMinimize(): Observable<undefined>;
    public abstract whenMaximize(): Observable<undefined>;
    public abstract whenClickNode(): Observable<INode>;
}

export { IAppMenu };