interface INode {
    verboseName: string;
    iconHtml: string;
    smallIconHtml: string;
    href: string;
    nodeUuid: string;
    parentNodeUuid: string;
    childNodes: Array<INode>;
}

export { INode };