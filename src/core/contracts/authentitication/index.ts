import { CanActivate, CanLoad } from "@angular/router";
import { Observable } from "rxjs";

abstract class IAuthenticationService {
    public abstract authenticate(login: string, password: string): Observable<null>;
    public abstract checkToken(token: string): Observable<null>;
    public abstract logout(): Observable<null>;
}

abstract class IAuthGuard implements CanActivate, CanLoad {
    public abstract canActivate(): boolean | Promise<boolean> | Observable<boolean>;
    public abstract canLoad(): boolean | Promise<boolean> | Observable<boolean>;
}

export { IAuthenticationService, IAuthGuard };