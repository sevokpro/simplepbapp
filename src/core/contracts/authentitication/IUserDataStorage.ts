import { Observable } from "rxjs/Observable";
abstract class IUserDataStorage<TUserData>{
    public abstract getUserData(): Observable<TUserData>;
}

export {IUserDataStorage};