import { Observable } from "rxjs/Observable";
abstract class ITokenStorage {
    public abstract getToken(): Observable<string>;
}