import { IDataSource } from "../../../common/IDataSource/IDataSource";

abstract class IInMemoryRepository extends IDataSource { }

export { IInMemoryRepository };