import { ITabsDrawer } from "coreContracts/tabsDrawer/ITabsDrawer";
import { Observable } from "rxjs/Observable";

interface ISingleTab{
    label: Observable<string>;
    alias: string;
}

type ITabs = Array<ISingleTab>;

abstract class ITabsDrawerFactory{
    public abstract create(tabs: ITabs): ITabsDrawer;
}

export {ITabsDrawerFactory, ISingleTab, ITabs};