import { ComponentRef, Injector, ViewContainerRef } from "@angular/core";
import { Observable } from "rxjs/Observable";

abstract class ITabsDrawer{
    public abstract render(viewContainer: ViewContainerRef, injector: Injector): ComponentRef<any>;
    public abstract whenClickTab(): Observable<String>;
    public abstract activateTab(tabAlias: string);
}

export {ITabsDrawer};