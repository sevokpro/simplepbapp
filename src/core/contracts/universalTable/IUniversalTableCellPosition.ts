abstract class IUniversalTableCellPosition {
    public abstract rowIndex: number;
    public abstract cellIndex: number;
    public abstract cellData: any;
}

export { IUniversalTableCellPosition };