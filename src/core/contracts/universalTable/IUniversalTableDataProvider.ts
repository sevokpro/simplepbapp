import { Observable } from "rxjs/Observable";
import { IDataSourceReadRequestFilterNode } from "../../../common/IDataSource/read/request/IDataSourceReadRequestFilterBlock";
import { IDataSourceReadRequestSort } from "../../../common/IDataSource/read/request/IDataSourceReadRequestSort";

interface IUniversalTableQueryConfig {
    filter?: IDataSourceReadRequestFilterNode;
    fields?: Array<string>;
    sort?: IDataSourceReadRequestSort;
    page?: number;
    pageLimit?: number;
}

interface IUniversalTableGetHeadersQuery {
    viewFields: Array<string>;
}

interface IUniversalTableDataProperty {
    value: any;
    type: string;
}

interface IUniversalTableGetDataResponse {
    data: Array<Map<string, IUniversalTableDataProperty>>;
}

interface IUniversalTableHeader {
    label: string;
    type: string;
}

interface IUniversalTableGetHeadersResponse {
    headers: Array<IUniversalTableHeader>;
}

interface IUniversalTableCountQuery {
    filter: IDataSourceReadRequestFilterNode;
}

interface IUniversalTableDataProvider {
    getData(query: IUniversalTableQueryConfig): Observable<IUniversalTableGetDataResponse>;
    getHeaders(query: IUniversalTableGetHeadersQuery): Observable<IUniversalTableGetHeadersResponse>;
    getCount(query: IUniversalTableCountQuery): Observable<number>;
}

export { IUniversalTableDataProvider, IUniversalTableGetHeadersQuery, IUniversalTableGetDataResponse, IUniversalTableGetHeadersResponse, IUniversalTableQueryConfig, IUniversalTableCountQuery, IUniversalTableHeader, IUniversalTableDataProperty };