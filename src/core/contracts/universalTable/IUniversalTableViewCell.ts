interface IUniversalTableViewCell {
    type: string;
    value: string;
    instance?: any;
}

export { IUniversalTableViewCell };