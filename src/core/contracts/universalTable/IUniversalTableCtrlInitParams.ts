import {
    IUniversalTableDataProvider,
    IUniversalTableGetDataResponse
} from "coreContracts/universalTable/IUniversalTableDataProvider";
import { Observable } from "rxjs";
import { IUniversalTableComponents } from "./IUniversalTableDefaultComponents";
import { IUniversalTableGetDataRequest } from "./IUniversalTableGetDataRequest";
import { IUniversalTableState } from "./IUniversalTableState";

interface IUniversalTableCtrlInitParams {
    getDataMethod?: (query: IUniversalTableGetDataRequest) => Observable<IUniversalTableGetDataResponse>;
    componentsMap?: IUniversalTableComponents;
    initState?: IUniversalTableState;
    dataProvider?: IUniversalTableDataProvider;
}

export { IUniversalTableCtrlInitParams };