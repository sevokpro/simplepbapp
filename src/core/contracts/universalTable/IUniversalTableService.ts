import { IUniversalTableCtrl } from "./IUniversalTableCtrl";
import { IUniversalTableCtrlInitParams } from "./IUniversalTableCtrlInitParams";

abstract class IUniversalTableCtrlFactory {
    public abstract createInstance(initParams: IUniversalTableCtrlInitParams): IUniversalTableCtrl;
}

export { IUniversalTableCtrlFactory };