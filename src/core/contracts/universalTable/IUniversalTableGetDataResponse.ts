import { IDictionary } from "@itexpert-dev/i-dictionary";
import { IUniversalTableFieldMetadata } from "coreContracts/universalTable/IUniversalTableFieldMetadata";
import { IUniversalTableFieldValue } from "coreContracts/universalTable/IUniversalTableFieldValue";

interface IUniversalTableGetDataResponse {
    data: Array<IDictionary<IUniversalTableFieldValue>>;
    metadata: IDictionary<IUniversalTableFieldMetadata>;
    totalResult: number;
}

export { IUniversalTableGetDataResponse };