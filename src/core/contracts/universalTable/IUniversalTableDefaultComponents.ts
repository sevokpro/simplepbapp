interface IUniversalTableComponents {
    view?: string;
    container?: string;
    grid?: string;
    headers?: string;
    rows?: string;
    cell?: string;
    paginator?: string;
}

export { IUniversalTableComponents };