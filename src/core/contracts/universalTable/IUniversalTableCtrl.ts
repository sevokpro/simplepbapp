import { Injector, ViewContainerRef } from "@angular/core";
import { Observable } from "rxjs";

import { IUniversalTableCellTextClickParams } from "coreContracts/universalTable/IUniversalTableCellTextClickParams";
import { IUniversalTableDiffState } from "coreContracts/universalTable/IUniversalTableDiffState";
import { IUniversalTableCellPosition } from "./IUniversalTableCellPosition";
import { IUniversalTableCtrlInitParams } from "./IUniversalTableCtrlInitParams";
import { IUniversalTableComponents } from "./IUniversalTableDefaultComponents";
import { IUniversalTableHeaderData } from "./IUniversalTableHeaderData";
import { IUniversalTableState } from "./IUniversalTableState";
import { IUniversalTableViewScope } from "./IUniversalTableViewScope";

abstract class IUniversalTableCtrl {
    public abstract initParams: IUniversalTableCtrlInitParams;
    public abstract viewScope: IUniversalTableViewScope;
    public abstract currentState: IUniversalTableState;
    public abstract tableComponents: IUniversalTableComponents;
    public abstract viewContainerRefs: Map<string, ViewContainerRef>;
    public abstract updateState(state: IUniversalTableState);
    public abstract stateStream: Observable<IUniversalTableDiffState>;

    public abstract cellTextClickStream: Observable<IUniversalTableCellTextClickParams>;

    public abstract renderContainer(container: ViewContainerRef, parentInjector: Injector): void;
    public abstract renderPaginator(container: ViewContainerRef, parentInjector: Injector): void;
    public abstract renderCell(container: ViewContainerRef, parentInjector: Injector, cellPosition: IUniversalTableCellPosition): void;
    public abstract renderGrid(container: ViewContainerRef, parentInjector: Injector): void;
    public abstract renderHeader(container: ViewContainerRef, parentInjector: Injector, headerData: IUniversalTableHeaderData): void;
}

export { IUniversalTableCtrl };