interface IUniversalTableState {
    limit?: number;
    offset?: number;
    fields?: Array<string>;
    sort?: Array<{ field: string, direction: 'asc' | 'desc' }>;
    filter?: any;
    viewFields?: Array<string>;
    totalResult?: number;
    isUpdate?: boolean;
}

export { IUniversalTableState };