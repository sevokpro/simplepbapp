import { IUniversalTableState } from "coreContracts/universalTable/IUniversalTableState";

interface IUniversalTableDiffState {
    currentState: IUniversalTableState;
    difference: IUniversalTableState;
}

export { IUniversalTableDiffState };