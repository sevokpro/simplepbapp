interface IUniversalTableFieldMetadata {
    type: string;
    verboseName: string;
}
export { IUniversalTableFieldMetadata };