import { KeyValueStorage } from "@itexpert-dev/key-value-storage";
import { IUniversalTableComponents } from "./IUniversalTableDefaultComponents";
import { IUniversalTableState } from "./IUniversalTableState";

abstract class IUniversalTableConfigurator {
    public abstract componentsStorage: Map<string, any>;
    public abstract cellsStorage: Map<string, any>;
    public abstract cellTypeToComponentMap: KeyValueStorage<string>;

    public abstract setDefaultInitTableState(newState: IUniversalTableState): void;
    public abstract getDefaultInitTableState(): IUniversalTableState;
    public abstract setDefaultComponents(components: IUniversalTableComponents): void;
    public abstract getDefaultComponents(): IUniversalTableComponents;
    public abstract setDefaultLimitSteps(...steps: Array<number>): void;
    public abstract getDefaultLimitSteps(): Array<number>;
}

export { IUniversalTableConfigurator };