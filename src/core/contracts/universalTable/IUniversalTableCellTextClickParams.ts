import { BaseUniversalTableCellComponent } from "coreModulesSdk/universalTable/BaseUniversalTableDataCellComponent";
abstract class IUniversalTableCellTextClickParams {
    public abstract context: BaseUniversalTableCellComponent;
    public abstract event: MouseEvent;
}

export { IUniversalTableCellTextClickParams };