abstract class IUniversalTableHeaderData {
    public abstract headerIndex: number;
    public abstract headerData: any;
}

export { IUniversalTableHeaderData };