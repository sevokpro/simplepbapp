import { IUniversalTableDataProperty } from "coreContracts/universalTable/IUniversalTableDataProvider";

interface IUniversalTableDataRows {
    data: Array<Map<string, IUniversalTableDataProperty>>;
    viewFields: Array<string>;

}
export { IUniversalTableDataRows };