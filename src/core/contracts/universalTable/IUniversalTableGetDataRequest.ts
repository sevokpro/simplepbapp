interface IUniversalTableGetDataRequest {
    limit?: number;
    offset?: number;
    fields?: Array<string>;
    sort?: Array<string>;
    filter?: any;
}
export { IUniversalTableGetDataRequest };