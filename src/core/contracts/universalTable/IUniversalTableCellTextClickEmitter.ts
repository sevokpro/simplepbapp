import { IUniversalTableCellTextClickParams } from "coreContracts/universalTable/IUniversalTableCellTextClickParams";
abstract class IUniversalTableCellTextClickEmitter {
    public abstract emit: (clickParams: IUniversalTableCellTextClickParams) => void;
}

export {IUniversalTableCellTextClickEmitter};