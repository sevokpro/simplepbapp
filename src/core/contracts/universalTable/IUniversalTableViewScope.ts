import { IUniversalTableGetHeadersResponse } from "coreContracts/universalTable/IUniversalTableDataProvider";
import { IUniversalTableDataRows } from "coreContracts/universalTable/IUniversalTableDataRows";
import { Observable } from "rxjs";

interface IUniversalTableViewScope {
    headers: Observable<IUniversalTableGetHeadersResponse>;
    rows: Observable<IUniversalTableDataRows>;
}

export { IUniversalTableViewScope };