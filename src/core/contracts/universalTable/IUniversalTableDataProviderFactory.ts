import { Observable } from "rxjs/Observable";
import { IDataSource } from "../../../common/IDataSource/IDataSource";
import { UniversalTableDataProvider } from "../../modules/universalTable/universalTableDataProviderFactory";

abstract class IUniversalTableDataProviderFactory {
    public abstract createInstanceFromDataSource(dataSource: IDataSource, entityIdentifier: string): Observable<UniversalTableDataProvider>;
}

export { IUniversalTableDataProviderFactory };