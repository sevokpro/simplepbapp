import { Injector, ViewContainerRef } from "@angular/core";
import { Observable } from "rxjs";
import { IChatConfigs } from "../../modules/chat/modules/chat/services/types/interfaces/IChatConfigs";

abstract class IChat {
    public abstract render(viewRef: ViewContainerRef, injector: Injector, initConfig: IChatConfigs);
    public abstract open();
    public abstract close();
    public abstract get unreadMessagesCount(): Observable<number>;
}

export { IChat };