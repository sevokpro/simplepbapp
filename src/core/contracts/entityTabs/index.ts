import { Injector, ViewContainerRef } from "@angular/core";
import { Observable } from "rxjs/Observable";

interface IEntityTabs {
    render(viewContainer: ViewContainerRef, injector: Injector);
    subscriptionOnChangeTab(): Observable<{ entityIdentifier: string }>;
}

abstract class IEntityTabsFactory {
    public abstract createEntityTabs(filter: string, entityName: string): Observable<IEntityTabs>;
}

export { IEntityTabsFactory, IEntityTabs };