import { Observable } from "rxjs";

abstract class IServerMethodsExpansionPoint {
    public abstract registerMethod(observable: Observable<any>);
}