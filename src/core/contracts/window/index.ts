import { Observable } from "rxjs";

interface IWindowUnits{
    getNumber(): number;
    getPixel(): string;
}

abstract class IWindowUnitsFactory{
    public abstract createFromPixel(count: number): IWindowUnits;
}

abstract class IWindow {
    public abstract getWindowHeight(): Observable<IWindowUnits>;
    public abstract getWindowWidth(): Observable<IWindowUnits>;
    public abstract getTopNavbarHeight(): Observable<IWindowUnits>;
    public abstract publishTopNavbarHeight(newHeight: IWindowUnits);
}

export { IWindow, IWindowUnits, IWindowUnitsFactory };