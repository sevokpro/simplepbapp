import { Injector, ViewContainerRef } from "@angular/core";

abstract class IMessageBox {
    public abstract render(viewContainer: ViewContainerRef, injector: Injector);
}

export { IMessageBox };