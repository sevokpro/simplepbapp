import { IDataSource } from "../../../common/IDataSource/IDataSource";

abstract class IBackendSource extends IDataSource { }

export { IBackendSource };