import { Injector, ViewContainerRef } from "@angular/core";

abstract class IRootAppContainer {
    public abstract get viewContainerRef(): ViewContainerRef
    public abstract set viewContainerRef(viewContainer: ViewContainerRef)

    public abstract get injector(): Injector
    public abstract set injector(injector: Injector)
}

export { IRootAppContainer };