import { Component, Injector, ViewContainerRef } from "@angular/core";
import { Observable } from "rxjs/Observable";

/**
 * @desc Tree configurator
 */
interface ITreeConfigurator{
    nodeComponentsStorage: Map<string, Component>;
    nodeContentComponentsStorage: Map<string, Component>;
    nodeExpandIconComponentsStorage: Map<string, Component>;

    setBaseNode( componentId: string ): void;
    setBaseNodeContent( componentId ): void;
    setBaseExpandIcon( componentId: string ): void;
}

/**
 * @desc Tree Controller Factory
 */
interface ITreeControllerFactory{
    createTree(config?: {autoOpenChilds: Boolean}): ITreeController;
}

/**
 * @desc Tree component manager
 */
interface ITreeController{
    registerFirstLevelNodes(nodes: Array<IBaseTreeNode>);
    renderNodes(viewContainer: ViewContainerRef, injector: Injector);

    // TODO: add event register
    whenClickNode: Observable<IBaseTreeNode>;
    whenClickContent: Observable<IBaseTreeNode>;
    whenClickExpandIco: Observable<IBaseTreeNode>;
}

/**
 * @desc Base tree element, which can be override by config service
 */
interface IBaseTreeNode{
    getUuid(): number;

    childs?: Observable<Array<IBaseTreeNode>>;
    whenChildsLoadingStateChage: Observable<'notStartedYet' | 'loading' | 'await' | 'complete' | 'fail'>;

    nodeContent: string;
    contentType: 'html' | 'string';

    hideExpandIcon(): void;
    showExpandIcon(): void;

    showChilds(): void;
    hideChilds(): void;

    overrideNodeComponent?: string;
    overrideExpandIcoComponent?: string;
    overrideContentWrapper?: string;

    getBaseContent(): IBaseTreeNodeContent;
    getContent<TNodeContentType>(type: TNodeContentType): Component;
}

interface IHtmlContentTreeNode{
    setHtml(htmlText: string): void;
    getHtml(): string;
}

/**
 * @desc Base element for represent node content
 */
interface IBaseTreeNodeContent{
    setText(text: string): void;
    getText(): string;
}