abstract class IAdvancedFormFieldConfig {
    public abstract widthInPercent?: number;
}

export { IAdvancedFormFieldConfig };