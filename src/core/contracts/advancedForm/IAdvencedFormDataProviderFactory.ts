import { IAdvancedFormMetaField } from "coreContracts/advancedForm/IAdvancedFormMetaField";
import { Observable } from "rxjs/Observable";
import { IDataSource } from "../../../common/IDataSource/IDataSource";

interface IAdvancedFormDataProviderResult {
    metadata: Map<string, IAdvancedFormMetaField>;
    data?: Map<string, any>;
}

abstract class IAdvancedFormDataProvider {
    public abstract getInitData(): Observable<IAdvancedFormDataProviderResult>;
}

interface IAdvancedFormDataProviderFromDataSourceParams {
    entityName: string;
    entityUuid?: string;
    source: IDataSource;
}

abstract class IAdvancedFormDataProviderFactory {
    public abstract createDataProviderFromDataSource(
        params: IAdvancedFormDataProviderFromDataSourceParams
    ): Observable<IAdvancedFormDataProvider>;
}

export { IAdvancedFormDataProviderFactory, IAdvancedFormDataProvider, IAdvancedFormDataProviderFromDataSourceParams, IAdvancedFormDataProviderResult };