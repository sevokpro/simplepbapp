import { IAdvancedFormCtrl } from "./IAdvancedFormCtrl";
import { IAdvancedFormCtrlInitParams } from "./IAdvancedFormInitParams";

abstract class IAdvancedFormFactory {
    public abstract createCtrlInstance(initParams: IAdvancedFormCtrlInitParams): IAdvancedFormCtrl;
}

export { IAdvancedFormFactory };