abstract class IAdvancedFormBlockKey {
    public abstract block: string;
}

export { IAdvancedFormBlockKey };