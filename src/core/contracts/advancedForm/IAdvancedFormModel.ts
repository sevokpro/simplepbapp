import { IAdvancedFormBlockConfig } from "./IAdvancedFormBlockConfig";
import { IAdvancedFormMetaField } from "./IAdvancedFormMetaField";

abstract class IAdvancedFormModel {
    public abstract metadata: Map<string, IAdvancedFormMetaField>;
    public abstract data: Map<string, any>;
    public abstract blocks: Map<string, IAdvancedFormBlockConfig>;
}

export { IAdvancedFormModel };