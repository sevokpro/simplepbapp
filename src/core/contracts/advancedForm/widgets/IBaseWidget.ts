import { Observable } from "rxjs/Observable";

abstract class IBaseWidget<TExportType>{
    public abstract setValue(value: TExportType);
    public abstract getValue(): TExportType;
    public abstract clear();
    public abstract label: Observable<string>;
}

export { IBaseWidget };