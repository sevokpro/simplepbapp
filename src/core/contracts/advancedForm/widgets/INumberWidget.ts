import { IBaseWidget } from "coreContracts/advancedForm/widgets/IBaseWidget";

type INumberWidgetValue = number;

abstract class INumberWidget extends IBaseWidget<INumberWidgetValue>{
}

export { INumberWidget, INumberWidgetValue };