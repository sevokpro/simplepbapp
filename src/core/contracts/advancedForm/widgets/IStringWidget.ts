import { IBaseWidget } from "coreContracts/advancedForm/widgets/IBaseWidget";

type IStringWidgetValue = string;

abstract class IStringWidget extends IBaseWidget<IStringWidgetValue>{
}

export { IStringWidget, IStringWidgetValue };