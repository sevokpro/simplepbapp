import { IBaseWidget } from "coreContracts/advancedForm/widgets/IBaseWidget";

interface IAutoCompleteWidgetValue {
    value: string;
    referenceName: string;
    reference: string;
}

abstract class IAutoCompleteWidget extends IBaseWidget<IAutoCompleteWidgetValue> {
}

export { IAutoCompleteWidget, IAutoCompleteWidgetValue };