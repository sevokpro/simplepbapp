import { IBaseWidget } from "coreContracts/advancedForm/widgets/IBaseWidget";
import { Observable } from "rxjs/Observable";

interface IDataListElement {
    label: string;
    value: any;
}

interface IDataList {
    elements: Array<IDataListElement>;
}

abstract class IDropDownListWidget extends IBaseWidget<IDataListElement> {
    public abstract setListData(dataListStream: IDataList);

    public abstract pushPreloaderState(state: { isActive: boolean });

    public abstract userSelectStream: Observable<IDataListElement>;
    public abstract userOpenAndCloseStream: Observable<{ isOpen: boolean }>;
}

export { IDataListElement, IDataList, IDropDownListWidget };