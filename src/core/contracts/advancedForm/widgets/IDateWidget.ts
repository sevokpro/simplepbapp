import { IBaseWidget } from "coreContracts/advancedForm/widgets/IBaseWidget";

type IDateTimeWidgetValue = string;

abstract class IDateTimeWidget extends IBaseWidget<IDateTimeWidgetValue> {
}

export { IDateTimeWidget, IDateTimeWidgetValue };