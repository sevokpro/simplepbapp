import { IBaseWidget } from "coreContracts/advancedForm/widgets/IBaseWidget";

type IBooleanWidgetValue = boolean;

abstract class IBooleanWidget extends IBaseWidget<IBooleanWidgetValue>{

}

export { IBooleanWidgetValue, IBooleanWidget };