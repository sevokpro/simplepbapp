abstract class IAdvancedFormMetaField {
    public abstract type: string;
    public abstract label: string;
    public abstract available?: boolean;
    public abstract required?: boolean;
    public abstract visible?: boolean;
    public abstract additionalProperties?: any;
}

export { IAdvancedFormMetaField };