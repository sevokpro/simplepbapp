abstract class IAdvancedFormFieldKey {
    public abstract key: string;
}

export { IAdvancedFormFieldKey };