import { IAdvancedFormMetaField } from "coreContracts/advancedForm/IAdvancedFormMetaField";
import { IAdvancedFormBlockConfig } from "./IAdvancedFormBlockConfig";

abstract class IAdvancedFormCtrlInitParams {
    public abstract metadata: Map<string, IAdvancedFormMetaField>;
    public abstract data?: Map<string, any>;
    public abstract blocks?: Map<string, IAdvancedFormBlockConfig>;
}

export { IAdvancedFormCtrlInitParams };