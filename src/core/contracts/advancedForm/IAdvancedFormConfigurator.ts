import { Component } from "@angular/core";

abstract class IAdvancedFormConfigurator {
    public abstract widgetsStorage: Map<string, any>;
    public abstract advancedFormViewComponent: any;
    public abstract typeToWidgetNameMap: Map<string, string>;
}

export { IAdvancedFormConfigurator };