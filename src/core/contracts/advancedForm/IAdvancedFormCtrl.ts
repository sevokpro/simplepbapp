import { Injector, ViewContainerRef } from "@angular/core";
import { IBaseWidget } from "coreContracts/advancedForm/widgets/IBaseWidget";
import { Observable } from "rxjs/Observable";
import { IAdvancedFormModel } from "./IAdvancedFormModel";

abstract class IAdvancedFormCtrl {
    public abstract formModel: IAdvancedFormModel;

    public abstract renderFormBlock(block: string, container: ViewContainerRef, parentInjector: Injector);
    public abstract renderWidget(field: string, container: ViewContainerRef, parentInjector: Injector): IBaseWidget<any>;

    public abstract getFieldLabel(field: string): Observable<string>;
    public abstract pushInitializedWidget(field: string, context: IBaseWidget<any>);

    public abstract getEntity(): Observable<any>;

    public abstract getDefaultWidget(field: string): Observable<IBaseWidget<any>>;
    public abstract getExtendWidget<TWidgetType>(field: string, widgetType?: any): Observable<TWidgetType>;
}

export { IAdvancedFormCtrl };
