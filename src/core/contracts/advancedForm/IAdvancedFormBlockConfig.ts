import { IBaseWidget } from "coreContracts/advancedForm/widgets/IBaseWidget";
import { Subject } from "rxjs/Subject";
import { IAdvancedFormFieldConfig } from "./IAdvancedFormFieldConfig";

abstract class IAdvancedFormBlockConfig {
    public abstract fields: Array<string>;
    public abstract fieldsWidgets?: Map<string, Subject<IBaseWidget<any>>>;
    public abstract config?: Map<string, IAdvancedFormFieldConfig>;
}

export { IAdvancedFormBlockConfig };