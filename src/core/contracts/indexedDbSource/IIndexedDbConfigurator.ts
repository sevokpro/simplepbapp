import { IIndexedDbUpgradeScript } from "coreContracts/indexedDbSource/IUpgradeScript";

interface IIndexedDbConfig {
    version: number;
    upgradeScripts: Array<IIndexedDbUpgradeScript>;
}

abstract class IIndexedDbConfigurator {
    public abstract setDatabases(databases: Array<{ dbName: string, dbConfig: IIndexedDbConfig }>);
    public abstract setDefaultDataBase(dataBaseName: string);

    public abstract getDefaultDataBaseName(): string;
}

export {IIndexedDbConfigurator, IIndexedDbConfig};