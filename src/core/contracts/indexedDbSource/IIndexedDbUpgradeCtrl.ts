interface IIndexedDbFieldMetadata{
    type: string;
}

type IIndexedDbMetadata = Map<string, IIndexedDbFieldMetadata>;

interface IIndexedDbUpgradeCtrl {
    addTable(entityName: string, metadata: IIndexedDbMetadata);
    updateTable(entityName: string, metadata: IIndexedDbMetadata);
}

export {IIndexedDbUpgradeCtrl, IIndexedDbFieldMetadata, IIndexedDbMetadata};