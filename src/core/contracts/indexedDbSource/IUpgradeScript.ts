import { IIndexedDbUpgradeCtrl } from "coreContracts/indexedDbSource/IIndexedDbUpgradeCtrl";
import { Observable } from "rxjs/Observable";

interface IIndexedDbUpgradeScript {
    version: number;
    script: (upgradeCtrl: IIndexedDbUpgradeCtrl) => Observable<boolean>;
}

export {IIndexedDbUpgradeScript};