import { IButtonDrawer } from "coreContracts/buttonDrawer/IButtonDrawer";

interface IButtonDrawerCreateParam{
    label: string;
    alias?: string;
    type?: 'default' | 'dropdown';
    style?: 'base' | 'lite' | 'danger';
    attributes?: {[attribute: string]: any};
}

type IButtonDrawerCreateParams = Array<IButtonDrawerCreateParam>;

abstract class IButtonDrawerFactory{
    public abstract create(createParams: IButtonDrawerCreateParams): IButtonDrawer;
}

export {IButtonDrawerFactory, IButtonDrawerCreateParam, IButtonDrawerCreateParams};