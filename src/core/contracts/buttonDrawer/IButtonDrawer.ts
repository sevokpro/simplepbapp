import { ComponentRef, Injector, ViewContainerRef } from "@angular/core";
import { Observable } from "rxjs/Observable";

abstract class IButtonDrawer{
    public abstract render(viewContainer: ViewContainerRef, injector: Injector): ComponentRef<any>;
    public abstract whenClick(): Observable<string>;
}

export {IButtonDrawer};