import { CommonModule } from "@angular/common";
import { ComponentFactoryResolver, Injectable, Injector, NgModule, ViewContainerRef } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { IChat } from "coreContracts/chat";
import { Observable } from "rxjs/Observable";
import { ChatConfigService } from "src/core/modules/chat/modules/chat/services/chatConfig.service";
import { IChatConfigs } from "src/core/modules/chat/modules/chat/services/types/interfaces/IChatConfigs";
import { ChatComponent } from "./chat/chat.component";
import { ChatContainerComponent } from "./externalLibs/baseChat/components/chatContainer/baseChatContainerComponent.component";
import { ChatLocalScope } from "./externalLibs/baseChat/directives/common/chatLocalScope";
import { ChatModule } from "./modules/chat/chat.module";
import { ChatManagerHelperService } from "./modules/chat/services/chatManagerHelper.service";

/*
 * Platform and Environment providers/directives/pipes
 */

@Injectable()
export class ChatManager extends IChat {
    constructor(
        private componentFactoryResolver: ComponentFactoryResolver,
        private config: ChatConfigService,
        private chatHelperService: ChatManagerHelperService
    ) {
        super();
    }

    public open() {
        this.chatHelperService.openChatStream.next(true);
    }

    public close() {
        throw new Error('ЧАТ: Not implemented');
    }

    public get unreadMessagesCount(): Observable<number> {
        return this.chatHelperService.unreadMessagesCountStream;
    }

    public render(viewRef: ViewContainerRef, injector: Injector, initConfig: IChatConfigs) {
        this.config.appConfig = initConfig;
        const component = this.componentFactoryResolver.resolveComponentFactory(ChatComponent);
        viewRef.createComponent(component);
    }
}

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ChatModule
    ],
    declarations: [
        ChatComponent,
        ChatContainerComponent
    ],
    entryComponents: [
        ChatComponent,
        ChatContainerComponent
    ],
    providers: [ // expose our Services and Providers into Angular's dependency injection
        ChatLocalScope,

    ]
})
class ChatAppModule {
    constructor() { }
}

export { ChatAppModule };

