import { Component, Input, OnInit } from "@angular/core";
import { ActivatedRoute, Params } from "@angular/router";
import { IChatComponentsMap } from "../externalLibs/baseChat/services/baseChatConfigurator/interfaces/IChatComponentsMap";
import { IChatCtrlInitParams } from "../externalLibs/baseChat/services/baseChatService/interfaces/IChatCtrlInitParams";
import { IChatState } from "../externalLibs/baseChat/services/baseChatService/interfaces/IChatState";
import { ChatCtrl } from "../externalLibs/baseChat/services/chatCtrl";
import { ChatService } from "../modules/chat/services/chat.service";
import { ChatManagerHelperService } from "../modules/chat/services/chatManagerHelper.service";
import { GlobalChatService } from "../modules/chat/services/globalChat.service";
import { ChatData } from "./ChatData";

@Component({
    selector: 'chat-app',
    templateUrl: 'chat.template.pug',
    styleUrls: [
        './chtpzChatBox.styles.styl'
    ]
})
export class ChatComponent implements OnInit {
    @Input()
    public chatCtrl: ChatCtrl;
    public opened: boolean = true;  // private config: IAppConfigs;

    constructor(private chatService: ChatService,
        private activatedRoute: ActivatedRoute,
        private globalChatService: GlobalChatService,
        private chatManagerHelper: ChatManagerHelperService) {
        ChatData.init(
            this.chatService,
            this.globalChatService);

    }

    public ngOnInit() {
        const chatCtrlInitParams: IChatCtrlInitParams = {
            componentsMap: {} as IChatComponentsMap,
            getCurrentUser: this.activatedRoute.params.map((params: Params) => {
                const userId = params['userId'];
                console.log(userId);
                return userId;
            })
        };

        this.chatCtrl = this.chatService.createInstance(chatCtrlInitParams);
        const initState: IChatState = {};

        this.chatCtrl.init(initState).then(e => null).catch(rej => rej);

        this.chatManagerHelper.openChatStream.subscribe((value) => {
            this.opened = value;
        });

        this.chatCtrl.threadsService.unreadMessagesCount
            .subscribe(this.chatManagerHelper.unreadMessagesCountStream);
    }

    public close() {
        this.opened = false;
    }
}
