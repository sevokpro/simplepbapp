import { inject, TestBed } from "@angular/core/testing";
import { ActivatedRoute, Data } from "@angular/router";
import { ChatService } from "../modules/chat/services/chat.service";
// Load the implementations that should be tested
import { ChatComponent } from "./chat.component";

describe('About', () => {
    // provide our implementations or mocks to the dependency injector
    beforeEach(() => TestBed.configureTestingModule({
        providers: [
            // provide a better mock
            {
                provide: ActivatedRoute,
                useValue: {
                    data: {
                        subscribe: (fn: (value: Data) => void) => fn({
                            yourData: 'yolo'
                        })
                    }
                }
            },
            ChatService,
            ChatComponent
        ]
    }));

    // it('should log ngOnInit', inject([ChatComponent], (chat: ChatComponent) => {
    //     spyOn(console, 'log');
    //     expect(console.log).not.toHaveBeenCalled();
    //
    //     chat.ngOnInit();
    //     expect(console.log).toHaveBeenCalled();
    // }));

});
