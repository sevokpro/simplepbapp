import { ChatService } from "../modules/chat/services/chat.service";
import { GlobalChatService } from "../modules/chat/services/globalChat.service";
import { ChtpzUser } from "../modules/chat/services/types/classes/ChtpzUser";

export class ChatData {
    public static init(chatService: ChatService,
        globalChatService: GlobalChatService): void {
        const contactsService = chatService.chatContactsService.contactsService;
        contactsService.contacts.subscribe(() => ({}));

        const currentChtpzUser: ChtpzUser = globalChatService.currentUser;
        chatService.userService.setCurrentUser(currentChtpzUser);

        chatService.init();
    }
}
