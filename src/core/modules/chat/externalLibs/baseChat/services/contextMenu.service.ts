import { ComponentFactoryResolver, ComponentRef, Injectable, Injector, ReflectiveInjector } from "@angular/core";
import { isNullOrUndefined } from "@itexpert-dev/tiny-helpers";
import { IRootAppContainer } from "coreContracts/rootAppContainer";
import { ContextMenuComponent } from "../components/common/contextMenu/contextMenu.component";
import { MenuInitParams } from "../types/classes/MenuInitParams";

const documentClick = (function(event: Event) {
    if (!this.menuInstance.menuEl || event.currentTarget !== this.menuInstance.menuEl.nativeElement) {
        this.destroyMenu();
    }
});

@Injectable()
export class ContextMenuService {
    private menuInstance: ContextMenuComponent;
    private componentRef: ComponentRef<ContextMenuComponent>;

    private readonly documentClick = documentClick.bind(this);

    constructor(private rootAppContainer: IRootAppContainer,
        private componentResolver: ComponentFactoryResolver, ) {
    }

    public createMenu(initParams: MenuInitParams, injector: Injector): ContextMenuComponent {
        this.destroyMenu();

        const notificationsComponentFactory = this.componentResolver.resolveComponentFactory(ContextMenuComponent);
        const childInjector = ReflectiveInjector.resolveAndCreate(
            [
                {
                    provide: MenuInitParams,
                    useValue: initParams
                }
            ],
            injector
        );

        this.componentRef = this.rootAppContainer.viewContainerRef.createComponent(notificationsComponentFactory, null, childInjector);
        this.menuInstance = this.componentRef.instance;

        document.addEventListener("click", this.documentClick);

        return this.menuInstance;
    }

    public destroyMenu() {
        document.removeEventListener("click", this.documentClick);
        if (!isNullOrUndefined(this.componentRef))
            this.componentRef.destroy();
    }
}
