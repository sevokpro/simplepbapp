import { isNullOrUndefined } from "@itexpert-dev/tiny-helpers";
import { BehaviorSubject, Observable, Subject } from "rxjs";
import { Message } from "../types/classes/Message";
import { Thread } from "../types/classes/Thread";
import { User } from "../types/classes/User";
import { IDataDialogsManager } from "../types/interfaces/IDataDialogsManager";
import { IDataMessagesManager } from "../types/interfaces/IDataMessagesManager";
import { ThreadsService } from "./ThreadsService";
import { UserService } from "./UserService";


interface IMessagesOperation extends Function {
    (messagesStore: IMessagesStore): IMessagesStore;
}

interface IMessagesStore {
    [key: string]: Message;
}

export class MessagesService {
    private initialStore: IMessagesStore = {};

    // a stream that publishes new messages only once
    public newMessages: Subject<Array<Message>> = new Subject<Array<Message>>();

    // `messages` is a stream that emits an array of the most up to date messages
    public messages: BehaviorSubject<Array<Message>> = new BehaviorSubject<Array<Message>>([]);
    public messagesStore: Observable<IMessagesStore>;

    // `updates` receives _operations_ to be applied to our `messages`
    // it's a way we can perform changes on *all* messages (that are currently
    // stored in `messages`)
    public updates: Subject<any> = new Subject<any>();

    // action streams
    public create: Subject<Array<Message>> = new Subject<Array<Message>>();
    public markThreadAsRead: Subject<any> = new Subject<any>();
    // `unreadMessagesCount` contains the number of unread messages of host thread
    public unreadMessagesCount = new BehaviorSubject<number>(0);

    constructor(private userService: UserService,
        private threadsService: ThreadsService,
        private messagesManager: IDataMessagesManager,
        private dialogsManager: IDataDialogsManager,
        private thread: Thread) {
        this.messagesStore = this.updates
            // watch the updates and accumulate operations on the messages
            .scan((store: IMessagesStore,
                operation: IMessagesOperation) => {
                return operation(store);
            },
            this.initialStore)
            // make sure we can share the most recent list of messages across anyone
            // who's interested in subscribing and cache the last known list of
            // messages
            .publishReplay(1)
            .refCount();
        this.messagesStore.subscribe();

        this.messagesStore
            .subscribe((store: IMessagesStore) => {
                let messages: Array<Message> = Object.keys(store).map(key => store[key]);
                messages = messages.sort((m1: Message, m2: Message) =>
                    (new Date(m1.timestamp)).getTime() - (new Date(m2.timestamp).getTime())
                );
                this.messages.next(messages);
            });

        // `create` takes a Message and then puts an operation (the inner function)
        // on the `updates` stream to add the Message to the list of messages.
        //
        // That is, for each item that gets added to `create` (by using `next`)
        // this stream emits a concat operation function.
        //
        // Next we subscribe `this.updates` to listen to this stream, which means
        // that it will receive each operation that is created
        //
        // Note that it would be perfectly acceptable to simply modify the
        // "pushMessage" function below to simply add the inner operation function to
        // the update stream directly and get rid of this extra action stream
        // entirely. The pros are that it is potentially clearer. The cons are that
        // the stream is no longer composable.
        this.create
            .map((incomingMessages: Array<Message>): IMessagesOperation => {
                return (store: IMessagesStore) => {
                    incomingMessages.forEach(message => {
                        if (isNullOrUndefined(message.thread.lastMessage) || message.thread.lastMessage.timestamp < message.timestamp) {
                            message.thread.lastMessage = message;
                        }
                        message.isPersonal = message.thread.isPersonal;

                        store[message.id] = isNullOrUndefined(store[message.id]) ? message : store[message.id];
                    });
                    console.log('xxx messagestore', store);
                    return store;
                };
            })
            .subscribe(this.updates);

        this.newMessages
            .subscribe(this.create);

        // similarly, `markThreadAsRead` takes a Thread and then puts an operation
        // on the `updates` stream to mark the Messages as read
        this.markThreadAsRead
            .map((thread: Thread) => {
                return (store: IMessagesStore) => {
                    if (!isNullOrUndefined(thread) && thread.id === this.thread.id)
                        Object.keys(store).map((key: string) => {
                            // note that we're manipulating `json` directly here. Mutability
                            // can be confusing and there are lots of reasons why you might want
                            // to, say, copy the Message object or some other 'immutable' here
                            // if (message.thread.id === this.thread.id) {
                            (store[key] as Message).isRead = true;
                            // }
                        });
                    return store;
                };
            })
            .subscribe(this.updates);


        this.threadsService.currentThread.subscribe(this.markThreadAsRead);

        /*    this.unreadMessagesCount = this.messages
         .map((messages: Message[]): number => {
         return messages.reduce(
         (sum: number, m: Message) => {
         // note: in a "real" app you should also exclude
         // messages that were authored by the current user b/c they've
         // already been "read"
         if (m
         && !m.isRead
         && !m.isInitial
         && !m.isHidden) {
         sum = sum + 1;
         }
         return sum;
         },
         0);
         });*/

        this.unreadMessagesCount
            .pairwise()
            .map(([c1, c2]) => c2 - c1)
            .subscribe(this.threadsService.unreadMessagesCountDelta);
    }

    public setUnreadMessagesCount(params?: { delta: number }) {
        const newValue = isNullOrUndefined(params) || isNaN(params.delta) ?
            this.thread.unreadMessagesCount
            : this.unreadMessagesCount.value + params.delta;
        this.unreadMessagesCount.next(newValue);
        this.thread.unreadMessagesCount = newValue;
    }

    // an imperative function call to this action stream
    public pushMessage(message: Message): void {
        this.newMessages.next([message]);
    }

    public pushMessages(messages: Array<Message>): void {
        this.newMessages.next(messages);
    }

    public createMessage(message: Message): void {
        let dialogId: Observable<string>;
        // tslint:disable-next-line
        message.id = Date.now() + message.thread.id;
        this.newMessages.next([message]);

        if (message.thread.isDraft) {
            if (message.thread.isPersonal
                && message.thread.id === message.thread.userId)
                dialogId = this.dialogsManager
                    .getPersonalDialog({ contactsIds: [message.thread.userId] })
                    .do(thread => {
                        this.threadsService.replaceThreadId(message.thread.id, thread.id);
                    })
                    .map(thread => thread.id);
        }
        else
            dialogId = Observable.of(message.thread.id);

        dialogId.subscribe(id => {
            message.thread.id = id;
            this.messagesManager.createMessage(message)
                .subscribe(id => {
                    const oldId = message.id;
                    this.replaceMessageId(message.id, id);
                    // this.newMessages.next([message]);
                });
        });
    }

    public replaceMessageId(oldId: string, newId: string) {
        const update: IMessagesOperation = (store: IMessagesStore) => {
            const message = store[oldId];
            message.id = newId;
            store[newId] = message;
            delete store[oldId];

            return store;
        };


        this.updates.next(update);
    }

    public messagesForThreadUser(thread: Thread, user: User): Observable<Message> {
        return this.newMessages
            .flatMap((messages: Array<Message>) => Observable.from(messages))
            .filter((message: Message) => {
                // belongs to this thread
                return (message.thread.id === thread.id) &&
                    // and isn't authored by this user
                    (message.author.id !== user.id);
            });
    }

    // createInitMessage(thread: Thread, contact: Observable<User>): BehaviorSubject<boolean> {
    public createInitMessage(contact: Observable<User>): BehaviorSubject<boolean> {
        const resultSubject = new BehaviorSubject<boolean>(false);
        const result = true;
        const initMessage = new Message({
            id: 'init',
            author: this.userService.currentUser.value,
            isInitial: true,
            isHidden: true,
            isRead: true,
            thread: this.thread,
            timestamp: new Date(Date.now())
        });
        /*    if (this.thread.isPersonal) {
         if (!contact)
         result = false;
         else
         contact
         .subscribe(contact => {
         if (contact) {
         // initMessage.thread.name = contact.name;
         console.log('xxx createInitMessage', initMessage)
         this.pushMessage(initMessage);
         }
         else
         result = false;
         });
         }
         else*/
        this.pushMessage(initMessage);

        resultSubject.next(result);
        return resultSubject;
    }
}

export let messagesServiceInjectables: Array<any> = [
    MessagesService
];
