import { isNullOrUndefined } from "@itexpert-dev/tiny-helpers";
import { BehaviorSubject, Observable, Subject } from "rxjs";
import { ContactsFilter } from "../types/classes/ContactsFilter";
import { User } from "../types/classes/User";

const initialContacts: Array<User> = [];

interface IContactsOperation extends Function {
    (contacts: Array<User>): Array<User>;
}

export class ContactsService {
    // a stream that publishes new messages only once
    public newContacts: Subject<User> = new Subject<User>();

    // `currentContact` contains the current contact
    public currentContact: BehaviorSubject<User> = new BehaviorSubject<User>(null);

    // `contacts` is a stream that emits an array of the most up to date contacts
    public contacts: Observable<Array<User>>;
    // `orderedThreads` contains a newest-first chronological list of contacts
    public orderedContacts: Observable<Array<User>>;
    public orderedFilteredContacts: Observable<Array<User>>;

    // `updates` receives _operations_ to be applied to our `contacts`
    // it's a way we can perform changes on *all* messages (that are currently
    // stored in `contacts`)
    public updates: Subject<any> = new Subject<any>();

    // action streams
    public create: Subject<User> = new Subject<User>();
    public remove: Subject<User> = new Subject<User>();
    public filter: BehaviorSubject<ContactsFilter>; // = new Subject<ContactsFilter>();

    private contactsStore: { [key: string]: User } = {};

    constructor() {
        this.contactsStore = {};
        this.contacts = this.updates
            // watch the updates and accumulate operations on the messages
            .startWith((contacts: Array<User>) => {
                return contacts.concat([]);
            })
            .scan((contacts: Array<User>, operation: IContactsOperation) => {
                return operation(contacts).filter(c => c.inUserList);
            },
            initialContacts)
            // make sure we can share the most recent list of contacts across anyone
            // who's interested in subscribing and cache the last known list of
            // messages
            .publishReplay(1)
            .refCount();

        class InitFilter extends ContactsFilter {
            public filterByTerm(contacts: Array<User>) {
                return contacts;
            }
        }

        this.orderedContacts = this.contacts
            .map((contacts: Array<User>) => {
                let ordered: Array<User>;
                ordered = contacts.sort((user1: User, user2: User) => {
                    const name1 = user1.name.toUpperCase();
                    const name2 = user2.name.toUpperCase();

                    if (name1 < name2) //sort string ascending
                        return -1;
                    if (name1 > name2)
                        return 1;
                    return 0; //default return value (no sorting)
                });
                return ordered;
            }).publishReplay(1)
            .refCount();

        this.filter = new BehaviorSubject<ContactsFilter>(new InitFilter());
        this.orderedFilteredContacts = this.filter
            .combineLatest(this.orderedContacts,
            (filter: ContactsFilter, contacts: Array<User>) => {
                return filter.filterByTerm(contacts);
            }).publishReplay(1)
            .refCount();

        this.create
            .map((contact: User) => {
                console.log('create MAP -> updates', contact, this.contactsStore);
                const contactInStore = this.contactsStore[contact.id];
                this.contactsStore[contact.id] = !isNullOrUndefined(contactInStore) ? contact : null;

                return (contacts: Array<User>) => {
                    if (!isNullOrUndefined(contactInStore))
                        return contacts;
                    else
                        return contacts.concat(contact);
                };
            })
            .subscribe(this.updates);

        this.remove
            .map((contact: User): IContactsOperation => {
                return (contacts: Array<User>) => {
                    return contacts.map(item => {
                        if (item.id === contact.id)
                            item.inUserList = false;
                        return item;
                    });
                };
            })
            .subscribe(this.updates);

        this.newContacts
            .subscribe(this.create);
    }

    public setCurrentContact(newUser: User): void {
        if (!isNullOrUndefined(newUser)) {
            const oldUser = this.currentContact.value;
            this.contactsStore[newUser.id] = isNullOrUndefined(this.contactsStore[newUser.id]) ? newUser : this.contactsStore[newUser.id];
            if ( !isNullOrUndefined(oldUser) ) {
                this.contactsStore[oldUser.id].isCurrent = false;
            }
            this.contactsStore[newUser.id].isCurrent = true;
        }

        this.currentContact.next(newUser);
    }

    // an imperative function call to this action stream
    public addContact(contact: User): void {
        if (!isNullOrUndefined(contact)) {
            this.newContacts.next(contact);
        }
    }

    public addContactToUserList(contact: User): void {
        contact.inUserList = true;
        this.addContact(contact);
    }
    // an imperative function call to this action stream
    public removeContact(contact: User): void {
        this.remove.next(contact);
    }

    public getContactFromStoreById(id: string): User {
        return isNullOrUndefined(this.contactsStore[id]) ? null : this.contactsStore[id];
    }
}

