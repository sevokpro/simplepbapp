import merge from "lodash/merge";
import { BaseChatConfigurator } from "./baseChatConfigurator/baseChatConfigurator";
import { IChatComponentsMap } from "./baseChatConfigurator/interfaces/IChatComponentsMap";
import { IChatCtrlInitParams } from "./baseChatService/interfaces/IChatCtrlInitParams";
import { IChatState } from "./baseChatService/interfaces/IChatState";
import { IChatViewScope } from "./baseChatService/interfaces/IChatViewScope";
import { ContactsService } from "./ContactsService";
import { ThreadsService } from "./ThreadsService";
import { UserService } from "./UserService";

export class ChatCtrl {
    public initPromise: Promise<any> = new Promise(resolve => this.resolveInit = resolve);
    private resolveInit: any;

    public chatComponents: IChatComponentsMap = {};
    public viewScope: IChatViewScope = {
        rootData: {
            children: [],
            value: null
        }
    } as IChatViewScope;
    public currentState: IChatState = {
        viewTabsContainer: false
    };

    public setRootData(data: {}) {
        this.viewScope.rootData = data;
    }

    constructor(public initParams: IChatCtrlInitParams,
        public config: BaseChatConfigurator,
        public threadsService: ThreadsService,
        public userService: UserService,
        public contactsService: ContactsService
    ) {
        if (!initParams.hasOwnProperty('componentsMap')) {
            initParams.componentsMap = {};
        }
        merge(this.chatComponents, this.config.getDefaultComponents(), initParams.componentsMap);
    }

    public init(initState: IChatState = {}): Promise<ChatCtrl> {
        const resultInitParams = merge({}, this.config.getDefaultInitChatState(), initState);

        return new Promise((resolve, reject) => {
            this.initParams.getCurrentUser.subscribe(
                user => {
                    this.resolveInit();
                    resolve(this);
                }
            );
        });
    }
}
