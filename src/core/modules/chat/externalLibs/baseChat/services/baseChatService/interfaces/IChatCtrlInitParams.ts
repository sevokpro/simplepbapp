import { Observable } from "rxjs/Observable";
import { User } from "../../../types/classes/User";
import { IChatComponentsMap } from "../../baseChatConfigurator/interfaces/IChatComponentsMap";
import { IUniversalTreeGetDataRequest } from "./IUniversalTreeGetDataRequest";
import { IUniversalTreeGetDataResponse } from "./IUniversalTreeGetDataResponse";

interface IGetDataMethod {
    (query: IUniversalTreeGetDataRequest): Promise<IUniversalTreeGetDataResponse>;
}

interface IChatCtrlInitParams {
    getCurrentUser: Observable<User>;
    componentsMap?: IChatComponentsMap;
}
export { IChatCtrlInitParams };
