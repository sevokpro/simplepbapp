import { Inject } from "@angular/core";
import { isNullOrUndefined } from "@itexpert-dev/tiny-helpers";
import { Thread } from "../../types/classes/Thread";
import { IDataContactsManager } from "../../types/interfaces/IDataContactsManager";
import { IDataDialogsManager } from "../../types/interfaces/IDataDialogsManager";
import { BaseChatConfigurator } from "../baseChatConfigurator/baseChatConfigurator";
import { ChatContactsService } from "../ChatContactsService";
import { ChatCtrl } from "../chatCtrl";
import { ThreadsService } from "../ThreadsService";
import { UserService } from "../UserService";
import { IChatCtrlInitParams } from "./interfaces/IChatCtrlInitParams";

class BaseChatService {
    public constructor(
        @Inject(ThreadsService) public threadsService: ThreadsService,
        @Inject(UserService) public userService: UserService,
        @Inject(ChatContactsService) public chatContactsService: ChatContactsService,
        @Inject(IDataContactsManager) public dataContactsManagerService: IDataContactsManager,
        @Inject(IDataDialogsManager) public dataDialogsManagerService: IDataDialogsManager) {
    }

    public config: BaseChatConfigurator = new BaseChatConfigurator();

    public createInstance(initParams: IChatCtrlInitParams) {
        return new ChatCtrl(
            initParams,
            this.config,
            // this.messagesService,
            this.threadsService,
            this.userService,
            this.chatContactsService.contactsService
        );
    }

    public init() {
        this.dataContactsManagerService.getChatContacts().subscribe(ids => {
            if (!isNullOrUndefined(ids) && ids.length !== 0)
                this.dataContactsManagerService.getUsersByIds(ids)
                    .subscribe(users => {
                        users.map((contact, index) => {
                            this.chatContactsService.contactsService.addContactToUserList(contact);
                        });

                        this.dataDialogsManagerService.getDialogsInfo()
                            .subscribe(dialogsInfo => {
                                this.threadsService.addThreads(dialogsInfo)
                                    .combineLatest(this.threadsService.orderedFilteredThreads,
                                    (isSuccess: boolean, orderedThreads: Array<Thread>) => {
                                        return orderedThreads;
                                    })
                                    .subscribe((orderedThreads) => {
                                        this.threadsService.setCurrentThread(orderedThreads[0]);
                                    });
                            });


                    });
        });
    }
}

export { BaseChatService };
