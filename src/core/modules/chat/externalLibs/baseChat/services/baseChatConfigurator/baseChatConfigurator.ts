import { IDictionary } from "@itexpert-dev/i-dictionary";
import { KeyValueStorage } from "@itexpert-dev/key-value-storage";
import { IChatState } from "../baseChatService/interfaces/IChatState";
import { IChatComponentsMap } from "./interfaces/IChatComponentsMap";

class BaseChatConfigurator {
    constructor() { }
    public componentsStorage: Map<string, any> = new Map();

    protected _defaultInitChatState: IChatState;
    protected componentsScope: IDictionary<any>;

    public setDefaultInitChatState(newState: IChatState) {
        this._defaultInitChatState = newState;
    }
    public getDefaultInitChatState(): IChatState {
        return this._defaultInitChatState;
    }


    private defaultComponentsField: IChatComponentsMap;
    public setDefaultComponents(components: IChatComponentsMap) {
        this.defaultComponentsField = components;
    }
    public getDefaultComponents(): IChatComponentsMap {
        return this.defaultComponentsField;
    }

    private chatElementTypeToComponentMapStorageField = new KeyValueStorage<string>();
    public setChatElementTypeToComponentLink(...params: Array<{ elementType: string, componentKey: string }>) {
        for (const element of params) {
            this.chatElementTypeToComponentMapStorageField.add({
                key: element.elementType,
                value: element.componentKey
            });
        }
    }
    public getElementNodeComponentByType(params: { elementType: string }): string {
        return this.chatElementTypeToComponentMapStorageField.get(params.elementType);
    }


}

export { BaseChatConfigurator };
