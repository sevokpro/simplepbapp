export interface IChatComponentsMap {
    chatContainer?: {
        self?: string,
        tabsContainer?: {
            self?: string,
            tabsHeader?: {
                self?: string,
                tabsSearchbox?: string,
                tabsHeaderAvatar?: {
                    self?: string,
                    statusMenuItem?: string
                },
            },
            contacts?: {
                self?: string,
                contact?: string
            },
            threads?: {
                self?: string,
                thread?: string
            },
            allContacts?: string,
        },
        threadContainer?: {
            self?: string
            threadHeader?: string,
            threadBody?: {
                self?: string,
                threadMessage?: {
                    self?: string,
                    menuItem?: string
                },
            },
            threadFooter?: {
                self?: string,
                threadInput?: string
            }
        },
    };
}
