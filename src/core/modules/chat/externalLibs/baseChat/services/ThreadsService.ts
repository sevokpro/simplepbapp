import { Injectable } from "@angular/core";
import { isNullOrUndefined } from "@itexpert-dev/tiny-helpers";
import { BehaviorSubject, Observable, Subject, Subscription } from "rxjs";
import { Message } from "../types/classes/Message";
import { Thread } from "../types/classes/Thread";
import { ThreadsFilter } from "../types/classes/ThreadsFilter";
import { User } from "../types/classes/User";
import { IDataDialogsManager } from "../types/interfaces/IDataDialogsManager";
import { IDataMessagesManager } from "../types/interfaces/IDataMessagesManager";
import { IThreadInitParams } from "../types/interfaces/IThreadInitParams";
import { ChatContactsService } from "./ChatContactsService";
import { MessagesService } from "./MessagesService";
import { UserService } from "./UserService";

interface IThreadsOperation extends Function {
    (threadsStore: IThreadsStore): IThreadsStore;
}

interface IThreadsStore {
    [key: string]: Thread;
}

const initialStore: IThreadsStore = {};

class InitFilter extends ThreadsFilter {
    public filterByTerm(contacts: Array<Thread>) {
        return contacts;
    }
}

@Injectable()
export class ThreadsService {

    public newThreads: Subject<Array<Thread>> = new Subject<Array<Thread>>();
    // `contacts` is a observable that contains the most up to date list of contacts
    public threads: Observable<IThreadsStore>;
    public updates: Subject<any> = new Subject<any>();
    public create: Subject<Array<Thread>> = new Subject<Array<Thread>>();

    // `orderedThreads` contains a newest-first chronological list of contacts
    public orderedThreads: Observable<Array<Thread>>;
    public orderedFilteredThreads: Observable<Array<Thread>>;

    // `currentThread` contains the currently selected thread
    public currentThread: BehaviorSubject<Thread> = new BehaviorSubject<Thread>(null);

    public filter: BehaviorSubject<ThreadsFilter>;

    // `currentThreadMessages` contains the set of messages for the currently
    // selected thread
    public currentThreadMessages: Observable<Array<Message>>;

    // `unreadMessagesCount` contains the number of total unread messages
    public unreadMessagesCount = new BehaviorSubject<number>(0);
    public unreadMessagesCountDelta: Subject<number> = new Subject();

    private threadsStore: { [key: string]: Thread } = {};

    public constructor(private userService: UserService,
        private chatContactsService: ChatContactsService,
        private messagesManager: IDataMessagesManager,
        private dialogsManager: IDataDialogsManager) {

        this.threads = this.updates
            // watch the updates and accumulate operations on the messages
            .scan((store: IThreadsStore,
                operation: IThreadsOperation) => {
                return operation(store);
            },
            initialStore)
            // make sure we can share the most recent list of messages across anyone
            // who's interested in subscribing and cache the last known list of
            // messages
            .publishReplay(1)
            .refCount();

        this.orderedThreads = this.threads
            .map((threadGroups: IThreadsStore) => {
                let threads: Array<Thread> = Object.keys(threadGroups).map(key => threadGroups[key]);
                threads = threads.sort((t1: Thread, t2: Thread) =>
                    (new Date(t1.lastMessage.timestamp)).getTime() - (new Date(t2.lastMessage.timestamp).getTime())
                ).reverse();
                return threads;
            });




        this.create
            .map((threads: Array<Thread>): IThreadsOperation => {
                return (store: IThreadsStore) => {
                    threads.forEach(thread => {
                        const id = thread.id;
                        if (isNullOrUndefined(store[id])) {
                            store[id] = thread;
                        }
                    });
                    return store;
                };
            })
            .subscribe(this.updates);

        this.newThreads
            .subscribe(this.create);

        this.filter = new BehaviorSubject<ThreadsFilter>(new InitFilter());
        this.orderedFilteredThreads = this.orderedThreads
            .combineLatest(this.filter,
            (threads: Array<Thread>, filter: ThreadsFilter) => {
                return filter.filterByTerm(threads);
            });


        this.currentThreadMessages = this.currentThread
            .switchMap(thread => {
                if (!(!isNullOrUndefined(thread) && !isNullOrUndefined(thread.id)))
                    return Observable.of([]);

                const messages = thread.messagesService.messages.value;
                if (messages.length === 1 && messages[0].isInitial) {
                    return this.dialogsManager.getDialog({ id: thread.id })
                        .switchMap((threadParams: IThreadInitParams) => {
                            return thread.addMessages(
                                threadParams.messages,
                                (id) => this.chatContactsService.getContactById(id)
                            )
                                .do(() => {
                                    if (!isNullOrUndefined(thread)) {
                                        this.markMessagesAsRead(thread);
                                    }
                                })
                                .switchMap((isSuccess: boolean) => {
                                    return thread.messagesService.messages as BehaviorSubject<Array<Message>>;
                                });
                        });
                }
                else
                    return thread.messagesService.messages;

            }
            );


        this.unreadMessagesCountDelta
            .subscribe(delta => {
                const newCount = this.unreadMessagesCount.value + delta;
                this.unreadMessagesCount
                    .next(newCount);
            });
    }

    public markMessagesAsRead(thread: Thread) {
        const messages = thread.messagesService.messages.value;
        const messagesToMark = messages.filter(message => !message.isRead);

        const oldCount = thread.unreadMessagesCount;
        if (messagesToMark.length !== 0) {
            const subscription: Subscription = this.messagesManager.markMessagesAsRead(messagesToMark)
                .catch((err) => {
                    return Observable.of(err);
                })
                .subscribe((message) => {
                    thread.messagesService.setUnreadMessagesCount({ delta: -oldCount });
                    subscription.unsubscribe();
                });
        }
    }

    public pushThreads(threads: Array<Thread>): void {
        this.newThreads.next(threads);
    }

    public pushThread(thread: Thread, messages?: Array<Message>): void {
        this.newThreads.next([thread]);
    }

    public replaceThreadId(oldId: string, newId: string) {
        const update: IThreadsOperation = (store: IThreadsStore) => {
            const thread = store[oldId];
            thread.id = newId;
            thread.isDraft = false;
            store[newId] = thread;
            delete store[oldId];

            return store;
        };

        this.updates.next(update);
    }

    public setCurrentThread(newThread: Thread): void {
        this.currentThread.next(newThread);
    }

    public setCurrentThreadByContact(contact: User): void {
        let thread: Thread = null;
        Observable.of(contact).combineLatest(this.threads,
            (contact: User, store: IThreadsStore) => {
                const ids = Object.keys(store);
                for (let i = 0; i < ids.length; i++) {
                    const id = ids[i];
                    thread = store[id];
                    if (thread.isPersonal && thread.userId === contact.id) {
                        this.setCurrentThread(thread);
                        return;
                    }
                    else
                        thread = null;
                }

                if (isNullOrUndefined(thread)) {
                    thread = new Thread({
                        id: contact.id,
                        isDraft: true,
                        userId: contact.id,
                        isPersonal: true,
                        name: contact.name
                    });

                    this.addThread(thread);
                    this.threads.subscribe(threads => {
                        this.setCurrentThread(thread);
                    });
                }
            }).subscribe();
    }

    private getContactById = (id: string) => {
        return this.chatContactsService.getContactById(id);
    }

    public addThreads(threadsParams: Array<IThreadInitParams>): Subject<boolean> {
        const resultSubject = new Subject<boolean>();
        const threadsObservables: Array<Observable<boolean>> = new Array<Observable<boolean>>();
        const currentContact: User = this.userService.currentUser.value;

        const threads = threadsParams.map(threadParams => new Thread(threadParams));

        threads.forEach((thread, threadIdx) => {
            const contactStream: Observable<User> =
                thread.isPersonal ? this.getContactById(thread.userId)
                    : Observable.of(null);
            const threadsObservable = Observable.create((observer) => {
                contactStream.subscribe(contact => {
                    if (thread.isPersonal)
                        if (!isNullOrUndefined(contact)) {
                            thread.name = contact.name;
                        }

                    thread.messagesService = new MessagesService(
                        this.userService,
                        this,
                        this.messagesManager,
                        this.dialogsManager,
                        thread
                    );

                    thread.messagesService.setUnreadMessagesCount();

                    thread.messagesService
                        .createInitMessage(Observable.of(currentContact))
                        .subscribe((isSuccess) => {
                            observer.next(isSuccess);
                            observer.complete();
                        });
                });
            });

            threadsObservables.push(threadsObservable);
        });

        Observable.forkJoin(threadsObservables)
            .catch(result => {
                resultSubject.next(false);
                return Observable.of(false);
            })
            .subscribe(result => {
                console.log('xxx threadObservable result', result);
                this.pushThreads(threads);
                this.threads.subscribe(store => {
                    resultSubject.next(true);
                });
            });
        return resultSubject;
    }

    public addThread(thread: Thread): Subject<boolean> {
        return this.addThreads([thread]);
    }

}

export let threadsServiceInjectables: Array<any> = [
    ThreadsService
];
