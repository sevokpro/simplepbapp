import { Inject, Injectable } from "@angular/core";
import { isNullOrUndefined } from "@itexpert-dev/tiny-helpers";
import { Observable } from "rxjs";
import { User } from "../types/classes/User";
import { IDataContactsManager } from "../types/interfaces/IDataContactsManager";
import { ContactsService } from "./ContactsService";

@Injectable()
export class ChatContactsService {
    public contactsService = new ContactsService();
    constructor( @Inject(IDataContactsManager) public dataContactsManagerService: IDataContactsManager) {
    }

    public getContactById(id: string): Observable<User> {
        const contact: User = this.contactsService.getContactFromStoreById(id) as User;
        if (!isNullOrUndefined(contact)) {
            return Observable.of(contact);
        } else {
            return this.dataContactsManagerService.getUserById(id)
                .map(contact => {
                    if (isNullOrUndefined(contact))
                        return null;

                    contact.inUserList = false;
                    this.contactsService.addContact(contact);
                    return contact;
                });
        }
    }
}

