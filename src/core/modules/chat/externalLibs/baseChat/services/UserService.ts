import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { User } from "../types/classes/User";
import { ChatContactsService } from "./ChatContactsService";


@Injectable()
export class UserService {
    // `currentUser` contains the current user
    public currentUser: BehaviorSubject<User> = new BehaviorSubject<User>(null);

    constructor(private chatContactsService: ChatContactsService) {

    }

    public setCurrentUser(newUser: User): void {
        this.currentUser.next(newUser);
        this.chatContactsService.contactsService.setCurrentContact(newUser);
    }
}

export let userServiceInjectables: Array<any> = [
    UserService
];
