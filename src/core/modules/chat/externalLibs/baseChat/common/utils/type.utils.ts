import isEmpty from "lodash/isEmpty";
import trim from "lodash/trim";

export function isValueEmpty(value: string): boolean {
    return isEmpty(trim(value));
}

export function isRenamable(value: any): boolean {
    return value.setName !== undefined;
}
