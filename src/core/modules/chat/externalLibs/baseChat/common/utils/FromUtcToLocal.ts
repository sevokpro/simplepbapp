import { Pipe } from "@angular/core";
import * as moment from "moment";

@Pipe({
    name: 'fromUtcToLocal'
})
export class FromUtcToLocal {
    public transform(value: any, args: Array<any>): string {
        return moment(value).locale("ru").format('LLL');
    }
}


