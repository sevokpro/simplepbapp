import { Pipe } from "@angular/core";
import * as moment from "moment";

@Pipe({
    name: 'fromNow'
})
export class FromNowPipe {
    public transform(value: any, args: Array<any>): string {
        return moment(value).locale("ru").fromNow();
    }
}


