import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { ChatContainerDirective } from "./directives/chatContainer.directive";

import { FromNowPipe } from "./common/utils/FromNowPipe";
import { FromUtcToLocal } from "./common/utils/FromUtcToLocal";
import { ChatViewComponent } from "./components/chatView/chatView.component";
import { ContextMenuComponent } from "./components/common/contextMenu/contextMenu.component";
import { ContextMenuItemDirective } from "./directives/contextMenuItem.directive";
import { TabsAllContactsDirective } from "./directives/tabsAllContacts.directive";
import { TabsContactDirective } from "./directives/tabsContact.directive";
import { TabsContactsDirective } from "./directives/tabsContacts.directive";
import { TabsContainerDirective } from "./directives/tabsContainer.directive";
import { TabsHeaderDirective } from "./directives/tabsHeader.directive";
import { TabsHeaderAvatarDirective } from "./directives/tabsHeaderAvatar.directive";
import { TabsSearchboxDirective } from "./directives/tabsSearchbox.directive";
import { TabsThreadDirective } from "./directives/tabsThread.directive";
import { TabsThreadsDirective } from "./directives/tabsThreads.directive";
import { ThreadBodyDirective } from "./directives/threadBody.directive";
import { ThreadContainerDirective } from "./directives/threadContainer.directive";
import { ThreadFooterDirective } from "./directives/threadFooter.directive";
import { ThreadHeaderDirective } from "./directives/threadHeader.directive";
import { ThreadInputDirective } from "./directives/threadInput.directive";
import { ThreadMessageDirective } from "./directives/threadMessage.directive";
import { ChatContactsService } from "./services/ChatContactsService";
import { ContextMenuService } from "./services/contextMenu.service";
import { RootAppContainer } from "./services/rootAppContainer.service";
import { ThreadsService } from "./services/ThreadsService";
import { UserService } from "./services/UserService";
import { IRootAppContainer } from "./types/interfaces/IRootAppContainer";

@NgModule({
    imports: [CommonModule],
    declarations: [
        ChatViewComponent,
        ContextMenuComponent,

        ChatContainerDirective,

        TabsContainerDirective,
        TabsHeaderDirective,
        TabsHeaderAvatarDirective,
        TabsSearchboxDirective,
        TabsContactsDirective,
        TabsContactDirective,
        TabsThreadsDirective,
        TabsThreadDirective,
        TabsAllContactsDirective,

        ThreadContainerDirective,
        ThreadHeaderDirective,
        ThreadBodyDirective,
        ThreadMessageDirective,
        ThreadFooterDirective,
        ThreadInputDirective,

        ContextMenuItemDirective,

        FromNowPipe,
        FromUtcToLocal
    ],
    entryComponents: [
        ContextMenuComponent
    ],
    providers: [
        ThreadsService,
        UserService,
        ChatContactsService,
        ContextMenuService,
        { provide: IRootAppContainer, useClass: RootAppContainer }
    ],
    exports: [
        ChatViewComponent,
        ContextMenuComponent,

        ChatContainerDirective,

        TabsContainerDirective,
        TabsHeaderDirective,
        TabsHeaderAvatarDirective,
        TabsSearchboxDirective,
        TabsContactsDirective,
        TabsContactDirective,
        TabsThreadsDirective,
        TabsThreadDirective,
        TabsAllContactsDirective,

        ThreadContainerDirective,
        ThreadHeaderDirective,
        ThreadBodyDirective,
        ThreadMessageDirective,
        ThreadFooterDirective,
        ThreadInputDirective,

        ContextMenuItemDirective,

        FromNowPipe,
        FromUtcToLocal
    ]
})
export class BaseChatModule {
}
