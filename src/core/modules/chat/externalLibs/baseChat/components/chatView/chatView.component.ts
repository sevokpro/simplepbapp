import { Component, Input } from "@angular/core";
import { ChatLocalScope } from "../../directives/common/chatLocalScope";
import { ChatCtrl } from "../../services/chatCtrl";

@Component({
    selector: 'chat-view',
    templateUrl: './chatView.template.pug',
    providers: [
        ChatLocalScope
    ]
})
class ChatViewComponent {
    @Input()
    public chatCtrl: ChatCtrl;

    public constructor(
        protected scope: ChatLocalScope
    ) {
    }

    public ngOnInit() {
        this.scope.chatCtrl = this.chatCtrl;
    }
}

export { ChatViewComponent };
