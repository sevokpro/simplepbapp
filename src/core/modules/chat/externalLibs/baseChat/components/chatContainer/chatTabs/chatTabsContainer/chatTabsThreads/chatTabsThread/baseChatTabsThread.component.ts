import { Input, OnDestroy, OnInit } from "@angular/core";
import { isNullOrUndefined } from "@itexpert-dev/tiny-helpers";
import { Subscription } from "rxjs";
import { Thread } from "../../../../../../types/classes/Thread";
import { BaseChatComponent } from "../../../../../baseChatComponent";

export abstract class BaseChatTabsThreadComponent extends BaseChatComponent implements OnInit, OnDestroy {
    @Input()
    public thread: Thread;

    public selected: boolean = false;
    public unreadCount: number = 0;
    private selectedSubscription: Subscription;
    private countSubscription: Subscription;

    public constructor() {
        super();
    }

    public ngOnInit() {
        this.selectedSubscription = this.chatCtrl.threadsService.currentThread
            .subscribe((currentThread: Thread) => {
                this.selected = !isNullOrUndefined(currentThread) && !isNullOrUndefined(this.thread) && (currentThread.id === this.thread.id);
            });
        this.countSubscription = this.thread.messagesService.unreadMessagesCount
            .subscribe(count => this.unreadCount = count);
    }


    public ngOnDestroy() {
        this.selectedSubscription.unsubscribe();
        this.countSubscription.unsubscribe();
    }

    public clicked(event: any): void {
        this.chatCtrl.threadsService.setCurrentThread(this.thread);
        event.preventDefault();
    }
}
