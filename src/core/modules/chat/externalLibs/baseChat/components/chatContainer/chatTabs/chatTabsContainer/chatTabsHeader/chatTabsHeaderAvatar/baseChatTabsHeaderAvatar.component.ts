import { OnDestroy, OnInit } from "@angular/core";
import { Subscription } from "rxjs";
import { User } from "../../../../../../types/classes/User";
import { BaseChatComponent } from "../../../../../baseChatComponent";

export abstract class BaseChatTabsHeaderAvatarComponent extends BaseChatComponent implements OnInit, OnDestroy {
    protected currentUser: User;
    private subscription: Subscription;

    constructor() {
        super();
    }

    public ngOnInit() {
        this.subscription = this.chatCtrl.userService.currentUser
            .subscribe(
            (user: User) => {
                this.currentUser = user;
            });
    }

    public ngOnDestroy() {
        this.subscription.unsubscribe();
    }
}
