import { OnInit } from "@angular/core";
import { BaseChatComponent } from "../../../../../baseChatComponent";


export abstract class BaseChatThreadInputComponent extends BaseChatComponent implements OnInit {
    constructor() {
        super();
    }

    public ngOnInit() {
    }
}
