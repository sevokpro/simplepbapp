import {
    ComponentFactoryResolver,
    ElementRef,
    Injector,
    Input,
    OnDestroy,
    OnInit,
    ViewChild,
    ViewContainerRef
} from "@angular/core";
import { isNullOrUndefined } from "@itexpert-dev/tiny-helpers";
import { Observable, Subscription } from "rxjs";
import { ChatLocalScope } from "../../../../../../directives/common/chatLocalScope";
import { ContextMenuService } from "../../../../../../services/contextMenu.service";
import { MenuInitParams } from "../../../../../../types/classes/MenuInitParams";
import { MenuItem } from "../../../../../../types/classes/MenuItem";
import { Message } from "../../../../../../types/classes/Message";
import { User } from "../../../../../../types/classes/User";
import { BaseChatComponent } from "../../../../../baseChatComponent";
import { ContextMenuComponent } from "../../../../../common/contextMenu/contextMenu.component";


export abstract class BaseChatThreadMessageComponent extends BaseChatComponent implements OnInit, OnDestroy {
    @Input()
    public message: Message;

    @ViewChild('messageEl')
    public messageEl: ElementRef;

    public currentUser: User;
    public incoming: boolean;
    public classes: any;
    public timestamp: Observable<Date>;
    private subscription: Subscription;
    private contextMenu: ContextMenuComponent;

    public menuItems: Array<MenuItem> = [];
    protected menuItemComponentKey: string;

    constructor(
        protected scope: ChatLocalScope,
        protected componentResolver: ComponentFactoryResolver,
        protected viewContainer: ViewContainerRef,
        protected contextMenuService: ContextMenuService,
        protected injector: Injector
    ) {
        super();
    }


    public ngOnInit() {
        this.subscription = this.chatCtrl.userService.currentUser
            .subscribe((user: User) => {
                this.currentUser = user;
                if (!isNullOrUndefined( this.message.author ) && !isNullOrUndefined(user)) {
                    this.incoming = this.message.author.id !== user.id;
                } else {
                    this.incoming = true;
                }
            });

        this.classes = {
            'msg-sent': this.incoming,
            'msg-receive': !this.incoming,
            'initial': this.message.isInitial,
            'hidden': this.message.isHidden
        };
        console.log('classes', this.classes);
    }

    public ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    protected abstract initMenu();

    public openContextMenu(event: MouseEvent) {
        const initParams = new MenuInitParams(
            this.menuItems,
            this.menuItemComponentKey,
            event
        );

        this.contextMenuService.createMenu(initParams, this.injector);
        event.preventDefault();
        event.stopPropagation();
    }
}
