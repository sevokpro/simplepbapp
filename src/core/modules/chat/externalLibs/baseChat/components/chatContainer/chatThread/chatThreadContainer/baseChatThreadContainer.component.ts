import { OnInit } from "@angular/core";
import { BaseChatComponent } from "../../../baseChatComponent";


export abstract class BaseChatThreadContainerComponent extends BaseChatComponent implements OnInit {
    public constructor() {
        super();
    }

    public ngOnInit() {
    }
}
