import { Component, OnInit } from "@angular/core";
import { BaseChatComponent } from "../baseChatComponent";

abstract class BaseChatContainerComponent extends BaseChatComponent implements OnInit {
    protected rootData: any;
    constructor() {
        super();
    }

    public ngOnInit() {
        this.rootData = this.chatCtrl.viewScope.rootData;
    }
}

@Component({
    templateUrl: './chatContainerComponent.template.pug',
})
class ChatContainerComponent extends BaseChatContainerComponent {
    constructor() {
        super();
    }
}

export { ChatContainerComponent, BaseChatContainerComponent };
