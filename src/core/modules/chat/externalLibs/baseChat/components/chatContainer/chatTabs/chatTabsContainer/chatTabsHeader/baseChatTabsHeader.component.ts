import { OnInit } from "@angular/core";
import { BaseChatComponent } from "../../../../baseChatComponent";


export abstract class BaseChatTabsHeaderComponent extends BaseChatComponent implements OnInit {
    constructor() {
        super();
    }

    public ngOnInit() {
    }
}
