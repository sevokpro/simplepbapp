import { OnInit } from "@angular/core";
import { BaseChatComponent } from "../../../../baseChatComponent";


export abstract class BaseChatThreadFooterComponent extends BaseChatComponent implements OnInit {
    public constructor() {
        super();
    }

    public ngOnInit() {
    }
}
