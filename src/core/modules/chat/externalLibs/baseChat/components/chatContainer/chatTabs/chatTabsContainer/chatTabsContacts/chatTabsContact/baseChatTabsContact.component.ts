import { OnInit } from "@angular/core";
import { User } from "../../../../../../types/classes/User";
import { BaseChatComponent } from "../../../../../baseChatComponent";


export abstract class BaseChatTabsContactComponent extends BaseChatComponent implements OnInit {
    constructor() {
        super();
    }

    public ngOnInit() {
    }

    public clicked(contact: User, event: any): void {
        event.preventDefault();
        this.chatCtrl.threadsService.setCurrentThreadByContact(contact);
    }
}
