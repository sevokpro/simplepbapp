import { Input } from "@angular/core";
import { ChatCtrl } from "../services/chatCtrl";

export class BaseChatComponent {
    @Input()
    public chatCtrl: ChatCtrl;

    public constructor() { }
}
