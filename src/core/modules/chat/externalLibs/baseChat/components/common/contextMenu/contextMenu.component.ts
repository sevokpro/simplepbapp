import { Component, ElementRef, EventEmitter, OnInit, ViewChild } from "@angular/core";
import { MenuInitParams } from "../../../types/classes/MenuInitParams";
import { MenuItem } from "../../../types/classes/MenuItem";
import { BaseChatComponent } from "../../baseChatComponent";

abstract class BaseContextMenuComponent extends BaseChatComponent implements OnInit {
    @ViewChild('menu')
    public menuEl: ElementRef;

    public menuItems: Array<MenuItem>;
    protected menuItemComponentKey: string;
    public selectEvents: EventEmitter<MenuItem>;
    public mouseEvent: MouseEvent;

    public constructor(initParams: MenuInitParams) {
        super();
        this.menuItems = initParams.menuItems;
        this.menuItemComponentKey = initParams.menuItemComponentKey;
        this.selectEvents = initParams.selectEvents;
        this.mouseEvent = initParams.mouseEvent;
    }


    public ngOnInit() {
    }


    protected ngOnDestroy() {
    }

    protected select(item: MenuItem, event: Event) {
        event.stopPropagation();
        if (item.selected)
            return;
        this.menuItems.forEach(i => i.selected = false);
        item.selected = true;
        this.selectEvents.emit(item);
    }
}

@Component({
    selector: 'context-menu-component',
    templateUrl: './contextMenuComponent.template.pug'
})
class ContextMenuComponent extends BaseContextMenuComponent implements OnInit {
    constructor(initParams: MenuInitParams) {
        super(initParams);
    }



    public ngOnInit() {
        super.ngOnInit();
    }
}

export { ContextMenuComponent, BaseContextMenuComponent };
