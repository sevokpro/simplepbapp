import { OnDestroy, OnInit } from "@angular/core";
import { BaseChatComponent } from "../../../baseChatComponent";

abstract class BaseContextMenuItemComponent extends BaseChatComponent implements OnInit, OnDestroy {
    public ngOnInit() {
    }

    public ngOnDestroy() {
    }
}

export { BaseContextMenuItemComponent };
