export enum ContactStatus {
    Online = 1,
    Away = 2,
    Busy = 0
}
