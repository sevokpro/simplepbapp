export enum MessageStatus {
    Online,
    Away,
    DoNotDisturb
}
