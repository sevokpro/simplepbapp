import { IContactsFilterParams } from "../interfaces/IContactsFilterParams";
import { User } from "./User";

export abstract class ContactsFilter {
    public constructor(protected params?: IContactsFilterParams) {
    }

    public abstract filterByTerm(contacts: Array<User>): Array<User>;
}
