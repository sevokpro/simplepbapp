export class MenuItem {
    public className?: string;
    public caption: string;
    public selected: boolean;
}
