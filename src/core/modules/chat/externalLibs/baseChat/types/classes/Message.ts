import { isNull, isNullOrUndefined } from "@itexpert-dev/tiny-helpers";
import { getPropertyValueOrNull } from "../../../../../../../common/getPropertyValueOrNull";
import { IMessageInitParams } from "../interfaces/IMessageInitParams";
import { Thread } from "./Thread";
import { User } from "./User";

export class Message {
    public id?: string;
    public thread: Thread;
    public text: string;
    public filesHrefs: Array<string>;
    public timestamp: Date;
    public lastModified?: Date;
    public author: User;
    public authorId?: string;
    public isRead: boolean;
    public isInitial?: boolean;
    public isHidden?: boolean;
    public isPersonal?: boolean;
    public isDraft?: boolean;

    public constructor(obj?: IMessageInitParams) {

        this.id = getPropertyValueOrNull(obj, obj => obj.id);
        const isRead = getPropertyValueOrNull(obj, obj => obj.isRead);
        this.isRead = isNull(isRead) ? false : isRead;
        const timestamp = getPropertyValueOrNull(obj, obj => obj.timestamp);
        const sentAt = getPropertyValueOrNull(obj, obj => obj.sentAt);
        this.timestamp = isNull(timestamp) ? isNull(sentAt) ? new Date : sentAt : timestamp;
        this.author = getPropertyValueOrNull(obj, obj => obj.author);
        this.authorId = getPropertyValueOrNull(obj, obj => obj.authorId);
        this.text = getPropertyValueOrNull(obj, obj => obj.text);
        this.thread = getPropertyValueOrNull(obj, obj => obj.thread);
        const filesHrefs = getPropertyValueOrNull(obj, obj => obj.filesHrefs);
        this.filesHrefs = isNull(filesHrefs) ? [] : filesHrefs;
        this.lastModified = getPropertyValueOrNull(obj, obj => obj.lastModified);
        const isInitial = getPropertyValueOrNull(obj, obj => obj.isInitial);
        this.isInitial = isNull(isInitial) ? false : isInitial;
        const isHidden = getPropertyValueOrNull(obj, obj => obj.isHidden);
        this.isHidden = isNull(isHidden) ? false : isHidden;
        const isPersonal = getPropertyValueOrNull(obj, obj => obj.isPersonal);
        this.isPersonal = isNull(isPersonal) ? false : isPersonal;
    }
}
