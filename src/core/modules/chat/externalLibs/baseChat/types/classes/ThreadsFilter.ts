import { IThreadsFilterParams } from "../interfaces/IThreadsFilterParams";
import { Thread } from "./Thread";

export abstract class ThreadsFilter {
    public constructor(protected params?: IThreadsFilterParams) {
    }

    public abstract filterByTerm(threads: Array<Thread>): Array<Thread>;
}
