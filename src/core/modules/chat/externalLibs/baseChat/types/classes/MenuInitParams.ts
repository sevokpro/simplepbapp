import { EventEmitter } from "@angular/core";
import { MenuItem } from "./MenuItem";

export class MenuInitParams {
    private selectEventsField = new EventEmitter<MenuItem>();

    public get selectEvents() {
        return this.selectEventsField;
    }

    public get menuItems() {
        return this.menuItemsField;
    }

    public get menuItemComponentKey() {
        return this.menuItemComponentKeyField;
    }

    public get mouseEvent() {
        return this.mouseEventField;
    }

    public constructor(private menuItemsField: Array<MenuItem>,
        private menuItemComponentKeyField: string,
        private mouseEventField: MouseEvent) {
    }
}
