import { getPropertyValueOrNull } from "../../../../../../../common/getPropertyValueOrNull";
import { IChatPresentation } from "../interfaces/IChatPresentation";
import { IUserInitParams } from "../interfaces/IUserInitParams";

export class User implements IChatPresentation {
    public name: string;
    public avatarSrc: string;
    public id: string;
    public inUserList: boolean;
    public isCurrent: boolean;


    public constructor(obj: IUserInitParams) {
        this.id = getPropertyValueOrNull(obj, obj => obj.id);
        this.name = getPropertyValueOrNull(obj, obj => obj.name);
        this.avatarSrc = getPropertyValueOrNull(obj, obj => obj.iconHref);
    }
}

