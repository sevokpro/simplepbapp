import { isNull } from "@itexpert-dev/tiny-helpers";
import { Observable } from "rxjs";
import { getPropertyValueOrNull } from "../../../../../../../common/getPropertyValueOrNull";
import { MessagesService } from "../../services/MessagesService";
import { IChatPresentation } from "../interfaces/IChatPresentation";
import { IThreadInitParams } from "../interfaces/IThreadInitParams";
import { Message } from "./Message";
import { User } from "./User";

export class Thread implements IChatPresentation {
    public id: string;
    public isDraft: boolean;
    public lastMessage: Message;
    public name: string;
    public avatarSrc: string;
    public participants: Array<string>;
    public isPersonal: boolean;
    public isInvalid: boolean;
    public userId?: string;
    public messagesService: MessagesService;
    public unreadMessagesCount: number;

    constructor(obj?: IThreadInitParams) {
        this.id = getPropertyValueOrNull(obj, obj => obj.id);
        const isDraft = getPropertyValueOrNull(obj, obj => obj.isDraft);
        this.isDraft = isNull(isDraft) ? false : isDraft;
        this.name = getPropertyValueOrNull(obj, obj => obj.name);
        this.avatarSrc = getPropertyValueOrNull(obj, obj => obj.avatarSrc);
        this.participants = getPropertyValueOrNull(obj, obj => obj.participants);
        const isPersonal = getPropertyValueOrNull(obj, obj => obj.isPersonal);
        this.isPersonal = isNull(isPersonal) ? false : isPersonal;
        this.userId = getPropertyValueOrNull(obj, obj => obj.userId);
        const numberOfUnreadMessages = getPropertyValueOrNull(obj, obj => obj.numberOfUnreadMessages);
        this.unreadMessagesCount = isNull(numberOfUnreadMessages) ? 0 : numberOfUnreadMessages;
    }

    public addMessages(messages: Array<Message>,
        contactProvider: (val) => Observable<User>): Observable<boolean> {
        const messagesObservables = messages
            .map((threadMessage) => {
                const message = new Message(threadMessage);

                return contactProvider(
                    message.authorId)
                    .switchMap(contact => {
                        message.author = contact;
                        message.thread = this;

                        this.messagesService.pushMessage(message);
                        return Observable.of(true);
                    });
            });

        return Observable.forkJoin(messagesObservables)
            .map((result) => {
                return true;
            });
    }
}
