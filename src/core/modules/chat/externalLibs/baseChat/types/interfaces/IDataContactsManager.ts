import { Observable } from "rxjs";
import { User } from "../classes/User";

abstract class IDataContactsManager {
    public abstract getUsersByTerm(term: string): Observable<Array<User>>;
    public abstract getUsersByIds(contactsIds: Array<string>): Observable<Array<User>>;
    public abstract getUserById(contactId: string): Observable<User>;
    public abstract addChatContacts(ids: Array<string>): Observable<string>;
    public abstract removeChatContacts(ids: Array<string>): Observable<string>;
    public abstract getChatContacts(): Observable<Array<string>>;
}

export { IDataContactsManager };
