import { Message } from "../classes/Message";
import { IThreadInfo } from "./IThreadInfo";

export interface IThreadInitParams extends IThreadInfo {
    isDraft?: boolean;
    name?: string;
    avatarSrc?: string;
    participants?: Array<string>;
    messages?: Array<Message>;
}
