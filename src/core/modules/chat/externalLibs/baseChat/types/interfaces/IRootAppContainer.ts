import { Injector, ViewContainerRef } from "@angular/core";

abstract class IRootAppContainer {
    abstract get viewContainerRef(): ViewContainerRef
    abstract set viewContainerRef(viewContainer: ViewContainerRef)

    abstract get injector(): Injector
    abstract set injector(injector: Injector)
}

export { IRootAppContainer };
