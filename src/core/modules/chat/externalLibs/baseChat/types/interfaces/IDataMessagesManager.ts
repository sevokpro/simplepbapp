import { Observable } from "rxjs";
import { Message } from "../classes/Message";

abstract class IDataMessagesManager {
    public abstract createMessage(message: Message): Observable<string>;
    public abstract markMessagesAsRead(messages: Array<Message>): Observable<string>;
}

export { IDataMessagesManager };
