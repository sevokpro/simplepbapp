export interface IUserInitParams {
    name: string;
    iconHref?: string;
    id?: string;
    position?: string;
}
