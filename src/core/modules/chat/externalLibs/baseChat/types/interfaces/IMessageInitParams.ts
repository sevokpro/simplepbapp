import { Thread } from "../classes/Thread";
import { User } from "../classes/User";

export interface IMessageInitParams {
    id?: string;
    thread: Thread;
    text?: string;
    filesHrefs?: Array<string>;
    timestamp?: Date;
    sentAt?: Date;
    lastModified?: Date;
    author?: User;
    authorId?: string;
    isRead?: boolean;
    isInitial?: boolean;
    isHidden?: boolean;
    isPersonal?: boolean;
}
