export interface IMouseEvent {
    x: number;
    y: number;
}
