import { Observable } from "rxjs";
import { IChatCrudRepositoryReadRequest } from "../../../../modules/chat/services/dataManager/repositories/interfaces/IChatCrudRepositoryReadRequest";
import { Thread } from "../classes/Thread";
import { IThreadInfo } from "./IThreadInfo";
import { IThreadInitParams } from "./IThreadInitParams";

abstract class IDataDialogsManager {
    public abstract getDialogsInfo(params?: IChatCrudRepositoryReadRequest): Observable<Array<IThreadInfo>>;
    public abstract getDialog(params?: IChatCrudRepositoryReadRequest): Observable<IThreadInitParams>;
    public abstract getPersonalDialog(params?: IChatCrudRepositoryReadRequest): Observable<Thread>;
}

export { IDataDialogsManager };
