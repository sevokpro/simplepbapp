export interface IChatPresentation {
    name: string;
    avatarSrc: string;
}
