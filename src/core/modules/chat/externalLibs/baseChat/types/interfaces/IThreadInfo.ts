export interface IThreadInfo {
    id?: string;
    isPersonal?: boolean;
    userId?: string;
    numberOfUnreadMessages?: number;
}
