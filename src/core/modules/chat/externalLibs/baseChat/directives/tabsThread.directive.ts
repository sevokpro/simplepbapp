import { ComponentFactoryResolver, Directive, Input, OnInit, ViewContainerRef } from "@angular/core";
import { Thread } from "../types/classes/Thread";
import { BaseChatDirective } from "./baseChatDirective";
import { ChatLocalScope } from "./common/chatLocalScope";

class BaseTabsThreadDirective extends BaseChatDirective implements OnInit {
    protected componentResolver: ComponentFactoryResolver;
    protected viewContainer: ViewContainerRef;
    protected localScope: ChatLocalScope;
    protected thread: Thread;

    constructor() {
        super();
    }

    public ngOnInit() {
        //создание компоненты контейнера табов с пробросом контроллера
        const chatCtrl = this.localScope.chatCtrl;
        const componentRef = this.initChatPartWithCtrl(chatCtrl.chatComponents.chatContainer.tabsContainer.threads.thread);
        componentRef.instance['thread'] = this.thread;
    }
}

@Directive({
    selector: 'tabs-thread'
})
class TabsThreadDirective extends BaseTabsThreadDirective {
    @Input()
    public thread: Thread;

    constructor(
        protected componentResolver: ComponentFactoryResolver,
        protected viewContainer: ViewContainerRef,
        protected localScope: ChatLocalScope
    ) {
        super();
    }
}

export { TabsThreadDirective };
