import { Component, ComponentFactoryResolver, ComponentRef, ViewContainerRef } from "@angular/core";
import { ChatLocalScope } from "./common/chatLocalScope";

export abstract class BaseChatDirective {
    protected componentResolver: ComponentFactoryResolver;
    protected viewContainer: ViewContainerRef;
    protected localScope: ChatLocalScope;

    protected initChatPart(componentName: string): ComponentRef<any> {
        const chatCtrl = this.localScope.chatCtrl;
        const component: Component = chatCtrl.config.componentsStorage.get(componentName);
        const componentFactory = this.componentResolver.resolveComponentFactory(component as any);
        const componentRef = this.viewContainer.createComponent(componentFactory);
        // componentRef.instance['chatCtrl'] = chatCtrl;
        return componentRef;
    }

    protected initChatPartWithCtrl(componentName: string): ComponentRef<any> {
        const chatCtrl = this.localScope.chatCtrl;
        const componentRef = this.initChatPart(componentName);
        componentRef.instance['chatCtrl'] = chatCtrl;
        return componentRef;
    }
}
