import { ChatCtrl } from "../../services/chatCtrl";

export class ChatLocalScope {
    constructor() {
    }
    private chatCtrlField: ChatCtrl;
    set chatCtrl(chatCtrl: ChatCtrl) {
        this.chatCtrlField = chatCtrl;
    }
    get chatCtrl(): ChatCtrl {
        return this.chatCtrlField;
    }
}
