import { ComponentFactoryResolver, Directive, Input, OnInit, ViewContainerRef } from "@angular/core";
import { MenuItem } from "../types/classes/MenuItem";
import { BaseChatDirective } from "./baseChatDirective";
import { ChatLocalScope } from "./common/chatLocalScope";

class BaseTabsContactDirective extends BaseChatDirective implements OnInit {
    @Input() protected menuItem: MenuItem;
    @Input() protected componentKey: string;

    protected componentResolver: ComponentFactoryResolver;
    protected viewContainer: ViewContainerRef;
    protected localScope: ChatLocalScope;

    constructor() {
        super();
    }

    public ngOnInit() {
        //создание компоненты контейнера табов с пробросом контроллера
        const chatCtrl = this.localScope.chatCtrl;
        const componentRef = this.initChatPartWithCtrl(this.componentKey);
        componentRef.instance['menuItem'] = this.menuItem;
    }
}

@Directive({
    selector: 'context-menu-item'
})
class ContextMenuItemDirective extends BaseTabsContactDirective {
    constructor(protected componentResolver: ComponentFactoryResolver,
        protected viewContainer: ViewContainerRef,
        protected localScope: ChatLocalScope) {
        super();
    }
    @Input('menuItem')
    public menuItem: any;

    @Input('componentKey')
    public componentKey: any;
}

export { ContextMenuItemDirective };
