import { ComponentFactoryResolver, Directive, OnInit, ViewContainerRef } from "@angular/core";
import { BaseChatDirective } from "./baseChatDirective";
import { ChatLocalScope } from "./common/chatLocalScope";

class BaseTabsHeaderDirective extends BaseChatDirective implements OnInit {
    protected componentResolver: ComponentFactoryResolver;
    protected viewContainer: ViewContainerRef;
    protected localScope: ChatLocalScope;

    constructor() {
        super();
    }

    public ngOnInit() {
        /**
         * @desc создание компоненты контейнера табов с пробросом контроллера
         */
        const chatCtrl = this.localScope.chatCtrl;
        this.initChatPartWithCtrl(chatCtrl.chatComponents.chatContainer.tabsContainer.tabsHeader.self);
    }
}

@Directive({
    selector: 'tabs-header'
})
class TabsHeaderDirective extends BaseTabsHeaderDirective {
    constructor(
        protected componentResolver: ComponentFactoryResolver,
        protected viewContainer: ViewContainerRef,
        protected localScope: ChatLocalScope
    ) {
        super();
    }
}

export { TabsHeaderDirective };
