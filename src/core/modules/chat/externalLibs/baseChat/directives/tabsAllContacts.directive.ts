import { ComponentFactoryResolver, Directive, OnInit, ViewContainerRef } from "@angular/core";
import { BaseChatDirective } from "./baseChatDirective";
import { ChatLocalScope } from "./common/chatLocalScope";

class BaseTabsAllContactsDirective extends BaseChatDirective implements OnInit {
    protected componentResolver: ComponentFactoryResolver;
    protected viewContainer: ViewContainerRef;
    protected localScope: ChatLocalScope;

    constructor() {
        super();
    }

    public ngOnInit() {
        //создание компоненты контейнера табов с пробросом контроллера
        const chatCtrl = this.localScope.chatCtrl;
        this.initChatPartWithCtrl(chatCtrl.chatComponents.chatContainer.tabsContainer.allContacts);
    }
}

@Directive({
    selector: 'tabs-all-contacts'
})
class TabsAllContactsDirective extends BaseTabsAllContactsDirective {
    constructor(
        protected componentResolver: ComponentFactoryResolver,
        protected viewContainer: ViewContainerRef,
        protected localScope: ChatLocalScope
    ) {
        super();
    }
}

export { TabsAllContactsDirective };
