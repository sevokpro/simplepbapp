import { ComponentFactoryResolver, Directive, Input, OnInit, ViewContainerRef } from "@angular/core";
import { Message } from "../types/classes/Message";
import { BaseChatDirective } from "./baseChatDirective";
import { ChatLocalScope } from "./common/chatLocalScope";

class BaseThreadMessageDirective extends BaseChatDirective implements OnInit {
    protected componentResolver: ComponentFactoryResolver;
    protected viewContainer: ViewContainerRef;
    protected localScope: ChatLocalScope;
    protected message: Message;

    constructor() {
        super();
    }

    public ngOnInit() {
        const chatCtrl = this.localScope.chatCtrl;
        const componentRef = this.initChatPartWithCtrl(chatCtrl.chatComponents.chatContainer.threadContainer.threadBody.threadMessage.self);
        componentRef.instance['message'] = this.message;
    }
}

@Directive({
    selector: 'thread-message'
})
class ThreadMessageDirective extends BaseThreadMessageDirective {
    @Input()
    public message: Message;

    constructor(
        protected componentResolver: ComponentFactoryResolver,
        protected viewContainer: ViewContainerRef,
        protected localScope: ChatLocalScope
    ) {
        super();
    }
}

export { ThreadMessageDirective };
