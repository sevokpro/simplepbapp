import { ComponentFactoryResolver, Directive, OnInit, ViewContainerRef } from "@angular/core";
import { BaseChatDirective } from "./baseChatDirective";
import { ChatLocalScope } from "./common/chatLocalScope";

class BaseThreadInputDirective extends BaseChatDirective implements OnInit {
    protected componentResolver: ComponentFactoryResolver;
    protected viewContainer: ViewContainerRef;
    protected localScope: ChatLocalScope;

    constructor() {
        super();
    }

    public ngOnInit() {
        const chatCtrl = this.localScope.chatCtrl;
        const componentRef = this.initChatPartWithCtrl(chatCtrl.chatComponents.chatContainer.threadContainer.threadFooter.threadInput);
    }
}

@Directive({
    selector: 'thread-input'
})
class ThreadInputDirective extends BaseThreadInputDirective {

    constructor(
        protected componentResolver: ComponentFactoryResolver,
        protected viewContainer: ViewContainerRef,
        protected localScope: ChatLocalScope
    ) {
        super();
    }
}

export { ThreadInputDirective };
