import { ComponentFactoryResolver, Directive, Input, OnInit, ViewContainerRef } from "@angular/core";
import { BaseChatDirective } from "./baseChatDirective";
import { ChatLocalScope } from "./common/chatLocalScope";

class BaseTabsContainerDirective extends BaseChatDirective implements OnInit {
    protected componentResolver: ComponentFactoryResolver;
    protected viewContainer: ViewContainerRef;
    protected localScope: ChatLocalScope;


    constructor() {
        super();
    }

    public ngOnInit() {
        /**
         * @desc создание компоненты контейнера табов с пробросом контроллера
         */
        const chatCtrl = this.localScope.chatCtrl;
        this.initChatPartWithCtrl(chatCtrl.chatComponents.chatContainer.tabsContainer.self);
    }
}

@Directive({
    selector: 'tabs-container'
})
class TabsContainerDirective extends BaseTabsContainerDirective {
    @Input()
    public indexInParent: number;

    constructor(
        protected componentResolver: ComponentFactoryResolver,
        protected viewContainer: ViewContainerRef,
        protected localScope: ChatLocalScope
    ) {
        super();
    }
}

export { TabsContainerDirective };
