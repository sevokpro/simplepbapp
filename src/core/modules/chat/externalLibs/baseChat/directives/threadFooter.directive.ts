import { ComponentFactoryResolver, Directive, OnInit, ViewContainerRef } from "@angular/core";
import { BaseChatDirective } from "./baseChatDirective";
import { ChatLocalScope } from "./common/chatLocalScope";

class BaseThreadFooterDirective extends BaseChatDirective implements OnInit {
    protected componentResolver: ComponentFactoryResolver;
    protected viewContainer: ViewContainerRef;
    protected localScope: ChatLocalScope;

    constructor() {
        super();
    }

    public ngOnInit() {
        const chatCtrl = this.localScope.chatCtrl;
        this.initChatPartWithCtrl(chatCtrl.chatComponents.chatContainer.threadContainer.threadFooter.self);
    }
}

@Directive({
    selector: 'thread-footer'
})
class ThreadFooterDirective extends BaseThreadFooterDirective {
    constructor(
        protected componentResolver: ComponentFactoryResolver,
        protected viewContainer: ViewContainerRef,
        protected localScope: ChatLocalScope
    ) {
        super();
    }
}

export { ThreadFooterDirective };
