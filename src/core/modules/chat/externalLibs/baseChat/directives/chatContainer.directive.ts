import { ComponentFactoryResolver, Directive, OnInit, ViewContainerRef } from "@angular/core";
import { BaseChatDirective } from "./baseChatDirective";
import { ChatLocalScope } from "./common/chatLocalScope";

class BaseChatContainerDirective extends BaseChatDirective implements OnInit {
    protected componentResolver: ComponentFactoryResolver;
    protected viewContainer: ViewContainerRef;
    protected localScope: ChatLocalScope;

    constructor() {
        super();
    }

    public ngOnInit() {
        const chatCtrl = this.localScope.chatCtrl;
        chatCtrl.initPromise.then((result) => {
            this.initChatPartWithCtrl(chatCtrl.chatComponents.chatContainer.self);
        }).catch(reject => console.warn(reject));
    }
}

@Directive({
    selector: 'chat-container'
})
class ChatContainerDirective extends BaseChatContainerDirective {
    constructor(
        protected componentResolver: ComponentFactoryResolver,
        protected viewContainer: ViewContainerRef,
        protected localScope: ChatLocalScope
    ) {
        super();
    }
}


export { ChatContainerDirective };
