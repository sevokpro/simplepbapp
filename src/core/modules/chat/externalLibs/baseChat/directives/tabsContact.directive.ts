import { ComponentFactoryResolver, Directive, Input, OnInit, ViewContainerRef } from "@angular/core";
import { User } from "../types/classes/User";
import { BaseChatDirective } from "./baseChatDirective";
import { ChatLocalScope } from "./common/chatLocalScope";

class BaseTabsContactDirective extends BaseChatDirective implements OnInit {
    protected componentResolver: ComponentFactoryResolver;
    protected viewContainer: ViewContainerRef;
    protected localScope: ChatLocalScope;
    protected contact: User;

    constructor() {
        super();
    }

    public ngOnInit() {
        //создание компоненты контейнера табов с пробросом контроллера
        const chatCtrl = this.localScope.chatCtrl;
        const componentRef = this.initChatPartWithCtrl(chatCtrl.chatComponents.chatContainer.tabsContainer.contacts.contact);
        componentRef.instance['contact'] = this.contact;
    }
}

@Directive({
    selector: 'tabs-contact'
})
class TabsContactDirective extends BaseTabsContactDirective {
    @Input()
    public contact: User;

    constructor(
        protected componentResolver: ComponentFactoryResolver,
        protected viewContainer: ViewContainerRef,
        protected localScope: ChatLocalScope
    ) {
        super();
    }
}

export { TabsContactDirective };
