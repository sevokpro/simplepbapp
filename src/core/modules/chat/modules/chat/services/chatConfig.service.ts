import { Injectable } from "@angular/core";
import { IChatConfigs } from "./types/interfaces/IChatConfigs";
import { IEntityConfig } from "./types/interfaces/IEntityConfig";

@Injectable()
export class ChatConfigService {
    private default: IEntityConfig = {
        limit: 15
    };

    public messages: IEntityConfig = this.default;

    public dialogs: IEntityConfig = this.default;

    public contacts: IEntityConfig = this.default;

    private configField: IChatConfigs;

    set appConfig(obj: IChatConfigs) {
        this.configField = obj;
    }

    get appConfig(): IChatConfigs {
        return this.configField;
    }
}
