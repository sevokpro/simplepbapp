import { BaseChatService } from "../../../externalLibs/baseChat/services/baseChatService/baseChat.service";
import { CustomInjectable } from "../../../externalLibs/baseChat/utils/CustomInjectable.decorator";
import { ChatConfigurator } from "./chatConfigurator";

@CustomInjectable()
export class ChatService extends BaseChatService {
    public config: ChatConfigurator = new ChatConfigurator();
}
