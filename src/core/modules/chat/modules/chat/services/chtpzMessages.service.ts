import { Injectable } from "@angular/core";
import { Subject } from "rxjs";
import { ContactsService } from "../../../externalLibs/baseChat/services/ContactsService";
import { DataContactsManagerService } from "./dataManager/dataContactsManager.service";
import { ChtpzMessage } from "./types/classes/ChtpzMessage";
import { ChtpzUser } from "./types/classes/ChtpzUser";
import { ContactsType } from "./types/classes/ContactsType";

@Injectable()
export class ChtpzMessagesService {
    private draftContacts: ContactsService;
    private currentList: Array<ChtpzUser>;

    public resetSelected = new Subject<ContactsType>();
    public saveDraft = new Subject();
    public stopLoading = new Subject();


    constructor(private dataManagerService: DataContactsManagerService) { }

    public sendMessage(message: ChtpzMessage) {
        this.dataManagerService;
    }
}
