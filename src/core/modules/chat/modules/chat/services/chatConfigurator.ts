import { BaseChatConfigurator } from "../../../externalLibs/baseChat/services/baseChatConfigurator/baseChatConfigurator";
import { IChatComponentsMap } from "../../../externalLibs/baseChat/services/baseChatConfigurator/interfaces/IChatComponentsMap";
import { ChtpzChatTabsAllContactsComponent } from "../components/chtpzChatContainer/chatTabs/chtpzChatTabsContainer/chtpzChatTabsAllContacts/chtpzChatTabsAllContacts.component";
import { ChtpzChatTabsContactComponent } from "../components/chtpzChatContainer/chatTabs/chtpzChatTabsContainer/chtpzChatTabsContacts/chtpzChatTabsContact/chtpzChatTabsContact.component";
import { ChtpzChatTabsContactsComponent } from "../components/chtpzChatContainer/chatTabs/chtpzChatTabsContainer/chtpzChatTabsContacts/chtpzChatTabsContacts.component";
import { ChtpzChatTabsContainerComponent } from "../components/chtpzChatContainer/chatTabs/chtpzChatTabsContainer/chtpzChatTabsContainer.component";
import { ChtpzChatTabsHeaderComponent } from "../components/chtpzChatContainer/chatTabs/chtpzChatTabsContainer/chtpzChatTabsHeader/chtpzChatTabsHeader.component";
import { ChtpzChatTabsHeaderAvatarComponent } from "../components/chtpzChatContainer/chatTabs/chtpzChatTabsContainer/chtpzChatTabsHeader/chtpzChatTabsHeaderAvatar/chtpzChatTabsHeaderAvatar.component";
import { ChtpzChatTabsHeaderAvatarMenuItemComponent } from "../components/chtpzChatContainer/chatTabs/chtpzChatTabsContainer/chtpzChatTabsHeader/chtpzChatTabsHeaderAvatar/statusMenuItem/chtpzChatTabsHeaderAvatarMenuItem.component";
import { ChtpzChatTabsSearchboxComponent } from "../components/chtpzChatContainer/chatTabs/chtpzChatTabsContainer/chtpzChatTabsHeader/chtpzChatTabsSearchbox/chtpzChatTabsSearchbox.component";
import { ChtpzChatTabsThreadComponent } from "../components/chtpzChatContainer/chatTabs/chtpzChatTabsContainer/chtpzChatTabsThreads/chtpzChatTabsThread/chtpzChatTabsThread.component";
import { ChtpzChatTabsThreadsComponent } from "../components/chtpzChatContainer/chatTabs/chtpzChatTabsContainer/chtpzChatTabsThreads/chtpzChatTabsThreads.component";
import { ChtpzChatThreadBodyComponent } from "../components/chtpzChatContainer/chatThread/chtpzChatThreadContainer/chtpzChatThreadBody/chtpzChatThreadBody.component";
import { ChtpzChatThreadMessageComponent } from "../components/chtpzChatContainer/chatThread/chtpzChatThreadContainer/chtpzChatThreadBody/chtpzChatThreadMessage/chtpzChatThreadMessage.component";
import { ChtpzChatThreadMessageMenuItemComponent } from "../components/chtpzChatContainer/chatThread/chtpzChatThreadContainer/chtpzChatThreadBody/chtpzChatThreadMessage/messageMenuItem/chtpzChatThreadMessageMenuItem.component";
import { ChtpzChatThreadContainerComponent } from "../components/chtpzChatContainer/chatThread/chtpzChatThreadContainer/chtpzChatThreadContainer.component";
import { ChtpzChatThreadFooterComponent } from "../components/chtpzChatContainer/chatThread/chtpzChatThreadContainer/chtpzChatThreadFooter/chtpzChatThreadFooter.component";
import { ChtpzChatThreadInputComponent } from "../components/chtpzChatContainer/chatThread/chtpzChatThreadContainer/chtpzChatThreadFooter/chtpzChatThreadInput/chtpzChatThreadInput.component";
import { ChtpzChatThreadHeaderComponent } from "../components/chtpzChatContainer/chatThread/chtpzChatThreadContainer/chtpzChatThreadHeader/chtpzChatThreadHeader.component";
import { ChtpzChatContainerComponent } from "../components/chtpzChatContainer/chtpzChatContainer.component";

class ChatConfigurator extends BaseChatConfigurator {
    public constructor() {
        super();

        const chatCompsMap: IChatComponentsMap = {
            chatContainer: {
                self: 'ContainerComponent',
                tabsContainer: {
                    self: 'TabsContainerComponent',
                    tabsHeader: {
                        self: 'TabsHeaderComponent',
                        tabsHeaderAvatar: {
                            self: 'TabsHeaderAvatarComponent ',
                            statusMenuItem: 'HeaderAvatarStatusMenuItem'
                        },
                        tabsSearchbox: 'TabsSearchBoxComponent',
                    },
                    contacts: {
                        self: 'TabsContactsComponents',
                        contact: 'TabsContactComponents'
                    },
                    threads: {
                        self: 'TabsThreadsComponents',
                        thread: 'TabsThreadComponents'
                    },
                    allContacts: 'TabsAllContactsComponents'
                },
                threadContainer: {
                    self: 'ThreadContainerComponent',
                    threadHeader: 'ThreadHeaderComponent',
                    threadBody: {
                        self: 'ThreadBodyComponent',
                        threadMessage: {
                            self: 'ThreadMessageComponent',
                            menuItem: 'ThreadMessageMenuItemComponent'
                        }
                    },
                    threadFooter: {
                        self: 'ThreadFooterComponent',
                        threadInput: 'ThreadInputComponent'
                    }
                }
            }
        };

        this.componentsStorage.set(chatCompsMap.chatContainer.self, ChtpzChatContainerComponent);
        this.componentsStorage.set(chatCompsMap.chatContainer.tabsContainer.self, ChtpzChatTabsContainerComponent);
        this.componentsStorage.set(chatCompsMap.chatContainer.threadContainer.self, ChtpzChatThreadContainerComponent);
        this.componentsStorage.set(chatCompsMap.chatContainer.tabsContainer.tabsHeader.self, ChtpzChatTabsHeaderComponent);
        this.componentsStorage.set(chatCompsMap.chatContainer.tabsContainer.tabsHeader.tabsHeaderAvatar.self, ChtpzChatTabsHeaderAvatarComponent);
        this.componentsStorage.set(chatCompsMap.chatContainer.tabsContainer.tabsHeader.tabsHeaderAvatar.statusMenuItem, ChtpzChatTabsHeaderAvatarMenuItemComponent);
        this.componentsStorage.set(chatCompsMap.chatContainer.tabsContainer.tabsHeader.tabsSearchbox, ChtpzChatTabsSearchboxComponent);
        this.componentsStorage.set(chatCompsMap.chatContainer.tabsContainer.contacts.self, ChtpzChatTabsContactsComponent);
        this.componentsStorage.set(chatCompsMap.chatContainer.tabsContainer.contacts.contact, ChtpzChatTabsContactComponent);
        this.componentsStorage.set(chatCompsMap.chatContainer.tabsContainer.threads.self, ChtpzChatTabsThreadsComponent);
        this.componentsStorage.set(chatCompsMap.chatContainer.tabsContainer.threads.thread, ChtpzChatTabsThreadComponent);
        this.componentsStorage.set(chatCompsMap.chatContainer.tabsContainer.allContacts, ChtpzChatTabsAllContactsComponent);

        this.componentsStorage.set(chatCompsMap.chatContainer.threadContainer.threadHeader, ChtpzChatThreadHeaderComponent);
        this.componentsStorage.set(chatCompsMap.chatContainer.threadContainer.threadBody.self, ChtpzChatThreadBodyComponent);
        this.componentsStorage.set(chatCompsMap.chatContainer.threadContainer.threadBody.threadMessage.self, ChtpzChatThreadMessageComponent);
        this.componentsStorage.set(chatCompsMap.chatContainer.threadContainer.threadBody.threadMessage.menuItem, ChtpzChatThreadMessageMenuItemComponent);
        this.componentsStorage.set(chatCompsMap.chatContainer.threadContainer.threadFooter.self, ChtpzChatThreadFooterComponent);
        this.componentsStorage.set(chatCompsMap.chatContainer.threadContainer.threadFooter.threadInput, ChtpzChatThreadInputComponent);

        this.setDefaultComponents(chatCompsMap as IChatComponentsMap);
    }
}


export { ChatConfigurator };
