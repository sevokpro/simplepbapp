import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { CanActivate } from "@angular/router";
import { Observable } from "rxjs/Observable";
import { ChatConfigService } from "./chatConfig.service";

export interface ICanComponentActivate {
    canActivate: () => Observable<boolean> | Promise<boolean> | boolean;
}

@Injectable()
export class CanActivateConfigGuard implements CanActivate {
    public constructor(private http: Http,
        private config: ChatConfigService) {
    }

    public canActivate() {
        return this.http.get('./appConfig.json', { search: `t=${Math.random().toString()}` })
            .map(response => {
                this.config.appConfig = response.json();
                return true;
            })
            .catch(() => {
                const result = confirm(`При загрузке кофигурации чата произошла ошибка.\nПерезагрузить чат?`);
                if (result === true) {
                    window.location.reload();
                }
                return Observable.of(false);
            }
            );
    }
}
