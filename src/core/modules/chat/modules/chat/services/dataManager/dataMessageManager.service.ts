import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { Observable } from "rxjs";
import { IDataMessagesManager } from "../../../../externalLibs/baseChat/types/interfaces/IDataMessagesManager";
import { ChatConfigService } from "../chatConfig.service";
import { GlobalChatService } from "../globalChat.service";
import { ChtpzMessage } from "../types/classes/ChtpzMessage";
import { DataManagerService } from "./dataManager.service";
import { IChatCrudRepositoryUpdateRequest } from "./repositories/interfaces/IChatCrudRepositoryUpdateRequest";
import { ICrudRepositoryCreateRequest } from "./repositories/interfaces/ICrudRepositoryCreateRequest";

@Injectable()
export class DataMessageManagerService extends DataManagerService implements IDataMessagesManager {
    constructor(protected http: Http,
        protected globalChatService: GlobalChatService,
        protected config: ChatConfigService) {
        super(http, globalChatService, config);
    }

    public createMessage(message: ChtpzMessage): Observable<string> {
        const query: ICrudRepositoryCreateRequest =
            {
                entity: 'message',
                data: message
            };

        return this.chatCrud.create(query)
            .map(response => response.json['messageId']);
    }

    public markMessagesAsRead(messages: Array<ChtpzMessage>): Observable<string> {
        const messagesIds = messages.map(message => message.id);
        const query: IChatCrudRepositoryUpdateRequest =
            {
                entity: 'message',
                messagesIds: messagesIds
            };

        return this.chatCrud.update(query)
            .map(response => response.message);
    }

}
