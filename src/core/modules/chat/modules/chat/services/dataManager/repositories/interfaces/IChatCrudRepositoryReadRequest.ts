import { ICrudRepositoryReadRequestFilterBlockFormat } from "@itexpert-dev/i-crud-repository";
export interface IChatCrudRepositoryReadRequest {
    entity?: 'dialog' | 'personal' | 'message' | 'contact' | 'employee';
    id?: string;
    limit?: number;
    offset?: number;
    fields?: string | Array<string>;
    sort?: string;
    stringFilter?: string;
    filter?: ICrudRepositoryReadRequestFilterBlockFormat;
    complexFilter?: ICrudRepositoryReadRequestFilterBlockFormat;
    contactsIds?: Array<string>;
}

