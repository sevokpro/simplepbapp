import { Request, RequestMethod } from "@angular/http";
import { RequestArgs } from "@angular/http/src/interfaces";
import { isNullOrUndefined } from "@itexpert-dev/tiny-helpers";
import { GlobalChatService } from "../../../globalChat.service";
import { ChtpzMessage } from "../../../types/classes/ChtpzMessage";
import { IChatCrudRepositoryReadRequest } from "../interfaces/IChatCrudRepositoryReadRequest";
import { IChatCrudRepositoryUpdateRequest } from "../interfaces/IChatCrudRepositoryUpdateRequest";
import { ICrudRepositoryCreateRequest } from "../interfaces/ICrudRepositoryCreateRequest";
import { ICrudRepositoryDeleteRequest } from "../interfaces/ICrudRepositoryDeleteRequest";

export class ChatMiddleWareQueryAdapter {
    constructor(private basePath: string,
        private globalChatService: GlobalChatService) {
    }

    public resolveGetHttpOptions(request: IChatCrudRepositoryReadRequest): Request {
        let url: string;
        const queryString = ['limit', 'offset'].map((key) => {
            const value = request[key];
            return value ? `${key}=${value}` : null;
        })
            .filter((v) => v !== null)
            .join('&');

        url = `${this.basePath}/${request.entity}/`;

        if (!isNullOrUndefined(request.id))
            url += `${request.id}/`;

        if (!isNullOrUndefined(queryString))
            url += `?${queryString}`;

        const requestArgs: RequestArgs = {
            url: url,
            method: RequestMethod.Get
        };

        return this.getRequest(requestArgs);
    }

    public resolvePostHttpOptions(request: ICrudRepositoryCreateRequest): Request {
        const requestArgs: RequestArgs = {
            url: `${this.basePath}/${request.entity}`,
            method: `POST`
        };
        const message = request.data as ChtpzMessage;
        switch (request.entity) {
            case "contact": {
                requestArgs.body = {
                    'UsersIds': request.data
                };
            }
                break;
            case "message": {
                requestArgs.body = {
                    "DialogId": message.thread.id,
                    "Text": message.text
                };
            }
                break;
        }
        return this.getRequest(requestArgs);
    }

    public resolvePatchHttpOptions(request: IChatCrudRepositoryUpdateRequest): Request {
        const requestArgs: RequestArgs = {
            url: `${this.basePath}/`,
            method: `PATCH`
        };
        switch (request.entity) {
            case "message": {
                requestArgs.url += `message/`;
                if (!isNullOrUndefined( request.messagesIds )) {
                    requestArgs.body = {
                        "MessagesIds": request.messagesIds,
                    };
                    requestArgs.url += `read`;
                }
            }
                break;
        }
        return this.getRequest(requestArgs);
    }

    public resolveDeleteHttpOptions(request: ICrudRepositoryDeleteRequest): Request {
        const requestArgs: RequestArgs = {
            url: `${this.basePath}/${request.entity}`,
            method: `Delete`
        };

        switch (request.entity) {
            case "contact": {
                requestArgs.url += `?ContactsIds=${(request.data as Array<string>).join(',')}`;
            }
                break;
            default:
                requestArgs.body = {};
        }
        return this.getRequest(requestArgs);
    }

    private getRequest(requestArgs: RequestArgs) {
        const request = new Request(requestArgs);
        // request.headers.append('X-Session-Id', this.globalChatService.get().userData.EmployeeGuid);
        // request.headers.append('Token', this.globalChatService.get().userData.EmployeeGuid);
        return request;
    }
}
