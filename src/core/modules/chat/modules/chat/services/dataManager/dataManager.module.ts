import { NgModule } from "@angular/core";
import { HttpModule } from "@angular/http";
import { IDataContactsManager } from "../../../../externalLibs/baseChat/types/interfaces/IDataContactsManager";
import { IDataDialogsManager } from "../../../../externalLibs/baseChat/types/interfaces/IDataDialogsManager";
import { IDataMessagesManager } from "../../../../externalLibs/baseChat/types/interfaces/IDataMessagesManager";
import { DataContactsManagerService } from "./dataContactsManager.service";
import { DataDialogsManagerService } from "./dataDialogsManager.service";
import { DataMessageManagerService } from "./dataMessageManager.service";

@NgModule({
    imports: [HttpModule],
    declarations: [],
    exports: [],
    providers: [
        { provide: IDataContactsManager, useClass: DataContactsManagerService },
        { provide: IDataDialogsManager, useClass: DataDialogsManagerService },
        { provide: IDataMessagesManager, useClass: DataMessageManagerService },
    ]
})
export class DataManagerModule { }
