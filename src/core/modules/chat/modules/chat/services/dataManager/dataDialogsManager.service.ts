import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { isNullOrUndefined } from "@itexpert-dev/tiny-helpers";
import { Observable } from "rxjs";
import { IDataDialogsManager } from "../../../../externalLibs/baseChat/types/interfaces/IDataDialogsManager";
import { IThreadInfo } from "../../../../externalLibs/baseChat/types/interfaces/IThreadInfo";
import { IThreadInitParams } from "../../../../externalLibs/baseChat/types/interfaces/IThreadInitParams";
import { ChatConfigService } from "../chatConfig.service";
import { GlobalChatService } from "../globalChat.service";
import { ChtpzThread } from "../types/classes/ChtpzThread";
import { DataManagerService } from "./dataManager.service";
import { IChatCrudRepositoryReadRequest } from "./repositories/interfaces/IChatCrudRepositoryReadRequest";

@Injectable()
export class DataDialogsManagerService extends DataManagerService implements IDataDialogsManager {
    constructor(protected http: Http,
        protected globalChatService: GlobalChatService,
        protected config: ChatConfigService
    ) {
        super(http, globalChatService, config);
    }

    private getDefaultReadRequest(params: IChatCrudRepositoryReadRequest): IChatCrudRepositoryReadRequest {
        const limit = !isNullOrUndefined(params.limit) ? params.limit : this.config.dialogs.limit;
        const offset = !isNullOrUndefined(params.offset) ? params.offset : 0;
        return {
            entity: 'dialog',
            limit: limit,
            offset: offset
        };
    }

    private getDialogHelper(query: IChatCrudRepositoryReadRequest): Observable<IThreadInitParams> {
        return this.chatCrud.read(query)
            .map(response => {
                const initParams: IThreadInitParams = {};
                initParams.id = response['dialogId'];
                initParams.messages = response['value'];
                initParams.participants = response['participants'];

                return initParams;
            });
    }

    public getDialogsInfo(params?: IChatCrudRepositoryReadRequest): Observable<Array<IThreadInfo>> {
        const query: IChatCrudRepositoryReadRequest = this.getDefaultReadRequest(params);

        return this.chatCrud.read(query)
            .map(response => {
                return response['value'];
            });
    }

    public getDialog(params?: IChatCrudRepositoryReadRequest): Observable<IThreadInitParams> {
        const query: IChatCrudRepositoryReadRequest = this.getDefaultReadRequest(params);
        if (isNullOrUndefined(params.id)) {
            throw new Error("Пропущен обязательный параметр id в методе getDialog");
        }
        query.id = params.id;
        return this.getDialogHelper(query);
    }

    public getPersonalDialog(params?: IChatCrudRepositoryReadRequest): Observable<ChtpzThread> {
        const query: IChatCrudRepositoryReadRequest = this.getDefaultReadRequest(params);
        if (isNullOrUndefined(params.id) && isNullOrUndefined(params.contactsIds)) {
            throw new Error("ЧТПЗ. В метод getDialog необходимо передать либо id, либо contactsIds");
        }
        query.id = isNullOrUndefined(params.id) ? params.contactsIds[0] : params.id;
        query.entity = "personal";
        return this.getDialogHelper(query) as any;
    }

}
