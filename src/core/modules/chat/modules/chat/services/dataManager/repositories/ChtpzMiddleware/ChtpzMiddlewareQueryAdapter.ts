import { Request } from "@angular/http";
import { RequestArgs } from "@angular/http/src/interfaces";
import { IChatCrudRepositoryReadRequest } from "../interfaces/IChatCrudRepositoryReadRequest";

export class ChtpzMiddlewareQueryAdapter {
    constructor(private basePath: string) { }

    public resolvePostHttpOptions(request: IChatCrudRepositoryReadRequest): Request {
        let httpRequest: Request;
        let requestArgs: RequestArgs;

        switch (request.entity) {
            default:
                requestArgs = {
                    url: `${this.basePath}/entity/${request.entity}/read`,
                    method: `POST`,
                    body: this.createBodyReadRequestObject(request)
                };
        }
        httpRequest = new Request(requestArgs);
        return httpRequest;
    }

    private createBodyReadRequestObject(request: IChatCrudRepositoryReadRequest): any {
        const result: any = {...request};

        delete result.entity;
        delete result.id;
        delete result.stringFilter;

        if (request.fields instanceof Array)
            result.fields = request.fields.join(',');

        return result;
    }
}

