export interface ICrudRepositoryCreateRequest {
    entity: 'dialog' | 'message' | 'contact';
    data: any;
}
