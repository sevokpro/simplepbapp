export interface IChatCrudRepositoryPatchResponse {
    status?: number;
    message?: string;
}
