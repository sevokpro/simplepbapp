export interface ICrudRepositoryDeleteRequest {
    entity: 'dialog' | 'message' | 'contact';
    data?: string | Array<string>;
}
