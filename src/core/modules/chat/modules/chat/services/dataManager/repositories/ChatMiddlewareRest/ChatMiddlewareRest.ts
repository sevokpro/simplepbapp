import { Http } from "@angular/http";
import { ICrudRepositoryReadResponse } from "@itexpert-dev/i-crud-repository";
import { Observable } from "rxjs";
import { ChatConfigService } from "../../../chatConfig.service";
import { GlobalChatService } from "../../../globalChat.service";
import { IChatCrudRepositoryPatchResponse } from "../interfaces/IChatCrudRepositoryPatchResponse";
import { IChatCrudRepositoryReadRequest } from "../interfaces/IChatCrudRepositoryReadRequest";
import { IChatCrudRepositoryUpdateRequest } from "../interfaces/IChatCrudRepositoryUpdateRequest";
import { ICrudRepositoryCreateRequest } from "../interfaces/ICrudRepositoryCreateRequest";
import { ICrudRepositoryCreateResponse } from "../interfaces/ICrudRepositoryCreateResponse";
import { ICrudRepositoryDeleteRequest } from "../interfaces/ICrudRepositoryDeleteRequest";
import { ICrudRepositoryDeleteResponse } from "../interfaces/ICrudRepositoryDeleteResponse";
import { ChatMiddleWareQueryAdapter } from "./ChatMiddlewareQueryAdapter";

class ChatMiddlewareRest {
    private queryAdapter: ChatMiddleWareQueryAdapter;

    constructor(private http: Http,
        private globalChatService: GlobalChatService,
        private config: ChatConfigService) {
        const apiPath = this.config.appConfig.serverConfig.chatApi;
        this.queryAdapter = new ChatMiddleWareQueryAdapter(apiPath, globalChatService);
    }

    public read(requestParams: IChatCrudRepositoryReadRequest): Observable<ICrudRepositoryReadResponse> {
        const request = this.queryAdapter.resolveGetHttpOptions(requestParams);
        const querySubject = this.http.request(request)
            .map(response => {
                return response.json();
            });

        return querySubject;
    }

    public create(requestParams: ICrudRepositoryCreateRequest): Observable<ICrudRepositoryCreateResponse> {
        const request = this.queryAdapter.resolvePostHttpOptions(requestParams);

        const querySubject = this.http.request(request)
            .map(response => {
                const jsonResponse = response.json();
                const readResponse: ICrudRepositoryCreateResponse = {
                    status: response.status,
                    json: jsonResponse
                };
                return readResponse;
            });

        return querySubject;
    }

    public update(requestParams: IChatCrudRepositoryUpdateRequest): Observable<IChatCrudRepositoryPatchResponse> {
        const request = this.queryAdapter.resolvePatchHttpOptions(requestParams);

        const querySubject = this.http.request(request)
            .map(response => {
                const jsonResponse = response.json();
                const readResponse: IChatCrudRepositoryPatchResponse = {
                    status: response.status,
                    message: jsonResponse
                };
                return readResponse;
            });

        return querySubject;
    }

    public delete(requestParams: ICrudRepositoryDeleteRequest): Observable<ICrudRepositoryDeleteResponse> {
        const request = this.queryAdapter.resolveDeleteHttpOptions(requestParams);

        const querySubject = this.http.request(request)
            .map(response => {
                const jsonResponse = response.json();
                const readResponse: ICrudRepositoryDeleteResponse = {
                    status: response.status,
                    message: jsonResponse
                };
                return readResponse;
            });

        return querySubject;
    }
}

export { ChatMiddlewareRest };
