export interface ICrudRepositoryCreateResponse {
    status?: number;
    json?: any;
}
