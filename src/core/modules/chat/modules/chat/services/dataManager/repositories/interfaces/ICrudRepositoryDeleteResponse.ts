export interface ICrudRepositoryDeleteResponse {
    status?: number;
    message?: string;
}
