import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { ChatConfigService } from "../chatConfig.service";
import { GlobalChatService } from "../globalChat.service";
import { ChatMiddlewareRest } from "./repositories/ChatMiddlewareRest/ChatMiddlewareRest";
import { ChtpzMiddleware } from "./repositories/ChtpzMiddleware/ChtpzMiddleware";

@Injectable()
export class DataManagerService {
    public constructor(
        protected http: Http,
        protected globalChatService: GlobalChatService,
        protected config: ChatConfigService
    ) { }

    private connectors: any = {
        CHTPZ: new ChtpzMiddleware(this.http, this.globalChatService, this.config),
        ChatMiddlewareRest: new ChatMiddlewareRest(this.http, this.globalChatService, this.config)
    };

    public chtpz: ChtpzMiddleware = this.connectors.CHTPZ;
    public chatCrud: ChatMiddlewareRest = this.connectors.ChatMiddlewareRest;

    protected defaultConnector: string = Object.keys(this.connectors)[0];
}
