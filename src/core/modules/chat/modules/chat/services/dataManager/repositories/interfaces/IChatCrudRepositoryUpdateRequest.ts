export interface IChatCrudRepositoryUpdateRequest {
    entity?: 'dialog' | 'personal' | 'message' | 'contact' | 'employee';
    messagesIds?: Array<string>;
    messageId?: string;
}

