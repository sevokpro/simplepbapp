import { Http } from "@angular/http";
import { ICrudRepositoryReadResponse } from "@itexpert-dev/i-crud-repository";
import { Observable } from "rxjs";
import { ChatConfigService } from "../../../chatConfig.service";
import { GlobalChatService } from "../../../globalChat.service";
import { IChatCrudRepositoryReadRequest } from "../interfaces/IChatCrudRepositoryReadRequest";
import { ChtpzMiddlewareQueryAdapter } from "./ChtpzMiddlewareQueryAdapter";

class ChtpzMiddleware {
    private queryAdapter: ChtpzMiddlewareQueryAdapter;

    constructor(
        private http: Http,
        private globalChatService: GlobalChatService,
        private config: ChatConfigService
    ) {
        const apiPath = this.config.appConfig.serverConfig.systemApi;
        this.queryAdapter = new ChtpzMiddlewareQueryAdapter(apiPath);
    }

    public read(requestParams: IChatCrudRepositoryReadRequest): Observable<ICrudRepositoryReadResponse> {
        const request = this.queryAdapter.resolvePostHttpOptions(requestParams);
        request.headers.append('token', this.globalChatService.get().userData.TokenId);

        const querySubject = this.http.request(request)
            .map(response => {
                return response.json();
            });

        return querySubject;
    }
}

export { ChtpzMiddleware };
