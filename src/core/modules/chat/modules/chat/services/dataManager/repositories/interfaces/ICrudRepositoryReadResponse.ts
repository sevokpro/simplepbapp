export interface ICrudRepositoryReadResponse {
    value?: Array<any>;
    userIds?: Array<string>;
    participants?: Array<string>;
    entities?: Array<any>;
    totalResult?: number;
    dialogId?: string;
}
