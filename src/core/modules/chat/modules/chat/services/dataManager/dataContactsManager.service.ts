import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { ICrudRepositoryReadRequestFieldFormat } from "@itexpert-dev/i-crud-repository";
import { isNullOrUndefined } from "@itexpert-dev/tiny-helpers";
import { BehaviorSubject, Observable } from "rxjs";
import { IDataContactsManager } from "../../../../externalLibs/baseChat/types/interfaces/IDataContactsManager";
import { ChatConfigService } from "../chatConfig.service";
import { GlobalChatService } from "../globalChat.service";
import { ChtpzUser } from "../types/classes/ChtpzUser";
import { DataManagerService } from "./dataManager.service";
import { IChatCrudRepositoryReadRequest } from "./repositories/interfaces/IChatCrudRepositoryReadRequest";
import { ICrudRepositoryCreateRequest } from "./repositories/interfaces/ICrudRepositoryCreateRequest";
import { ICrudRepositoryDeleteRequest } from "./repositories/interfaces/ICrudRepositoryDeleteRequest";

@Injectable()
export class DataContactsManagerService extends DataManagerService implements IDataContactsManager {
    public termForAutoComplete: BehaviorSubject<string> = new BehaviorSubject<string>(null);
    public chtpzContactsForAutoComplete: Observable<Array<ChtpzUser>> = new Observable<Array<ChtpzUser>>();

    constructor(
        protected http: Http,
        protected globalChatService: GlobalChatService,
        protected config: ChatConfigService
    ) {
        super(http, globalChatService, config);
    }

    public getUsersByTerm(term: string): Observable<Array<ChtpzUser>> {
        const query: IChatCrudRepositoryReadRequest = {
            entity: 'employee',
            stringFilter: term
        };

        return this.getContactsWithFilter(query);
    }

    public getUsersByIds(contactsIds: Array<string>): Observable<Array<ChtpzUser>> {
        const query: IChatCrudRepositoryReadRequest = {
            entity: 'employee',
            contactsIds: contactsIds
        };

        return this.getContactsWithFilter(query);
    }

    public getUserById(contactsId: string): Observable<ChtpzUser> {
        return this.getUsersByIds([contactsId])
            .map(contacts => {
                let contact: ChtpzUser = null;
                if (!isNullOrUndefined(contacts) && contacts.length === 1) {
                    contact = contacts[0] as ChtpzUser;
                }

                return contact;
            });
    }

    private getContactsWithFilter(query: IChatCrudRepositoryReadRequest) {
        query = {
            ...query,
            entity: "employee",
            fields: ["link", "name", "organization", "position"],
            sort: "name"
        };

        let filterNode: ICrudRepositoryReadRequestFieldFormat;
        if (!isNullOrUndefined(query.stringFilter)) {
            filterNode = {
                field: "name",
                operator: "Contains",
                value: `${query.stringFilter}`
            };
        } else {
            if (!isNullOrUndefined(query.contactsIds) && query.contactsIds.length >= 0) {
                filterNode = {
                    field: "link",
                    operator: "In",
                    value: `[${query.contactsIds}]`
                };
                delete query.contactsIds;
            } else {
                filterNode = null;
            }
        }

        if (!isNullOrUndefined(filterNode)) {
            query.complexFilter = {
                operator: "AND",
                nodes: [filterNode]
            };
        }

        query.limit = !isNullOrUndefined(query.limit) ? query.limit : this.config.contacts.limit;
        query.offset = !isNullOrUndefined(query.offset) ? query.offset : 0;

        return this.chtpz.read(query)
            .map(response => {
                let result: Array<ChtpzUser>;
                result = response['entities']
                    .map((item) =>
                        new ChtpzUser({
                            name: item.name,
                            id: !isNullOrUndefined(item.link) ? item.link.value : '',
                            position: item.position
                        })
                    );

                return result;
            });
    }

    public addChatContacts(ids: Array<string>): Observable<string> {
        const query: ICrudRepositoryCreateRequest = {
            entity: 'contact',
            data: ids
        };
        return this.chatCrud.create(query)
            .map(response => {
                let result: string = response.json;
                if (response.status === 204) {
                    result = response.json;
                }

                return result;
            });
    }

    public removeChatContacts(ids: Array<string>): Observable<string> {
        const query: ICrudRepositoryDeleteRequest = {
            entity: 'contact',
            data: ids
        };
        return this.chatCrud.delete(query)
            .map(response => {
                let result: string = response.message;
                if (response.status === 204) {
                    result = response.message;
                }

                return result;
            });
    }

    public getChatContacts(): Observable<Array<string>> {
        const query: IChatCrudRepositoryReadRequest = {
            entity: 'contact',
        };
        return this.chatCrud.read(query)
            .map(response => {
                const result: Array<string> = response['userIds'];
                return result;
            });
    }
}
