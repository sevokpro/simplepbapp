import { Injectable } from "@angular/core";
import { isNullOrUndefined } from "@itexpert-dev/tiny-helpers";
import { ChtpzGlobalChat } from "../components/chtpzChatContainer/chatTabs/chtpzChatTabsContainer/chtpzChatTabsAllContacts/ChtpzGlobalChat";
import { ChtpzUser } from "./types/classes/ChtpzUser";

@Injectable()
export class GlobalChatService {
    private globalChat: ChtpzGlobalChat;
    private currentUserField: ChtpzUser;

    public constructor() {
        let globalChat;
         if ( isNullOrUndefined( window.parent.document ) && isNullOrUndefined(window.parent.document["GLOBAL_CHAT"])) {
            globalChat = this.globalChat = new ChtpzGlobalChat();
        }
        if (isNullOrUndefined(globalChat)) {
            globalChat = {};
        }

        globalChat.userData = {
            name: 'Абелян',
            TokenId: '5eadba2c54e24429b3956a0abd28ce7e',
            EmployeeGuid: 'e05a5cbc-41aa-11e5-8252-001c42c9212f'
        };


        this.currentUserField = new ChtpzUser({
            name: 'Абелян',
            iconHref: 'src/static/images/avatars/male-avatar-1.png',
            id: 'e05a5cbc-41aa-11e5-8252-001c42c9212f'
        });
    }

    public get() {
        return this.globalChat;
    }

    public get currentUser(): ChtpzUser {
        return this.currentUserField;
    }
}
