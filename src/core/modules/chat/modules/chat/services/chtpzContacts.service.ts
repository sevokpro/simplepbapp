import { Injectable } from "@angular/core";
import { isNullOrUndefined } from "@itexpert-dev/tiny-helpers";
import { Observable, Subject, Subscription } from "rxjs";
import { ChatContactsService } from "../../../externalLibs/baseChat/services/ChatContactsService";
import { ContactsService } from "../../../externalLibs/baseChat/services/ContactsService";
import { IDataContactsManager } from "../../../externalLibs/baseChat/types/interfaces/IDataContactsManager";
import { ChtpzContactsFilter } from "./types/classes/ChtpzContactsFilter";
import { ChtpzUser } from "./types/classes/ChtpzUser";
import { ContactsType } from "./types/classes/ContactsType";

@Injectable()
export class ChtpzContactsService {
    public selectedChtpzUsers: Array<ChtpzUser>;
    public selectedChatContacts: Array<ChtpzUser>;
    private chatContacts: ContactsService;

    private draftContacts: ContactsService;
    private currentList: Array<ChtpzUser>;

    public resetSelected = new Subject<ContactsType>();
    public saveDraft = new Subject();
    public stopLoading = new Subject();

    private draftContactsSubscription: Subscription;
    private orderedContactsSubscription: Subscription;
    private draftOrderedContactsSubscription: Subscription;
    private saveDraftSubscription: Subscription;
    private saveNewContactsListSubscription: Subscription;


    constructor(private dataManagerService: IDataContactsManager,
        private chatContactsService: ChatContactsService) {
        this.chatContacts = chatContactsService.contactsService;
        this.reinit();
    }


    public getContactsForAutocomplete(term: string, contactsType: ContactsType): Observable<Array<ChtpzUser>> {
        const filter = new ChtpzContactsFilter({ term: term });
        switch (contactsType) {
            case ContactsType.chtpz:
                return this.dataManagerService.getUsersByTerm(term) as any;
            case ContactsType.chat: {
                console.log('FILTER CHAT ENTRANCE');
                // return this.contacts
                return this.draftContacts.orderedContacts
                    .map((contacts) => {
                        console.log('FILTER CHTPZ', contacts);
                        contacts = contacts.filter((c: ChtpzUser) => c.inUserList && !c.isCurrent);
                        return filter.filterByTerm(contacts as Array<ChtpzUser>);
                    });
            }
        }
    }

    public setSelected(selected: Array<ChtpzUser>, contactsType: ContactsType) {
        switch (contactsType) {
            case ContactsType.chtpz:
                this.selectedChtpzUsers = selected;
                break;
            case ContactsType.chat:
                this.selectedChatContacts = selected;
                break;
        }
    }

    public addContacts() {
        this.selectedChtpzUsers.forEach(user => {
            const current = this.chatContacts.currentContact.value;
            const isCurrent = current.id === user.id;
            if (!isCurrent && !this.currentList.some(item => item.id === user.id)) {
                console.log(`addContacts user`, user);
                this.draftContacts.addContactToUserList(user);
            }
        }
        );

        this.resetSelected.next(ContactsType.chtpz);
    }

    public removeContacts() {
        this.selectedChatContacts.forEach(contact => {
            this.draftContacts.removeContact(contact);
        });

        this.resetSelected.next(ContactsType.chat);
    }

    public reinit() {
        this.unsubscribe();

        this.resetSelected = new Subject<ContactsType>();
        this.saveDraft = new Subject();
        this.stopLoading = new Subject();

        this.draftContacts = new ContactsService();
        this.draftContactsSubscription = this.draftContacts.contacts.subscribe(() => {
        });

        this.orderedContactsSubscription =
            Observable.of(null).withLatestFrom(this.chatContacts.orderedContacts)
                .subscribe(([stub, list]) => {
                    console.log('ORDEREDCONTACTS CHAT->CHTPZ', list);
                    list.forEach(item => this.draftContacts.addContact(item));
                    this.currentList = list as Array<ChtpzUser>;
                });
        this.draftOrderedContactsSubscription =
            this.draftContacts.orderedContacts
                .subscribe(list => {
                    console.log('ORDEREDCONTACTS CHTPZ', list);
                    this.currentList = list as Array<ChtpzUser>;
                });

        this.saveDraftSubscription =
            this.saveDraft.switchMap(() => {
                return this.draftContacts.contacts
                    .withLatestFrom(this.chatContacts.contacts)
                    .map(([draft, chat]) => {
                        return { draft, chat };
                    });
            }).first()
                .subscribe(latestValues => {
                    console.log('latestValues', latestValues);

                    const itemsToAdd = new Array<ChtpzUser>();
                    latestValues.draft.forEach(draftItem => {
                        if (!latestValues.chat.some(chatItem => chatItem.id === draftItem.id))
                            itemsToAdd.push(draftItem as ChtpzUser);
                    });
                    console.log(`saveDraft: itemsToAdd`, itemsToAdd);

                    const itemsToRemove = [];
                    latestValues.chat.forEach(chatItem => {
                        if (!latestValues.draft.some(draftItem => chatItem.id === draftItem.id))
                            itemsToRemove.push(chatItem);
                    });
                    console.log(`saveDraft: itemsToRemove`, itemsToRemove);

                    const saveNewContactsList = new Array<Observable<string>>();
                    if (itemsToAdd.length !== 0) {
                        const addObservable = this.dataManagerService.addChatContacts(itemsToAdd.map(item => item.id))
                            .map((message) => {
                                console.log(`addChatContacts message`, message);
                                itemsToAdd.forEach((item) => {
                                    this.chatContacts.addContactToUserList(item);
                                });
                                return message;
                            });
                        saveNewContactsList.push(addObservable);
                    }

                    if (itemsToRemove.length !== 0) {
                        const removeObservable = this.dataManagerService.removeChatContacts(itemsToRemove.map(item => item.id))
                            .map((message) => {
                                console.log(`removeContact message`, message);
                                itemsToRemove.forEach((item) => this.chatContacts.removeContact(item));
                                return message;
                            });
                        saveNewContactsList.push(removeObservable);
                    }

                    if (saveNewContactsList.length !== 0) {
                        console.log(`saveNewContactsList messages`, saveNewContactsList);
                        this.saveNewContactsListSubscription =
                            Observable.zip(...saveNewContactsList)
                                .subscribe(([addedMessage, removedMessage]) => {
                                    console.log(`saveNewContactsList messages`, addedMessage, removedMessage);
                                    this.stopLoading.next(true);
                                });
                    } else {
                        this.stopLoading.next(true);
                    }
                });
    }

    private unsubscribe() {
        if (!isNullOrUndefined(this.draftContactsSubscription))
            this.draftContactsSubscription.unsubscribe();
        if (!isNullOrUndefined(this.orderedContactsSubscription))
            this.orderedContactsSubscription.unsubscribe();
        if (!isNullOrUndefined(this.draftOrderedContactsSubscription))
            this.draftOrderedContactsSubscription.unsubscribe();
        if (!isNullOrUndefined(this.saveDraftSubscription))
            this.saveDraftSubscription.unsubscribe();
        if (!isNullOrUndefined(this.saveNewContactsListSubscription))
            this.saveNewContactsListSubscription.unsubscribe();
    }
}
