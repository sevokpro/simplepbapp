import { isNullOrUndefined } from "@itexpert-dev/tiny-helpers";
import { ThreadsFilter } from "../../../../../externalLibs/baseChat/types/classes/ThreadsFilter";
import { IContactsFilterParams } from "../../../../../externalLibs/baseChat/types/interfaces/IContactsFilterParams";
import { ChtpzThread } from "./ChtpzThread";

export class ChtpzThreadsFilter extends ThreadsFilter {
    public constructor(params: IContactsFilterParams) {
        super(params);
    }

    public filterByTerm(threads: Array<ChtpzThread>) {
        return (threads)
            .filter(thread => {
                return !isNullOrUndefined( thread ) && !isNullOrUndefined(thread.name) && thread.name.toUpperCase().includes(this.params.term.toUpperCase());
            });
    }
}

