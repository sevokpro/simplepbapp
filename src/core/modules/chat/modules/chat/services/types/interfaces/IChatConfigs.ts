export interface IChatConfigs {
    serverConfig: IServerConfig;
}

interface IServerConfig {
    chatApi: string;
    systemApi: string;
}
