import { Message } from "../../../../../externalLibs/baseChat/types/classes/Message";
import { IMessageInitParams } from "../../../../../externalLibs/baseChat/types/interfaces/IMessageInitParams";
import { ChtpzUser } from "./ChtpzUser";

export class ChtpzMessage extends Message {
    public author: ChtpzUser;
    public constructor(obj?: IMessageInitParams) {
        super(obj);
    }
}
