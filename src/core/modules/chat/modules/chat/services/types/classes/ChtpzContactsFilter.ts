import { isNullOrUndefined } from "@itexpert-dev/tiny-helpers";
import { ContactsFilter } from "../../../../../externalLibs/baseChat/types/classes/ContactsFilter";
import { IContactsFilterParams } from "../../../../../externalLibs/baseChat/types/interfaces/IContactsFilterParams";
import { ChtpzUser } from "./ChtpzUser";

export class ChtpzContactsFilter extends ContactsFilter {
    public constructor(params: IContactsFilterParams) {
        super(params);
    }

    public filterByTerm(contacts: Array<ChtpzUser>) {
        return (contacts)
            .filter(contact => {
                return !isNullOrUndefined(contact) && !isNullOrUndefined(contact.name) && contact.name.toUpperCase().includes(this.params.term.toUpperCase());
            });
    }
}

