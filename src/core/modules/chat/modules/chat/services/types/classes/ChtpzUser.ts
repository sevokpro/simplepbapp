import { isNullOrUndefined } from "@itexpert-dev/tiny-helpers";
import { User } from "../../../../../externalLibs/baseChat/types/classes/User";
import { IUserInitParams } from "../../../../../externalLibs/baseChat/types/interfaces/IUserInitParams";
export class ChtpzUser extends User {
    public position: string;
    public constructor(obj: IUserInitParams) {
        super(obj);
        this.position = (!isNullOrUndefined(obj) && isNullOrUndefined(obj.position)) ? null : obj.position;
    }
}

