import { Thread } from "../../../../../externalLibs/baseChat/types/classes/Thread";
import { IThreadInitParams } from "../../../../../externalLibs/baseChat/types/interfaces/IThreadInitParams";
import { ChtpzMessage } from "./ChtpzMessage";

export class ChtpzThread extends Thread {
    public lastMessage: ChtpzMessage;

    public constructor(obj?: IThreadInitParams) {
        super(obj);
    }
}
