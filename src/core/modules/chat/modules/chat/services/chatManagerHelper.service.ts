import { Injectable } from "@angular/core";
import { Subject } from "rxjs/Subject";

@Injectable()
export class ChatManagerHelperService {
    public openChatStream: Subject<boolean> = new Subject<boolean>();
    public unreadMessagesCountStream: Subject<number> = new Subject<number>();
}
