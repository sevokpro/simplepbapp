import { Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from "@angular/core";
import { NgbModal, NgbTabChangeEvent } from "@ng-bootstrap/ng-bootstrap";
import { Subscription } from "rxjs";
import { BaseChatContainerComponent } from "../../../../../../externalLibs/baseChat/components/chatContainer/baseChatContainerComponent.component";
import { ChtpzNgbTabset } from "../chtpzNgbTabset/chtpzNgbTabset.component";
import { ChtpzChatTabsAllContactsComponent } from "./chtpzChatTabsAllContacts/chtpzChatTabsAllContacts.component";
import { ChtpzTabs } from "./services/ChtpzTabs";

@Component({
    templateUrl: './chtpzChatTabsContainer.template.pug',
    styleUrls: [
        './chtpzChatTabsContainer.styles.styl'
    ],
    providers: [ChtpzTabs],
    encapsulation: ViewEncapsulation.None
})
export class ChtpzChatTabsContainerComponent extends BaseChatContainerComponent implements OnInit, OnDestroy {
    @ViewChild(ChtpzNgbTabset)
    public tabsetComponent: ChtpzNgbTabset;

    private subscription: Subscription;

    constructor(private modalService: NgbModal,
        private chtpzTabs: ChtpzTabs) {
        super();
    }

    public tabChange($event: NgbTabChangeEvent) {
        if ($event.nextId === 'tabs-all-contacts') {
            $event.preventDefault();
            const modalRef = this.modalService.open(ChtpzChatTabsAllContactsComponent, { keyboard: false, windowClass: 'chat-tabs-all-contacts-modal' });
        }
    }

    public ngOnInit() {
        this.subscription = this.chtpzTabs.activeTab
            .subscribe(id => this.tabsetComponent.select(id));
    }

    public ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    public closeResult: string;

    public open() {
        const modalRef = this.modalService.open(ChtpzChatTabsAllContactsComponent);
    }
}
