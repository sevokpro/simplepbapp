import { Component } from "@angular/core";
import { BaseChatTabsHeaderComponent } from "../../../../../../../externalLibs/baseChat/components/chatContainer/chatTabs/chatTabsContainer/chatTabsHeader/baseChatTabsHeader.component";

@Component({
    templateUrl: './chtpzChatTabsHeader.template.pug',
    styleUrls: [
        './chtpzChatTabsHeader.styles.styl'
    ]
})
export class ChtpzChatTabsHeaderComponent extends BaseChatTabsHeaderComponent {
    public constructor() {
        super();
    }
}
