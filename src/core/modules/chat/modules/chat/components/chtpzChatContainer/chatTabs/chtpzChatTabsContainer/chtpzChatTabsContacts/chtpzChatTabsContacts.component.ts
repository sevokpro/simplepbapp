import { Component, OnInit } from "@angular/core";
import { Observable } from "rxjs";
import { BaseChatTabsContactsComponent } from "../../../../../../../externalLibs/baseChat/components/chatContainer/chatTabs/chatTabsContainer/chatTabsContacts/baseChatTabsContacts.component";

@Component({
    templateUrl: './chtpzChatTabsContacts.template.pug',
    styleUrls: [
        './chtpzChatTabsContacts.styles.styl'
    ]
})
export class ChtpzChatTabsContactsComponent extends BaseChatTabsContactsComponent implements OnInit {
    public contacts: Observable<any>;

    constructor() {
        super();
    }

    public ngOnInit(): void {
        this.contacts = this.chatCtrl.contactsService.orderedFilteredContacts;
    }
}
