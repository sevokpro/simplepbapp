import { Component, Input, OnDestroy, OnInit } from "@angular/core";
import { Observable, Subject, Subscription } from "rxjs";
import { ChtpzContactsService } from "../../../../../../services/chtpzContacts.service";
import { ChtpzUser } from "../../../../../../services/types/classes/ChtpzUser";
import { ContactsType } from "../../../../../../services/types/classes/ContactsType";

@Component({
    selector: 'chtpz-chat-contacts-list',
    templateUrl: './chtpzChatContactsList.template.pug',
    styleUrls: [
        './chtpzChatContactsList.styles.styl'
    ]
})
export class ChtpzChatContactsList implements OnInit, OnDestroy {
    @Input()
    public title: string;
    @Input()
    public contactsType: ContactsType;
    public contacts: Subject<Array<ChtpzUser>>;
    public contactsList: Array<ChtpzUser> = [];
    public loading: boolean = true;

    private selected = new Array<ChtpzUser>();
    private searchTerms = new Subject<string>();

    private contactsSubscription: Subscription;
    private resetSelectedSubscription: Subscription;

    constructor(private chtpzContactsService: ChtpzContactsService) {
    }

    public search(term: string): void {
        console.log('SEARCH', term === undefined);
        this.searchTerms.next(term);
    }

    public ngOnInit() {
        const debounced = this.searchTerms
            .debounceTime(600)
            .do(() => {
                this.loading = true;
            })
            .share();
        this.contacts = debounced.distinctUntilChanged()
            .startWith('')
            .switchMap(term => {
                return this.chtpzContactsService
                    .getContactsForAutocomplete(term, this.contactsType);
            })
            .catch(error => {
                // TODO: add real error handling
                console.error(error);
                this.loading = false;
                return Observable.of<Array<ChtpzUser>>([]);
            }) as Subject<Array<ChtpzUser>>;

        this.contactsSubscription = this.contacts.subscribe((list) => {
            console.log(`LIST SUBSCRIBE ${this.contactsType}`, list);
            this.contactsList = list;
            this.loading = false;
        });

        this.resetSelectedSubscription = this.chtpzContactsService.resetSelected.subscribe(type => {
            if (type === this.contactsType)
                this.selected = [];
        });
    }

    public ngOnDestroy() {
        this.contactsSubscription.unsubscribe();
        this.resetSelectedSubscription.unsubscribe();
    }

    private isSelected(contact: ChtpzUser): boolean {
        return this.selected.some(item => item.id === contact.id);
    }

    private toggleSelected(contact: ChtpzUser) {
        if (this.isSelected(contact))
            this.selected = this.selected.filter(item => item.id !== contact.id);
        else
            this.selected.push(contact);

        this.chtpzContactsService.setSelected(this.selected, this.contactsType);
    }


}
