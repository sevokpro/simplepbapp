import { Injectable } from "@angular/core";
import { Subject } from "rxjs";

@Injectable()
export class ChtpzTabs {
    public activeTab = new Subject<string>();
}
