import { MenuItem } from "../../../../../../externalLibs/baseChat/types/classes/MenuItem";
import { ContactStatus } from "../../../../../../externalLibs/baseChat/types/enums/ContactStatus";
export class StatusMenuItem extends MenuItem {
    public status: ContactStatus;
}