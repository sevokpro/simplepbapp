import { Component, ElementRef, Injector, Input, OnInit, ViewChild } from "@angular/core";
import { isNullOrUndefined } from "@itexpert-dev/tiny-helpers";
import { Subscription } from "rxjs";
import { BaseChatComponent } from "../../../../../../externalLibs/baseChat/components/baseChatComponent";
import { ChatLocalScope } from "../../../../../../externalLibs/baseChat/directives/common/chatLocalScope";
import { ContextMenuService } from "../../../../../../externalLibs/baseChat/services/contextMenu.service";
import { MenuInitParams } from "../../../../../../externalLibs/baseChat/types/classes/MenuInitParams";
import { ContactStatus } from "../../../../../../externalLibs/baseChat/types/enums/ContactStatus";
import { StatusMenuItem } from "./StatusMenuItem";

abstract class BaseAvatarStatusComponent extends BaseChatComponent implements OnInit {
    public avatarClass: string;
    public menuItems: Array<StatusMenuItem> = [];

    @ViewChild('menu')
    public menuEl: ElementRef;

    @Input()
    public isCurrentUser: boolean;

    private selectEventsSubscription: Subscription;
    private statusField: ContactStatus;
    private readonly classes = {
        online: "avatar-status-online",
        away: "avatar-status-away",
        busy: "avatar-status-busy"
    };

    @Input()
    set status(value: ContactStatus) {
        this.statusField = value;
        switch (value) {
            case ContactStatus.Online:
                this.avatarClass = this.classes.online;
                break;
            case ContactStatus.Away:
                this.avatarClass = this.classes.away;
                break;
            case ContactStatus.Busy:
                this.avatarClass = this.classes.busy;
                break;
            default:
                this.avatarClass = this.classes.online;
        }

    }


    get status(): ContactStatus {
        return this.statusField;
    }

    private menuItemComponentKey: string;

    constructor(
        protected scope: ChatLocalScope,
        protected contextMenuService: ContextMenuService,
        protected injector: Injector
    ) {
        super();
        this.initMenu();
    }

    public ngOnInit() {
        console.log('BaseAvatarStatusComponent ngOnInit', this);
        this.menuItemComponentKey = this.scope.chatCtrl.chatComponents.chatContainer.tabsContainer.tabsHeader.tabsHeaderAvatar.statusMenuItem;
    }

    public open(event) {
        event.preventDefault();
        event.stopPropagation();

        if (!this.isCurrentUser)
            return;

        this.menuItems.filter(i => i.status === this.status)
            .forEach(i => i.selected = true);
        this.openContextMenu(event);
    }

    public openContextMenu(event: MouseEvent) {
        const initParams = new MenuInitParams(
            this.menuItems,
            this.menuItemComponentKey,
            event
        );

        const menu = this.contextMenuService.createMenu(initParams, this.injector);
        this.selectEventsSubscription = menu.selectEvents.subscribe((item: StatusMenuItem) => {
            this.status = item.status;
            this.contextMenuService.destroyMenu();
        });
    }


    public ngOnDestroy() {
        if (!isNullOrUndefined(this.selectEventsSubscription))
            this.selectEventsSubscription.unsubscribe();
    }

    private initMenu() {
        const online = new StatusMenuItem();
        online.className = this.classes.online;
        online.caption = "В сети";
        online.status = ContactStatus.Online;
        this.menuItems.push(online);

        const away = new StatusMenuItem();
        away.className = this.classes.away;
        away.caption = "Отошел";
        away.status = ContactStatus.Away;
        this.menuItems.push(away);

        const busy = new StatusMenuItem();
        busy.className = this.classes.busy;
        busy.caption = "Занят";
        busy.status = ContactStatus.Busy;
        this.menuItems.push(busy);
    }

}

@Component({
    selector: 'contact-status-avatar',
    templateUrl: './contactAvatarStatusComponent.template.pug'
})
class ContactAvatarStatusComponent extends BaseAvatarStatusComponent implements OnInit {

    constructor(
        protected scope: ChatLocalScope,
        protected contextMenuService: ContextMenuService,
        protected injector: Injector) {
        super(scope, contextMenuService, injector);
    }

    public ngOnInit() {
        super.ngOnInit();
    }


}


export { ContactAvatarStatusComponent, BaseAvatarStatusComponent };
