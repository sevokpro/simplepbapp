import { Component } from "@angular/core";
import { BaseChatTabsSearchboxComponent } from "../../../../../../../../externalLibs/baseChat/components/chatContainer/chatTabs/chatTabsContainer/chatTabsHeader/chatTabsSearchbox/baseChatTabsSearchbox.component";
import { ChtpzContactsFilter } from "../../../../../../services/types/classes/ChtpzContactsFilter";
import { ChtpzThreadsFilter } from "../../../../../../services/types/classes/ChtpzThreadsFilter";

@Component({
    templateUrl: './chtpzChatTabsSearchbox.template.pug',
    styleUrls: [
        './chtpzChatTabsSearchbox.styles.styl'
    ]
})
export class ChtpzChatTabsSearchboxComponent extends BaseChatTabsSearchboxComponent {
    constructor() {
        super();
    }

    public search(term: string) {
        const contactsFilter = new ChtpzContactsFilter({ term: term });
        const threadsFilter = new ChtpzThreadsFilter({ term: term });
        this.chatCtrl.contactsService.filter.next(contactsFilter);
        this.chatCtrl.threadsService.filter.next(threadsFilter);
    }
}
