import { Component } from "@angular/core";
import { BaseChatTabsThreadComponent } from "../../../../../../../../externalLibs/baseChat/components/chatContainer/chatTabs/chatTabsContainer/chatTabsThreads/chatTabsThread/baseChatTabsThread.component";

@Component({
    templateUrl: './chtpzChatTabsThread.template.pug',
    styleUrls: [
        './chtpzChatTabsThread.styles.styl'
    ]
})
export class ChtpzChatTabsThreadComponent extends BaseChatTabsThreadComponent {
    public constructor() {
        super();
    }
}
