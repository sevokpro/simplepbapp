import { Component, Input, OnDestroy, OnInit } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { Subscription } from "rxjs";
import { BaseChatTabsAllContactsComponent } from "../../../../../../../externalLibs/baseChat/components/chatContainer/chatTabs/chatTabsContainer/chatTabsAllContacts/baseChatTabsAllContacts.component";
import { ChtpzContactsService } from "../../../../../services/chtpzContacts.service";
import { GlobalChatService } from "../../../../../services/globalChat.service";
import { ContactsType } from "../../../../../services/types/classes/ContactsType";
import { ChtpzGlobalChat } from "./ChtpzGlobalChat";

@Component({
    selector: 'ngbd-chtpz-all-contacts-modal-content',
    templateUrl: './chtpzChatTabsAllContacts.template.pug',
    styleUrls: [
        './chtpzChatTabsAllContacts.styles.styl'
    ]
})
export class ChtpzChatTabsAllContactsComponent extends BaseChatTabsAllContactsComponent implements OnInit, OnDestroy {
    @Input()
    public name;

    private globalChat: ChtpzGlobalChat;

    public contactsType = ContactsType;
    private stopLoadingSubscription: Subscription;
    public loading: boolean = false;

    constructor(public activeModal: NgbActiveModal,
        private chtpzContactsService: ChtpzContactsService,
        private globalChatService: GlobalChatService) {
        super();

        this.globalChat = globalChatService.get();
        console.log('globalChat', this.globalChat);
    }

    public ngOnInit() {
        this.globalChat.setFullSize();
        this.stopLoadingSubscription = this.chtpzContactsService.stopLoading
            .subscribe((toClose) => {
                this.loading = false;
                if (toClose === true) {
                    this.close();
                }
            });
    }

    public ngOnDestroy() {
        this.stopLoadingSubscription.unsubscribe();
    }

    public close() {
        this.activeModal.close('Close click');
        this.onModalClose();
    }

    public dismiss() {
        this.activeModal.dismiss('Cross click');
        this.onModalClose();
    }

    private onModalClose() {
        this.globalChat.unsetFullSize();
        this.chtpzContactsService.reinit();
    }

    public removeContacts() {
        this.chtpzContactsService.removeContacts();
    }

    public addContacts() {
        this.chtpzContactsService.addContacts();
    }

    public save() {
        this.loading = true;
        this.chtpzContactsService.saveDraft.next();
    }
}


