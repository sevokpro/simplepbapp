import { Component, OnInit } from "@angular/core";
import { BaseChatTabsHeaderAvatarComponent } from "../../../../../../../../externalLibs/baseChat/components/chatContainer/chatTabs/chatTabsContainer/chatTabsHeader/chatTabsHeaderAvatar/baseChatTabsHeaderAvatar.component";
import { ChtpzUser } from "../../../../../../services/types/classes/ChtpzUser";

@Component({
    templateUrl: './chtpzChatTabsHeaderAvatar.template.pug',
    styleUrls: [
        './chtpzChatTabsHeaderAvatar.styles.styl'
    ]
})
export class ChtpzChatTabsHeaderAvatarComponent extends BaseChatTabsHeaderAvatarComponent implements OnInit {

    public currentUser: ChtpzUser;

    constructor() {
        super();
    }


    public ngOnInit(): void {
        super.ngOnInit();
    }
}
