import { Component, Injector, OnInit, ViewChild, ViewContainerRef, ViewEncapsulation } from "@angular/core";
import { IRootAppContainer } from "coreContracts/rootAppContainer";
import { BaseChatContainerComponent } from "../../../../externalLibs/baseChat/components/chatContainer/baseChatContainerComponent.component";

@Component({
    templateUrl: './chtpzChatContainer.template.pug',
    styleUrls: [
        './chtpzChatContainer.styles.styl'
    ],
    encapsulation: ViewEncapsulation.None
})
export class ChtpzChatContainerComponent extends BaseChatContainerComponent implements OnInit {
    @ViewChild('containerChild', { read: ViewContainerRef })
    public viewContainer;

    constructor(private rootAppContainer: IRootAppContainer,
        private injector: Injector) {
        super();
    }

    public ngOnInit() {
        this.rootAppContainer.injector = this.injector;
        this.rootAppContainer.viewContainerRef = this.viewContainer;
    }
}
