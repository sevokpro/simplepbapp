import { Component } from "@angular/core";
import { BaseChatThreadContainerComponent } from "../../../../../../externalLibs/baseChat/components/chatContainer/chatThread/chatThreadContainer/baseChatThreadContainer.component";

@Component({
    templateUrl: './chtpzChatThreadContainer.template.pug',
    styleUrls: [
        './chtpzChatThreadContainer.styles.styl'
    ]
})
export class ChtpzChatThreadContainerComponent extends BaseChatThreadContainerComponent {
    public constructor() {
        super();
    }
}
