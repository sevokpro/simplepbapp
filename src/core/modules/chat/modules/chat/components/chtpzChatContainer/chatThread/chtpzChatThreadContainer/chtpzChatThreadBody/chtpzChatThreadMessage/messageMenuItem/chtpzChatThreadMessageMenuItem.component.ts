import { Component, OnInit } from "@angular/core";
import { BaseContextMenuItemComponent } from "../../../../../../../../../externalLibs/baseChat/components/common/contextMenu/contextMenuItem/BaseContextMenuItemComponent.component";

@Component({
    templateUrl: './chtpzChatThreadMessageMenuItem.template.pug',
    styleUrls: [
        './chtpzChatThreadMessageMenuItem.styles.styl'
    ]
})
export class ChtpzChatThreadMessageMenuItemComponent extends BaseContextMenuItemComponent implements OnInit {
    constructor() {
        super();
    }

    public ngOnInit(): void {
        super.ngOnInit();
    }
    public menuItem: any;
}
