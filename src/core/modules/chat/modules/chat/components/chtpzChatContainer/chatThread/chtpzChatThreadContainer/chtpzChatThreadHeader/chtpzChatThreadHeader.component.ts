import { Component, OnInit } from "@angular/core";
import { Observable, Subscription } from "rxjs";
import { BaseChatThreadHeaderComponent } from "../../../../../../../externalLibs/baseChat/components/chatContainer/chatThread/chatThreadContainer/chatThreadHeader/baseChatThreadHeader.component";
import { GlobalChatService } from "../../../../../services/globalChat.service";
import { ChtpzThread } from "../../../../../services/types/classes/ChtpzThread";

@Component({
    templateUrl: './chtpzChatThreadHeader.template.pug',
    styleUrls: [
        './chtpzChatThreadHeader.styles.styl'
    ]
})
export class ChtpzChatThreadHeaderComponent extends BaseChatThreadHeaderComponent implements OnInit {
    public unreadMessagesCount: Observable<number>;
    public currentThread: ChtpzThread;
    private subscription: Subscription;

    constructor(private globalChatService: GlobalChatService) {
        super();
    }

    public ngOnInit(): void {
        this.unreadMessagesCount = this.chatCtrl.threadsService.unreadMessagesCount
            .do((count) => {
                //this.globalChatService.get().setUnreadCount(count);
            });

        this.subscription = this.chatCtrl.threadsService.currentThread
            .subscribe(
            (currentThread: ChtpzThread) => {
                this.currentThread = currentThread;
            });
    }

    public ngOnDestroy() {
        this.subscription.unsubscribe();
    }
}
