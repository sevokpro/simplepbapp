import { Component } from "@angular/core";
import { NgbTabset, NgbTabsetConfig } from "@ng-bootstrap/ng-bootstrap";

@Component({
    selector: 'chtpz-ngb-tabset',
    templateUrl: './chtpzNgbTabset.template.pug',
    styleUrls: [
        './chtpzNgbTabset.styles.styl'
    ]
})
export class ChtpzNgbTabset extends NgbTabset {
    constructor(config: NgbTabsetConfig) {
        super(config);
    }
}
