import { Component, OnInit } from "@angular/core";
import { Observable } from "rxjs";
import { BaseChatTabsThreadsComponent } from "../../../../../../../externalLibs/baseChat/components/chatContainer/chatTabs/chatTabsContainer/chatTabsThreads/baseChatTabsThreads.component";

@Component({
    templateUrl: './chtpzChatTabsThreads.template.pug',
    styleUrls: [
        './chtpzChatTabsThreads.styles.styl'
    ]
})
export class ChtpzChatTabsThreadsComponent extends BaseChatTabsThreadsComponent implements OnInit {
    public threads: Observable<any>;

    constructor() {
        super();
    }

    public ngOnInit(): void {
        this.threads = this.chatCtrl.threadsService.orderedFilteredThreads;
    }
}
