//описывает глобальный объект окна чата в хост-приложении ЧТПЗ
export class ChtpzGlobalChat {
    public controller: any = {}; // контроллер директивы окна чата
    public userData: {
        TokenId?: string,
        EmployeeGuid?: string,
        name?: string
    } = {};
    public setFullSize: () => void = () => { };
    public unsetFullSize: () => void = () => { };
    public setUnreadCount: (val) => void = () => { };
}
