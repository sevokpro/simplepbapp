import { Component, Input, OnInit } from "@angular/core";
import { BaseChatTabsContactComponent } from "../../../../../../../../externalLibs/baseChat/components/chatContainer/chatTabs/chatTabsContainer/chatTabsContacts/chatTabsContact/baseChatTabsContact.component";
import { ChtpzUser } from "../../../../../../services/types/classes/ChtpzUser";
import { ChtpzTabs } from "../../services/ChtpzTabs";

@Component({
    templateUrl: './chtpzChatTabsContact.template.pug',
    styleUrls: [
        './chtpzChatTabsContact.styles.styl'
    ]
})
export class ChtpzChatTabsContactComponent extends BaseChatTabsContactComponent implements OnInit {
    @Input()
    public contact: ChtpzUser;

    public selected: boolean = false;

    public constructor(private chtpzTabs: ChtpzTabs) {
        super();
    }

    public ngOnInit(): void { }

    public clicked(contact: ChtpzUser, event: any): void {
        super.clicked(contact, event);
        this.chtpzTabs.activeTab.next("tabs-threads");
    }
}
