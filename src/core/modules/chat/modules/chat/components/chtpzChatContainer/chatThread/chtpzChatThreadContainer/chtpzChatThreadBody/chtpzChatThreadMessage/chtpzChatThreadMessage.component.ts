import { Component, ComponentFactoryResolver, Injector, OnInit, ViewContainerRef } from "@angular/core";
import { BaseChatThreadMessageComponent } from "../../../../../../../../externalLibs/baseChat/components/chatContainer/chatThread/chatThreadContainer/chatThreadBody/chatThreadMessage/baseChatThreadMessage.component";
import { ChatLocalScope } from "../../../../../../../../externalLibs/baseChat/directives/common/chatLocalScope";
import { ContextMenuService } from "../../../../../../../../externalLibs/baseChat/services/contextMenu.service";
import { MenuItem } from "../../../../../../../../externalLibs/baseChat/types/classes/MenuItem";

@Component({
    templateUrl: './chtpzChatThreadMessage.template.pug',
    styleUrls: [
        './chtpzChatThreadMessage.styles.styl'
    ]
})
export class ChtpzChatThreadMessageComponent extends BaseChatThreadMessageComponent implements OnInit {

    constructor(protected scope: ChatLocalScope,
        protected componentResolver: ComponentFactoryResolver,
        protected viewContainer: ViewContainerRef,
        protected contextMenuService: ContextMenuService,
        protected injector: Injector) {
        super(
            scope,
            componentResolver,
            viewContainer,
            contextMenuService,
            injector
        );
        this.initMenu();
    }

    protected initMenu() {
        const online = new MenuItem();
        online.caption = "Редактировать";
        this.menuItems.push(online);
    }

    public ngOnInit(): void {
        super.ngOnInit();
        this.menuItemComponentKey = this.scope.chatCtrl.chatComponents.chatContainer.threadContainer.threadBody.threadMessage.menuItem;
    }

    public img: any;
    public responsive: any;
}
