import { Component, Input, OnInit } from "@angular/core";
import { isNullOrUndefined } from "@itexpert-dev/tiny-helpers";
import { BaseChatComponent } from "../../../../../../externalLibs/baseChat/components/baseChatComponent";
import { ContactStatus } from "../../../../../../externalLibs/baseChat/types/enums/ContactStatus";
import { IChatPresentation } from "../../../../../../externalLibs/baseChat/types/interfaces/IChatPresentation";

abstract class BaseNamingAvatarComponent extends BaseChatComponent implements OnInit {
    @Input()
    public presentation: IChatPresentation;

    @Input()
    public class: Array<string>;

    private acronym: string;

    constructor() {
        super();
    }
    public ContactStatus = ContactStatus;

    public ngOnInit() {
        if (isNullOrUndefined(this.presentation))
            throw new Error('ЧАТ: ContactAvatarComponent не получает параметр "presentation"');
        if (!isNullOrUndefined(this.presentation.name)) {
            const nameElements = this.presentation.name.split(' ');
            this.acronym = nameElements
                .filter((value) => value !== '')
                .filter((value, idx) => !isNullOrUndefined(value) && idx < 2)
                .map((value) => {
                    return value.toUpperCase()[0];
                })
                .join('');
        }
        else {
            this.acronym = '?';
        }
    }
}

@Component({
    selector: 'contact-avatar',
    templateUrl: './contactAvatarComponent.template.pug'
})
class ContactAvatarComponent extends BaseNamingAvatarComponent implements OnInit {
    constructor() {
        super();
    }

    public ngOnInit() {
        super.ngOnInit();
    }
}

export { ContactAvatarComponent, BaseNamingAvatarComponent };
