import { Component } from "@angular/core";
import { BaseChatThreadFooterComponent } from "../../../../../../../externalLibs/baseChat/components/chatContainer/chatThread/chatThreadContainer/chatThreadFooter/baseChatThreadFooter.component";

@Component({
    templateUrl: './chtpzChatThreadFooter.template.pug',
    styleUrls: [
        './chtpzChatThreadFooter.styles.styl'
    ]
})
export class ChtpzChatThreadFooterComponent extends BaseChatThreadFooterComponent {
    constructor() {
        super();
    }
}
