import { Component, Input, OnInit } from "@angular/core";
import { BaseContextMenuItemComponent } from "../../../../../../../../../externalLibs/baseChat/components/common/contextMenu/contextMenuItem/BaseContextMenuItemComponent.component";
import { StatusMenuItem } from "../../../../../common/contactAvatarStatusComponent/StatusMenuItem";

@Component({
    templateUrl: './chtpzChatTabsHeaderAvatarMenuItem.template.pug',
    styleUrls: [
        './chtpzChatTabsHeaderAvatarMenuItem.styles.styl'
    ]
})
export class ChtpzChatTabsHeaderAvatarMenuItemComponent extends BaseContextMenuItemComponent implements OnInit {
    @Input()
    public menuItem: StatusMenuItem;

    public constructor() {
        super();
    }


    public ngOnInit(): void {
        super.ngOnInit();
    }
}
