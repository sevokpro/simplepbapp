import { Component, OnInit } from "@angular/core";
import { isNullOrUndefined } from "@itexpert-dev/tiny-helpers";
import { Subscription } from "rxjs";
import { BaseChatThreadInputComponent } from "../../../../../../../../externalLibs/baseChat/components/chatContainer/chatThread/chatThreadContainer/chatThreadFooter/chatThreadInput/baseChatThreadInput.component";
import { ChtpzMessage } from "../../../../../../services/types/classes/ChtpzMessage";
import { ChtpzThread } from "../../../../../../services/types/classes/ChtpzThread";
import { ChtpzUser } from "../../../../../../services/types/classes/ChtpzUser";

@Component({
    templateUrl: './chtpzChatThreadInput.template.pug',
    styleUrls: [
        './chtpzChatThreadInput.styles.styl'
    ]
})
export class ChtpzChatThreadInputComponent extends BaseChatThreadInputComponent implements OnInit {
    public currentThread: ChtpzThread;
    public draftMessage: ChtpzMessage;
    public currentUser: ChtpzUser;
    private currentThreadSubscription: Subscription;
    private currentUserSubscription: Subscription;

    constructor() {
        super();
    }

    public ngOnInit(): void {
        this.draftMessage = new ChtpzMessage();

        this.currentThreadSubscription = this.chatCtrl.threadsService.currentThread.subscribe(
            (thread: ChtpzThread) => {
                this.currentThread = thread;
            });

        this.currentUserSubscription = this.chatCtrl.userService.currentUser
            .subscribe(
            (user: ChtpzUser) => {
                this.currentUser = user;
            });
    }

    public ngOnDestroy() {
        this.currentUserSubscription.unsubscribe();
        this.currentThreadSubscription.unsubscribe();
    }

    public onEnter(event: any): void {
        this.sendMessage();
        event.preventDefault();
    }

    public sendMessage(): void {
        const m: ChtpzMessage = this.draftMessage;
        m.author = this.currentUser;
        m.thread = this.currentThread;
        m.isRead = m.isDraft = true;
        if (!isNullOrUndefined(m.text.trim()))
            this.currentThread.messagesService.createMessage(m);
        this.draftMessage = new ChtpzMessage();
    }
}
