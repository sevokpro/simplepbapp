import { Component, ElementRef, OnDestroy, OnInit } from "@angular/core";
import { Observable, Subscription } from "rxjs";
import { BaseChatThreadBodyComponent } from "../../../../../../../externalLibs/baseChat/components/chatContainer/chatThread/chatThreadContainer/chatThreadBody/baseChatThreadBody.component";
import { ChtpzMessage } from "../../../../../services/types/classes/ChtpzMessage";
import { ChtpzThread } from "../../../../../services/types/classes/ChtpzThread";
import { ChtpzUser } from "../../../../../services/types/classes/ChtpzUser";

@Component({
    templateUrl: './chtpzChatThreadBody.template.pug',
    styleUrls: [
        './chtpzChatThreadBody.styles.styl'
    ]
})
export class ChtpzChatThreadBodyComponent extends BaseChatThreadBodyComponent implements OnInit, OnDestroy {
    public messages: Observable<any>;
    public currentThread: ChtpzThread;
    public draftMessage: ChtpzMessage;
    public currentUser: ChtpzUser;
    private currentThreadSubscription: Subscription;
    private currentUserSubscription: Subscription;

    constructor(public el: ElementRef) {
        super();
    }


    public ngOnInit(): void {
        this.messages = this.chatCtrl.threadsService.currentThreadMessages
            .do(
            (messages: Array<ChtpzMessage>) => {
                setTimeout(() => {
                    this.scrollToBottom();
                });
            });

        this.draftMessage = new ChtpzMessage();

        this.currentThreadSubscription = this.chatCtrl.threadsService.currentThread.subscribe(
            (thread: ChtpzThread) => {
                this.currentThread = thread;
            });

        this.currentUserSubscription = this.chatCtrl.userService.currentUser
            .subscribe(
            (user: ChtpzUser) => {
                this.currentUser = user;
            });
    }

    public ngOnDestroy() {
        this.currentThreadSubscription.unsubscribe();
        this.currentUserSubscription.unsubscribe();
    }

    public scrollToBottom(): void {
        const scrollPane: any = this.el
            .nativeElement.querySelector('.msg-container-base');
        scrollPane.scrollTop = scrollPane.scrollHeight;
    }
}
