import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { BaseChatModule } from "../../externalLibs/baseChat/baseChat.module";
import { ChatViewComponent } from "../../externalLibs/baseChat/components/chatView/chatView.component";
import { ContextMenuService } from "../../externalLibs/baseChat/services/contextMenu.service";
import { ChtpzChatContactsList } from "./components/chtpzChatContainer/chatTabs/chtpzChatTabsContainer/chtpzChatTabsAllContacts/chtpzChatContactsList/chtpzChatContactsList.component";
import { ChtpzChatTabsAllContactsComponent } from "./components/chtpzChatContainer/chatTabs/chtpzChatTabsContainer/chtpzChatTabsAllContacts/chtpzChatTabsAllContacts.component";
import { ChtpzChatTabsContactComponent } from "./components/chtpzChatContainer/chatTabs/chtpzChatTabsContainer/chtpzChatTabsContacts/chtpzChatTabsContact/chtpzChatTabsContact.component";
import { ChtpzChatTabsContactsComponent } from "./components/chtpzChatContainer/chatTabs/chtpzChatTabsContainer/chtpzChatTabsContacts/chtpzChatTabsContacts.component";
import { ChtpzChatTabsContainerComponent } from "./components/chtpzChatContainer/chatTabs/chtpzChatTabsContainer/chtpzChatTabsContainer.component";
import { ChtpzChatTabsHeaderComponent } from "./components/chtpzChatContainer/chatTabs/chtpzChatTabsContainer/chtpzChatTabsHeader/chtpzChatTabsHeader.component";
import { ChtpzChatTabsHeaderAvatarComponent } from "./components/chtpzChatContainer/chatTabs/chtpzChatTabsContainer/chtpzChatTabsHeader/chtpzChatTabsHeaderAvatar/chtpzChatTabsHeaderAvatar.component";
import { ChtpzChatTabsHeaderAvatarMenuItemComponent } from "./components/chtpzChatContainer/chatTabs/chtpzChatTabsContainer/chtpzChatTabsHeader/chtpzChatTabsHeaderAvatar/statusMenuItem/chtpzChatTabsHeaderAvatarMenuItem.component";
import { ChtpzChatTabsSearchboxComponent } from "./components/chtpzChatContainer/chatTabs/chtpzChatTabsContainer/chtpzChatTabsHeader/chtpzChatTabsSearchbox/chtpzChatTabsSearchbox.component";
import { ChtpzChatTabsThreadComponent } from "./components/chtpzChatContainer/chatTabs/chtpzChatTabsContainer/chtpzChatTabsThreads/chtpzChatTabsThread/chtpzChatTabsThread.component";
import { ChtpzChatTabsThreadsComponent } from "./components/chtpzChatContainer/chatTabs/chtpzChatTabsContainer/chtpzChatTabsThreads/chtpzChatTabsThreads.component";
import { ChtpzNgbTabset } from "./components/chtpzChatContainer/chatTabs/chtpzNgbTabset/chtpzNgbTabset.component";
import { ChtpzChatThreadBodyComponent } from "./components/chtpzChatContainer/chatThread/chtpzChatThreadContainer/chtpzChatThreadBody/chtpzChatThreadBody.component";
import { ChtpzChatThreadMessageComponent } from "./components/chtpzChatContainer/chatThread/chtpzChatThreadContainer/chtpzChatThreadBody/chtpzChatThreadMessage/chtpzChatThreadMessage.component";
import { ChtpzChatThreadMessageMenuItemComponent } from "./components/chtpzChatContainer/chatThread/chtpzChatThreadContainer/chtpzChatThreadBody/chtpzChatThreadMessage/messageMenuItem/chtpzChatThreadMessageMenuItem.component";
import { ChtpzChatThreadContainerComponent } from "./components/chtpzChatContainer/chatThread/chtpzChatThreadContainer/chtpzChatThreadContainer.component";
import { ChtpzChatThreadFooterComponent } from "./components/chtpzChatContainer/chatThread/chtpzChatThreadContainer/chtpzChatThreadFooter/chtpzChatThreadFooter.component";
import { ChtpzChatThreadInputComponent } from "./components/chtpzChatContainer/chatThread/chtpzChatThreadContainer/chtpzChatThreadFooter/chtpzChatThreadInput/chtpzChatThreadInput.component";
import { ChtpzChatThreadHeaderComponent } from "./components/chtpzChatContainer/chatThread/chtpzChatThreadContainer/chtpzChatThreadHeader/chtpzChatThreadHeader.component";
import { ChtpzChatContainerComponent } from "./components/chtpzChatContainer/chtpzChatContainer.component";
import { ContactAvatarComponent } from "./components/chtpzChatContainer/common/contactAvatarComponent/ContactAvatarComponent.component";
import { ContactAvatarStatusComponent } from "./components/chtpzChatContainer/common/contactAvatarStatusComponent/ContactAvatarStatusComponent.component";
import { CanActivateConfigGuard } from "./services/CanActivateConfigGuard.service";
import { ChatService } from "./services/chat.service";
import { ChatConfigService } from "./services/chatConfig.service";
import { ChatManagerHelperService } from "./services/chatManagerHelper.service";
import { ChtpzContactsService } from "./services/chtpzContacts.service";
import { DataManagerModule } from "./services/dataManager/dataManager.module";
import { GlobalChatService } from "./services/globalChat.service";

@NgModule({
    imports: [
        NgbModule,
        FormsModule,
        BrowserModule,
        BaseChatModule,
        DataManagerModule
    ],
    declarations: [
        ContactAvatarComponent,
        ContactAvatarStatusComponent,

        ChtpzChatTabsContainerComponent,
        ChtpzChatThreadContainerComponent,
        ChtpzChatContainerComponent,
        ChtpzChatTabsHeaderComponent,
        ChtpzChatTabsHeaderAvatarComponent,
        ChtpzChatTabsHeaderAvatarMenuItemComponent,
        ChtpzChatTabsSearchboxComponent,
        ChtpzChatTabsContactsComponent,
        ChtpzChatTabsContactComponent,
        ChtpzChatTabsThreadsComponent,
        ChtpzChatTabsThreadComponent,
        ChtpzChatTabsAllContactsComponent,
        ChtpzChatContactsList,
        ChtpzChatThreadHeaderComponent,
        ChtpzChatThreadBodyComponent,
        ChtpzChatThreadMessageComponent,
        ChtpzChatThreadMessageMenuItemComponent,
        ChtpzChatThreadFooterComponent,
        ChtpzChatThreadInputComponent,

        ChtpzNgbTabset
    ],
    exports: [
        ChatViewComponent,


    ],
    providers: [
        GlobalChatService,
        ChatService,
        ContextMenuService,
        ChtpzContactsService,
        ChatConfigService,
        CanActivateConfigGuard,
        ChatManagerHelperService],
    entryComponents: [
        ChtpzChatTabsContainerComponent,
        ChtpzChatThreadContainerComponent,
        ChtpzChatContainerComponent,
        ChtpzChatTabsHeaderComponent,
        ChtpzChatTabsHeaderAvatarComponent,
        ChtpzChatTabsHeaderAvatarMenuItemComponent,
        ChtpzChatTabsSearchboxComponent,
        ChtpzChatTabsContactsComponent,
        ChtpzChatTabsContactComponent,
        ChtpzChatTabsThreadsComponent,
        ChtpzChatTabsThreadComponent,
        ChtpzChatTabsAllContactsComponent,
        ChtpzChatThreadHeaderComponent,
        ChtpzChatThreadBodyComponent,
        ChtpzChatThreadMessageComponent,
        ChtpzChatThreadMessageMenuItemComponent,
        ChtpzChatThreadFooterComponent,
        ChtpzChatThreadInputComponent
    ]
})
class ChatModule {
}

export { ChatModule };
