import { Injectable } from "@angular/core";
import { IWindow, IWindowUnits, IWindowUnitsFactory } from "coreContracts/window";
import { Observable, ReplaySubject } from "rxjs";
import { Subject } from "rxjs/Subject";

interface IWidthAndHeight {
    width: IWindowUnits;
    height: IWindowUnits;
}

@Injectable()
class Window extends IWindow {

    private whenUpdateWindowProperties: Observable<IWidthAndHeight>;
    private whenNavBarHeightChange: Subject<IWindowUnits> = new ReplaySubject(1);

    private getWindowProperties(): IWidthAndHeight {
        return {
            width: this.windowUnitFactory.createFromPixel(window.innerWidth),
            height: this.windowUnitFactory.createFromPixel(window.innerHeight)
        };
    }

    public constructor(
        private windowUnitFactory: IWindowUnitsFactory
    ) {
        super();
        const subject = new ReplaySubject<IWidthAndHeight>(1);
        subject.next(this.getWindowProperties());
        window.onresize = function() {
            subject.next(this.getWindowProperties());
        }.bind(this);
        this.whenUpdateWindowProperties = subject.asObservable();
    }

    public getWindowHeight(): Observable<IWindowUnits> {
        return this
            .whenUpdateWindowProperties
            .pluck('height');
    }
    public getWindowWidth(): Observable<IWindowUnits> {
        return this
            .whenUpdateWindowProperties
            .pluck('width');
    }

    public getTopNavbarHeight(): Observable<IWindowUnits> {
        return this
            .whenNavBarHeightChange;
    }

    public publishTopNavbarHeight(newHeight: IWindowUnits) {
        this
            .whenNavBarHeightChange
            .next(newHeight);
    }
}

export { Window };