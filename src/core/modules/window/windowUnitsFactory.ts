import { Injectable } from "@angular/core";
import { IWindowUnits, IWindowUnitsFactory } from "coreContracts/window";
import { WindowUnits } from "./windowUnits";

@Injectable()
class WindowUnitsFactory extends IWindowUnitsFactory{
    public createFromPixel(count: number): IWindowUnits {
        return new WindowUnits(count);
    }
}

export {WindowUnitsFactory};