import { IWindowUnits } from "coreContracts/window";

class WindowUnits implements IWindowUnits{
    private countInPixels: number;
    constructor(count: number) {
        this.countInPixels = count;
    }

    public getNumber(): number {
        return this.countInPixels;
    }

    public getPixel(): string {
        return `${this.countInPixels}px`;
    }
}

export {WindowUnits};