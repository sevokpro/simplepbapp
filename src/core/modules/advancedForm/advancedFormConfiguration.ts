import { Component } from "@angular/core";
import { IAdvancedFormConfigurator } from "coreContracts/advancedForm/IAdvancedFormConfigurator";

class AdvancedFormConfigurator extends IAdvancedFormConfigurator {
    constructor() {
        super();
    }
    public widgetsStorage = new Map();
    public advancedFormViewComponent: Component = null;
    public typeToWidgetNameMap = new Map();
}
export { AdvancedFormConfigurator };