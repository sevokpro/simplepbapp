import { isNull, isNullOrUndefined } from "@itexpert-dev/tiny-helpers";
import { IAdvancedFormMetaField } from "coreContracts/advancedForm/IAdvancedFormMetaField";
import {
    IAdvancedFormDataProvider,
    IAdvancedFormDataProviderFactory,
    IAdvancedFormDataProviderFromDataSourceParams,
    IAdvancedFormDataProviderResult
} from "coreContracts/advancedForm/IAdvencedFormDataProviderFactory";
import { IAutoCompleteWidgetValue } from "coreContracts/advancedForm/widgets/IAutocompleteWidget";
import { IBooleanWidgetValue } from "coreContracts/advancedForm/widgets/IBooleanWidget";
import { IDateTimeWidgetValue } from "coreContracts/advancedForm/widgets/IDateWidget";
import { IStringWidgetValue } from "coreContracts/advancedForm/widgets/IStringWidget";
import { Observable } from "rxjs/Observable";
import { IDataSourceReadRequestFilterComparsion } from "src/common/IDataSource/read/request/IDataSourceReadRequestFilterBaseField";
import { IDataSource } from "../../../common/IDataSource/IDataSource";
import { IDataSourceReadRequestFilterNode } from "../../../common/IDataSource/read/request/IDataSourceReadRequestFilterBlock";

class AdvancedFormDataProviderFromDataSource extends IAdvancedFormDataProvider {
    private dataSource: IDataSource;
    private entityName: string;
    private entityUuid: string;

    public constructor(params: IAdvancedFormDataProviderFromDataSourceParams) {
        super();

        this.dataSource = params.source;
        this.entityName = params.entityName;
        if (isNullOrUndefined(params.entityUuid)) {
            this.entityUuid = null;
        } else {
            this.entityUuid = params.entityUuid;
        }
    }

    private getMetadataObservable() {
        return this.dataSource.getMetadata({ entityName: this.entityName })
            .map(next => {
                const result: Map<string, IAdvancedFormMetaField> = new Map();

                for (const [key, { reference, type }] of Array.from(next.properties.entries())) {
                    result.set(key, {
                        type: type,
                        label: key,
                        additionalProperties: {
                            reference: reference
                        },
                        available: true,
                        required: false,
                        visible: true
                    });
                }

                return result;
            });
    }

    private getDataObservable() {
        let getDataObservable: Observable<Map<string, any>>;

        if (isNull(this.entityUuid)) {
            getDataObservable = Observable.of(new Map());
        } else {
            const linkField: IDataSourceReadRequestFilterComparsion = {
                operator: "equal",
                value: this.entityUuid,
                field: 'ссылка'
            };
            const filter: IDataSourceReadRequestFilterNode = {
                operator: "and",
                nodes: [linkField]
            };

            getDataObservable = this.dataSource
                .read({ entity: this.entityName, filter: filter })
                .map(next => next.entities)
                .map(next => next[0])
                .map(next => next.entity);
        }

        return getDataObservable;
    }

    public getInitData(): Observable<IAdvancedFormDataProviderResult> {
        return Observable.zip(
            this.getMetadataObservable(),
            this.getDataObservable()
        ).map(([metadata, data]) => {
            const result: IAdvancedFormDataProviderResult = {
                metadata: metadata,
                data: data
            };

            for (const key of Array.from(data.keys())) {
                if (metadata.has(key)) {
                    switch (metadata.get(key).type) {
                        case 'String': {
                            let result: IStringWidgetValue;
                            result = data.get(key)['value'];
                            data.set(key, result);
                            break;
                        }
                        case 'Guid': {
                            let result: IAutoCompleteWidgetValue;
                            const fieldData = data.get(key);
                            const fieldMetadata = metadata.get(key);
                            result = {
                                value: fieldData['value'],
                                referenceName: fieldData['referenceName'],
                                reference: fieldMetadata['additionalProperties']['reference']
                            };
                            data.set(key, result);
                            break;
                        }
                        case 'DateTime': {
                            let result: IDateTimeWidgetValue;
                            result = data.get(key)['value'];
                            data.set(key, result);
                            break;
                        }
                        case 'Boolean': {
                            let result: IBooleanWidgetValue;
                            const fieldData = data.get(key);
                            const errors: Array<string> = [];
                            if (!fieldData.hasOwnProperty('value')) {
                                errors.push(`expected that current data source return value property`);
                            }
                            if (typeof fieldData.value !== "boolean") {
                                errors.push(`expected that boolean field ${key} contain boolean value`);
                            }

                            if (errors.length !== 0) {
                                for (const err of errors) {
                                    console.error(err);
                                }
                                data.set(key, undefined);
                            } else {
                                result = fieldData.value;
                                data.set(key, result);
                            }

                            break;
                        }
                        default:
                            console.warn(`can not parse type ${metadata.get(key).type}`);
                    }
                }
            }

            return result;
        });
    }
}

class AdvancedFormDataProviderFactory extends IAdvancedFormDataProviderFactory {
    public createDataProviderFromDataSource(params: IAdvancedFormDataProviderFromDataSourceParams): Observable<IAdvancedFormDataProvider> {
        return Observable.of(new AdvancedFormDataProviderFromDataSource(params));
    }
}

export { AdvancedFormDataProviderFactory };