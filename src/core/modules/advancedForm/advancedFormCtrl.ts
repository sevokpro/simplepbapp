import { ComponentFactoryResolver, Injector, ReflectiveInjector, Type, ViewContainerRef } from "@angular/core";

import { isNullOrUndefined } from "@itexpert-dev/tiny-helpers";
import { IAdvancedFormBlockConfig } from "coreContracts/advancedForm/IAdvancedFormBlockConfig";
import { IAdvancedFormConfigurator } from "coreContracts/advancedForm/IAdvancedFormConfigurator";
import { IAdvancedFormCtrl } from "coreContracts/advancedForm/IAdvancedFormCtrl";
import { IAdvancedFormFieldKey } from "coreContracts/advancedForm/IAdvancedFormFieldKey";
import { IAdvancedFormCtrlInitParams } from "coreContracts/advancedForm/IAdvancedFormInitParams";
import { IAdvancedFormModel } from "coreContracts/advancedForm/IAdvancedFormModel";
import { IAdvancedFormBlockKey } from "coreContracts/advancedForm/IAdvancedFormViewConfig";
import { IBaseWidget } from "coreContracts/advancedForm/widgets/IBaseWidget";
import { Observable } from "rxjs/Observable";
import { ReplaySubject } from "rxjs/ReplaySubject";
import { Subject } from "rxjs/Subject";

export class AdvancedFormCtrl extends IAdvancedFormCtrl {

    public formModel: IAdvancedFormModel;
    private fields: Map<string, Subject<IBaseWidget<any>>>;

    constructor(
        initParams: IAdvancedFormCtrlInitParams,
        public config: IAdvancedFormConfigurator,
        private componentFactoryResolver: ComponentFactoryResolver
    ) {
        super();

        let blocks: Map<string, IAdvancedFormBlockConfig>;
        if (isNullOrUndefined(initParams.blocks)) {
            blocks = new Map();
            const blockProps: IAdvancedFormBlockConfig = {
                fields: Array.from(initParams.metadata.keys())
            };
            blocks.set('main', blockProps);
        } else {
            blocks = initParams.blocks;
        }

        this.fields = new Map();
        for (const block of Array.from(blocks.keys())) {
            const fieldsWidgets = new Map();
            for (const field of Array.from(blocks.get(block).fields)) {
                const widgetSubject: Subject<IBaseWidget<any>> = new ReplaySubject(1);
                this.fields.set(field, widgetSubject);
                fieldsWidgets.set(field, widgetSubject);
            }
            blocks.get(block).fieldsWidgets = fieldsWidgets;
        }

        this.formModel = {
            data: initParams.data,
            metadata: initParams.metadata,
            blocks: blocks
        };

        if (initParams.hasOwnProperty('data')) {
            for (const [key, val] of Array.from(initParams.data.entries())) {
                this.getDefaultWidget(key).first().subscribe(
                    next => {
                        next.setValue(val);
                    }
                );
            }
        }
    }

    public renderFormBlock(block: string, container: ViewContainerRef, parentInjector: Injector) {
        const resolvedViewComponent = this.componentFactoryResolver.resolveComponentFactory(this.config.advancedFormViewComponent as any);

        const injector = ReflectiveInjector.resolveAndCreate(
            [{
                provide: IAdvancedFormCtrl,
                useValue: this
            }, {
                provide: IAdvancedFormBlockKey,
                useValue: {
                    block: block
                }
            }],
            parentInjector
        );
        return container.createComponent(resolvedViewComponent, 0, injector);
    }

    public renderWidget(field: string, container: ViewContainerRef, parentInjector: Injector): IBaseWidget<any> {
        const componentType = this.formModel.metadata.get(field).type;
        const componentName = this.config.typeToWidgetNameMap.get(componentType);
        const component = this.config.widgetsStorage.get(componentName) as Type<IBaseWidget<any>>;
        const resolvedComponent = this.componentFactoryResolver.resolveComponentFactory<IBaseWidget<any>>(component);

        const injector = ReflectiveInjector.resolveAndCreate(
            [{
                provide: IAdvancedFormCtrl,
                useValue: this
            }, {
                provide: IAdvancedFormFieldKey,
                useValue: {
                    key: field
                }
            }],
            parentInjector
        );

        const uiComponent = container.createComponent<IBaseWidget<any>>(resolvedComponent, 0, injector);
        return uiComponent.instance;
    }

    public getEntity(): Observable<Map<string, any>> {
        return Observable
            .from( Array.from(this.fields.entries()) )
            .switchMap( ([fieldKey, fieldWidgetObservable]) =>
                Observable.of(fieldKey).withLatestFrom(fieldWidgetObservable) )
            .map( ([fieldKey, widget]) => [fieldKey, widget.getValue()] )
            .toArray()
            .map(next => new Map(next as any));
    }

    public getFieldLabel(field: string): Observable<string> {
        return Observable.of(this.formModel.metadata.get(field).label);
    }

    public pushInitializedWidget(field: string, context: IBaseWidget<any>) {
        this.fields.get(field).next(context);
    }

    public getDefaultWidget(field: string): Observable<IBaseWidget<any>> {
        if (!this.fields.has(field)) {
            throw new Error(`field with key: ${field} not found.\nCurrent form widget keys: ${Array.from(this.fields.keys()).join(', ')}`);
        }
        return this.fields.get(field);
    }

    public getExtendWidget<TWidgetType>(field: string, widgetType?: any): Observable<TWidgetType> {
        return this.fields.get(field)
            .map(next => {
                if (next instanceof widgetType === false) {
                    throw new Error(`field: ${field} is not assign to ${widgetType.name} type`);
                }
                return next as any;
            });
    }
}