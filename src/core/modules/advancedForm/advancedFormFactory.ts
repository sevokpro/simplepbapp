import { ComponentFactoryResolver, Injectable } from "@angular/core";
import { IAdvancedFormConfigurator } from "coreContracts/advancedForm/IAdvancedFormConfigurator";
import { IAdvancedFormCtrl } from "coreContracts/advancedForm/IAdvancedFormCtrl";
import { IAdvancedFormFactory } from "coreContracts/advancedForm/IAdvancedFormFactory";
import { IAdvancedFormCtrlInitParams } from "coreContracts/advancedForm/IAdvancedFormInitParams";
import { AdvancedFormCtrl } from "./advancedFormCtrl";

@Injectable()
class AdvancedFormFactory extends IAdvancedFormFactory {
    constructor(private componentFactoryResolver: ComponentFactoryResolver, private configurator: IAdvancedFormConfigurator) {
        super();
    }

    public createCtrlInstance(initParams: IAdvancedFormCtrlInitParams): IAdvancedFormCtrl {
        return new AdvancedFormCtrl(initParams, this.configurator, this.componentFactoryResolver);
    }
}

export { AdvancedFormFactory };