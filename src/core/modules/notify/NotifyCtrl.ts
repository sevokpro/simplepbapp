import { INotifyConfig, INotifyCtrl } from "coreContracts/notify";
import { Observable } from "rxjs";
import { INotification } from "./interfaces/notification.type";
import { NotificationsService } from "./services/notificationsService";

export class NotifyCtrl implements INotifyCtrl {
    public constructor(public notification: INotification,
        private notificationsService: NotificationsService) {
    }

    public hide(): Observable<null> {
        this.notification.invisible.next(true);
        return Observable.of(null);
    }

    public show(): Observable<null> {
        this.notification.invisible.next(false);
        return Observable.of(null);
    }

    public destroy() {
        this.notificationsService.remove(this.notification);
    }

    public prolong(duration: number) {

    }

    public updateConfig(config: INotifyConfig) {

    }
}