import { IPosition } from "coreContracts/notify";
import { NotificationsService } from "./notificationsService";

type NotificationsComponentParamsFactory = () => {
    position: IPosition,
    notificationsService: NotificationsService;
};

export { NotificationsComponentParamsFactory };