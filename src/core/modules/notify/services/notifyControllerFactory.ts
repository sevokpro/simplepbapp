import { Injectable } from "@angular/core";
import { isNull, isNullOrUndefined } from "@itexpert-dev/tiny-helpers";
import { HorizontalPosition, INotify, INotifyConfig, IPosition, VerticalPosition } from "coreContracts/notify";
import { Observable } from "rxjs";
import { getPropertyValueOrNull } from "../../../../common/getPropertyValueOrNull";
import { NotifyCtrl } from "../NotifyCtrl";
import { GridNotificationsService } from "./gridNotificationsService";

@Injectable()
export class NotifyControllerFactory extends INotify {
    private defaultPosition: IPosition = {
        horizontal: HorizontalPosition.right,
        vertical: VerticalPosition.top
    };

    public constructor(private gridNotificationsService: GridNotificationsService) {
        super();
    }

    public success(message: string, config?: INotifyConfig): Observable<NotifyCtrl> {
        const configFromArguments = getPropertyValueOrNull(config, config => config.position);
        const notifyPosition = isNull(configFromArguments) ? this.defaultPosition : configFromArguments;
        const notificationInfo = this.gridNotificationsService.setNotification({
            content: message,
            type: 'success',
            override: config
        }, notifyPosition);

        const ctrl = new NotifyCtrl(notificationInfo.notification, notificationInfo.notificationsService);
        return Observable.of(ctrl);
    }

    public warn(message: string, config?: INotifyConfig): Observable<NotifyCtrl> {
        throw new Error('PLATFORM: not implemented');
    }

    public info(message: string, config?: INotifyConfig): Observable<NotifyCtrl> {
        const configFromArguments = getPropertyValueOrNull(config, config => config.position);
        const notifyPosition = isNull(configFromArguments) ? this.defaultPosition : configFromArguments;
        const notificationInfo = this.gridNotificationsService.setNotification({
            content: message,
            type: 'info',
            override: config
        }, notifyPosition);
        const ctrl = new NotifyCtrl(notificationInfo.notification, notificationInfo.notificationsService);
        return Observable.of(ctrl);
    }

    public error(message: string, config?: INotifyConfig): Observable<NotifyCtrl> {
        const configFromArguments = getPropertyValueOrNull(config, config => config.position);
        const notifyPosition = isNull(configFromArguments) ? this.defaultPosition : configFromArguments;
        const notificationInfo = this.gridNotificationsService.setNotification({
            content: message,
            type: 'error',
            override: config
        }, notifyPosition);
        const ctrl = new NotifyCtrl(notificationInfo.notification, notificationInfo.notificationsService);
        return Observable.of(ctrl);
    }
}