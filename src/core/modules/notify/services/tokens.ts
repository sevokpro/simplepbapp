import { InjectionToken } from "@angular/core";
import { IPosition } from "coreContracts/notify";
import { NotificationsService } from "src/core/modules/notify/services/notificationsService";

export const I_POSITION_TOKEN = new InjectionToken<IPosition>('position');
export const NOTIFICATIONS_SERVICE = new InjectionToken<NotificationsService>('NotificationsService');