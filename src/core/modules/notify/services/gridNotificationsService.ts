import { ComponentFactoryResolver, Injectable, Injector, ReflectiveInjector } from "@angular/core";
import { HorizontalPosition, IPosition, VerticalPosition } from "coreContracts/notify";
import { IRootAppContainer } from "coreContracts/rootAppContainer";
import { NotificationsComponent } from "../components/notifications.component";
import { INotification } from "../interfaces/notification.type";
import { INotificationOptions } from "../interfaces/options.type";
import { NotificationsService } from "./notificationsService";
import { I_POSITION_TOKEN, NOTIFICATIONS_SERVICE } from "./tokens";

@Injectable()
class GridNotificationsService {

    private defaultOptions: INotificationOptions = {
        position: {
            horizontal: HorizontalPosition.left,
            vertical: VerticalPosition.bottom
        },
        timeOut: 5000,
        lastOnBottom: false,
        showProgressBar: false,
        clickToClose: false,
        showClose: true,
        maxLength: 30
    };

    private grid = new Map<string, NotificationsService>();

    public constructor(private rootAppContainer: IRootAppContainer,
        private componentResolver: ComponentFactoryResolver,
        private injector: Injector) {
        this.createGrid();
    }

    private createGrid() {
        for (const vertical in VerticalPosition) {
            if (isNaN(Number(vertical)))
                for (const horizontal in HorizontalPosition) {
                    if (isNaN(Number(horizontal))) {
                        const position: IPosition = {
                            horizontal: HorizontalPosition[HorizontalPosition[horizontal]],
                            vertical: VerticalPosition[VerticalPosition[vertical]]
                        };
                        const notificationsService = new NotificationsService(this.defaultOptions);
                        this.grid.set(GridNotificationsService.getToken(position), notificationsService);
                        this.renderNotificationsComponent(position, notificationsService);
                    }
                }
        }
    }

    private renderNotificationsComponent(position: IPosition, notificationsService: NotificationsService) {
        const notificationsComponentFactory = this.componentResolver.resolveComponentFactory(NotificationsComponent);
        const childInjector = ReflectiveInjector.resolveAndCreate(
            [
                {
                    provide: I_POSITION_TOKEN,
                    useValue: position
                },
                {
                    provide: NOTIFICATIONS_SERVICE,
                    useValue: notificationsService
                }
            ],
            this.injector
        );
        const componentRef = this.rootAppContainer.viewContainerRef.createComponent(notificationsComponentFactory, null, childInjector);
    }

    private static getToken(position: IPosition) {
        let horizontal: string;
        let vertical: string;
        if (!isNaN(Number(position.horizontal)))
            horizontal = HorizontalPosition[position.horizontal];
        if (!isNaN(Number(position.vertical)))
            vertical = VerticalPosition[position.vertical];

        // TODO: what?
        // tslint:disable-next-line
        return (horizontal || position.horizontal) + '-' + (vertical || position.vertical);
    }

    private getNotificationsServiceFromGrid(position: IPosition) {
        return this.grid.get(GridNotificationsService.getToken(position));
    }

    public setNotification(notification: INotification, position: IPosition): {
        notification: INotification,
        notificationsService: NotificationsService
    } {
        position = this.defaultOptions.position;
        const notificationsService = this.getNotificationsServiceFromGrid(position);
        notificationsService.setNotofication(notification);
        return {
            notification,
            notificationsService
        };
    }

    // Remove all notifications method
    public remove(position: IPosition, notification?: INotification) {
        const notificationsService = this.grid.get(GridNotificationsService.getToken(position));
        notificationsService.remove(notification);
    }

}

export { GridNotificationsService };