import { ComponentFactoryResolver, EventEmitter, Injectable, Injector } from "@angular/core";
import { isNull, isNullOrUndefined } from "@itexpert-dev/tiny-helpers";
import { IRootAppContainer } from "coreContracts/rootAppContainer";
import { Subject } from "rxjs/Subject";
import { Commands } from "../interfaces/commands";
import { defaultIcons, Icons } from "../interfaces/icons";
import { INotification } from "../interfaces/notification.type";
import { INotificationEvent } from "../interfaces/notificationEvent.type";
import { INotificationOptions } from "../interfaces/options.type";

@Injectable()
export class NotificationsService {
    private emitter: Subject<INotificationEvent> = new Subject<INotificationEvent>();
    public containerMouseEnter: Subject<boolean> = new Subject<boolean>();

    private icons: Icons = defaultIcons;

    private rootAppContainer: IRootAppContainer;
    private componentResolver: ComponentFactoryResolver;
    private injector: Injector;

    public constructor(private options: INotificationOptions) {
    }

    // Attach all the changes received in the options object
    public applyDefaultOptions(obj: any): void {
        Object.keys(this.options).forEach(key => {
            if (obj.hasOwnProperty(key)) {
                (obj as any)[key] = this.options[key];
            }
        });
    }

    public setNotofication(notification: INotification) {
        if (!isNullOrUndefined(notification.id) || !isNullOrUndefined(notification.override) && notification.override.id) {
            notification.id = notification.override.id;
        }else {
            notification.id = Math.random().toString(36).substring(3);
        }

        if (!isNullOrUndefined(notification.override) && !isNull(notification.override.duration)) {
            notification.override.timeOut = notification.override.duration;
        }
        notification.click = new EventEmitter<{}>();
        notification.invisible = new Subject<boolean>();

        this.emitter.next({ command: Commands.set, notification: notification });
        return notification;
    }

    public getChangeEmitter() {
        return this.emitter;
    }

    public getContainerMouseEnter() {
        return this.containerMouseEnter;
    }

    // Remove all notifications method
    public remove(notification: INotification) {
        if (!isNullOrUndefined(notification))
            this.emitter.next({ command: Commands.clean, notification });
        else
            throw new Error('в метод remove должен быть передан объект нотификации');
    }

    public removeAll() {
        this.emitter.next({ command: Commands.cleanAll });
    }

}