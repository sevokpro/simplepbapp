import { Component, Inject, OnDestroy, OnInit, ViewEncapsulation } from "@angular/core";
import { isNullOrUndefined } from "@itexpert-dev/tiny-helpers";
import { IPosition } from "coreContracts/notify";
import { Subscription } from "rxjs";
import { Commands } from "../interfaces/commands";
import { INotification } from "../interfaces/notification.type";
import { NotificationsService } from "../services/notificationsService";
import { I_POSITION_TOKEN, NOTIFICATIONS_SERVICE } from "../services/tokens";

@Component({
    selector: 'notifications',
    encapsulation: ViewEncapsulation.Emulated,
    templateUrl: './notifications.pug',
    styleUrls: [
        './notifications.style.styl'
    ]
})
export class NotificationsComponent implements OnInit, OnDestroy {
    public notifications: Array<INotification> = [];

    private lastNotificationCreated: INotification;
    private listener: Subscription;

    // Received values
    private lastOnBottom = true;
    private maxStack = 8;
    private preventLastDuplicates: any = false;
    private preventDuplicates = false;

    constructor(
        @Inject(I_POSITION_TOKEN) public position: IPosition,
        @Inject(NOTIFICATIONS_SERVICE) public service: NotificationsService
    ) { }

    public ngOnInit(): void {
        // Listen for changes in the service
        this.listener = this.service.getChangeEmitter()
            .subscribe(item => {
                switch (item.command) {
                    case Commands.cleanAll:
                        this.notifications = [];
                        break;
                    case Commands.clean:
                        this.cleanSingle(item.notification);
                        break;
                    case Commands.set:
                        this.set(item.notification);
                        break;
                    default:
                        this.defaultBehavior(item.notification);
                        break;
                }
            });
    }

    // Default behavior on event
    public defaultBehavior(item: INotification): void {
        this.cleanSingle(item);
    }

    public setPosition(position: IPosition) {
        this.position = position;
    }


    // Add the new notification to the notification array
    public set(item: INotification): void {
        item.createdOn = new Date();

        const toBlock: boolean = this.preventLastDuplicates || this.preventDuplicates ?
            this.shouldBeBlocked(item) : false;

        // Save this as the last created notification
        this.lastNotificationCreated = item;

        if (!toBlock) {
            // Check if the notification should be added at the start or the end of the array
            if (this.lastOnBottom) {
                if (this.notifications.length >= this.maxStack) this.notifications.splice(0, 1);
                this.notifications.push(item);
            } else {
                if (this.notifications.length >= this.maxStack) this.notifications.splice(this.notifications.length - 1, 1);
                this.notifications.unshift(item);
            }
        }
    }

    // Check if notifications should be prevented from changes
    public shouldBeBlocked(item: INotification): boolean {
        if (this.preventDuplicates && this.notifications.length > 0) {
            for (let i = 0; i < this.notifications.length; i++) {
                if (NotificationsComponent.checkBlocking(this.notifications[i], item)) {
                    return true;
                }
            }
        }

        if (this.preventLastDuplicates) {
            let comparableTo: INotification;

            if (this.preventLastDuplicates === 'visible' && this.notifications.length > 0) {
                if (this.lastOnBottom) {
                    comparableTo = this.notifications[this.notifications.length - 1];
                } else {
                    comparableTo = this.notifications[0];
                }
            } else if (this.preventLastDuplicates === 'all' && !isNullOrUndefined(this.lastNotificationCreated)) {
                comparableTo = this.lastNotificationCreated;
            } else {
                return false;
            }
            return NotificationsComponent.checkBlocking(comparableTo, item);
        }

        return false;
    }

    private static checkBlocking(checker: INotification, item: INotification): boolean {
        const fields = ['type', 'title', 'content'];
        let result = true;
        fields.forEach(field => {
            result = result && (checker[field] === item[field]);
        });

        if (item.html)
            result = result && checker.html && (checker.html === item.html);

        return result;
    }

    public buildEmit(notification: INotification, to: boolean) {
        const toEmit: INotification = {
            createdOn: notification.createdOn,
            type: notification.type,
            icon: notification.icon,
            id: notification.id
        };

        if (notification.html) {
            toEmit.html = notification.html;
        } else {
            toEmit.title = notification.title;
            toEmit.content = notification.content;
        }

        if (!to) {
            toEmit.destroyedOn = new Date();
        }

        return toEmit;
    }

    public cleanSingle(item: INotification): void {
        item.state = `${item.state}Out`;
        setTimeout(() => {
            const idx = this.notifications.indexOf(item);

            if (idx !== -1)
                this.notifications.splice(idx, 1);
        }, 310);
    }

    public ngOnDestroy(): void {
        if (!isNullOrUndefined(this.listener)) {
            this.listener.unsubscribe();
        }
    }
}
