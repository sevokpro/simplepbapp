import { animate, state, style, transition, trigger } from "@angular/animations";
import { Component, Inject, Input, NgZone, OnDestroy, OnInit, ViewEncapsulation } from "@angular/core";
import { DomSanitizer, SafeHtml } from "@angular/platform-browser";
import { isNullOrUndefined } from "@itexpert-dev/tiny-helpers";
import { Subscription } from "rxjs";
import { INotification } from "../../interfaces/notification.type";
import { NotificationsService } from "../../services/notificationsService";
import { NOTIFICATIONS_SERVICE } from "../../services/tokens";

@Component({
    selector: 'simple-notification',
    encapsulation: ViewEncapsulation.None,
    animations: [
        trigger('enterLeave', [

            // Enter from right
            state('fromRight', style({ opacity: 1, transform: 'translateX(0)' })),
            transition('* => fromRight', [
                style({ opacity: 0, transform: 'translateX(5%)' }),
                animate('400ms ease-in-out')
            ]),
            state('fromRightOut', style({ opacity: 0, transform: 'translateX(5%)' })),
            transition('fromRight => fromRightOut', [
                style({ opacity: 1, transform: 'translateX(0)' }),
                animate('300ms ease-in-out')
            ]),

            // Enter from left
            state('fromLeft', style({ opacity: 1, transform: 'translateX(0)' })),
            transition('* => fromLeft', [
                style({ opacity: 0, transform: 'translateX(-5%)' }),
                animate('400ms ease-in-out')
            ]),
            state('fromLeftOut', style({ opacity: 0, transform: 'translateX(-5%)' })),
            transition('fromLeft => fromLeftOut', [
                style({ opacity: 1, transform: 'translateX(0)' }),
                animate('300ms ease-in-out')
            ]),

            // Rotate
            state('scale', style({ opacity: 1, transform: 'scale(1)' })),
            transition('* => scale', [
                style({ opacity: 0, transform: 'scale(0)' }),
                animate('400ms ease-in-out')
            ]),
            state('scaleOut', style({ opacity: 0, transform: 'scale(0)' })),
            transition('scale => scaleOut', [
                style({ opacity: 1, transform: 'scale(1)' }),
                animate('400ms ease-in-out')
            ]),

            // Scale
            state('rotate', style({ opacity: 1, transform: 'rotate(0deg)' })),
            transition('* => rotate', [
                style({ opacity: 0, transform: 'rotate(5deg)' }),
                animate('400ms ease-in-out')
            ]),
            state('rotateOut', style({ opacity: 0, transform: 'rotate(-5deg)' })),
            transition('rotate => rotateOut', [
                style({ opacity: 1, transform: 'rotate(0deg)' }),
                animate('400ms ease-in-out')
            ])
        ])
    ],
    templateUrl: './notification.pug',
    styleUrls: ['./notification.style.styl']
})
class NotificationComponent implements OnInit, OnDestroy {
    @Input() public position: number;
    @Input() public item: INotification;

    //configurable options
    private timeOut = 0;
    public showProgressBar = false;
    public showClose = true;
    private pauseOnHover = true;
    private clickToClose = false;
    private maxLength = 0;
    public theClass = '';
    private rtl = false;
    private animate: 'fromRight' | 'fromLeft' | 'rotate' | 'scale' = 'fromRight';

    // Progress bar variables
    public progressWidth = 0;
    public safeSvg: SafeHtml;

    private stopTime = false;
    private timer: any;
    private steps: number;
    private speed: number;
    private count = 0;
    private start: any;

    private diff: any;
    private icon: string;
    public class: any = {};

    private hoverListener: Subscription;
    private invisibleSubscribtion: Subscription;

    public constructor( @Inject(NOTIFICATIONS_SERVICE) private notificationsService: NotificationsService,
        private domSanitizer: DomSanitizer,
        private zone: NgZone) {
    }

    public ngOnInit(): void {
        this.notificationsService.applyDefaultOptions(this);

        this.class['click-to-close'] = this.clickToClose;
        this.class['rtl-mode'] = this.rtl;
        this.class[this.item.type] = true;
        this.invisibleSubscribtion = this.item.invisible.subscribe(value => {
            this.class['invisible'] = value;
        });

        if (isNullOrUndefined(this.animate)) {
            this.item.state = this.animate;
        }
        if (isNullOrUndefined(this.item.override)) {
            this.attachOverrides();
        }

        if (isNullOrUndefined(this.timeOut)) {
            this.timeOut = 0;
        }
        if (this.timeOut !== 0) {
            this.startTimeOut();
        }

        this.safeSvg = this.domSanitizer.bypassSecurityTrustHtml( isNullOrUndefined(this.icon) ? this.item.icon : this.icon);

        this.hoverListener = this.notificationsService
            .getContainerMouseEnter().subscribe(onEnter => {
                if (this.pauseOnHover) {
                    this.stopTime = onEnter;
                    if (onEnter === false) {
                        setTimeout(this.instance, (this.speed - this.diff));
                    }
                }
            }
            );
    }

    // Attach all the changes received in the options object
    public attachChanges(options: any): void {
        Object.keys(options).forEach(a => {
            if (this.hasOwnProperty(a)) {
                (this as any)[a] = options[a];
            }
        });
    }

    private startTimeOut(): void {
        this.steps = this.timeOut / 10;
        this.speed = this.timeOut / this.steps;
        this.start = new Date().getTime();
        this.zone.runOutsideAngular(() => this.timer = setTimeout(this.instance, this.speed));
    }

    public onEnter(): void {
    }

    public onLeave(): void {
    }

    public setPosition(): number {
        return this.position !== 0 ? this.position * 90 : 0;
    }

    public onClick($e: MouseEvent): void {
        this.item.click!.emit($e);

        if (this.clickToClose) {
            this.remove();
        }
    }

    private onCloseClick($e: MouseEvent): void {
        this.item.click!.emit($e);

        if (this.showClose) {
            this.remove();
        }
    }

    // Attach all the overrides
    public attachOverrides(): void {
        Object.keys(this.item.override).forEach(a => {
            if (this.hasOwnProperty(a)) {
                (this as any)[a] = this.item.override[a];
            }
        });
    }

    public ngOnDestroy(): void {
        clearTimeout(this.timer);
        if (!isNullOrUndefined(this.hoverListener))
            this.hoverListener.unsubscribe();
        if (!isNullOrUndefined(this.invisibleSubscribtion))
            this.invisibleSubscribtion.unsubscribe();
    }

    private instance = () => {
        this.zone.runOutsideAngular(() => {
            this.zone.run(() => this.diff = (new Date().getTime() - this.start) - (this.count * this.speed));

            if (this.count++ === this.steps) this.zone.run(() => this.remove());
            else if (!this.stopTime) {
                if (this.showProgressBar) this.zone.run(() => this.progressWidth += 100 / this.steps);

                this.timer = setTimeout(this.instance, (this.speed - this.diff));
            }
        });
    }

    private remove() {
        if (!isNullOrUndefined(this.animate)) {
            this.zone.runOutsideAngular(() => {
                this.notificationsService.remove(this.item);
            });
        } else {
            this.notificationsService.remove(this.item);
        }
    }
}

export { NotificationComponent };