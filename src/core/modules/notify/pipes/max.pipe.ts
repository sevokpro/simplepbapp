import { Pipe, PipeTransform } from "@angular/core";
import { isNullOrUndefined } from "@itexpert-dev/tiny-helpers";

@Pipe({ name: 'max' })
class MaxPipe implements PipeTransform {
    public transform(value: string, ...args: Array<any>): any {
        if (isNullOrUndefined(value) || value === '') return value;

        const allowed = args[0];
        const ellipsis = args[1];
        const received = value.length;

        if (received > allowed && allowed !== 0) {
            const toCut = allowed - received;
            return value.slice(0, toCut).trim() + '...';
        }

        return value;
    }
}
export { MaxPipe };