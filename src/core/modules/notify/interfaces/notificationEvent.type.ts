import { Commands } from "./commands";
import { INotification } from "./notification.type";

interface INotificationEvent {
    command: Commands;
    id?: string;
    notification?: INotification;
}

export { INotificationEvent };
