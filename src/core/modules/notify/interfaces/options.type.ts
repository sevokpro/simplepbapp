import { IPosition } from "coreContracts/notify";
import { Icons } from "./icons";

interface INotificationOptions {
    timeOut?: number;
    showProgressBar?: boolean;
    pauseOnHover?: boolean;
    lastOnBottom?: boolean;
    clickToClose?: boolean;
    showClose?: boolean;
    maxLength?: number;
    maxStack?: number;
    preventDuplicates?: number;
    preventLastDuplicates?: boolean | string;
    theClass?: string;
    rtl?: boolean;
    animate?: 'fromRight' | 'fromLeft' | 'rotate' | 'scale';
    icons?: Icons;
    position?: IPosition;
}

export { INotificationOptions };