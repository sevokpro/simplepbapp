import { NgModule } from "@angular/core";
import { GridNotificationsService } from "./services/gridNotificationsService";

@NgModule({
    providers: [
        GridNotificationsService
    ]
})
class NotifyCommonServicesModule { }

export { NotifyCommonServicesModule };