import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NotificationComponent } from "./components/notification/notification.component";
import { NotificationsComponent } from "./components/notifications.component";
import { MaxPipe } from "./pipes/max.pipe";

@NgModule({
    imports: [
        CommonModule,
        BrowserAnimationsModule
    ],
    declarations: [
        NotificationComponent,
        MaxPipe,
        NotificationsComponent
    ],
    entryComponents: [NotificationsComponent]
})
class NotifyComponentsContainerModule {
    public constructor() {
    }
}

export { NotifyComponentsContainerModule };