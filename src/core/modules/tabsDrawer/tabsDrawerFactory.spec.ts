import { TestBed } from "@angular/core/testing";
import { ITabsDrawer } from "coreContracts/tabsDrawer/ITabsDrawer";
import { ITabsDrawerFactory } from "coreContracts/tabsDrawer/ITabsDrawerFactory";
import { TabsDrawerFactory } from "./tabsDrawerFactory";

describe('test for tabsDrawer factory', () => {
    let tabsDrawerFactory: TabsDrawerFactory;

    beforeEach(done => {
        TestBed
            .resetTestingModule()
            .configureTestingModule({
                providers: [TabsDrawerFactory]
            });

        tabsDrawerFactory = TestBed.get(TabsDrawerFactory);

        done();
    });

    it('check that tabs instance not undefined', () => {
        expect(tabsDrawerFactory).not.toBeUndefined();
    });

    it('factory must extend IFactory', () => {
        expect(tabsDrawerFactory instanceof ITabsDrawerFactory).toBeTruthy();
    });

    it('create return instance of ITabsDrawer', () => {
        const tabsDrawerInstance = tabsDrawerFactory.create([]);
        expect(tabsDrawerInstance instanceof ITabsDrawer).toBeTruthy();
    });
});