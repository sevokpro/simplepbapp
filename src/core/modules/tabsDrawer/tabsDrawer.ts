import { ComponentFactoryResolver, ComponentRef, Injector, ReflectiveInjector, ViewContainerRef } from "@angular/core";
import { IRenderablePageElement } from "coreContracts/pageBuilder/IPageBuilderComponentRegistry";
import { ITabsDrawer } from "coreContracts/tabsDrawer/ITabsDrawer";
import { ITabs } from "coreContracts/tabsDrawer/ITabsDrawerFactory";
import { Observable } from "rxjs/Observable";
import { ITabsDataProvider } from "./common/ITabsWrapper";
import { TabsDrawerComponent } from "./tabsDrawerComponent";

class TabsDrawer extends ITabsDrawer{
    constructor(
        private cfr: ComponentFactoryResolver,
        private tabs: ITabs
    ) {
        super();
    }

    public render(viewContainer: ViewContainerRef, injector: Injector): ComponentRef<any> {
        return (viewContainer.createComponent(
            this.cfr.resolveComponentFactory(TabsDrawerComponent),
            0,
            ReflectiveInjector.resolveAndCreate(
                [{provide: ITabsDataProvider, useValue: {getTabs: () => this.tabs} as ITabsDataProvider}],
                injector
            )
        ));
    }

    public whenClickTab(): Observable<String> {
        throw new Error("Method not implemented.");
    }

    public activateTab(tabAlias: string) {
        throw new Error("Method not implemented.");
    }
}

export {TabsDrawer};