import { Component, ComponentFactoryResolver, Injector, NgModule, ViewContainerRef } from "@angular/core";
import { TestBed } from "@angular/core/testing";
import { ITabsDrawer } from "coreContracts/tabsDrawer/ITabsDrawer";
import { TabsDrawer } from "./tabsDrawer";
import { TabsDrawerModule } from "./tabsDrawer.module";

@Component({
    template: ``
})
class TestEnvComponent{
    constructor(
       public vcr: ViewContainerRef
    ) {}
}

describe(`test cases for tabs drawer`, () => {
    let tabDrawerInstance: TabsDrawer;
    let viewContainer: ViewContainerRef;
    let injector: Injector;

    beforeEach(done => {
        TestBed
            .resetTestingModule()
            .configureTestingModule({
                imports: [TabsDrawerModule],
                declarations: [TestEnvComponent]
            });
        viewContainer = TestBed.createComponent(TestEnvComponent).componentInstance.vcr;
        injector = TestBed.get(Injector);
        tabDrawerInstance = new TabsDrawer(
            TestBed.get(ComponentFactoryResolver),
            []
        );
        done();
    });

    it('instance not null', () => {
        expect(tabDrawerInstance).not.toBeUndefined();
    });

    it('instance must extend interface-class', () => {
        expect(tabDrawerInstance instanceof ITabsDrawer).toBeTruthy();
    });

    it('render method not to throw error', () => {
        expect(() => tabDrawerInstance.render(viewContainer, injector)).not.toThrowError();
    });

    it('view container must be clean', () => {
        expect(viewContainer.length).toEqual(0);
    });

    it('view container ,ust increment after render', () => {
        tabDrawerInstance.render(viewContainer, injector);
        expect(viewContainer.length).toEqual(1);
    });
});