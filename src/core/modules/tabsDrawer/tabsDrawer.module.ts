import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { TabsDrawerComponent } from "./tabsDrawerComponent";

@NgModule({
    imports: [CommonModule],
    declarations: [TabsDrawerComponent],
    entryComponents: [TabsDrawerComponent]
})
class TabsDrawerModule{}

export {TabsDrawerModule};