import { ITabs } from "coreContracts/tabsDrawer/ITabsDrawerFactory";

abstract class ITabsDataProvider{
    public abstract getTabs(): ITabs;
}

export {ITabsDataProvider};