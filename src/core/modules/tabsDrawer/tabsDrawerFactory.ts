import { ComponentFactoryResolver, Injectable, Injector } from "@angular/core";
import { ITabsDrawer } from "coreContracts/tabsDrawer/ITabsDrawer";
import { ITabs, ITabsDrawerFactory } from "coreContracts/tabsDrawer/ITabsDrawerFactory";
import { TabsDrawer } from "./tabsDrawer";

@Injectable()
class TabsDrawerFactory extends ITabsDrawerFactory{
    constructor(
        private cfr: ComponentFactoryResolver
    ) {
        super();
    }

    public create(tabs: ITabs): ITabsDrawer {
        return new TabsDrawer(this.cfr, tabs);
    }

}

export {TabsDrawerFactory};