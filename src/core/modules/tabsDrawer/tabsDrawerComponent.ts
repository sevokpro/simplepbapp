import { Component } from "@angular/core";
import { ISingleTab, ITabs } from "coreContracts/tabsDrawer/ITabsDrawerFactory";
import { Observable } from "rxjs/Observable";
import { Subject } from "rxjs/Subject";
import { ITabsDataProvider } from "./common/ITabsWrapper";

interface IExtendSingleTab extends ISingleTab{
    ngStyle: Observable<Object>;
}

@Component({
    templateUrl: './tabsDrawerComponent.template.pug'
})
class TabsDrawerComponent{
    public tabs: Array<IExtendSingleTab>;
    public whenTabClick: Subject<ISingleTab> =
        new Subject();
    public whenActiveTabChange: Observable<String> =
        this.whenTabClick.asObservable().map(n => n.alias);
    public activeTabStyle =
        {color: '#3271d2', 'border-bottom': 'solid 2px #3271d2'};
    constructor(
        tabs: ITabsDataProvider
    ) {
        this.tabs = tabs.getTabs().map( tab => {
            return {...tab, ...{ngStyle: this.whenActiveTabChange.map( alias => {
                if (alias === tab.alias) {
                    return this.activeTabStyle;
                }else {
                    return null;
                }
            })}} as IExtendSingleTab;
        });
    }
}

export {TabsDrawerComponent};