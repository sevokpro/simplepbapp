import { Injectable } from "@angular/core";
import { isNull, isUndefined } from "@itexpert-dev/tiny-helpers";
import { IIndexedDbConfigurator } from "coreContracts/indexedDbSource/IIndexedDbConfigurator";
import { IIndexedDbSource } from "coreContracts/indexedDbSource/IIndexedDbSource";
import { read } from "fs";
import { Observable } from "rxjs/Observable";
import { Observer } from "rxjs/Observer";
import { Equal } from "tslint/lib/utils";
import { IDataSourceCompositeRequest } from "../../../common/IDataSource/compositeQuery/request/IDataSourceCompositeRequest";
import { IDataSourceCompositeResponse } from "../../../common/IDataSource/compositeQuery/response/IDataSourceCompositeResponse";
import { IDataSourceCreateRequest } from "../../../common/IDataSource/create/request/IDataSourceCreateRequest";
import { IDataSourceCreateResponse } from "../../../common/IDataSource/create/response/IDataSourceCreateResponse";
import { IDataSourceDeleteRequest } from "../../../common/IDataSource/delete/request/IDataSourceDeleteRequest";
import { IDataSourceDeleteResponse } from "../../../common/IDataSource/delete/response/IDataSourceDeleteResponse";
import { IDataSourceAvailableEntity } from "../../../common/IDataSource/IDataSource";
import { IDataSourceMetadataRequest } from "../../../common/IDataSource/metadata/request/IDataSourceMetadataRequest";
import { IDataSourceMetadataResponse } from "../../../common/IDataSource/metadata/response/IDataSourceMetadataResponse";
import { IDataSourceReadRequest } from "../../../common/IDataSource/read/request/IDataSourceReadRequest";
import { IDataSourceReadRequestFilterComparsion } from "../../../common/IDataSource/read/request/IDataSourceReadRequestFilterBaseField";
import { IDataSourceReadRequestFilterNode } from "../../../common/IDataSource/read/request/IDataSourceReadRequestFilterBlock";
import { IDataSourceReadResponse } from "../../../common/IDataSource/read/response/IDataSourceReadResponse";
import { IDataSourceReadResponseEntity } from "../../../common/IDataSource/read/response/IDataSourceReadResponseEntity";
import { IDataSourceUpdateRequest } from "../../../common/IDataSource/update/request/IDataSourceUpdateRequest";
import { IDataSourceUpdateResponse } from "../../../common/IDataSource/update/response/IDataSourceUpdateResponse";

@Injectable()
class IndexedDbSource implements IIndexedDbSource{
    constructor(
        private configurator: IIndexedDbConfigurator
    ) {}

    //TODO: add support for all filter cases
    private applyFilterForEntity(entity: Object, filter: IDataSourceReadRequestFilterNode): boolean {
        const and = (previousCondition, nextCondition) => previousCondition && nextCondition;
        const or = (previousCondition, nextCondition) => previousCondition || nextCondition;
        const combineComparsion = (filter.operator === 'and') ? and : or;

        let result: boolean;
        Object
            .keys(filter.nodes)
            .map(key => [key, filter.nodes[key]])
            .forEach(([key, val]) => {
                const equalOperator = (currentValue, requireValue) => currentValue === requireValue;

                const selectOperator = operatorName => {
                    switch (operatorName){
                        case 'Equal':
                            return equalOperator;
                        default:
                            throw  new Error(`unsupported operator: ${operatorName}`);
                    }
                };
                const operator = selectOperator(val.operator);
                if (isUndefined(result)) {
                    result = operator(entity[val.field], val.value);
                }else {
                    result = combineComparsion(result, operator(entity[val.field], val.value));
                }
            });

        return result;
    }

    public read(request: IDataSourceReadRequest): Observable<IDataSourceReadResponse> {
        return Observable.create((observer: Observer<IDataSourceReadResponse>) => {
            const dbName: string = this.configurator.getDefaultDataBaseName();
            const openRequest = indexedDB.open(dbName);
            openRequest.onsuccess = event => {
                const db = event.target['result'];
                const readTransaction = db.transaction(request.entity, 'readwrite');
                const entityObjectStore = readTransaction.objectStore(request.entity);
                const cursor = entityObjectStore.openCursor();
                const indexedDbResponse = [];
                cursor.onsuccess = event => {
                    const cursor = event.target.result;
                    if (!isNull(cursor)) {
                        if (this.applyFilterForEntity(cursor.value, request.filter)) {
                            indexedDbResponse.push(cursor.value);
                        }
                        cursor.continue();
                    }else {
                        const response: IDataSourceReadResponse = {
                            entities: indexedDbResponse.map(value => {
                                const responseEntity: IDataSourceReadResponseEntity = {
                                    uuid: value.uuid,
                                    entity: new Map(),
                                    displayName: value.uuid
                                };
                                Object
                                    .keys(value)
                                    .forEach(key => responseEntity.entity.set(key, {value: value[key]}));
                                return responseEntity;
                            })
                        };

                        observer.next(response);
                        observer.complete();
                    }
                };
            };
        });
    }

    public create(request: IDataSourceCreateRequest): Observable<IDataSourceCreateResponse> {
        const dbName: string = this.configurator.getDefaultDataBaseName();
        return Observable.create((observer: Observer<IDataSourceCreateResponse>) => {
            const db = indexedDB.open(dbName);
            db.onsuccess = event => {
                const entityName = request.entities[0].entityName;
                const entityProps = request.entities[0].entity;
                const entityObject = {};
                entityProps.forEach((value, key) => entityObject[key] = value.value);

                const uuid = Math.trunc(Math.random() * 1e10);
                entityObject['uuid'] = uuid;
                const openedDb = event.target['result'];
                const createEntityTransaction = openedDb.transaction(entityName, "readwrite");
                const createEntityObjectStore = createEntityTransaction.objectStore(entityName);
                const createEntityRequest = createEntityObjectStore.add(entityObject);
                createEntityRequest.onsuccess = event => {
                    db.result.close();

                    const filter: IDataSourceReadRequestFilterNode = {
                        operator: 'and',
                        isNegative: false,
                        nodes: [{
                            field: 'uuid',
                            operator: 'Equal',
                            value: uuid
                        } as IDataSourceReadRequestFilterComparsion]
                    };
                    this
                        .read({entity: entityName, filter: filter})
                        .do(next => {
                            if (next.entities.length !== 1) {
                                throw new Error(`after create entity return ${next.entities.length} elements, but must return one element`);
                            }
                        })
                        .map(next => next.entities[0])
                        .subscribe(
                        next => {
                            const response: IDataSourceCreateResponse = {
                                entities: [{
                                    entity: next.entity,
                                    entityUuid: {
                                        uuid: next.uuid,
                                    }
                                }]
                            };
                            observer.next(response);
                        },
                        err => console.warn(err),
                        () => observer.complete()
                    );

                };
            };
        });
    }

    public update(request: IDataSourceUpdateRequest): Observable<IDataSourceUpdateResponse> {
        throw new Error("Method not implemented.");
    }

    public delete(request: IDataSourceDeleteRequest): Observable<IDataSourceDeleteResponse> {
        throw new Error("Method not implemented.");
    }

    public compositeQuery(request: IDataSourceCompositeRequest): Observable<IDataSourceCompositeResponse> {
        throw new Error("Method not implemented.");
    }

    public getMetadata(request: IDataSourceMetadataRequest): Observable<IDataSourceMetadataResponse> {
        throw new Error("Method not implemented.");
    }

    public getAvailableEntities(): Observable<Array<IDataSourceAvailableEntity>> {
        throw new Error("Method not implemented.");
    }

    public count(request: { entity: string; filter?: IDataSourceReadRequestFilterNode }): Observable<number> {
        throw new Error("Method not implemented.");
    }
}

export {IndexedDbSource};