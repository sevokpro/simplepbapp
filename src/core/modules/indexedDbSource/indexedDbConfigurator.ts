import { Injectable } from "@angular/core";
import { IIndexedDbConfig, IIndexedDbConfigurator } from "coreContracts/indexedDbSource/IIndexedDbConfigurator";
import { IIndexedDbUpgradeCtrl } from "coreContracts/indexedDbSource/IIndexedDbUpgradeCtrl";

@Injectable()
class IndexedDbConfigurator implements IIndexedDbConfigurator{
    private defaultDataBasename: string;

    public setDatabases(databases: Array<{ dbName: string; dbConfig: IIndexedDbConfig }>) {
        databases.forEach(database => {
            const {dbName, dbConfig} = database;
            const {version, upgradeScripts} = dbConfig;

            const scripts = upgradeScripts
                .filter(next => next.version <= version)
                .sort((a, b) => a.version - b.version);
            //TODO: add suport for migrations
            const firstScript = scripts[0];

            const openDbRequest = indexedDB.open(dbName, version);
            openDbRequest.onupgradeneeded = (event) => {
                const currentVersion = event.oldVersion;
                const requireversion = event.newVersion;
                const db = event.target['result'];

                const upgradeAdapter: IIndexedDbUpgradeCtrl = {
                    addTable: (entityName, metadata) => {
                        const store = db.createObjectStore(entityName, {keyPath: 'uuid', autoIncrement: true});
                        metadata.forEach((value, key) => {
                            store.createIndex(key, key, {unique: false});
                        });
                    },
                    updateTable: (entityName, metadata) => {

                    }
                };
                firstScript.script(upgradeAdapter);
            };

        });
    }

    public setDefaultDataBase(dataBaseName: string) {
        this.defaultDataBasename = dataBaseName;
    }

    public getDefaultDataBaseName(): string {
        return this.defaultDataBasename;
    }
}

export {IndexedDbConfigurator};