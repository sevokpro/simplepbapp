import { ComponentFactoryResolver, Injector, ReflectiveInjector, ViewContainerRef } from "@angular/core";
import { IUniversalTableCellPosition } from "coreContracts/universalTable/IUniversalTableCellPosition";
import { IUniversalTableCellTextClickEmitter } from "coreContracts/universalTable/IUniversalTableCellTextClickEmitter";
import { IUniversalTableCellTextClickParams } from "coreContracts/universalTable/IUniversalTableCellTextClickParams";
import { IUniversalTableConfigurator } from "coreContracts/universalTable/IUniversalTableConfigurator";
import { IUniversalTableCtrl } from "coreContracts/universalTable/IUniversalTableCtrl";
import { IUniversalTableCtrlInitParams } from "coreContracts/universalTable/IUniversalTableCtrlInitParams";
import {
    IUniversalTableGetHeadersResponse,
    IUniversalTableQueryConfig
} from "coreContracts/universalTable/IUniversalTableDataProvider";
import { IUniversalTableDataRows } from "coreContracts/universalTable/IUniversalTableDataRows";
import { IUniversalTableComponents } from "coreContracts/universalTable/IUniversalTableDefaultComponents";
import { IUniversalTableDiffState } from "coreContracts/universalTable/IUniversalTableDiffState";
import { IUniversalTableHeaderData } from "coreContracts/universalTable/IUniversalTableHeaderData";
import { IUniversalTableState } from "coreContracts/universalTable/IUniversalTableState";
import { IUniversalTableViewScope } from "coreContracts/universalTable/IUniversalTableViewScope";
import { Observable, ReplaySubject, Subject } from "rxjs";

class UniversalTableCtrl extends IUniversalTableCtrl {
    private headers: Subject<IUniversalTableGetHeadersResponse> = new ReplaySubject<IUniversalTableGetHeadersResponse>(1);
    private rows: Subject<IUniversalTableDataRows> = new ReplaySubject<IUniversalTableDataRows>(1);
    public viewScope: IUniversalTableViewScope = {
        headers: this.headers,
        rows: this.rows
    };

    public tableComponents: IUniversalTableComponents = {};

    public viewContainerRefs = new Map();

    public currentState: IUniversalTableState = {};
    private state: Subject<IUniversalTableDiffState> = new ReplaySubject(1);
    public stateStream: Observable<IUniversalTableDiffState> = this.state;

    private cellTextClickSubject: Subject<IUniversalTableCellTextClickParams> = new Subject();
    public get cellTextClickStream(): Observable<IUniversalTableCellTextClickParams> {
        return this.cellTextClickSubject;
    }

    private pushNewState(newState: IUniversalTableState): void {
        if (newState.offset === this.currentState.offset) {
            delete newState.offset;
        }
        if (newState.limit === this.currentState.limit) {
            delete newState.limit;
        }
        Object.assign(this.currentState, newState);
        this.state.next({
            currentState: this.currentState,
            difference: newState
        });
    }

    public constructor(
        public initParams: IUniversalTableCtrlInitParams,
        private config: IUniversalTableConfigurator,
        private componentFactoryResolver: ComponentFactoryResolver,
    ) {
        super();

        const subscribeOnHeaders = this.state
            .filter(next => {
                if (next.difference.hasOwnProperty('viewFields')) {
                    return true;
                }
                return false;
            })
            .subscribe(next => {
                this.initParams.dataProvider.getHeaders({
                    viewFields: next.currentState.viewFields,
                }).subscribe(
                    next => {
                        this.headers.next(next);
                    }
                    );
            });

        const subscribeOnData = this.state.filter(next => {
            if (
                next.difference.hasOwnProperty('filter') ||
                next.difference.hasOwnProperty('fields') ||
                next.difference.hasOwnProperty('sort') ||
                next.difference.hasOwnProperty('offset') ||
                next.difference.hasOwnProperty('limit') ||
                next.difference.hasOwnProperty('viewFields')
            ) {
                return true;
            }

            return false;
        }).subscribe(
            nextState => {
                this.rows.next(null);
                const dataQuery: IUniversalTableQueryConfig = {};
                if (nextState.currentState.hasOwnProperty('fields')) {
                    dataQuery.fields = nextState.currentState.fields;
                }
                if (nextState.currentState.hasOwnProperty('filter')) {
                    dataQuery.filter = nextState.currentState.filter;
                }
                if (nextState.currentState.hasOwnProperty('sort')) {
                    dataQuery.sort = [];
                    for (const sortItem of nextState.currentState.sort) {
                        dataQuery.sort.push({
                            field: sortItem.field,
                            direction: sortItem.direction
                        });
                    }
                }
                if (nextState.currentState.hasOwnProperty('offset')) {
                    dataQuery.page = nextState.currentState.offset / nextState.currentState.limit;
                }
                if (nextState.currentState.hasOwnProperty('limit')) {
                    dataQuery.pageLimit = nextState.currentState.limit;
                }
                this.initParams.dataProvider.getData(dataQuery).map(next => {
                    return {
                        data: next.data,
                        viewFields: nextState.currentState.viewFields
                    };
                }).subscribe(
                    next => {
                        this.rows.next(next);
                    }
                    );
            }
            );
        this.config = config;
        this.initParams = initParams;

        if (!this.initParams.hasOwnProperty('componentsMap')) {
            this.initParams.componentsMap = {};
        }
        Object.assign(
            this.tableComponents,
            this.config.getDefaultComponents(),
            this.initParams.componentsMap
        );
        Object.assign(
            this.currentState,
            this.config.getDefaultInitTableState(),
            this.initParams.initState
        );

        this.pushNewState({...this.config.getDefaultInitTableState(), ...this.initParams.initState});
    }

    public updateState(
        state: IUniversalTableState
    ) {
        this.pushNewState(state);
    }

    public renderContainer(container: ViewContainerRef, parentInjector: Injector): void {
        const componentKey = this.tableComponents.container;
        const componentFactory = this.config.componentsStorage.get(componentKey);

        const injector = ReflectiveInjector.resolveAndCreate(
            [{
                provide: IUniversalTableCtrl,
                useValue: this
            }],
            parentInjector
        );
        const component = this.componentFactoryResolver.resolveComponentFactory(componentFactory as any);
        container.createComponent(component, 0, injector);
    }

    public renderPaginator(container: ViewContainerRef, parentInjector: Injector): void {
        const componentKey = this.tableComponents.paginator;
        const componentFactory = this.config.componentsStorage.get(componentKey);

        const injector = ReflectiveInjector.resolveAndCreate(
            [{
                provide: IUniversalTableCtrl,
                useValue: this
            }],
            parentInjector
        );
        const component = this.componentFactoryResolver.resolveComponentFactory(componentFactory as any);
        container.createComponent(component, 0, injector);
    }

    public renderCell(container: ViewContainerRef, parentInjector: Injector, cellPosition: IUniversalTableCellPosition): void {
        const componentKey = this.tableComponents.cell;
        const componentFactory = this.config.componentsStorage.get(componentKey);

        const clickWrapper: IUniversalTableCellTextClickEmitter = {
            emit: (params: IUniversalTableCellTextClickParams) => {
                this.cellTextClickSubject.next(params);
            }
        };

        const injector = ReflectiveInjector.resolveAndCreate(
            [{
                provide: IUniversalTableCtrl,
                useValue: this
            }, {
                provide: IUniversalTableCellPosition,
                useValue: cellPosition
            }, {
                provide: IUniversalTableCellTextClickEmitter,
                useValue: clickWrapper
            }],
            parentInjector
        );
        const component = this.componentFactoryResolver.resolveComponentFactory(componentFactory as any);
        container.createComponent(component, 0, injector);
    }

    public renderGrid(container: ViewContainerRef, parentInjector: Injector): void {
        const componentKey = this.tableComponents.grid;
        const componentFactory = this.config.componentsStorage.get(componentKey);

        const injector = ReflectiveInjector.resolveAndCreate(
            [{
                provide: IUniversalTableCtrl,
                useValue: this
            }],
            parentInjector
        );
        const component = this.componentFactoryResolver.resolveComponentFactory(componentFactory as any);
        container.createComponent(component, 0, injector);
    }

    public renderHeader(container: ViewContainerRef, parentInjector: Injector, headerData: IUniversalTableHeaderData): void {
        const componentKey = this.tableComponents.headers;
        const componentFactory = this.config.componentsStorage.get(componentKey);

        const injector = ReflectiveInjector.resolveAndCreate(
            [{
                provide: IUniversalTableCtrl,
                useValue: this
            }, {
                provide: IUniversalTableHeaderData,
                useValue: headerData
            }],
            parentInjector
        );
        const component = this.componentFactoryResolver.resolveComponentFactory(componentFactory as any);
        container.createComponent(component, 0, injector);
    }
}

export { UniversalTableCtrl };