import {
    IUniversalTableCountQuery,
    IUniversalTableDataProperty,
    IUniversalTableDataProvider,
    IUniversalTableGetDataResponse,
    IUniversalTableGetHeadersQuery,
    IUniversalTableGetHeadersResponse,
    IUniversalTableQueryConfig
} from "coreContracts/universalTable/IUniversalTableDataProvider";
import { IUniversalTableDataProviderFactory } from "coreContracts/universalTable/IUniversalTableDataProviderFactory";
import { Observable } from "rxjs/Observable";
import { IDataSource } from "../../../common/IDataSource/IDataSource";

class UniversalTableDataProvider implements IUniversalTableDataProvider {
    public constructor(
        private dataSource: IDataSource,
        private sourceEntity: string
    ) {
        this.dataSource = dataSource;
    }

    public getData(query: IUniversalTableQueryConfig): Observable<IUniversalTableGetDataResponse> {
        return Observable
            .zip(
            this.dataSource.getMetadata({ entityName: this.sourceEntity }).map(next => {
                return {
                    metadata: next.properties
                };
            }),
            this.dataSource.read({
                entity: this.sourceEntity,
                fields: query.fields,
                offset: query.page * query.pageLimit,
                limit: query.pageLimit
            })
            )
            .map(next => {
                return {...next[0], ...next[1]};
            })
            .map(next => {
                const result: IUniversalTableGetDataResponse = {
                    data: []
                };
                let searchFields: Array<string>;
                if (query.fields.length === 0) {
                    searchFields = [];
                } else {
                    searchFields = query.fields;
                }
                for (const dataRow of next.entities) {
                    const resultRow: Map<string, IUniversalTableDataProperty> = new Map();

                    dataRow.entity.forEach((value, key) => {
                        const type = next.metadata.get(key);
                        let resultVal;
                        switch (type.type) {
                            case 'Guid':
                                resultVal = {
                                    value: value.value,
                                    referenceDisplayName: value['referenceName'],
                                    referenceEntity: type.reference
                                };
                                break;
                            default:
                                resultVal = value.value;

                        }
                        resultRow.set(key, {
                            value: resultVal,
                            type: type.type
                        });
                    });
                    result.data.push(resultRow);
                }
                return result;
            });
    }

    public getHeaders(query: IUniversalTableGetHeadersQuery): Observable<IUniversalTableGetHeadersResponse> {
        const response: IUniversalTableGetHeadersResponse = {
            headers: []
        };
        for (const field of query.viewFields) {
            response.headers.push({ label: field, type: `defaultField` });
        }

        return Observable.of(response);
    }

    public getCount(query: IUniversalTableCountQuery): Observable<number> {
        return this.dataSource.count({ entity: this.sourceEntity, filter: query.filter });
    }
}

class UniversalTableDataProviderFactory extends IUniversalTableDataProviderFactory {
    public createInstanceFromDataSource(dataSource: IDataSource, entityIdentifier: string): Observable<UniversalTableDataProvider> {
        return Observable.of(new UniversalTableDataProvider(dataSource, entityIdentifier));
    }

}

export { UniversalTableDataProvider, IUniversalTableQueryConfig, IUniversalTableCountQuery, IUniversalTableGetHeadersResponse, IUniversalTableGetDataResponse, IUniversalTableGetHeadersQuery, UniversalTableDataProviderFactory };