import { IDictionary } from "@itexpert-dev/i-dictionary";
import { KeyValueStorage } from "@itexpert-dev/key-value-storage";
import { IUniversalTableConfigurator } from "coreContracts/universalTable/IUniversalTableConfigurator";
import { IUniversalTableComponents } from "coreContracts/universalTable/IUniversalTableDefaultComponents";
import { IUniversalTableState } from "coreContracts/universalTable/IUniversalTableState";

class UniversalTableConfigurator extends IUniversalTableConfigurator {
    public constructor() {
        super();

        this.cellTypeToComponentMap.add({ key: 'primary', value: 'primaryCell' });
    }
    public componentsStorage = new Map();
    public cellsStorage = new Map();

    public cellTypeToComponentMap = new KeyValueStorage<string>();

    protected defaultInitTableState: IUniversalTableState;
    protected componentsScope: IDictionary<any>;

    public setDefaultInitTableState(newState: IUniversalTableState) {
        this.defaultInitTableState = newState;
    }
    public getDefaultInitTableState(): IUniversalTableState {
        return this.defaultInitTableState;
    }

    private defaultComponents: IUniversalTableComponents;
    public setDefaultComponents(components: IUniversalTableComponents) {
        this.defaultComponents = components;
    }
    public getDefaultComponents(): IUniversalTableComponents {
        return this.defaultComponents;
    }

    private defaultLimitSteps: Array<number>;
    public setDefaultLimitSteps(...steps: Array<number>) {
        this.defaultLimitSteps = steps;
    }
    public getDefaultLimitSteps(): Array<number> {
        return this.defaultLimitSteps;
    }
}

export { UniversalTableConfigurator };