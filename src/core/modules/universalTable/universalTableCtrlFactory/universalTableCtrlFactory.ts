import { ComponentFactoryResolver, Injectable } from "@angular/core";
import { IUniversalTableConfigurator } from "coreContracts/universalTable/IUniversalTableConfigurator";
import { IUniversalTableCtrlInitParams } from "coreContracts/universalTable/IUniversalTableCtrlInitParams";
import { IUniversalTableCtrlFactory } from "coreContracts/universalTable/IUniversalTableService";
import { UniversalTableCtrl } from "../universalTableCtrl";

@Injectable()
class UniversalTableCtrlFactory extends IUniversalTableCtrlFactory {
    public constructor(
        private config: IUniversalTableConfigurator,
        private componentFactoryResolver: ComponentFactoryResolver
    ) {
        super();
    }

    public createInstance(initParams: IUniversalTableCtrlInitParams) {
        return new UniversalTableCtrl(
            initParams,
            this.config,
            this.componentFactoryResolver
        );
    }
}

export { UniversalTableCtrlFactory };