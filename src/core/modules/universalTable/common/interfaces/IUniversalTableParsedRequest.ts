import { IUniversalTableGetDataResponse } from "coreContracts/universalTable/IUniversalTableGetDataResponse";
import { IUniversalTableState } from "coreContracts/universalTable/IUniversalTableState";

interface IUniversalTableParsedRequest extends IUniversalTableGetDataResponse {
    state: IUniversalTableState;
    viewFields: Array<string>;
}

export { IUniversalTableParsedRequest };