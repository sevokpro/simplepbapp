import { IUniversalTableViewCell } from "./IUniversalTableViewCell";

interface IUniversalTableViewRow {
    cells: Array<IUniversalTableViewCell>;
}

export { IUniversalTableViewRow };