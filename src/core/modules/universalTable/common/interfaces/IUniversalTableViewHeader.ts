interface IUniversalTableViewHeader {
    label: string;
    instance?: any;
}
export { IUniversalTableViewHeader };