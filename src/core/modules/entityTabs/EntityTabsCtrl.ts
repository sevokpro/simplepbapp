import { isNullOrUndefined, isUndefined } from "@itexpert-dev/tiny-helpers";
import { Observable } from "rxjs/Observable";
import { ReplaySubject } from "rxjs/ReplaySubject";

abstract class IEntityTabsArgs {
    public abstract tabs: Array<{
        verboseName: Observable<string>,
        entityIdentifier: string
    }>;
}

interface ITabModel {
    verboseName: Observable<string>;
    entityIdentifier: string;
    isActive: boolean;
}

class EntityTabsCtrl {
    public tabsModel: Array<ITabModel> = [];
    private updateTabStream = new ReplaySubject<{ entityIdentifier: string }>(1);
    private entityNameToModelMap: Map<string, ITabModel> = new Map();

    public constructor(
        private tabs: IEntityTabsArgs,
        private entityName: string
    ) {
        //TODO: add method for find duplicates entityIdentifiers
        tabs.tabs.map(val => {
            const tabModel: ITabModel = {
                entityIdentifier: val.entityIdentifier,
                verboseName: val.verboseName,
                isActive: false
            };
            this.entityNameToModelMap.set(val.entityIdentifier, tabModel);
            this.tabsModel.push(tabModel);
        });

        if (isUndefined(this.entityName)) {
            this.goToFirstTab();
        } else {
            this.cleanActiveTab();

            const findResult: ITabModel = this.tabsModel.find(val => {
                return val.entityIdentifier === this.entityName;
            });

            if (!isNullOrUndefined(findResult)) {
                this.changeTabTo(findResult.entityIdentifier);
            } else {
                this.goToFirstTab();
            }
        }
    }

    private cleanActiveTab() {
        this.tabsModel.map(val => {
            val.isActive = false;
        });
    }

    public changeTabTo(entityIdentifier: string) {
        this.cleanActiveTab();
        this.entityNameToModelMap.get(entityIdentifier).isActive = true;
        this.updateTabStream.next({ entityIdentifier: entityIdentifier });
    }

    private goToFirstTab() {
        this.changeTabTo(this.tabsModel[0].entityIdentifier);
    }

    public changeTabStream(): Observable<{ entityIdentifier: string }> {
        return this.updateTabStream;
    }
}

export { EntityTabsCtrl, IEntityTabsArgs };