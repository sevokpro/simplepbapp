import { Injectable } from "@angular/core";
import { isUndefined } from "@itexpert-dev/tiny-helpers";
import { IBackendSource } from "coreContracts/backendSource";
import { Observable } from "rxjs/Observable";
import { IEntityTabsArgs } from "./EntityTabsCtrl";

@Injectable()
class EntityTabsDataProvider {
    private entitiesSource: Observable<Array<{ entityName: string, verboseName: string }>>;
    public constructor(dataProvider: IBackendSource) {
        this.entitiesSource = dataProvider.getAvailableEntities();
    }

    public getEntityTabsByWorkspaceFilter(filter: string): Observable<IEntityTabsArgs> {
        return this.entitiesSource.map(next => {
            const result: IEntityTabsArgs = {
                tabs: []
            };

            const entities = [
                'Документ.ITEXP_Инцидент',
                'Документ.ITEXP_ЗапросНаИзменение',
                'Документ.ITEXP_ЗапросНаОбслуживание'
            ];

            for (const entityName of entities) {
                const entityFindResult = next.find(entity => entity.entityName === entityName);
                if (isUndefined(entityFindResult)) {
                    throw new Error(`${entityName} not found!`);
                } else {
                    result.tabs.push({
                        entityIdentifier: entityFindResult.entityName,
                        verboseName: Observable.of(entityFindResult.verboseName)
                    });
                }
            }

            return result;
        });
    }
}

export { EntityTabsDataProvider };