import { Component } from "@angular/core";
import { EntityTabsCtrl } from "./EntityTabsCtrl";

@Component({
    templateUrl: './EntityTabsComponent.template.pug',
    styleUrls: [
        './EntityTabsComponent.style.styl'
    ]
})
class EntityTabsComponent {
    public tabsModel;
    public constructor(
        private entityTabsCtrl: EntityTabsCtrl
    ) {
        this.tabsModel = entityTabsCtrl.tabsModel;
    }

    private changeTabTo(entityIdentifier: string) {
        this.entityTabsCtrl.changeTabTo(entityIdentifier);
    }
}

export { EntityTabsComponent };