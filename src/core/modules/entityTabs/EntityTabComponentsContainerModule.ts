import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { EntityTabsDataProvider } from "src/core/modules/entityTabs/EntityTabsDataProvider";
import { EntityTabsComponent } from "./EntityTabsComponent";

@NgModule({
    imports: [
        CommonModule
    ],
    providers: [
        EntityTabsDataProvider
    ],
    declarations: [
        EntityTabsComponent
    ],
    entryComponents: [
        EntityTabsComponent
    ]
})
class EntityTabComponentsContainerModule { }

export { EntityTabComponentsContainerModule };