import { ComponentFactoryResolver, Injectable, Injector, ReflectiveInjector, ViewContainerRef } from "@angular/core";
import { IEntityTabs, IEntityTabsFactory } from "coreContracts/entityTabs";
import { Observable } from "rxjs/Observable";
import { EntityTabsComponent } from "./EntityTabsComponent";
import { EntityTabsCtrl, IEntityTabsArgs } from "./EntityTabsCtrl";
import { EntityTabsDataProvider } from "./EntityTabsDataProvider";

class EntityTabs implements IEntityTabs {
    private entityTabsCtrl: EntityTabsCtrl;

    public constructor(
        private componentFactoryResolver: ComponentFactoryResolver,
        private tabs: IEntityTabsArgs,
        private entityName: string
    ) {
        this.entityTabsCtrl = new EntityTabsCtrl(this.tabs, this.entityName);
    }

    public render(viewContainer: ViewContainerRef, injector: Injector) {
        const component = this.componentFactoryResolver.resolveComponentFactory(EntityTabsComponent);
        const newInjector = ReflectiveInjector.resolveAndCreate(
            [{
                provide: EntityTabsCtrl,
                useValue: this.entityTabsCtrl
            }],
            injector
        );
        viewContainer.createComponent(component, 0, newInjector);
    }

    public subscriptionOnChangeTab(): Observable<{ entityIdentifier: string; }> {
        return this.entityTabsCtrl.changeTabStream();
    }
}

@Injectable()
class EntityTabsFactory extends IEntityTabsFactory {
    constructor(
        private componentFactoryResolver: ComponentFactoryResolver,
        private entityTabsDataProvider: EntityTabsDataProvider
    ) {
        super();
    }

    public createEntityTabs(filter: string, entityName: string): Observable<IEntityTabs> {
        return this.entityTabsDataProvider
            .getEntityTabsByWorkspaceFilter(filter)
            .map(entityTabCollection => {
                return new EntityTabs(
                    this.componentFactoryResolver,
                    entityTabCollection,
                    entityName
                );
            });
    }
}

export { EntityTabsFactory };