abstract class WhenSelectComponentProvider{
    public abstract provide(component: string);
}

export {WhenSelectComponentProvider};