abstract class WhenSelectBufferElementProvide{
    public abstract provide(bufferKey: string);
}

export {WhenSelectBufferElementProvide};