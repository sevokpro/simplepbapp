import { ComponentFactoryResolver, Injector, ReflectiveInjector, ViewContainerRef } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { Subject } from "rxjs/Subject";
import { WhenSelectBufferElementProvide } from "./common/whenSelectBufferElementProvide";
import { WhenSelectComponentProvider } from "./common/whenSelectComponentProvider";
import { ConstructorAddElementButtonComponent } from "./constructorAddElementButton.component";

class ConstructorAddElementButton{
    private whenSelectComponentSubject: Subject<string> = new Subject();

    constructor( private componentFactoryResolver: ComponentFactoryResolver) {}

    private selectComponentProvider: WhenSelectComponentProvider = {
        provide: next => this.whenSelectComponentSubject.next(next)
    };

    private whenSelectBuferElement: Subject<string> = new Subject();
    private whenSelectBufferElementProvide: WhenSelectBufferElementProvide = {
        provide: bufferKey => this.whenSelectBuferElement.next(bufferKey)
    };

    public render(viewContainerRef: ViewContainerRef, injector: Injector) {
        const injectorForAddElement = ReflectiveInjector
            .resolveAndCreate(
                [{
                    provide: WhenSelectComponentProvider,
                    useValue: this.selectComponentProvider
                }, {
                    provide: WhenSelectBufferElementProvide,
                    useValue: this.whenSelectBufferElementProvide
                }],
                injector
            );
        const componentInstance = this.componentFactoryResolver.resolveComponentFactory(ConstructorAddElementButtonComponent);
        viewContainerRef.createComponent(componentInstance, 0, injectorForAddElement);
    }

    public whenSelectComponent(): Observable<string> {
        return this.whenSelectComponentSubject.asObservable();
    }

    public whenSelectBufferSheme(): Observable<string> {
        return this.whenSelectBuferElement.asObservable();
    }
}

export {ConstructorAddElementButton};