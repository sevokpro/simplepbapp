import { Component } from "@angular/core";
import { IPageBuilderComponentRegistry } from "coreContracts/pageBuilder/IPageBuilderComponentRegistry";
import { ReplaySubject } from "rxjs/ReplaySubject";
import { Subject } from "rxjs/Subject";
import { PageBuilderConstructorShemaPathBuffer } from "../pageBuilderConstructorShemaPathBuffer";
import { WhenSelectComponentProvider } from "./common/whenSelectComponentProvider";
import {WhenSelectBufferElementProvide} from "./common/whenSelectBufferElementProvide";

@Component({
    templateUrl: './constructorAddElementButton.template.pug'
})
class ConstructorAddElementButtonComponent{
    public components: Array<string>;
    public whenClickElement: Subject<string> = new Subject();

    public whenPlusButtonClick: Subject<null> = new Subject();
    public isDropdownOpen: Subject<boolean> = new Subject();

    public bufferedElements: Subject<Array<string>> = new ReplaySubject(1);
    public whenSelectBufferElement: Subject<string> = new Subject();

    constructor(
        componentRegister: IPageBuilderComponentRegistry,
        whenSelectComponentProvider: WhenSelectComponentProvider,
        shemaBuffer: PageBuilderConstructorShemaPathBuffer,
        whenSelectBufferElementProvide: WhenSelectBufferElementProvide
    ) {
        this.whenSelectBufferElement.subscribe(
            next => whenSelectBufferElementProvide.provide(next)
        )

        this.components = componentRegister.getRegisteredComponents();
        this.whenClickElement.subscribe(
            next => whenSelectComponentProvider.provide(next)
        );

        this
            .whenPlusButtonClick
            .withLatestFrom(this.isDropdownOpen)
            .map(([click, dropdownState]) => !dropdownState)
            .multicast(this.isDropdownOpen)
            .connect();
        this.isDropdownOpen.next(false);

        this.isDropdownOpen
            .switchMap( next => shemaBuffer.getSavedShemeKeys())
            .multicast(this.bufferedElements)
            .connect();
    }

    public setComponent(componentName: string) {
        this.whenClickElement.next(componentName);
    }
}

export {ConstructorAddElementButtonComponent};