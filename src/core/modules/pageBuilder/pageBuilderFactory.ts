import { Injectable } from "@angular/core";
import { isNullOrUndefined } from "@itexpert-dev/tiny-helpers";
import { IPageBuilder } from "coreContracts/pageBuilder/IPageBuilder";
import { IPageBuilderComponentRegistry } from "coreContracts/pageBuilder/IPageBuilderComponentRegistry";
import { IPageBuilderFactory, IPageComponentDefenition } from "coreContracts/pageBuilder/IPageBuilderFactory";
import { IPageBuilderVariableRegistry } from "coreContracts/pageBuilder/IPageBuilderVariableRegister";
import { PageBuilder } from "./pageBuilder";

@Injectable()
class PageBuilderFactory extends IPageBuilderFactory{
    constructor(
        private componentRegister: IPageBuilderComponentRegistry,
        private variableRegister: IPageBuilderVariableRegistry
    ) {
        super();
    }

    public create(pageCreateParameters: IPageComponentDefenition): IPageBuilder {
        const componentName = pageCreateParameters.component;
        if (isNullOrUndefined(this.componentRegister.getComponentMetadata(componentName))) {
            throw new Error(`component with name: ${componentName} is not registred`);
        }

        return new PageBuilder(pageCreateParameters, this.componentRegister, this.variableRegister);
    }
}

export {PageBuilderFactory};