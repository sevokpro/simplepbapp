import { ChangeDetectorRef, ComponentFactoryResolver, Injector, ViewContainerRef } from "@angular/core";
import { async, TestBed } from "@angular/core/testing";
import { IPageBuilder } from "coreContracts/pageBuilder/IPageBuilder";
import { IPageComponentDefenition } from "coreContracts/pageBuilder/IPageBuilderFactory";
import { IPageBuilderVariableRegistry } from "coreContracts/pageBuilder/IPageBuilderVariableRegister";
import { Observable } from "rxjs/Observable";
import { EnvComponent } from "./common/testEnvComponent";
import { EnvModule } from "./common/testEnvModule";
import { SimpleComponent } from "./common/testSimpleComponent";
import { PageBuilder } from "./pageBuilder";
import { PageBuilderComponentRegistry } from "./pageBuilderComponentRegistry";
import { PageBuilderVariableRegister } from "./pageBuilderVariableRegister";

describe('page builder service tests', () => {
    let pageBuilder: PageBuilder;
    let vcr: ViewContainerRef;
    let injector: Injector;

    const simpleMetadata: IPageComponentDefenition = {
        component: 'simple',
    };

    beforeEach(done => {
        TestBed
            .resetTestingModule()
            .configureTestingModule({
                declarations: [],
                imports: [EnvModule]
            });
        const cfr = TestBed.get(ComponentFactoryResolver);

        const componentRegister = new PageBuilderComponentRegistry();
        const variableRegistry = new PageBuilderVariableRegister();
        componentRegister.registerComponent({
            componentAlias: 'simple',
            factoryMethod: parameters => {
                return {
                    render: (vcr, inj) => {
                        vcr.createComponent(
                            cfr.resolveComponentFactory(SimpleComponent),
                            0,
                            inj
                        );
                        return Observable.of(null);
                    },
                    getExposeRef: alias => {
                        return undefined;
                    }
                };
            }
        });
        pageBuilder = new PageBuilder(simpleMetadata, componentRegister, variableRegistry);
        injector = TestBed.get(Injector);
        const envCmp = TestBed.createComponent(EnvComponent);
        envCmp.whenStable().then(() => {
            vcr = envCmp.componentInstance.viewContainerReference;
            done();
        }).catch(err => console.log('error: \n', err));
    });

    it('check that page builer instance of interface', () => {
        expect(pageBuilder instanceof IPageBuilder).toBeTruthy();
    });

    it('check that render return observable', () => {
        expect(pageBuilder.render(vcr, injector) instanceof Observable).toBeTruthy();
    });

    it('vcr must be clear', () => {
        expect(vcr.length).toBe(0);
    });

    it('after render container length must equal 1', async(() => {
        pageBuilder.render(vcr, injector).subscribe(
            next => {
                expect(vcr.length).toBe(1);
            }
        );
    }));

    it('after publish event must return instance of page builder', () => {
        const result = pageBuilder.publishEvent({event: 'lol'});
        expect(result instanceof IPageBuilder).toBeTruthy();
    });
});