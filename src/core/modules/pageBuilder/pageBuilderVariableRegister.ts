import {
    IPageBuilderVariableMetadata,
    IPageBuilderVariableRegistry
} from "coreContracts/pageBuilder/IPageBuilderVariableRegister";

class PageBuilderVariableRegister extends IPageBuilderVariableRegistry{
    private variableMetadataStorage: Map<string, IPageBuilderVariableMetadata> = new Map();

    public getRegistryVariableMetadata(alias: string): IPageBuilderVariableMetadata {
        return this.variableMetadataStorage.get(alias);
    }

    public registerVariable(variableMetadata: IPageBuilderVariableMetadata): IPageBuilderVariableRegistry {
        this.variableMetadataStorage.set(variableMetadata.alias, variableMetadata);
        return this;
    }
}

export {PageBuilderVariableRegister};