import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { IPageBuilderComponentRegistry } from "coreContracts/pageBuilder/IPageBuilderComponentRegistry";
import { IPageBuilderVariableRegistry } from "coreContracts/pageBuilder/IPageBuilderVariableRegister";
import { RenderableInlineBarMetadata } from "src/core/modules/pageBuilder/renderableComponents/renderableInlineBar/renderableInlineBarMetadata";
import { ConstructorAddElementButtonComponent } from "./constructorAddElementButton/constructorAddElementButton.component";
import { ConstructSafeContainerComponent } from "./constructSafeContainer/constructSafeContainer.component";
import { PageBuilderButtonDrawerAdapterMetadataProvider } from "./renderableComponents/buttonDrawerRegister/pageBuilderButtonDrawerMetadataProvider";
import { EmptyMockElementMetadata } from "./renderableComponents/emptyMockElementRegister/emptyMockElementMetadata";
import { EmptyMockElementComponent } from "./renderableComponents/emptyMockElementRegister/emtyMockElement.component";
import { PageBuilderGridAdapterMetadataProvider } from "./renderableComponents/gridRegister/pageBuilderGridAdapterMetadataProvider";
import { ContentContainerComponent } from "./renderableComponents/renderableContainer/contentContainer.component";
import { RenderableContentContainerMetadataProvder } from "./renderableComponents/renderableContainer/renderableContentContainerMetadataProvder";
import { InlineBarComponent } from "./renderableComponents/renderableInlineBar/inlineBar.component";
import { SwitchContainerComponent } from "./renderableComponents/switchContainer/switchContainer.component";
import { SwitchContainerRegisterMetadata } from "./renderableComponents/switchContainer/switchContainerRegisterMetadata";
import { TabsDrawerRegisterMetadata } from "./renderableComponents/tabsDrawerRegister/tabsDrawerRegisterMetadata";
import { QueryParamsVariableMetadata } from "./variables/queryParams/queryParamsMetadata";

@NgModule({
    imports: [CommonModule, FormsModule],
    providers: [
        PageBuilderGridAdapterMetadataProvider,
        EmptyMockElementMetadata,
        PageBuilderButtonDrawerAdapterMetadataProvider,
        TabsDrawerRegisterMetadata,
        QueryParamsVariableMetadata,
        SwitchContainerRegisterMetadata,
        RenderableContentContainerMetadataProvder,
        SwitchContainerRegisterMetadata,
        RenderableInlineBarMetadata
    ],
    declarations: [
        EmptyMockElementComponent,
        ContentContainerComponent,
        InlineBarComponent,
        ConstructSafeContainerComponent,
        SwitchContainerComponent,
        ConstructorAddElementButtonComponent
    ],
    entryComponents: [
        EmptyMockElementComponent,
        ContentContainerComponent,
        InlineBarComponent,
        ConstructSafeContainerComponent,
        SwitchContainerComponent,
        ConstructorAddElementButtonComponent
    ]
})
class PageBuilderModule{
    constructor(
        pageBuilderComponentRegistry: IPageBuilderComponentRegistry,
        pageBuilderVariableRegistry: IPageBuilderVariableRegistry,
        pageBuilderGridAdapterMetadata: PageBuilderGridAdapterMetadataProvider,
        emptyMockElementMetadata: EmptyMockElementMetadata,
        pageBuilderButtonDrawerAdapterMetadata: PageBuilderButtonDrawerAdapterMetadataProvider,
        tabsMetaAdapterMetadata: TabsDrawerRegisterMetadata,
        queryParamsVariable: QueryParamsVariableMetadata,
        switchContainerMetadata: SwitchContainerRegisterMetadata,
        inlineBarMetadata: RenderableInlineBarMetadata,
        contentContainerMetadata: RenderableContentContainerMetadataProvder
    ) {
        pageBuilderComponentRegistry
            .registerComponent(pageBuilderGridAdapterMetadata.getMetadata())
            .registerComponent(emptyMockElementMetadata.getMetadata())
            .registerComponent(tabsMetaAdapterMetadata.getMetadata())
            .registerComponent(pageBuilderButtonDrawerAdapterMetadata.getMetadata())
            .registerComponent(switchContainerMetadata.getMetadata())
            .registerComponent(contentContainerMetadata.getMetadata())
            .registerComponent(inlineBarMetadata);

        pageBuilderVariableRegistry
            .registerVariable(queryParamsVariable.getMetadata());
    }

}

export { PageBuilderModule };