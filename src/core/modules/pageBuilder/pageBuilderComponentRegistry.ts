import {
    IComponentMetadataRegistry,
    IPageBuilderComponentRegistry
} from "coreContracts/pageBuilder/IPageBuilderComponentRegistry";


class PageBuilderComponentRegistry extends IPageBuilderComponentRegistry{
    private register: Map<string, IComponentMetadataRegistry> = new Map();

    public registerComponent(metadata: IComponentMetadataRegistry): IPageBuilderComponentRegistry {
        this.register.set(metadata.componentAlias, metadata);
        return this;
    }
    public getComponentMetadata(componentAlias: string): IComponentMetadataRegistry {
        return this.register.get(componentAlias);
    }

    public getRegisteredComponents(): Array<string> {
        return Array.from(this.register.keys());
    }
}

export {PageBuilderComponentRegistry};