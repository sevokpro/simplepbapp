import { TestBed } from "@angular/core/testing";
import {
    IComponentMetadataRegistry,
    IPageBuilderComponentRegistry, IRenderablePageElement
} from "coreContracts/pageBuilder/IPageBuilderComponentRegistry";
import { Observable } from "rxjs/Observable";
import { PageBuilderComponentRegistry } from "./pageBuilderComponentRegistry";

describe('tests for page builder registry', () => {
    let pageBuilderRegistry: PageBuilderComponentRegistry;
    const simpleMetadata: IComponentMetadataRegistry = {
        componentAlias: 'simpleComponent',
        factoryMethod: parameters => {
            const renderableElement: IRenderablePageElement = {
                render: (viewContainer, injector) => {
                    return Observable.of(null);
                },
                getExposeRef: refAlias => undefined
            };
            return renderableElement;
        }
    };

    beforeEach(done => {
        TestBed
            .resetTestingModule()
            .configureTestingModule({
                providers: [PageBuilderComponentRegistry]
            });
        pageBuilderRegistry = TestBed.get(PageBuilderComponentRegistry);
        done();
    });

    it('page builder not undefined', () => {
       expect(pageBuilderRegistry).toBeDefined();
    });

    it('page builder must extend interface', () => {
        expect(pageBuilderRegistry instanceof IPageBuilderComponentRegistry).toBeTruthy();
    });

    it('registry component don`t throw error', () => {
        expect(() => pageBuilderRegistry.registerComponent(simpleMetadata))
            .not.toThrowError();
    });

    it('get metadata not throw error', () => {
        expect(() => pageBuilderRegistry.getComponentMetadata('al')).not.toThrowError();
    });
});