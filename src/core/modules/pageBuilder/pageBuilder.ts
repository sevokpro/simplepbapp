import { ChangeDetectorRef, ComponentRef, Injector, ViewContainerRef } from "@angular/core";
import { isNull, isNullOrUndefined } from "@itexpert-dev/tiny-helpers";
import { IListenEvent, IPageBuilder, IPageBuilderEvent } from "coreContracts/pageBuilder/IPageBuilder";
import {
    IComponentMetadataRegistry,
    IPageBuilderComponentRegistry, IPageBuilderParametersMetadataAvailableTypes, IRenderablePageElement
} from "coreContracts/pageBuilder/IPageBuilderComponentRegistry";
import {
    IPageComponentDefenition
} from "coreContracts/pageBuilder/IPageBuilderFactory";
import {
    IPageBuilderVariableRegistry,
    IVariableAttributes
} from "coreContracts/pageBuilder/IPageBuilderVariableRegister";
import { Observable } from "rxjs/Observable";

interface IRenderableComponentSandbox{
    input?: (next: IPageBuilderEvent) => void;
    output?: Observable<IPageBuilderEvent>;
}

class PageBuilder extends IPageBuilder{
    private anonymousSandBoxes: Array<IRenderableComponentSandbox> = [];
    private namedSandBoxes: Map<string, IRenderableComponentSandbox> = new Map();
    private messageStash: Map<string, Array<IPageBuilderEvent>> = new Map();

    constructor(
        protected metadata: IPageComponentDefenition,
        private componentRegistry: IPageBuilderComponentRegistry,
        private variableRegistry: IPageBuilderVariableRegistry,
    ) {
        super();
    }

    public subscribeOnEvents(): Observable<IListenEvent> {
        return Observable.of(null)
        // return Observable
        //     .merge(
        //         ...this.anonymousSandBoxes.map( el => el.output) as any
        //     );

    }

    protected fixMetadata(metadata: IPageComponentDefenition): Observable<IPageComponentDefenition> {
        const metadataCopy = {...metadata};
        delete metadataCopy.bindExternalComponent;
        const strMeta = JSON.stringify(metadataCopy);
        const paramsReqexp = /\$[\w,\?,\=,\,]+/g;
        const variables = strMeta.match(paramsReqexp);

        const asyncQueries: Array<Observable<any>> = [];
        if (!isNull(variables)) {
            const errors: Array<string> = [];
            variables.forEach(value => {
                const [alias, params] = value.split('?');
                const metadata = this.variableRegistry.getRegistryVariableMetadata(alias);
                if (isNullOrUndefined(metadata)) {
                    errors.push(`alias for variabe ${alias} not found`);
                }

                const variableCallParams: IVariableAttributes = {};
                if (!isNullOrUndefined(params)) {
                    params
                        .split(',')
                        .map(next => next.split('='))
                        .forEach(([property, value]) => {
                            variableCallParams[property] = value;

                        });
                }
                asyncQueries.push(metadata.value(variableCallParams).catch(err => {
                    return Observable.throw(`Error when try resolve variable "${alias}" with params "${params}"\n${err}`);
                }));
            });

            if (errors.length > 0) {
                throw new Error(errors.join('\n'));
            }
        }
        if (asyncQueries.length > 0) {
            return Observable
                .zip(...asyncQueries)
                .map(next => {
                    let iterator = 0;
                    const newMeta = strMeta.replace(paramsReqexp, () => next[iterator++]);
                    const fixedMetadata: IPageComponentDefenition = JSON.parse(newMeta);
                    fixedMetadata.bindExternalComponent = metadata.bindExternalComponent;
                    return fixedMetadata;
                });
        }else {
            return Observable.of(metadata);
        }
    }

    private validateMetadata(jsonData: IPageComponentDefenition, componentMetadata: IComponentMetadataRegistry): Array<Error> {
        const errors: Array<Error> = [];

        const requireParams: {
            [paramKey: string]: boolean
        } = {};
        const typeHash: {
            [paramKey: string]: IPageBuilderParametersMetadataAvailableTypes
        } = {};
        const duplicateCatcher: {
            [paramKey: string]: boolean
        } = {};
        if (!isNullOrUndefined(componentMetadata.parametersMetadata)) {
            Object.keys(componentMetadata.parametersMetadata).forEach(key => {
                if (componentMetadata.parametersMetadata[key].required === true) {
                    requireParams[key] = false;
                }
                typeHash[key] = componentMetadata.parametersMetadata[key].type;
            });
        }
        if (!isNullOrUndefined(jsonData.parameters)) {
            Object.keys(jsonData.parameters).forEach(key => {
                if (!isNullOrUndefined(requireParams[key])) {
                    requireParams[key] = true;
                }
                if (isNullOrUndefined(componentMetadata.parametersMetadata[key])) {
                    errors.push(new Error(`parameter ${key} not found in component ${componentMetadata.componentAlias}`));
                }
                if (isNullOrUndefined(duplicateCatcher[key])) {
                    duplicateCatcher[key] = true;
                }else {
                    errors.push(new Error(`duplicate parameter: "${key}" into component: "${componentMetadata.componentAlias}"`));
                }
            });
        }
        Object.keys(requireParams).forEach(key => {
            if (requireParams[key] === false) {
                errors.push(new Error(`require parameter: "${key}" not defined into component: "${componentMetadata.componentAlias}"`));
            }
        });

        return errors;
    }


    private createComponentSandbox(renderableComponentInstance: IRenderablePageElement): IRenderableComponentSandbox {
        const sandbox: IRenderableComponentSandbox = {};
        if (!isNullOrUndefined(renderableComponentInstance.publishEvent)) {
            sandbox.input = message => renderableComponentInstance.publishEvent(message);
        }
        if (!isNullOrUndefined(renderableComponentInstance.listenEvents)) {
            sandbox.output = renderableComponentInstance.listenEvents();
        }
        return sandbox;
    }

    private registerSandbox(sandbox: IRenderableComponentSandbox, componentAlias: string) {
        this.anonymousSandBoxes.push(sandbox);
        if (!isNullOrUndefined(componentAlias)) {
            this.namedSandBoxes.set(componentAlias, sandbox);
            if (!isNullOrUndefined(sandbox.input) && this.messageStash.has(componentAlias)) {
                const stashMessages = this.messageStash.get(componentAlias).reverse();
                stashMessages.forEach(message => {
                    sandbox.input(message);
                });
            }
        }
    }

    private renderComponent(
        renderableComponentInstance: IRenderablePageElement,
        viewContainerRef: ViewContainerRef,
        injector: Injector,
        componentName: string,
        componentAlias: string
    ): Observable<ComponentRef<any>> {
        const renderProcess = renderableComponentInstance.render(viewContainerRef, injector).do( next => {
            if (isNullOrUndefined(next)) {
                throw new Error(`render method for component: "${componentName}", with unique name: "${componentAlias}" return null element reference`);
            }
        });
        if (isNullOrUndefined(renderProcess)) {
            return Observable.throw(new Error(`render method for component: "${componentName}" with unique name: "${componentAlias}" return undefined`));
        }
        return renderProcess;
    }

    protected getRenderableComponentMetadata(componentName: string): IComponentMetadataRegistry {
        return this.componentRegistry.getComponentMetadata(componentName);
    }

    protected getExposeChildMetadata(metadata: IPageComponentDefenition, childName: string): IPageComponentDefenition {
        return metadata.bindExternalComponent[childName];
    }

    private renderExposeChild(renderablePageElement: IRenderablePageElement, metadata: IPageComponentDefenition, childName: string, injector: Injector): Observable<IRenderablePageElement> {
        const childMetadata = this.getExposeChildMetadata(metadata, childName);
        const whenViewContainerRefChange = renderablePageElement.getExposeRef(childName);
        const renderProcess = whenViewContainerRefChange
            .combineLatest(this.fixMetadata(childMetadata))
            .do(([viewContainerRef, metadata]) => {
                console.log(metadata);
                viewContainerRef.clear();
            })
            .delay(0)
            .switchMap( ([viewContainer, fixedMeta]) => {
                return this.renderableComponentRender( fixedMeta, viewContainer, injector );
            });

        return renderProcess;
    }

    protected renderableComponentRender(metadata: IPageComponentDefenition, viewContainerReference: ViewContainerRef, injector: Injector): Observable<IRenderablePageElement> {
        const renderableComponentName = metadata.component;
        const renderableComponentMetadata = this.getRenderableComponentMetadata(renderableComponentName);
        if (isNullOrUndefined(renderableComponentMetadata)) {
            throw new Error(`not found metadata for component ${renderableComponentName}`);
        }
        const validateResult = this.validateMetadata(metadata, renderableComponentMetadata);
        if (validateResult.length > 0) {
            console.warn(validateResult);
        }

        const renderableComponentInstance = renderableComponentMetadata.factoryMethod(metadata.parameters);
        const sandbox: IRenderableComponentSandbox = this.createComponentSandbox(renderableComponentInstance);

        const componentAlias: string = metadata.componentAlias;
        this.registerSandbox(sandbox, componentAlias);

        return this.renderComponent(renderableComponentInstance, viewContainerReference, injector, metadata.component, this.metadata.componentAlias)
            .do(next => {
                next.changeDetectorRef.detectChanges()
                let exposeRefsRendering: Array<Observable<IRenderablePageElement>>;
                if (!isNullOrUndefined(metadata.bindExternalComponent)) {
                    exposeRefsRendering = Object
                        .keys( metadata.bindExternalComponent )
                        .map( key => {
                            console.log('lol');

                            return this.renderExposeChild(renderableComponentInstance, metadata, key, injector);
                        });
                }else {
                    exposeRefsRendering = [];
                }
                exposeRefsRendering.forEach(next => {
                    next.subscribe();
                });
            })
            .mapTo(renderableComponentInstance);
    }

    public render(viewContainer: ViewContainerRef, injector: Injector): Observable<any> {
        return this
            .fixMetadata(this.metadata)
            .switchMap(meta => this.renderableComponentRender(meta, viewContainer, injector));
    }


    public publishEvent(message: IPageBuilderEvent): IPageBuilder {
        if (!isNullOrUndefined(message.targetComponentAliases)) {
            message.targetComponentAliases.forEach(targetComponentAlias => {
                if (this.namedSandBoxes.has(targetComponentAlias)) {
                    const sandbox = this.namedSandBoxes.get(targetComponentAlias);
                    if (!isNullOrUndefined(sandbox.input)) {
                        sandbox.input(message);
                    }else {
                        console.warn(`no defined input sandbox for ${message}`);
                    }
                }else {
                    if (!this.messageStash.has(targetComponentAlias)) {
                        this.messageStash.set(targetComponentAlias, []);
                    }
                    const stash = this.messageStash.get(targetComponentAlias);
                    stash.push(message);
                }
            });
        }

        return this;
    }

}

export {PageBuilder};