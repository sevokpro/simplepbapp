import { Component, Injectable, Injector, ViewContainerRef } from "@angular/core";
import { TestBed } from "@angular/core/testing";
import { IPageBuilder } from "coreContracts/pageBuilder/IPageBuilder";
import { IPageBuilderComponentRegistry } from "coreContracts/pageBuilder/IPageBuilderComponentRegistry";
import { IPageBuilderFactory, IPageComponentDefenition } from "coreContracts/pageBuilder/IPageBuilderFactory";
import { IPageBuilderVariableRegistry } from "coreContracts/pageBuilder/IPageBuilderVariableRegister";
import { Observable } from "rxjs/Observable";
import { PageBuilderComponentRegistry } from "./pageBuilderComponentRegistry";
import { PageBuilderFactory } from "./pageBuilderFactory";
import { PageBuilderVariableRegister } from "./pageBuilderVariableRegister";

@Component({
    template: ''
})
class EnvComponent{
    constructor(
        public viewContainerReference: ViewContainerRef
    ) {}
}

describe('page builder service tests', () => {
    let pageBuilderFactory: PageBuilderFactory;
    let vcr: ViewContainerRef;
    let injector: Injector;

    const simplePageBuilderParams: IPageComponentDefenition = {
        component: 'grid'
    };

    const paramsWithUnregisterComponent: IPageComponentDefenition = {
        component: 'unregisteredComponent'
    };

    beforeEach(done => {
        TestBed
            .resetTestingModule()
            .configureTestingModule({
                providers: [
                    PageBuilderFactory,
                    {provide: IPageBuilderComponentRegistry, useClass: PageBuilderComponentRegistry},
                    {provide: IPageBuilderVariableRegistry, useClass: PageBuilderVariableRegister}
                ],
                declarations: [EnvComponent]
            });
        const componentsRegister: IPageBuilderComponentRegistry = TestBed.get(IPageBuilderComponentRegistry);
        componentsRegister.registerComponent({
            componentAlias: 'grid',
            factoryMethod: parameters => {
                return {
                    render: (vcr, inj) => {
                        return Observable.of(null);
                    },
                    getExposeRef: alias => undefined
                };
            }
        });
        pageBuilderFactory = TestBed.get(PageBuilderFactory);
        injector = TestBed.get(Injector);
        const envCmp = TestBed.createComponent(EnvComponent);
        envCmp.whenStable().then(() => {
            vcr = envCmp.componentInstance.viewContainerReference;
            done();
        }).catch(err => console.log('error: ', err));
    });

    it('check that page builer instance of interface', () => {
        expect(pageBuilderFactory instanceof IPageBuilderFactory).toBeTruthy();
    });

    it('create page builder instance without errors', () => {
        expect(() => pageBuilderFactory.create(simplePageBuilderParams)).not.toThrowError('Method not implemented.');
    });

    it('factory method must return instance of IPageBuilder', () => {
        expect(pageBuilderFactory.create(simplePageBuilderParams) instanceof IPageBuilder).toBeTruthy();
    });

    it('factory method must throw error when component not register', () => {
        expect(() => pageBuilderFactory.create(paramsWithUnregisterComponent))
            .toThrowError(`component with name: ${paramsWithUnregisterComponent.component} is not registred`);
    });
});