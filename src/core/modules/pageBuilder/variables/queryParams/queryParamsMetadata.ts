import { Injectable } from "@angular/core";
import { NavigationEnd, Router } from "@angular/router";
import { isNullOrUndefined } from "@itexpert-dev/tiny-helpers";
import {
    IPageBuilderVariableMetadata,
    IVariableAttributesMetadata
} from "coreContracts/pageBuilder/IPageBuilderVariableRegister";
import { Observable } from "rxjs/Observable";
import { ReplaySubject } from "rxjs/ReplaySubject";
import { Subject } from "rxjs/Subject";

@Injectable()
class QueryParamsVariableMetadata{
    private queryParamsWatcher: Observable<{[key: string]: string}> = this.route
        .events
        .filter( next => next instanceof NavigationEnd)
        .map( next => next as NavigationEnd)
        .map( next => next.url )
        .map( next => decodeURIComponent(next) )
        .map( next => next.split('?')[1])
        .map( next => isNullOrUndefined(next) ? null : next.split('&'))
        .map( next => {
            const obj: {
                [key: string]: any
            } = {};
            next.forEach( val => {
                const [key, value] = val.split('=');
                obj[key] = value;
            });
            return obj;
        });

    private queryParams: Subject<{[key: string]: string}> = new ReplaySubject(1);

    constructor(
        private route: Router
    ) {
        this.queryParamsWatcher.multicast(this.queryParams).connect();
    }
    public getMetadata(): IPageBuilderVariableMetadata {
        const attributesMetadata: IVariableAttributesMetadata = {
            param: {
                required: true
            }
        };
        return {
            alias: '$queryParams',
            attributesMetadata: attributesMetadata,
            value: attributes => {
                const errors: Array<string> = [];
                const requiredParams = {};
                Object
                    .keys(attributesMetadata)
                    .forEach( next => {
                        if (attributesMetadata[next].required === true) {
                            requiredParams[next] = false;
                        }
                    } );
                Object
                    .keys(attributes)
                    .forEach(key => {
                        if (!isNullOrUndefined(attributesMetadata[key])) {
                            requiredParams[key] = true;
                        }else {
                            errors.push(`param "${key}" not defined in metadata`);
                        }
                    });
                Object.keys(requiredParams).forEach(next => {
                    if (requiredParams[next] === false) {
                        errors.push(`required param "${next}" not found in params`);
                    }
                });
                if (errors.length > 0) {
                    return Observable.throw(`\n${errors.join('\n')}`);
                }
                return Observable.of(attributes.param)
                    .withLatestFrom(this.queryParams)
                    .map(([attribute, queryParams]) => queryParams[attribute] );
            }
        };
    }
}

export {QueryParamsVariableMetadata};