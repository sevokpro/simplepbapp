import { Injector, ReflectiveInjector } from "@angular/core";
import { inject, TestBed } from "@angular/core/testing";
import { RouterTestingModule } from "@angular/router/testing";
import { QueryParamsVariableMetadata } from "./queryParamsMetadata";

class InjectorStub{}

describe('tests for query params variable metadata', () => {
    let queryParamsMetadata: QueryParamsVariableMetadata;

    beforeEach(done => {
        const injector =
        TestBed
            .configureTestingModule({
                imports: [RouterTestingModule],
                providers: [
                    QueryParamsVariableMetadata,
                    {provide: Injector, useClass: InjectorStub}
                ]
            });
        queryParamsMetadata = TestBed.get(QueryParamsVariableMetadata);
        done();
    });

    it('service must be defined', () => {
        expect(queryParamsMetadata).toBeDefined();
    });

    it('metadata result must be defined', () => {
        expect(queryParamsMetadata.getMetadata()).toBeDefined();
    });

    it('alias is string', () => {
        expect(typeof queryParamsMetadata.getMetadata().alias).toBe('string');
    });

    it('aliases must start with $', () => {
        expect(queryParamsMetadata.getMetadata().alias.charAt(0)).toBe('$');
    });
});