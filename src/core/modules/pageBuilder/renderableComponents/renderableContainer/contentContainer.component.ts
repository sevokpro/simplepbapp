import { AfterViewChecked, AfterViewInit, Component, QueryList, ViewChildren, ViewContainerRef } from "@angular/core";
import { WhenContentContainerChangeProvider } from "./common/whenContentContainerChangeProvider";
import { WhenHeaderContainerChangeProvider } from "./common/whenHeaderContainerChangeProvider";

@Component({
    templateUrl: './contentContainer.component.template.pug'
})
class ContentContainerComponent implements AfterViewInit{
    @ViewChildren('headerContainer', {read: ViewContainerRef})
    private headerContainer: QueryList<any>;

    @ViewChildren('contentContainer', {read: ViewContainerRef})
    private contentContainer: QueryList<any>;

    constructor(
        private headerChangePublisher: WhenHeaderContainerChangeProvider,
        private contentChangePublisher: WhenContentContainerChangeProvider
    ) {}

    public ngAfterViewInit(): void {
        this
            .headerContainer
            .changes
            .startWith(this.headerContainer)
            .subscribe(next => {
                this.headerChangePublisher.publish(next.first);
            });

        this
            .contentContainer
            .changes
            .startWith(this.contentContainer)
            .subscribe(next => {
                this.contentChangePublisher.publish(next.first);
            });
    }
}

export {ContentContainerComponent};