import { ComponentFactoryResolver, Injectable } from "@angular/core";
import {
    IComponentMetadataRegistry, IParametersMetadata,
} from "coreContracts/pageBuilder/IPageBuilderComponentRegistry";
import { RenderableContentContainerWrapper } from "./renderableContentContainerWrapper";

@Injectable()
class RenderableContentContainerMetadataProvder{
    constructor(
        private cfr: ComponentFactoryResolver
    ) {}
    public getMetadata(): IComponentMetadataRegistry {
        const paramsMetadata: IParametersMetadata = {};
        const metadata: IComponentMetadataRegistry = {
            componentAlias: 'contentContainer',
            parametersMetadata: paramsMetadata,
            factoryMethod: parameters => {
                return new RenderableContentContainerWrapper(this.cfr);
            }
        };

        return metadata;
    }
}

export {RenderableContentContainerMetadataProvder};