import { ComponentFactoryResolver, ComponentRef, Injector, ReflectiveInjector, ViewContainerRef } from "@angular/core";
import { IPageBuilderEvent } from "coreContracts/pageBuilder/IPageBuilder";
import { IRenderablePageElement } from "coreContracts/pageBuilder/IPageBuilderComponentRegistry";
import { Observable } from "rxjs/Observable";
import { Subject } from "rxjs/Subject";
import { WhenContentContainerChangeProvider } from "src/core/modules/pageBuilder/renderableComponents/renderableContainer/common/whenContentContainerChangeProvider";
import { WhenHeaderContainerChangeProvider } from "./common/whenHeaderContainerChangeProvider";
import { ContentContainerComponent } from "./contentContainer.component";


class RenderableContentContainerWrapper implements IRenderablePageElement{
    constructor(
        private cfr: ComponentFactoryResolver
    ) {}

    private whenHeaderContainerChange: Subject<ViewContainerRef> = new Subject();
    private whenContentContainerChange: Subject<ViewContainerRef> = new Subject();

    public render(viewContainer: ViewContainerRef, injector: Injector): Observable<ComponentRef<any>> {
        const contentContainerInjector: Injector = ReflectiveInjector.resolveAndCreate(
            [{
                provide: WhenHeaderContainerChangeProvider,
                useValue: {publish: vcr => this.whenHeaderContainerChange.next(vcr)} as WhenHeaderContainerChangeProvider
            }, {
                provide: WhenContentContainerChangeProvider,
                useValue: {publish: vcr => this.whenContentContainerChange.next(vcr)} as WhenHeaderContainerChangeProvider
            }],
            injector
        );
        const componentInstance = viewContainer.createComponent(
            this.cfr.resolveComponentFactory(ContentContainerComponent),
            0,
            contentContainerInjector
        );
        return Observable.of( componentInstance );
    }

    public getExposeRef(refAlias: string): Observable<ViewContainerRef> {
        switch (refAlias){
            case 'header':
                return this.whenHeaderContainerChange;
            case 'container':
                return this.whenContentContainerChange;
            default:
                throw new Error(`not defined reference with alias: "${refAlias}"`);
        }
    }

    public getExposeRefAliases(): Observable<Array<string>> {
        return Observable.of(['header', 'container']);
    }
}

export {RenderableContentContainerWrapper};