import { ViewContainerRef } from "@angular/core";

abstract class WhenContentContainerChangeProvider{
    public abstract publish(vcr: ViewContainerRef);
}

export {WhenContentContainerChangeProvider};