import { ViewContainerRef } from "@angular/core";

abstract class WhenHeaderContainerChangeProvider{
    public abstract publish(vcr: ViewContainerRef);
}

export {WhenHeaderContainerChangeProvider};