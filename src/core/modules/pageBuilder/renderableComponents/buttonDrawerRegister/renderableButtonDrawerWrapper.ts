import { ComponentRef, Injector, ViewContainerRef } from "@angular/core";
import { isNullOrUndefined } from "@itexpert-dev/tiny-helpers";
import { IButtonDrawer } from "coreContracts/buttonDrawer/IButtonDrawer";
import {
    IButtonDrawerCreateParam, IButtonDrawerCreateParams,
    IButtonDrawerFactory
} from "coreContracts/buttonDrawer/IButtonDrawerFactory";
import { IPageBuilderEvent } from "coreContracts/pageBuilder/IPageBuilder";
import { IRenderablePageElement } from "coreContracts/pageBuilder/IPageBuilderComponentRegistry";
import { IPageComponentDefenitionParameters } from "coreContracts/pageBuilder/IPageBuilderFactory";
import { Observable } from "rxjs/Observable";

class RenderableButtonDrawerWrapper implements IRenderablePageElement{
    private buttonDrawerInstance: IButtonDrawer;
    constructor(
        buttonDrawerFactory: IButtonDrawerFactory,
        componentParams: IPageComponentDefenitionParameters
    ) {
        this.buttonDrawerInstance = buttonDrawerFactory.create(
            this.adaptParams(componentParams)
        );
    }

    private adaptParams(pageComponentParams: IPageComponentDefenitionParameters): IButtonDrawerCreateParams {
        if (!isNullOrUndefined(pageComponentParams) && !isNullOrUndefined(pageComponentParams.buttons)) {
            const params = JSON.parse(pageComponentParams.buttons);
            const buttonParams: IButtonDrawerCreateParams = params.map(val => {
                return {
                    label: val.label,
                    alias: val.alias,
                    type: isNullOrUndefined(val.type) ? 'default' : val.type,
                    style: isNullOrUndefined(val.style) ? 'base' : val.style
                } as IButtonDrawerCreateParam;
            });
            return buttonParams;
        }
        return [];
    }

    public render(viewContainer: ViewContainerRef, injector: Injector): Observable<ComponentRef<IRenderablePageElement>> {
        return Observable.of(this.buttonDrawerInstance.render(viewContainer, injector));
    }

    public getExposeRef(refAlias: string): Observable<ViewContainerRef> {
        return undefined;
    }
}

export {RenderableButtonDrawerWrapper};