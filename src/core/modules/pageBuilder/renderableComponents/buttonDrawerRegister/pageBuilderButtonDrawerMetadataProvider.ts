import { Injectable } from "@angular/core";
import { IButtonDrawerFactory } from "coreContracts/buttonDrawer/IButtonDrawerFactory";
import {
    IComponentMetadataRegistry
} from "coreContracts/pageBuilder/IPageBuilderComponentRegistry";
import { RenderableButtonDrawerWrapper } from "./renderableButtonDrawerWrapper";

@Injectable()
class PageBuilderButtonDrawerAdapterMetadataProvider{
    constructor(
        private buttonDrawerFactory: IButtonDrawerFactory
    ) {}

    public getMetadata(): IComponentMetadataRegistry {
        const meta: IComponentMetadataRegistry = {
            componentAlias: 'buttonDrawer',
            parametersMetadata: {
                'buttons': {
                    type: 'json',
                    required: false,
                    defaultValue: '[]',
                    verboseName: 'Кнопки',
                    example: {
                        exampleValue: '[{"label": "lol", "alias": "lol"}, {"label": "LOL", "alias": "lol2"}]',
                        verbose: 'Пример двух кнопок'
                    }
                }
            },
            factoryMethod: parameters => {
                return new RenderableButtonDrawerWrapper(this.buttonDrawerFactory, parameters);
            }
        };
        return meta;
    }
}

export {PageBuilderButtonDrawerAdapterMetadataProvider};