import { ComponentFactoryResolver, Injectable } from "@angular/core";
import {
    IComponentMetadataRegistry,
    IRenderablePageElement
} from "coreContracts/pageBuilder/IPageBuilderComponentRegistry";
import { Observable } from "rxjs/Observable";
import { EmptyMockElementComponent } from "./emtyMockElement.component";

@Injectable()
class EmptyMockElementMetadata{
    constructor(private cfr: ComponentFactoryResolver) {}
    public getMetadata(): IComponentMetadataRegistry {
        const meta: IComponentMetadataRegistry = {
            componentAlias: 'mockElement',
            factoryMethod: parameters => {
                const rendarablePageElement: IRenderablePageElement = {
                    render: (viewContainer, injector) => {
                        return Observable.of(viewContainer.createComponent(
                            this.cfr.resolveComponentFactory(EmptyMockElementComponent),
                            0,
                            injector
                        ));
                    },
                    getExposeRef: refAlias => Observable.of(null)
                };
                return rendarablePageElement;
            }
        };
        return meta;
    }
}

export {EmptyMockElementMetadata};