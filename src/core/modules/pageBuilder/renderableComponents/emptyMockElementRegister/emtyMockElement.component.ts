import { Component } from "@angular/core";

@Component({
    template: '<div>Warn!</div><div>this is mock element</div>'
})
class EmptyMockElementComponent{}

export {EmptyMockElementComponent};