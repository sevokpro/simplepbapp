import { ComponentFactoryResolver, Injectable } from "@angular/core";
import {
    IComponentMetadataRegistry,
} from "coreContracts/pageBuilder/IPageBuilderComponentRegistry";
import { ITabsDrawerFactory } from "coreContracts/tabsDrawer/ITabsDrawerFactory";
import { RenderableTabsDrawerWrapper } from "./renderableTabsDrawerWrapper";

@Injectable()
class TabsDrawerRegisterMetadata{
    constructor(
        private tabsDrawer: ITabsDrawerFactory
    ) {}
    public getMetadata(): IComponentMetadataRegistry {
        const meta: IComponentMetadataRegistry = {
            componentAlias: 'tabs',
            parametersMetadata: {
                tabs: {
                    type: 'json',
                    required: true,
                    verboseName: 'Табы',
                    description: 'Компонент для отображения табов',
                    example: {
                        verbose: 'два таба',
                        exampleValue: '[{"label": "таб1", "alias": "tab1"},{"label": "таб2", "alias": "tab2"}]'
                    }
                }
            },
            factoryMethod: parameters => {
                return new RenderableTabsDrawerWrapper(this.tabsDrawer, parameters);
            }
        };
        return meta;
    }
}

export {TabsDrawerRegisterMetadata};