import { ComponentRef, Injector, ViewContainerRef } from "@angular/core";
import { isNullOrUndefined } from "@itexpert-dev/tiny-helpers";
import { IRenderablePageElement } from "coreContracts/pageBuilder/IPageBuilderComponentRegistry";
import { IPageComponentDefenitionParameters } from "coreContracts/pageBuilder/IPageBuilderFactory";
import { ITabsDrawer } from "coreContracts/tabsDrawer/ITabsDrawer";
import { ISingleTab, ITabs, ITabsDrawerFactory } from "coreContracts/tabsDrawer/ITabsDrawerFactory";
import { Observable } from "rxjs/Observable";

class RenderableTabsDrawerWrapper implements IRenderablePageElement {
    private tabsDrawer: ITabsDrawer;

    constructor(tabsDrawerFactory: ITabsDrawerFactory, tabsParsms: IPageComponentDefenitionParameters) {
        this.tabsDrawer = tabsDrawerFactory.create(this.mapInitParams(tabsParsms));
    }

    private mapInitParams(params: IPageComponentDefenitionParameters): ITabs {
        if (!isNullOrUndefined(params) && !isNullOrUndefined(params.tabs)) {
            const parsedObj: Array<Object> = JSON.parse(params.tabs);
            const result: ITabs = parsedObj.map(next => {
                return {
                    label: Observable.of(next['label']),
                    alias: next['alias']
                } as ISingleTab;
            });
            return result;
        }
        return [];
    }

    public render(viewContainer: ViewContainerRef, injector: Injector): Observable<ComponentRef<IRenderablePageElement>> {
        return Observable.of(this.tabsDrawer.render(viewContainer, injector));
    }

    public getExposeRef(refAlias: string): Observable<ViewContainerRef> {
        throw new Error("Method not implemented.");
    }
}

export {RenderableTabsDrawerWrapper};