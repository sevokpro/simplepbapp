import { Component, Injector, ViewContainerRef } from "@angular/core";
import { TestBed } from "@angular/core/testing";
import { RouterTestingModule } from "@angular/router/testing";
import { ITabsDrawerFactory } from "coreContracts/tabsDrawer/ITabsDrawerFactory";
import { CoreModule } from "../../../../core.module";
import { TabsDrawerFactory } from "../../../tabsDrawer/tabsDrawerFactory";
import { RenderableTabsDrawerWrapper } from "./renderableTabsDrawerWrapper";

@Component({
    template: ``
})
class TestEnvComponent{
    constructor(
        public vcr: ViewContainerRef
    ) {}
}

describe('tests for renderable tabs drawer wrapper', () => {
    let renderableTabsDrawerInstance: RenderableTabsDrawerWrapper;
    let viewContainer: ViewContainerRef;
    let injector: Injector;

    beforeEach(done => {
        TestBed
            .resetTestingModule()
            .configureTestingModule({
                imports: [CoreModule, RouterTestingModule],
                declarations: [TestEnvComponent]
            });

        viewContainer = TestBed.createComponent(TestEnvComponent).componentInstance.vcr;
        injector = TestBed.get(Injector);
        renderableTabsDrawerInstance = new RenderableTabsDrawerWrapper(
            TestBed.get(ITabsDrawerFactory),
            []
        );
        done();
    });

    it('rendarable instance not undefined', () => {
        expect(renderableTabsDrawerInstance).not.toBeUndefined();
    });

    it('wrapper must have method "render"', () => {
        expect(typeof renderableTabsDrawerInstance.render).toBe('function');
    });

    it('wrapper must have method "getExposeRef"', () => {
        expect(typeof renderableTabsDrawerInstance.getExposeRef).toBe('function');
    });

    it('call render not to throw error', () => {
        expect(renderableTabsDrawerInstance.render(viewContainer, injector));
    });
});