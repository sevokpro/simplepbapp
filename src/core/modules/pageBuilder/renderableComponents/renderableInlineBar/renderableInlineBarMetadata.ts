import { ComponentFactoryResolver, Injectable } from "@angular/core";
import {
    IComponentMetadataRegistry, IParametersMetadata,
    IRenderablePageElement
} from "coreContracts/pageBuilder/IPageBuilderComponentRegistry";
import { IPageComponentDefenitionParameters } from "coreContracts/pageBuilder/IPageBuilderFactory";
import { RenderableInlineBarComponentWrapper } from "./renderableInlineBarComponentWrapper";

@Injectable()
class RenderableInlineBarMetadata implements IComponentMetadataRegistry{
    constructor(
        private cfr: ComponentFactoryResolver
    ) {}
    public readonly componentAlias: string = 'inlineBar';
    public readonly parametersMetadata: IParametersMetadata = {
        order: {
            type: 'json',
            required: true
        }
    };
    public readonly factoryMethod: (parameters: IPageComponentDefenitionParameters) => IRenderablePageElement =
            parameters => new RenderableInlineBarComponentWrapper(this.cfr, parameters)
}

export {RenderableInlineBarMetadata};