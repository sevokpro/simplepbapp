import { ViewContainerRef } from "@angular/core";

abstract class IViewContainersPublisher{
    public abstract publish(alias: string, vcr: ViewContainerRef);
}

export {IViewContainersPublisher};