import { Observable } from "rxjs/Observable";

abstract class IElementsProvider{
    public abstract getElements(): Observable<Array<string>>;
}

export {IElementsProvider};