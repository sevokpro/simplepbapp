import { ComponentFactoryResolver, ComponentRef, Injector, ReflectiveInjector, ViewContainerRef } from "@angular/core";
import { IRenderablePageElement } from "coreContracts/pageBuilder/IPageBuilderComponentRegistry";
import { IPageComponentDefenitionParameters } from "coreContracts/pageBuilder/IPageBuilderFactory";
import { Observable } from "rxjs/Observable";
import { subscribeOn } from "rxjs/operator/subscribeOn";
import { ReplaySubject } from "rxjs/ReplaySubject";
import { Subject } from "rxjs/Subject";
import { IElementsProvider } from "./common/IElementRefsProvider";
import { IViewContainersPublisher } from "./common/IViewContainersPublisher";
import { InlineBarComponent } from "./inlineBar.component";

class RenderableInlineBarComponentWrapper implements IRenderablePageElement{
    private whenViewStreamsChange: Subject<Map<string, Subject<ViewContainerRef>>> = new ReplaySubject(1);
    private whenPublishToViewStream: Subject<{alias: string, vcr: ViewContainerRef}> = new Subject();
    private order: {[element: string]: number} = {};
    constructor(private cfr: ComponentFactoryResolver, params: IPageComponentDefenitionParameters) {
        const parsedParams: Array<string> = JSON.parse(params.order);
        parsedParams.forEach((value, index) => {
            this.order[value] = index;
        });
        this.whenViewStreamsChange.next(new Map());
        this.whenPublishToViewStream
            .combineLatest(this.whenViewStreamsChange)
            .do( ([{alias}, streams]) => {
                if (!streams.has(alias)) {
                    streams.set(alias, new ReplaySubject(1));
                    this.whenViewStreamsChange.next(streams);
                }
            })
            .do( ([{alias, vcr}, streams]) => {
                streams.get(alias).next(vcr);
            } )
            .subscribe();
    }
    private elementsProvider: IElementsProvider = {
        getElements: () => this
            .whenViewStreamsChange
            .map( next => next.keys())
            .map( next => Array.from(next))
            .map( next => next.sort((a, b) => this.order[a] - this.order[b]))
    };

    private viewContainerPublisher: IViewContainersPublisher = {
        publish: (alias, vcr) => {
            this.whenPublishToViewStream.next({alias: alias, vcr: vcr});
        }
    };

    public render(viewContainer: ViewContainerRef, injector: Injector): Observable<ComponentRef<any>> {
        const renderableInlineBarInjector: Injector = ReflectiveInjector.resolveAndCreate(
            [{
                provide: IElementsProvider,
                useValue: this.elementsProvider
            }, {
                provide: IViewContainersPublisher,
                useValue: this.viewContainerPublisher
            }],
            injector
        );
        return Observable.of(viewContainer.createComponent(
            this.cfr.resolveComponentFactory(InlineBarComponent),
            0,
            renderableInlineBarInjector
        ));
    }

    public getExposeRef(refAlias: string): Observable<ViewContainerRef> {
        return Observable
            .of(refAlias)
            .combineLatest(this.whenViewStreamsChange)
            .do( ([alias, streams]) => {
                if (!streams.has(alias)) {
                    streams.set(alias, new ReplaySubject(1));
                    this.whenViewStreamsChange.next(streams);
                }
            })
            .switchMap( ([alias, viewStreams]) => {
                return viewStreams.get(alias);
            } );
    }
}

export {RenderableInlineBarComponentWrapper};