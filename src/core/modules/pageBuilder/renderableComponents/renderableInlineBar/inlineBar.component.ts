import { AfterViewInit, Component, QueryList, ViewChildren, ViewContainerRef } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { IElementsProvider } from "./common/IElementRefsProvider";
import { IViewContainersPublisher } from "./common/IViewContainersPublisher";

@Component({
    templateUrl: './inlineBar.component.template.pug'
})
class InlineBarComponent implements AfterViewInit{
    @ViewChildren('elementRefs', {read: ViewContainerRef})
    private elementRefs: QueryList<any>;

    public elements: Observable<Array<string>>;
    constructor(
        elements: IElementsProvider,
        private vcrPublisher: IViewContainersPublisher
    ) {
        this.elements = elements.getElements();
    }

    public ngAfterViewInit(): void {
        this.elementRefs
            .changes
            .startWith(this.elementRefs)
            .subscribe(
            next => {
                next.forEach(item => {
                    this.vcrPublisher.publish(item.element.nativeElement.attributes.alias.value, item);
                });
            }
        );
    }
}

export {InlineBarComponent};