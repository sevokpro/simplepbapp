import { Injectable } from "@angular/core";
import { IGridFactory } from "coreContracts/grid/IGridFactory";
import { IComponentMetadataRegistry, IParametersMetadata } from "coreContracts/pageBuilder/IPageBuilderComponentRegistry";
import { RenderableGridWrapper } from "./renderableGridWrapper";

@Injectable()
class PageBuilderGridAdapterMetadataProvider{
    constructor(
        private gridFactory: IGridFactory
    ) {
    }
    public getMetadata(): IComponentMetadataRegistry {
        const metadata: IParametersMetadata = {
            'rows': {
                type: 'json',
                required: false,
                defaultValue: '[[]]',
                verboseName: 'описание сетки',
                description: 'сетка описывается двумерным массивом',
                example: {
                    verbose: 'сетка 2х4',
                    exampleValue: '[[25, 25, 25, 25], [25, 25, 25, 25]]'
                }
            },
            'repeat': {
                type: 'string',
                required: false,
                defaultValue: 'no-repeat',
                verboseName: 'режим повтора строк',
                description: 'заданная сетка будет дублироваться, пока количество строк не станет равно описанному в параметре "количество строк"'
            },
            'rowsCount': {
                type: 'number',
                required: false,
                defaultValue: 0,
                verboseName: 'количество строк',
                description: 'количество строк в сетке'
            },
            'aliases': {
                type: 'json',
                required: false,
                defaultValue: '{}',
                verboseName: 'псевдонимы',
                description: 'псевдонимы необходимы, для экспозиции компонентов',
                example: {
                    verbose: 'псевдоним для 3й ячейки во 2й строке',
                    exampleValue: '{"cell":[1, 2]}'
                }
            }
        };
        const meta: IComponentMetadataRegistry = {
            componentAlias: 'grid',
            parametersMetadata: metadata,
            factoryMethod: parameters => {
                return new RenderableGridWrapper(this.gridFactory, parameters, metadata);
            }
        };
        return meta;
    }
}

export {PageBuilderGridAdapterMetadataProvider};