import { ComponentRef, Injector, ViewContainerRef } from "@angular/core";
import { isNullOrUndefined } from "@itexpert-dev/tiny-helpers";
import { GridRepeatMode } from "coreContracts/grid/GridRepeatMode";
import { IGrid } from "coreContracts/grid/IGrid";
import { IGridFactory } from "coreContracts/grid/IGridFactory";
import { IGridCell } from "coreContracts/grid/IGridICell";
import { IGridParameters } from "coreContracts/grid/IGridParameters";
import { IPageBuilderEvent } from "coreContracts/pageBuilder/IPageBuilder";
import { IParametersMetadata, IRenderablePageElement } from "coreContracts/pageBuilder/IPageBuilderComponentRegistry";
import { IPageComponentDefenitionParameters } from "coreContracts/pageBuilder/IPageBuilderFactory";
import { Observable } from "rxjs/Observable";

class RenderableGridWrapper implements IRenderablePageElement{
    private gridInstance: IGrid;
    private availableAliases: Array<string>;
    constructor(
        gridFactory: IGridFactory,
        pageComponentParams: IPageComponentDefenitionParameters,
        private gridParametersMetadata: IParametersMetadata
    ) {
        const initParams = this.parseParams(pageComponentParams);
        this.availableAliases = Array.from(initParams.aliases.keys());
        this.gridInstance = gridFactory.create(initParams);
    }

    private parseParams(params: IPageComponentDefenitionParameters): IGridParameters {
        let gridParams: IGridParameters;
        let repeatMode: GridRepeatMode;
        if (isNullOrUndefined(params)) {
            params = {};
        }
        if (isNullOrUndefined(params.repeat)) {
            params.repeat = this.gridParametersMetadata['repeat']['defaultValue'];
        }
        switch (params.repeat){
            case 'repeat':
                repeatMode = GridRepeatMode.repeat;
                break;
            case 'no-repeat':
                repeatMode = GridRepeatMode.noRepeat;
                break;
            default:
                throw new Error(`unhandable repeat mode: ${params.repeat}`);
        }

        const aliasProperty = isNullOrUndefined(params.aliases) ? null : JSON.parse(params.aliases);
        const aliasMap: Map<string, IGridCell> = new Map();
        if (!isNullOrUndefined(aliasProperty)) {
            Object
                .keys(aliasProperty)
                .map(key => [key, aliasProperty[key]])
                .forEach(([key, val]) => {
                    aliasMap.set(key, {
                        row: val[0],
                        column: val[1]
                    });
                });
        }

        gridParams = {
            rows: !isNullOrUndefined(params.rows) ? JSON.parse(params.rows) : null,
            rowsRepeatMode: repeatMode,
            rowsCount: params.rowsCount,
            aliases: aliasMap
        };

        return gridParams;
    }

    public render(viewContainer: ViewContainerRef, injector: Injector): Observable<ComponentRef<any>> {
        const gridInstance = this.gridInstance.render(viewContainer, injector);
        return Observable.of(gridInstance);
    }

    public getExposeRef(refAlias: string): Observable<ViewContainerRef> {
        return this.gridInstance.getCellByAlias(refAlias);
    }

    public getExposeRefAliases(): Observable<Array<string>> {
        return Observable.of(this.availableAliases);
    }
}

export {RenderableGridWrapper};