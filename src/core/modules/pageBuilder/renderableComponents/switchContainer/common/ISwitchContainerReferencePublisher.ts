import { ViewContainerRef } from "@angular/core";

abstract class ISwitchContainerReferencePublisher{
    public abstract publish(vcr: ViewContainerRef);
}

export {ISwitchContainerReferencePublisher};