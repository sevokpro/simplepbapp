import {
    ComponentFactoryResolver, ComponentRef, Injectable, Injector, ReflectiveInjector,
    ViewContainerRef
} from "@angular/core";
import { isNullOrUndefined } from "@itexpert-dev/tiny-helpers";
import { IPageBuilderEvent } from "coreContracts/pageBuilder/IPageBuilder";
import {
    IComponentMetadataRegistry,
    IRenderablePageElement
} from "coreContracts/pageBuilder/IPageBuilderComponentRegistry";
import { IPageComponentDefenitionParameters } from "coreContracts/pageBuilder/IPageBuilderFactory";
import { IPageBuilderSwitchContainerActivateContainerEvent } from "coreContracts/pageBuilder/renderableComponentsEvents/IPageBuilderSwitchContainerEventsFactory";
import { Observable } from "rxjs/Observable";
import { ReplaySubject } from "rxjs/ReplaySubject";
import { Subject } from "rxjs/Subject";
import { Subscription } from "rxjs/Subscription";
import { ISwitchContainerReferencePublisher } from "./common/ISwitchContainerReferencePublisher";
import { SwitchContainerComponent } from "./switchContainer.component";

type IStreamsContainer = Map<string, Subject<ViewContainerRef>>;

class RenderableSwitchContainer implements IRenderablePageElement{
    private container: Subject<ViewContainerRef> = new ReplaySubject(1);
    private whenStreamsChange: Subject<IStreamsContainer> = new ReplaySubject(1);
    private connectSubscribtion: Subscription;
    private inputEvents: Subject<IPageBuilderEvent> = new Subject();

    private whenActualContainerChange = this.inputEvents
        .filter(message => message instanceof IPageBuilderSwitchContainerActivateContainerEvent)
        .map(message => message as IPageBuilderSwitchContainerActivateContainerEvent)
        .combineLatest(this.whenStreamsChange)
        .do( next => {
            if (!isNullOrUndefined(this.connectSubscribtion)) {
                this.connectSubscribtion.unsubscribe();
            }
        })
        .filter(([event, containers]) => containers.has(event.containerName))
        .do( ([next, containers]) => {
            const streamContainer = containers.get(next.containerName);
            this.connectSubscribtion = this.container.multicast(streamContainer).connect();
        });

    private whenActualComponentChange: Observable<ComponentRef<any>>;

    constructor(
        params: IPageComponentDefenitionParameters,
        private componentFactoryResolver: ComponentFactoryResolver
    ) {
        this.whenStreamsChange.next(new Map());
        this.whenActualContainerChange.subscribe();
    }

    private switchContainerReferencePublisher: ISwitchContainerReferencePublisher = {
        publish: vcr => this.container.next(vcr)
    };
    public render(viewContainer: ViewContainerRef, injector: Injector): Observable<ComponentRef<any>> {
        return Observable.of(
            viewContainer.createComponent(
                this.componentFactoryResolver.resolveComponentFactory(SwitchContainerComponent),
                0,
                ReflectiveInjector.resolveAndCreate(
                    [{
                        provide: ISwitchContainerReferencePublisher,
                        useValue: this.switchContainerReferencePublisher
                    }],
                    injector
                )
            )
        );
    }

    public getExposeRef(refAlias: string): Observable<ViewContainerRef> {
        return Observable.of(refAlias)
            .combineLatest(this.whenStreamsChange)
            .do(([alias, streamsStore]) => {
                if (!streamsStore.has(alias)) {
                    streamsStore.set(alias, new ReplaySubject(1));
                    this.whenStreamsChange.next(streamsStore);
                }
            })
            .filter(([alias, streamsStore]) => streamsStore.has(alias))
            .switchMap(([alias, streamsStore]) => streamsStore.get(alias));
    }

    public publishEvent(event: IPageBuilderEvent): IRenderablePageElement {
        this.inputEvents.next(event);
        return this;
    }
}

@Injectable()
class SwitchContainerRegisterMetadata{
    constructor(private cfr: ComponentFactoryResolver) {}

    public getMetadata(): IComponentMetadataRegistry {
        const meta: IComponentMetadataRegistry = {
            componentAlias: 'switchContainer',
            factoryMethod: parameters => {
                const renderableElement: IRenderablePageElement = new RenderableSwitchContainer(parameters, this.cfr);
                return renderableElement;
            }
        };
        return meta;
    }
}

export {SwitchContainerRegisterMetadata};