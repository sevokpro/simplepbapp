import { Component, ViewContainerRef } from "@angular/core";
import { ISwitchContainerReferencePublisher } from "./common/ISwitchContainerReferencePublisher";

@Component({
    template: ''
})
class SwitchContainerComponent{
    constructor(
        vcr: ViewContainerRef,
        containerReferencePublisher: ISwitchContainerReferencePublisher
    ) {
        containerReferencePublisher.publish(vcr);
    }
}

export {SwitchContainerComponent};