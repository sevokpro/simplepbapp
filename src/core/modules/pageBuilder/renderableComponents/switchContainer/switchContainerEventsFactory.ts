import {
    IPageBuilderSwitchContainerActivateContainerEvent,
    IPageBuilderSwitchContainerEventsFactory
} from "coreContracts/pageBuilder/renderableComponentsEvents/IPageBuilderSwitchContainerEventsFactory";

class PageBuilderSwitchContainerActivateContainerEvent extends IPageBuilderSwitchContainerActivateContainerEvent{
    public containerName: string;
    public event: string;
    public targetComponentAliases: Array<string>;
    public message: string;

    constructor(containerName: string, targetAlias: string) {
        super();
        this.message = containerName;
        this.containerName = containerName;
        this.targetComponentAliases = [targetAlias];
    }
}

class SwitchContainerEventsFactory extends IPageBuilderSwitchContainerEventsFactory{
    public activateContainer(containerName: string, targetAlias: string): IPageBuilderSwitchContainerActivateContainerEvent {
        return new PageBuilderSwitchContainerActivateContainerEvent(containerName, targetAlias);
    }
}

export {SwitchContainerEventsFactory};