import { isNullOrUndefined } from "@itexpert-dev/tiny-helpers";
import { IPageComponentDefenition } from "coreContracts/pageBuilder/IPageBuilderFactory";
import { Observable } from "rxjs/Observable";

class PageBuilderConstructorShemaPathBuffer{
    private shemeBuffer: Map<string, IPageComponentDefenition> = new Map();
    public putSheme(sheme: IPageComponentDefenition) {
        const name: string = (isNullOrUndefined(sheme.componentAlias)) ? sheme.component : sheme.componentAlias;
        this.shemeBuffer.set(name, sheme);
    }
    public getSavedShemeKeys(): Observable<Array<string>> {
        return Observable.of(Array.from(this.shemeBuffer.keys()));
    }
    public getSheme(key: string): IPageComponentDefenition{
        return this.shemeBuffer.get(key)
    }
}

export {PageBuilderConstructorShemaPathBuffer};