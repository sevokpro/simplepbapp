import { ComponentFactoryResolver, ComponentRef, Injector, ReflectiveInjector, ViewContainerRef } from "@angular/core";
import { isNullOrUndefined } from "@itexpert-dev/tiny-helpers";
import {
    IPageBuilderComponentRegistry,
    IRenderablePageElement
} from "coreContracts/pageBuilder/IPageBuilderComponentRegistry";
import { IPageBuilderConstructor } from "coreContracts/pageBuilder/IPageBuilderConstructor";
import { IPageComponentDefenition } from "coreContracts/pageBuilder/IPageBuilderFactory";
import { IPageBuilderVariableRegistry } from "coreContracts/pageBuilder/IPageBuilderVariableRegister";
import { Observable } from "rxjs/Observable";
import { ReplaySubject } from "rxjs/ReplaySubject";
import { Subject } from "rxjs/Subject";
import { ConstructorAddElementButton } from "./constructorAddElementButton/constructorAddElementButton";
import { UpdateComponentJsonShemeProvider } from "./constructSafeContainer/common/updateJsonShemeProvider";
import { ConstructSafeContainer } from "./constructSafeContainer/constructSafeContainer";
import { PageBuilder } from "./pageBuilder";
import { PageBuilderConstructorShemaPathBuffer } from "./pageBuilderConstructorShemaPathBuffer";

class PageBuilderConstructor extends PageBuilder implements IPageBuilderConstructor{
    constructor(
        protected metadata: IPageComponentDefenition,
        private componentRegis: IPageBuilderComponentRegistry,
        private variableRegis: IPageBuilderVariableRegistry,
        private componentFactoryResolver: ComponentFactoryResolver
    ) {
        super(
            metadata,
            componentRegis,
            variableRegis
        );
    }
    private componentsStash: Map<string, any> = new Map();

    protected fixMetadata(metadata: IPageComponentDefenition): Observable<IPageComponentDefenition> {
        const removeSelf = metadata['__$removeSelf'];
        const removeChild = metadata['__$removeChild'];
        return super.fixMetadata(metadata).map( metadataResult => {
            if (!isNullOrUndefined(removeSelf)) {
                metadataResult['__$removeSelf'] = removeSelf;
            }
            if (!isNullOrUndefined(removeChild)) {
                metadataResult['__$removeChild'];
            }
            return metadataResult;
        });
    }

    protected getExposeChildMetadata(metadata: IPageComponentDefenition, childName: string): IPageComponentDefenition {
        const originMeta = super.getExposeChildMetadata(metadata, childName);
        originMeta['__$removeSelf'] = () => metadata['__$removeChild'](childName);
        console.log(originMeta);
        return originMeta;
    }

    protected renderableComponentRender(
        componentDefenition: IPageComponentDefenition,
        viewContainerReference: ViewContainerRef,
        injector: Injector
    ): Observable<any> {
        const whenUpdateSheme: Subject<IPageComponentDefenition> = new ReplaySubject(1);
        const updateComponentJsonShemeProvider: UpdateComponentJsonShemeProvider = {
            update: newSheme => {
                newSheme['__$removeChild'] = child => {
                    const sheme = {...newSheme};
                    this.shemaBuffer.putSheme(this.cleanUpMeta({...sheme.bindExternalComponent[child]}));
                    delete sheme.bindExternalComponent[child];
                    updateComponentJsonShemeProvider.update(sheme);
                };
                return whenUpdateSheme.next(newSheme);
            }
        };
        updateComponentJsonShemeProvider.update({...componentDefenition});

        const whenRenderComplete: Subject<IRenderablePageElement> = new Subject();
        whenRenderComplete
            .filter( next => !isNullOrUndefined(next.getExposeRefAliases) )
            .subscribe( componentInstance => {
                componentInstance.getExposeRefAliases().first().subscribe( aliases => {
                    Observable
                        .from(aliases)
                        .subscribe( alias => {
                            componentInstance
                                .getExposeRef(alias)
                                .delay(0)
                                .filter( next => next.length === 0 )
                                .subscribe(next => {
                                    const addElementsButton = new ConstructorAddElementButton(this.componentFactoryResolver);
                                    addElementsButton.render(next, injector);
                                    addElementsButton.whenSelectComponent().subscribe(next => {
                                        const componentDefenitionCopy = {...componentDefenition};
                                        if (isNullOrUndefined(componentDefenitionCopy.bindExternalComponent)) {
                                            componentDefenitionCopy.bindExternalComponent = {};
                                        }
                                        componentDefenitionCopy.bindExternalComponent[alias] = {
                                            component: next
                                        };
                                        updateComponentJsonShemeProvider.update(componentDefenitionCopy);
                                    });

                                    addElementsButton.whenSelectBufferSheme().subscribe(
                                        next => {
                                            const componentDefenitionCopy = {...componentDefenition};
                                            if (isNullOrUndefined(componentDefenitionCopy.bindExternalComponent)) {
                                                componentDefenitionCopy.bindExternalComponent = {};
                                            }
                                            componentDefenitionCopy.bindExternalComponent[alias] = this.shemaBuffer.getSheme(next);
                                            updateComponentJsonShemeProvider.update(componentDefenitionCopy);
                                        }
                                    );
                                });
                        });
                });
            } );

        const safeContainer = new ConstructSafeContainer(
            viewContainerReference,
            injector,
            this.componentFactoryResolver,
            componentDefenition,
            this.getRenderableComponentMetadata(componentDefenition.component),
            this,
            updateComponentJsonShemeProvider
        );
        const renderSubscription = safeContainer
            .getExposeRef()
            .combineLatest(whenUpdateSheme)
            .switchMap( ([fixedContainer, sheme]) => {
                fixedContainer.clear();
                Object.keys(componentDefenition).forEach(value => {
                    delete componentDefenition[value];
                });
                Object.keys(sheme).forEach(value => {
                    componentDefenition[value] = sheme[value];
                });
                const renderComponentProcess = super
                    .renderableComponentRender(sheme, fixedContainer, injector)
                    .do( next => whenRenderComplete.next(next) );

                return renderComponentProcess;
            });
        return renderSubscription;
    }

    private cleanUpMeta(metadata: IPageComponentDefenition): IPageComponentDefenition {
        if (metadata['__$removeSelf']) {
            delete metadata['__$removeSelf'];
        }
        if (metadata['__$removeChild']) {
            delete metadata['__$removeChild'];
        }
        if ( !isNullOrUndefined(metadata.bindExternalComponent) ) {
            Object.keys(metadata.bindExternalComponent).forEach( key => {
                this.cleanUpMeta(metadata.bindExternalComponent[key]);
            });
        }
        return metadata;
    }

    public exportSheme() : IPageComponentDefenition {
        const metadataCopy = this.cleanUpMeta({...this.metadata});
        console.log(metadataCopy);
        return metadataCopy;
    }

    private shemaBuffer: PageBuilderConstructorShemaPathBuffer = new PageBuilderConstructorShemaPathBuffer();
    public render(viewContainer: ViewContainerRef, injector: Injector) : Observable <boolean> {
        const constructorPageInjector = ReflectiveInjector.resolveAndCreate(
            [{
                provide: PageBuilderConstructorShemaPathBuffer,
                useValue: this.shemaBuffer
            }],
            injector
        );
        return super.render(viewContainer, constructorPageInjector);
    }
}

export {PageBuilderConstructor};