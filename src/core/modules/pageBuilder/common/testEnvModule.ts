import { NgModule } from "@angular/core";
import { EnvComponent } from "./testEnvComponent";
import { SimpleComponent } from "./testSimpleComponent";

@NgModule({
    declarations: [SimpleComponent, EnvComponent],
    entryComponents: [SimpleComponent, EnvComponent]
})
class EnvModule {

}
export {EnvModule};