import { IComponentMetadataRegistry } from "coreContracts/pageBuilder/IPageBuilderComponentRegistry";
import { IPageComponentDefenition } from "coreContracts/pageBuilder/IPageBuilderFactory";

abstract class ComponentMetadataProvider{
    public abstract getMetadata(): {
        defenition: IPageComponentDefenition,
        metadata: IComponentMetadataRegistry
    };
}

export {ComponentMetadataProvider};