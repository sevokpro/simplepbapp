import { ViewContainerRef } from "@angular/core";

abstract class IConstructSafeContainerExposeRefPublisher{
    public abstract publish(viewContainerRef: ViewContainerRef);
}

export {IConstructSafeContainerExposeRefPublisher};