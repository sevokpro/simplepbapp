import { IPageComponentDefenition } from "coreContracts/pageBuilder/IPageBuilderFactory";

abstract class UpdateComponentJsonShemeProvider{
    public abstract update(newSheme: IPageComponentDefenition);
}

export {UpdateComponentJsonShemeProvider};