import { ComponentFactoryResolver, Injector, ReflectiveInjector, ViewContainerRef } from "@angular/core";
import { IPageBuilder } from "coreContracts/pageBuilder/IPageBuilder";
import { IComponentMetadataRegistry } from "coreContracts/pageBuilder/IPageBuilderComponentRegistry";
import { IPageComponentDefenition } from "coreContracts/pageBuilder/IPageBuilderFactory";
import { Observable } from "rxjs/Observable";
import { ReplaySubject } from "rxjs/ReplaySubject";
import { Subject } from "rxjs/Subject";
import { ComponentMetadataProvider } from "./common/componentMetadataProvider";
import { IConstructSafeContainerExposeRefPublisher } from "./common/constructSafeContainerExposeRefPublisher";
import { UpdateComponentJsonShemeProvider } from "./common/updateJsonShemeProvider";
import { ConstructSafeContainerComponent } from "./constructSafeContainer.component";

class ConstructSafeContainer{
    private whenRefChange: Subject<ViewContainerRef> = new ReplaySubject(1);

    private refPublisher: IConstructSafeContainerExposeRefPublisher = {
        publish: viewContainerRef => this.whenRefChange.next(viewContainerRef)
    };
    private componentMetadataProvider: ComponentMetadataProvider = {
        getMetadata: () => {
            return {
                defenition: this.componentDefenition,
                metadata: this.componentMetadata
            };
        }
    };
    constructor(
        viewContainerRef: ViewContainerRef,
        injector: Injector,
        componentFactoryResolver: ComponentFactoryResolver,
        private componentDefenition: IPageComponentDefenition,
        private componentMetadata: IComponentMetadataRegistry,
        private pageBuilder: IPageBuilder,
        private updateComponentJsonShemeProvider: UpdateComponentJsonShemeProvider
    ) {
        const injectorForSafeContainer = ReflectiveInjector.resolveAndCreate(
            [{
                provide: IConstructSafeContainerExposeRefPublisher,
                useValue: this.refPublisher
            }, {
                provide: ComponentMetadataProvider,
                useValue: this.componentMetadataProvider
            }, {
                provide: IPageBuilder,
                useValue: this.pageBuilder
            }, {
                provide: UpdateComponentJsonShemeProvider,
                useValue: updateComponentJsonShemeProvider
            }],
            injector
        );
        viewContainerRef.createComponent(
            componentFactoryResolver.resolveComponentFactory(ConstructSafeContainerComponent),
            0,
            injectorForSafeContainer
        );
    }

    public getExposeRef(): Observable<ViewContainerRef> {
        return this.whenRefChange.asObservable();
    }
}

export {ConstructSafeContainer};