import { visitAll } from "@angular/compiler";
import {
    AfterViewInit, ChangeDetectorRef, Component, Injector, QueryList, ViewChildren,
    ViewContainerRef
} from "@angular/core";
import { isNullOrUndefined } from "@itexpert-dev/tiny-helpers";
import { IAdvancedFormFactory } from "coreContracts/advancedForm/IAdvancedFormFactory";
import { IAdvancedFormCtrlInitParams } from "coreContracts/advancedForm/IAdvancedFormInitParams";
import { IAdvancedFormMetaField } from "coreContracts/advancedForm/IAdvancedFormMetaField";
import { IPageBuilder } from "coreContracts/pageBuilder/IPageBuilder";
import { IPageComponentDefenition, IPageComponentDefenitionParameters } from "coreContracts/pageBuilder/IPageBuilderFactory";
import { IPageBuilderSwitchContainerEventsFactory } from "coreContracts/pageBuilder/renderableComponentsEvents/IPageBuilderSwitchContainerEventsFactory";
import { Observable } from "rxjs/Observable";
import { SubscribeOnObservable } from "rxjs/observable/SubscribeOnObservable";
import { ReplaySubject } from "rxjs/ReplaySubject";
import { Subject } from "rxjs/Subject";
import { ComponentMetadataProvider } from "./common/componentMetadataProvider";
import { IConstructSafeContainerExposeRefPublisher } from "./common/constructSafeContainerExposeRefPublisher";
import { UpdateComponentJsonShemeProvider } from "./common/updateJsonShemeProvider";

@Component({
    templateUrl: './constructSafeContainer.component.template.pug',
})
class ConstructSafeContainerComponent implements AfterViewInit{
    @ViewChildren('exposeReference', {read: ViewContainerRef})
    private exposeReference: QueryList<any>;
    public defenition: IPageComponentDefenition;

    @ViewChildren('paramsReference', {read: ViewContainerRef})
    private paramsReference: QueryList<any>;
    private whenParamsReferenceChange: Subject<ViewContainerRef> = new ReplaySubject(1);

    public whenClickCogs: Subject<null> = new Subject();
    public isConfigVisible: Subject<boolean> = new ReplaySubject(1);

    public whenClickScissors: Subject<null> = new Subject();

    public whenApplyConfigClick: Subject<null> = new Subject();

    public isComponentSwitchContainer: boolean;
    public switchContainerCases: Array<string> = [];
    public whenActualContainerChange: Subject<string> = new ReplaySubject(1);
    public selectedSwitchContainerModel: string;
    public changeActualContainer(event: string) {
        this.whenActualContainerChange.next(event);
    }

    public whenUpdateParametes: Subject<IPageComponentDefenitionParameters> = new Subject();

    constructor(
        private refPublisher: IConstructSafeContainerExposeRefPublisher,
        private changeDetectorRef: ChangeDetectorRef,
        componentMetadataProvider: ComponentMetadataProvider,
        advancedFormFactory: IAdvancedFormFactory,
        injector: Injector,
        pageBuilder: IPageBuilder,
        switchContainerEventFactory: IPageBuilderSwitchContainerEventsFactory,
        updateJsonShemeProvider: UpdateComponentJsonShemeProvider
    ) {
        this.isConfigVisible.next(false);
        const meta = componentMetadataProvider.getMetadata();
        const metadata = meta.metadata.parametersMetadata;
        this.defenition = meta.defenition;

        this.whenClickScissors.subscribe(
            next => {
                console.log(this.defenition);
                this.defenition['__$removeSelf']();
            }
        );

        this.isComponentSwitchContainer = meta.defenition.component === 'switchContainer';
        if (this.isComponentSwitchContainer) {
            Object
                .keys(this.defenition.bindExternalComponent)
                .forEach(value =>
                    this.switchContainerCases.push(value)
                );
        }
        this
            .whenClickCogs
            .withLatestFrom(this.isConfigVisible)
            .map( ([next, isConfigVisible]) => isConfigVisible )
            .map( next => !next )
            .multicast( this.isConfigVisible )
            .connect();

        if (!isNullOrUndefined(metadata) && Object.keys(metadata).length !== 0) {
            const advancedFormInitParams: IAdvancedFormCtrlInitParams = {
                metadata: new Map(),
                data: new Map()
            };
            if (!isNullOrUndefined(this.defenition.parameters)) {
                Object.keys(this.defenition.parameters).forEach(key => {
                    advancedFormInitParams.data.set(key, this.defenition.parameters[key]);
                });
            }

            Object.keys(metadata).forEach(key => {
                const property = metadata[key];
                const labelCollector: Array<string> = [
                    isNullOrUndefined(property.verboseName) ? key : property.verboseName,
                    ...!isNullOrUndefined(property.description) ? [property.description] : [],
                    ...!isNullOrUndefined(property.example) ? [`${property.example.verbose}. Пример: ${property.example.exampleValue}`] : []
                ];
                const label: string = labelCollector.join('\n');
                const formField: IAdvancedFormMetaField = {
                    required: property.required,
                    label: label,
                    available: true,
                    visible: true,
                    type: 'string'
                };
                advancedFormInitParams.metadata.set(key, formField);
            });

            this
                .whenParamsReferenceChange
                .filter( next => !isNullOrUndefined(next) )
                .switchMap(next => {
                    next.clear();
                    // TODO: don`t create new instances, fix set values when re-render form block
                    const formCtrl = advancedFormFactory.createCtrlInstance(advancedFormInitParams);
                    formCtrl.renderFormBlock('main', next, injector);
                    return this.whenApplyConfigClick.mapTo(formCtrl);
                })
                .switchMap( next => next.getEntity())
                .map( next => {
                    const result: IPageComponentDefenitionParameters = {};
                    next.forEach( (val, key) => {
                        result[key] = val;
                    } );
                    return result;
                } )
                .multicast(this.whenUpdateParametes)
                .connect();

            this.whenUpdateParametes.subscribe(
                next => {
                    updateJsonShemeProvider.update({...this.defenition, ...{parameters: next}});
                }
            );
        }

        this.whenActualContainerChange.subscribe(next => {
            pageBuilder.publishEvent(
                switchContainerEventFactory
                    .activateContainer(next, this.defenition.componentAlias)
            );
        });
    }

    public ngAfterViewInit(): void {
        this.exposeReference
            .changes
            .startWith(this.exposeReference)
            .subscribe(
                next => {
                    const container: ViewContainerRef = next.first;
                    container.clear();
                    this.refPublisher.publish(container);
                    this.changeDetectorRef.detectChanges();
                }
            );

        this.paramsReference
            .changes
            .startWith(this.paramsReference)
            .subscribe(
                next => {
                    next.first;
                    this.whenParamsReferenceChange.next(next.first);
                    this.changeDetectorRef.detectChanges();
                }
            );
    }

}

export {ConstructSafeContainerComponent};