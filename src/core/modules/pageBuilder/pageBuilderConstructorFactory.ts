import { ComponentFactoryResolver, Injectable } from "@angular/core";
import { IPageBuilderComponentRegistry } from "coreContracts/pageBuilder/IPageBuilderComponentRegistry";
import { IPageBuilderConstructor } from "coreContracts/pageBuilder/IPageBuilderConstructor";
import { IPageBuilderConstructorFactory } from "coreContracts/pageBuilder/IPageBuilderConstructorFactory";
import { IPageComponentDefenition } from "coreContracts/pageBuilder/IPageBuilderFactory";
import { IPageBuilderVariableRegistry } from "coreContracts/pageBuilder/IPageBuilderVariableRegister";
import { PageBuilderConstructor } from "./pageBuilderConstructor";

@Injectable()
class PageBuilderConstructorFactory extends IPageBuilderConstructorFactory{
    constructor(
        private pageBuilderComponentRegister : IPageBuilderComponentRegistry,
        private pageBuilderVariableRegister: IPageBuilderVariableRegistry,
        private componentFactoryResolver: ComponentFactoryResolver
    ) {
        super();
    }

    public create(pageCreateParameters: IPageComponentDefenition): IPageBuilderConstructor {
        return new PageBuilderConstructor(pageCreateParameters, this.pageBuilderComponentRegister, this.pageBuilderVariableRegister, this.componentFactoryResolver);
    }
}

export {PageBuilderConstructorFactory};