import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { isNullOrUndefined, isUndefined } from "@itexpert-dev/tiny-helpers";
import { IBackendSource } from "coreContracts/backendSource";
import { Observable } from "rxjs/Observable";
import { IDataSourceDeleteRequest } from "src/common/IDataSource/delete/request/IDataSourceDeleteRequest";
import { IDataSourceMetadataResponse } from "src/common/IDataSource/metadata/response/IDataSourceMetadataResponse";
import { IDataSourceCompositeRequest } from "../../../common/IDataSource/compositeQuery/request/IDataSourceCompositeRequest";
import { IDataSourceCompositeResponse } from "../../../common/IDataSource/compositeQuery/response/IDataSourceCompositeResponse";
import { IDataSourceCreateRequest } from "../../../common/IDataSource/create/request/IDataSourceCreateRequest";
import { IDataSourceCreateResponse } from "../../../common/IDataSource/create/response/IDataSourceCreateResponse";
import { IDataSourceDeleteResponse } from "../../../common/IDataSource/delete/response/IDataSourceDeleteResponse";
import { IEntityProperty, IMetadataProperty } from "../../../common/IDataSource/IDataSource";
import { IDataSourceMetadataRequest } from "../../../common/IDataSource/metadata/request/IDataSourceMetadataRequest";
import { IDataSourceReadRequest } from "../../../common/IDataSource/read/request/IDataSourceReadRequest";
import { IDataSourceReadRequestFilterNode } from "../../../common/IDataSource/read/request/IDataSourceReadRequestFilterBlock";
import { IDataSourceReadResponse } from "../../../common/IDataSource/read/response/IDataSourceReadResponse";
import { IDataSourceReadResponseEntity } from "../../../common/IDataSource/read/response/IDataSourceReadResponseEntity";
import { IDataSourceUpdateRequest } from "../../../common/IDataSource/update/request/IDataSourceUpdateRequest";
import { IDataSourceUpdateResponse } from "../../../common/IDataSource/update/response/IDataSourceUpdateResponse";

@Injectable()
class BackendSource extends IBackendSource {
    public constructor(private http: HttpClient) {
        super();
    }
    private readonly backendPrefix: string = `api/source`;
    private readonly serverErrorIntro: string = `invalid server response. `;

    public read(request: IDataSourceReadRequest): Observable<IDataSourceReadResponse> {
        const entities = `entities`;
        interface ISwaggerQuery {
            entity: string;
            fields?: Array<string>;
            sort?: Array<{ fields: string, isAscending: boolean }>;
            limit?: number;
            offset?: number;
            filter?: any;
        }
        const query: ISwaggerQuery = {
            entity: request.entity,
            limit: request.limit,
            offset: request.offset,
            fields: request.fields,
            filter: request.filter
        };

        //TODO: add response validator
        return this.http.post(`${this.backendPrefix}/query`, query)
            .map(next => {
                const result: IDataSourceReadResponse = {
                    entities: []
                };
                for (const propKey in next[entities]) {
                    const prop = next[entities][propKey];

                    const resultEntity: IDataSourceReadResponseEntity = {
                        uuid: prop['ссылка'].value,
                        displayName: prop['ссылка'].displayName,
                        entity: new Map()
                    };

                    for (const entityPropKey of Object.keys(prop)) {
                        if (!prop[entityPropKey].hasOwnProperty('value')) {
                            throw new Error(`missing value property! in backend source request`);
                        }
                        const resultEntityProp: IEntityProperty = prop[entityPropKey];

                        resultEntity.entity.set(entityPropKey, resultEntityProp);
                    }

                    result.entities.push(resultEntity);
                }
                return result;
            });
    }

    public create(request: IDataSourceCreateRequest): Observable<IDataSourceCreateResponse> {
        return Observable.from(request.entities)
            .map( ({entityName, entity}) => {
                const result = {};
                entity.forEach((prop, key) => {
                    if (isNullOrUndefined(prop)) {
                        prop = {value: null, type: null};
                    }
                    const entityProp = {
                        Value: prop.value
                    };
                    if (!isNullOrUndefined(prop.type)) {
                        entityProp['Type'] = prop.type;
                    }
                    result[key] = entityProp;
                });
                return {Entity: entityName, Value: result};
            } )
            .switchMap(request =>
                this.http.put<{value: string}>(`${this.backendPrefix}/insert`, request, {}).map(response => {
                    return {
                        response: response,
                        request: request
                    };
                }) )
            .map( ({response, request}) => {
                const mergedResponse = {
                    entityUuid: response.value,
                    entity: new Map( Object.keys(request.Value).map(key => [key, request.Value[key]] as any) )
                };
                return mergedResponse;
            } )
            .toArray()
            .map( next => {
                return { entities: next };
            })
            .map( next => next) as any;
    }

    public update(request: IDataSourceUpdateRequest): Observable<IDataSourceUpdateResponse> {
        return undefined;
    }

    public delete(request: IDataSourceDeleteRequest): Observable<IDataSourceDeleteResponse> {
        return undefined;
    }

    public compositeQuery(request: IDataSourceCompositeRequest): Observable<IDataSourceCompositeResponse> {
        return undefined;
    }

    public getMetadata(request: IDataSourceMetadataRequest): Observable<IDataSourceMetadataResponse> {
        const properties = `properties`;
        const typeProperty = `type`;
        const params = new HttpParams()
            .set('entity', request.entityName);
        return this.http
            .get(`${this.backendPrefix}/metadata`, {params: params})
            .do(next => {
                if (isUndefined(next[properties])) {
                    throw new Error(`${this.serverErrorIntro}Request should contain property ${properties}. now: \n${JSON.stringify(next)}`);
                }
            })
            .do(next => {
                if (next[properties] instanceof Object === false) {
                    throw new Error(`${this.serverErrorIntro}Request property ${properties} should be Array. now \n${JSON.stringify(next)}`);
                }
                for (const prop of next[properties]) {
                    if (isUndefined(prop[typeProperty])) {
                        throw new Error(`${this.serverErrorIntro}Every request property must contain ${typeProperty} property`);
                    }
                }
            })
            .map(next => {
                const result: IDataSourceMetadataResponse = {
                    properties: new Map<string, IMetadataProperty>()
                };
                for (const propKey in next[properties]) {
                    const prop = next[properties][propKey];

                    const resultProperty: IMetadataProperty = {
                        type: prop[typeProperty]
                    };
                    if (prop[typeProperty] === 'Guid') {
                        if (isUndefined(prop['reference'])) {
                            throw new Error(`${this.serverErrorIntro}type 'Guid' must contain reference property`);
                        }
                        resultProperty.reference = prop['reference'];
                    }
                    result.properties.set(propKey, resultProperty);
                }
                return result;
            });
    }

    public getAvailableEntities(): Observable<Array<{ entityName: string, verboseName: string }>> {
        const entitiesProperty = `entities`;
        const nameProperty = `name`;
        return this.http.get(`${this.backendPrefix}/available`)
            .do(next => {
                if (!next[entitiesProperty]) {
                    throw new Error(`${this.serverErrorIntro}Request should contain ${entitiesProperty} property. now:\n${JSON.stringify(next)}`);
                }
            })
            .do(next => {
                if (next[entitiesProperty] instanceof Array === false) {
                    throw new Error(`${this.serverErrorIntro}Request should contain array into ${entitiesProperty} property. now:\n${JSON.stringify(next)}`);
                }
            })
            .do(next => {
                for (const val of next[entitiesProperty]) {
                    if (val instanceof Object === false) {
                        throw new Error(`${this.serverErrorIntro}Every property of request must be a object. now:\n${JSON.stringify(next)}\nproblem property: ${val}`);
                    }
                    if (isUndefined(val[nameProperty])) {
                        throw new Error(`${this.serverErrorIntro}Every property must contain ${nameProperty} property. now:\n${JSON.stringify(next)}\nproblem property: ${val}`);
                    }
                    if (typeof val[nameProperty] !== 'string') {
                        throw new Error(`${this.serverErrorIntro}Every property must contain ${nameProperty} string property. now:\n${JSON.stringify(next)}\nproblem property: ${val}`);
                    }
                }
            })
            .map(next => {
                const request: Array<{ entityName: string, verboseName: string }> = [];
                for (const val of next[entitiesProperty]) {
                    request.push({
                        verboseName: val[nameProperty],
                        entityName: val[nameProperty]
                    });
                }
                return request;
            });
    }

    public count(request: { entity: string; filter?: IDataSourceReadRequestFilterNode; }): Observable<number> {
        const valueProperty = 'value';
        return this.http
            .post(`${this.backendPrefix}/count`, {
                entity: request.entity,
                filter: request.filter
            })
            .do(next => {
                if (isUndefined(next[valueProperty])) {
                    throw new Error(`${this.serverErrorIntro}Property missing ${valueProperty}. now:\n${JSON.stringify(next)}`);
                }
                if (typeof next[valueProperty] !== "number") {
                    throw new Error(`${this.serverErrorIntro}result must be a number. now:\n${JSON.stringify(next)}`);
                }
            })
            .map(next => next[valueProperty]);
    }
}

export { BackendSource };