import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { IAppMenu } from "coreContracts/appMenu/IAppMenu";
import { AppMenu } from "./appMenu.service";
import { AppMenuComponent } from "./appMenuComponent/appMenu.component";
import { AppMenuDataProvider } from "./appMenuDataProvider";


@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        AppMenuComponent
    ],
    entryComponents: [
        AppMenuComponent
    ],
    providers: [
        { provide: IAppMenu, useClass: AppMenu },
        AppMenuDataProvider
    ]
})

class AppMenuModule {
    constructor() {
    }
}

export { AppMenuModule };
