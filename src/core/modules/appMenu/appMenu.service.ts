import { ComponentFactoryResolver, Injectable, Injector, ReflectiveInjector, ViewContainerRef } from "@angular/core";
import { IAppMenu } from "coreContracts/appMenu/IAppMenu";
import { INode } from "coreContracts/appMenu/IAppMenuNode";
import { IWindow, IWindowUnitsFactory } from "coreContracts/window";
import { Observable } from "rxjs/Observable";
import { ReplaySubject } from "rxjs/ReplaySubject";
import { Subject } from "rxjs/Subject";
import { IWhenMenuElementClickPublisher } from "src/core/modules/appMenu/common/interfaces/IWhenMenuElementClickPublisher";
import { AppMenuComponent } from "./appMenuComponent/appMenu.component";
import { AppMenuDataProvider } from "./appMenuDataProvider";
import { IAppMenuNodes } from "./common/interfaces/IAppMenuNodes";
import { AppMenuStateCases, IAppMenuState } from "./common/interfaces/IAppMenuState";
import { IWhenMaximizeMenuClickPublisher } from "./common/interfaces/IWhenMaximizeMenuClickPublisher";
import { IWhenMinimizeMenuClickPublisher } from "./common/interfaces/IWhenMinimizeMenuClickPublisher";

@Injectable()
class AppMenu extends IAppMenu {
    private appMenuNodes: IAppMenuNodes;
    private whenMaximizeMenu: Subject<undefined> = new Subject();
    private whenMinimizeMenu: Subject<undefined> = new Subject();
    private appMenuState: IAppMenuState = {
        whenState: new ReplaySubject(1)
    };
    private whenClickNodeEmitter: Subject<INode> = new Subject();

    constructor(
        private componentFactoryResolver: ComponentFactoryResolver,
        private appMenuDataProvider: AppMenuDataProvider,
    ) {
        super();
        this.appMenuState.whenState.next(AppMenuStateCases.maximized);
        this.appMenuState
            .whenState
            .filter(next => next === AppMenuStateCases.maximized)
            .mapTo(undefined)
            .multicast(this.whenMaximizeMenu)
            .connect();

        this.appMenuState
            .whenState
            .filter(next => next === AppMenuStateCases.minimized)
            .mapTo(undefined)
            .multicast(this.whenMinimizeMenu)
            .connect();
        this.appMenuNodes = {
            nodes: this.appMenuDataProvider.getMenuData()
        };
    }

    public loadNodes(nodes: Array<INode>) {}

    public selectActiveNode(nodeUuid: string) {}

    public render(viewRef: ViewContainerRef, injector: Injector) {
        const newInjector = ReflectiveInjector.resolveAndCreate(
            [
                { provide: IAppMenuNodes, useValue: this.appMenuNodes },
                { provide: IWhenMaximizeMenuClickPublisher, useValue: {publish: () => this.whenMaximizeMenu.next()} },
                { provide: IWhenMinimizeMenuClickPublisher, useValue: {publish: () => this.whenMinimizeMenu.next()} },
                { provide: IAppMenuState, useValue: this.appMenuState },
                { provide: IWhenMenuElementClickPublisher, useValue: {publish: node => this.whenClickNodeEmitter.next(node)}}
            ],
            injector
        );

        const component = this.componentFactoryResolver.resolveComponentFactory(AppMenuComponent);
        viewRef.createComponent(
            component,
            0,
            newInjector
        );
    }

    public minimize() {
        return undefined;
    }

    public maximize() {
        return undefined;
    }

    public toggle() {
        const sub = this.appMenuState.whenState.first().subscribe(next => {
            switch (next){
                case AppMenuStateCases.maximized:
                    this.appMenuState.whenState.next(AppMenuStateCases.minimized);
                    break;
                case AppMenuStateCases.minimized:
                    this.appMenuState.whenState.next(AppMenuStateCases.maximized);
                    break;
                default:
                    throw new Error(`invalid menu state`);
            }
        });
    }

    public whenMinimize(): Observable<undefined> {
        return this.whenMinimizeMenu;
    }

    public whenMaximize(): Observable<undefined> {
        return this.whenMaximizeMenu;
    }

    public whenClickNode(): Observable<INode> {
        return this.whenClickNodeEmitter.asObservable();
    }
}

export {AppMenu};