import { Injectable } from '@angular/core';
import { INode } from "coreContracts/appMenu/IAppMenuNode";
import { Observable } from "rxjs/Observable";

@Injectable()
class AppMenuDataProvider{
    public getMenuData(): Observable<Array<INode>> {
        return Observable.of([{
            verboseName: 'Мои страницы',
            iconHtml: require('./common/icons/myPagesIco.html'),
            smallIconHtml: require('./common/icons/myPagesIco.html'),
            href: '',
            nodeUuid: '1',
            parentNodeUuid: 'string',
            childNodes: [{
                verboseName: 'Рабочее место',
                iconHtml: '',
                smallIconHtml: '',
                href: 'app/entity/list/userWorkflow',
                nodeUuid: '11',
                parentNodeUuid: '1',
                childNodes: null
            }, {
                verboseName: 'Моя страница',
                iconHtml: '',
                smallIconHtml: '',
                href: '',
                nodeUuid: '12',
                parentNodeUuid: '1',
                childNodes: null
            }, {
                verboseName: 'Каталог услуг',
                iconHtml: '',
                smallIconHtml: '',
                href: '',
                nodeUuid: '13',
                parentNodeUuid: '1',
                childNodes: null
            }/*, {
                verboseName: 'Конструктор страниц',
                href: 'app/pageConstructor',
                nodeUuid: 'lol'
            }*/]
        }, {
            verboseName: 'Управление инцидентами',
            iconHtml: require('./common/icons/incidentsControll.html'),
            smallIconHtml: require('./common/icons/incidentsControll.html'),
            href: '',
            nodeUuid: '2',
            parentNodeUuid: '',
            childNodes: [{
                verboseName: 'Инциденты',
                iconHtml: '',
                smallIconHtml: '',
                href: '',
                nodeUuid: '3',
                parentNodeUuid: '2',
                childNodes: null
            }]
        }, {
            verboseName: 'Управление запросами на обслуживание',
            iconHtml: require('./common/icons/znoControllIco.html'),
            smallIconHtml: require('./common/icons/znoControllIco.html'),
            href: '',
            nodeUuid: '4',
            parentNodeUuid: '',
            childNodes: null
        }, {
            verboseName: 'Управление проблемами',
            iconHtml: require('./common/icons/problems.html'),
            smallIconHtml: require('./common/icons/problems.html'),
            href: '',
            nodeUuid: '5',
            parentNodeUuid: '',
            childNodes: null
        }, {
            verboseName: 'Управление изменениями',
            iconHtml: require('./common/icons/changesControll.html'),
            smallIconHtml: require('./common/icons/changesControll.html'),
            href: '',
            nodeUuid: '6',
            parentNodeUuid: '',
            childNodes: null
        }]);
    }
}

export { AppMenuDataProvider };