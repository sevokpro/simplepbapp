abstract class IWhenMinimizeMenuClickPublisher{
    public abstract publish(): void;
}

export {IWhenMinimizeMenuClickPublisher};