import { Subject } from "rxjs/Subject";

enum AppMenuStateCases{
    maximized,
    minimized
}

abstract class IAppMenuState{
    public abstract whenState: Subject<AppMenuStateCases>;
}

export { IAppMenuState, AppMenuStateCases };