abstract class IWhenMaximizeMenuClickPublisher{
    public abstract publish(): void;
}

export {IWhenMaximizeMenuClickPublisher};