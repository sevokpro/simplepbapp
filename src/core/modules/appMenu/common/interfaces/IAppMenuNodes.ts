import { INode } from "coreContracts/appMenu/IAppMenuNode";
import { Observable } from "rxjs/Observable";

abstract class IAppMenuNodes{
    public abstract nodes: Observable<Array<INode>>;
}

export {IAppMenuNodes};