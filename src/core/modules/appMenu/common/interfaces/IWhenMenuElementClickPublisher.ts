import { INode } from "coreContracts/appMenu/IAppMenuNode";

abstract class IWhenMenuElementClickPublisher{
    public abstract publish(node: INode): void;
}

export {IWhenMenuElementClickPublisher};