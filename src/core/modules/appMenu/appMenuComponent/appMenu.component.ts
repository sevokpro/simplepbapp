import { AfterViewInit, Component, ViewChild, ViewChildren } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { isNullOrUndefined } from "@itexpert-dev/tiny-helpers";
import { INode } from "coreContracts/appMenu/IAppMenuNode";
import { IWindow } from "coreContracts/window";
import { Observable } from "rxjs/Observable";
import { ReplaySubject } from "rxjs/ReplaySubject";
import { Subject } from "rxjs/Subject";
import { IAppMenuNodes } from "../common/interfaces/IAppMenuNodes";
import { AppMenuStateCases, IAppMenuState } from "../common/interfaces/IAppMenuState";
import { IWhenMaximizeMenuClickPublisher } from "../common/interfaces/IWhenMaximizeMenuClickPublisher";
import { IWhenMenuElementClickPublisher } from "../common/interfaces/IWhenMenuElementClickPublisher";
import { IWhenMinimizeMenuClickPublisher } from "../common/interfaces/IWhenMinimizeMenuClickPublisher";

@Component({
    templateUrl: './appMenu.template.pug'
})
class AppMenuComponent implements AfterViewInit{
    public nodes: Observable<Array<INode>>;
    public state: Observable<string> = this.appMenuState.whenState.map(next => {
        switch (next){
            case AppMenuStateCases.maximized:
                return 'maximized';
            case AppMenuStateCases.minimized:
                return 'minimized';
            default:
                throw new Error(`invalid app menu state`);
        }
    });
    @ViewChildren('nodesContainer')
    private nodesContainer;

    public centralBlockWidthStyle = new ReplaySubject(1);

    private transform(value) {
        return this.sanitized.bypassSecurityTrustHtml(value);
    }

    public openNode: string;
    public toggleNode(node: INode) {
        this.activeNode = null;
        this.activeParent = null;
        if (!isNullOrUndefined(node.childNodes) && node.childNodes.length !== 0) {
            if (this.openNode === node.nodeUuid) {
                this.openNode = null;
            }else {
                this.openNode = node.nodeUuid;
                this.activateNode(node.childNodes[0]);
            }
        }
    }

    public activeNodeStyle = {
        color: '#4a90e2'
    };
    public activeNode: string;
    public activeParent: string;
    public activateNode(node: INode) {
        this.activeNode = node.nodeUuid;
        this.activeParent = node.parentNodeUuid;
        this.whenElementClick.publish(node);
    }

    public whenMouseEnterNode: Subject<INode> = new Subject();
    public whenMouseLeaveNode: Subject<INode> = new Subject();
    public hoverNode: string;

    constructor(
        private sanitized: DomSanitizer,
        private appMenuNodes: IAppMenuNodes,
        private whenMinimize: IWhenMinimizeMenuClickPublisher,
        private whenMaximize: IWhenMaximizeMenuClickPublisher,
        private appMenuState: IAppMenuState,
        private window: IWindow,
        private whenElementClick: IWhenMenuElementClickPublisher
    ) {
        this.nodes = this.appMenuNodes.nodes; //.do( next => this.toggleNode(next[0]));

        this.whenMouseEnterNode.subscribe(
            next => {
                this.hoverNode = next.nodeUuid;
            }
        );
        this.whenMouseLeaveNode.subscribe(
            next => {
                this.hoverNode = null;
            }
        );
    }

    public ngAfterViewInit(): void {
        this.nodesContainer
            .changes
            .startWith(this.nodesContainer)
            .map( next => next.first )
            .combineLatest(this.window
                .getWindowWidth()
                .merge(this.appMenuState.whenState.delay(0)))
            .map(next => next[0])
            .map( next => next.nativeElement.clientWidth )
            .map(next => {
                return {
                    width: `${next - 51}px`
                };
            })
            .multicast(this.centralBlockWidthStyle)
            .connect();
    }

    public minimize() {
        this.whenMinimize.publish();
    }

    public maximize() {
        this.whenMaximize.publish();
    }
}

export { AppMenuComponent };