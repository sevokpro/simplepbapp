import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { ButtonDrawerComponent } from "./buttonDrawer.component";

@NgModule({
    imports: [CommonModule],
    declarations: [ButtonDrawerComponent],
    entryComponents: [ButtonDrawerComponent]
})
class ButtonDrawerModule{}

export {ButtonDrawerModule};