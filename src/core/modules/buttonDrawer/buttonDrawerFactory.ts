import { ComponentFactoryResolver, Injectable } from "@angular/core";
import { IButtonDrawer } from "coreContracts/buttonDrawer/IButtonDrawer";
import {
    IButtonDrawerCreateParams,
    IButtonDrawerFactory
} from "coreContracts/buttonDrawer/IButtonDrawerFactory";
import { ButtonDrawer } from "./buttonDrawer";

@Injectable()
class ButtonDrawerFactory extends IButtonDrawerFactory{
    constructor(
        private cfr: ComponentFactoryResolver
    ) {
        super();
    }
    public create(createParams: IButtonDrawerCreateParams): IButtonDrawer {
        return new ButtonDrawer(this.cfr, createParams);
    }
}

export {ButtonDrawerFactory};