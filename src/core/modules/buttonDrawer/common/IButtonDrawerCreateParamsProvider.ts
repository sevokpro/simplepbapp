import { IButtonDrawerCreateParam } from "coreContracts/buttonDrawer/IButtonDrawerFactory";

abstract class IButtonDrawerCreateParamsProvider{
    public abstract getButtons(): Array<IButtonDrawerCreateParam>;
}

export {IButtonDrawerCreateParamsProvider};