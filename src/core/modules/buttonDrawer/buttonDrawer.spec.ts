import { CommonModule } from "@angular/common";
import { Component, ComponentFactoryResolver, Injector, NgModule, ViewContainerRef } from "@angular/core";
import { TestBed } from "@angular/core/testing";
import { IButtonDrawer } from "coreContracts/buttonDrawer/IButtonDrawer";
import { ButtonDrawer } from "./buttonDrawer";
import { ButtonDrawerComponent } from "./buttonDrawer.component";

@Component({
    template: ''
})
class TestEnvComponent{
    constructor(
        public vcr: ViewContainerRef
    ) {}
}

@NgModule({
    imports: [CommonModule],
    declarations: [ButtonDrawerComponent],
    entryComponents: [ButtonDrawerComponent]
})
class TestEnvModule{}

describe('button drawer tests', () => {
    let buttonDrawer: ButtonDrawer;
    let vcr: ViewContainerRef;
    let injector: Injector;

    beforeEach(done => {
        TestBed
            .resetTestingModule()
            .configureTestingModule({
                imports: [TestEnvModule],
                providers: [],
                declarations: [TestEnvComponent]
            });

        vcr = TestBed.createComponent(TestEnvComponent).componentInstance.vcr;
        injector = TestBed.get(Injector);
        const cfr = TestBed.get(ComponentFactoryResolver);
        buttonDrawer = new ButtonDrawer(cfr, [{label: 'lol'}]);
        done();
    });

    it('check that instance created', () => {
        expect(buttonDrawer).not.toBeUndefined();
    });

    it('drawer must extend IButtonDrawer abstract class', () => {
        expect(buttonDrawer instanceof IButtonDrawer).toBeTruthy();
    });

    it('expect that render don`t throw error', () => {
        expect(() => buttonDrawer.render(vcr, injector)).not.toThrow();
    });

    it('expect that after render will be one element', () => {
        buttonDrawer.render(vcr, injector);
        expect(vcr.length).toBe(1);
    });

    it('expect that when click don`t throw error', () => {
        expect(() => buttonDrawer.whenClick()).not.toThrowError();
    });

    it('expect that subscribe on clicks not throw errors', () => {
        expect(() => buttonDrawer.whenClick().subscribe()).not.toThrowError();
    });
});