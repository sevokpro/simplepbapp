import { Component } from "@angular/core";
import { IButtonDrawerCreateParam } from "coreContracts/buttonDrawer/IButtonDrawerFactory";
import { IButtonDrawerCreateParamsProvider } from "./common/IButtonDrawerCreateParamsProvider";

interface IExtendButtonParam extends IButtonDrawerCreateParam{
    ngStyle: Object;
    requireDropdownArrow: boolean;
}

@Component({
    templateUrl: './buttonDrawer.component.template.pug',
    styleUrls: ['./buttonDrawerComponentStyle.styl']
})
class ButtonDrawerComponent{
    public buttonsParams: Array<IExtendButtonParam>;
    constructor(
        buttonParamsProvider: IButtonDrawerCreateParamsProvider
    ) {
        this.buttonsParams = buttonParamsProvider.getButtons().map(val => {
            const additionalProps: {
                ngStyle: Object,
                requireDropdownArrow: boolean
            } = {
                ngStyle: null,
                requireDropdownArrow: null
            };
            switch (val.style){
                case "base":
                    additionalProps.ngStyle = { 'background-color': '#4a90e2', color: 'white', border: 'none' };
                    break;
                case "lite":
                    additionalProps.ngStyle = { 'background-color': 'white', color: '#4a90e2', border: 'solid 1px #4a90e2' };
                    break;
                case "danger":
                    additionalProps.ngStyle = { 'background-color': '#e40420', color: 'white', border: 'none' };
                    break;
                default:
                    throw new Error(`unknow style attribute: ${val.style}`);
            }

            switch (val.type){
                case "default":
                    additionalProps.requireDropdownArrow = false;
                    break;
                case "dropdown":
                    additionalProps.requireDropdownArrow = true;
                    break;
                default:
                    throw new Error(`unknow button type: ${val.type}`);
            }
            return {...val, ...additionalProps};
        });
    }
}

export {ButtonDrawerComponent};