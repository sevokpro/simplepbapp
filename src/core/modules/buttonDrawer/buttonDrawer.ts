import {
    ComponentFactoryResolver, ComponentRef, Injectable, Injector, ReflectiveInjector,
    ViewContainerRef
} from "@angular/core";
import { isNullOrUndefined } from "@itexpert-dev/tiny-helpers";
import { IButtonDrawer } from "coreContracts/buttonDrawer/IButtonDrawer";
import { IButtonDrawerCreateParams } from "coreContracts/buttonDrawer/IButtonDrawerFactory";
import { Observable } from "rxjs/Observable";
import { ButtonDrawerComponent } from "./buttonDrawer.component";
import { IButtonDrawerCreateParamsProvider } from "./common/IButtonDrawerCreateParamsProvider";

class ButtonDrawer extends IButtonDrawer{
    public render(viewContainer: ViewContainerRef, injector: Injector): ComponentRef<any> {
        const newInjector = ReflectiveInjector.resolveAndCreate(
            [
                {provide: IButtonDrawerCreateParamsProvider, useValue: {getButtons: () => this.params} as IButtonDrawerCreateParamsProvider}
            ],
            injector
        );

        return (viewContainer.createComponent(
            this.cfr.resolveComponentFactory(ButtonDrawerComponent),
            0,
            newInjector
        ));
    }

    public whenClick(): Observable<string> {
        return Observable.of(null);
    }

    constructor(
        private cfr: ComponentFactoryResolver,
        private params: IButtonDrawerCreateParams
    ) {
        super();
        if (isNullOrUndefined(params)) {
            throw new Error(`buttons params must be defined`);
        }
    }
}

export {ButtonDrawer};