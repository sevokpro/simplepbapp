import { TestBed } from "@angular/core/testing";
import { IButtonDrawer } from "coreContracts/buttonDrawer/IButtonDrawer";
import { IButtonDrawerFactory } from "coreContracts/buttonDrawer/IButtonDrawerFactory";
import { ButtonDrawerFactory } from "./buttonDrawerFactory";

describe('button drawer factory tests', () => {
    let buttonDrawerFactory: ButtonDrawerFactory;

    beforeEach(() => {
        TestBed
            .resetTestingModule()
            .configureTestingModule({
                providers: [ButtonDrawerFactory]
            });

        buttonDrawerFactory = TestBed.get(ButtonDrawerFactory);
    });

    it('factory must be defined', () => {
        expect(buttonDrawerFactory).not.toBeUndefined();
    });

    it('factory must exend IFactory', () => {
        expect(buttonDrawerFactory instanceof IButtonDrawerFactory).toBeTruthy();
    });

    it('factory method must`t throw error when create', () => {
        buttonDrawerFactory.create([{label: 'lol'}]);
    });

    it(`factory method must retirurn instance of IButtonDrawer`, () => {
        const buttonDrawerInstance = buttonDrawerFactory.create([{label: 'lol'}]);
        expect(buttonDrawerInstance instanceof IButtonDrawer).toBeTruthy();
    });
});