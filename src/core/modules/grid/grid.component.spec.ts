import { ComponentFixture, TestBed } from "@angular/core/testing";
import { Observable } from "rxjs/Observable";
import { ICellPublisher } from "./common/ICellPublisher";
import { IViewRowsProvider } from "./common/IViewRowsProvider";
import { GridComponent } from "./grid.component";

describe('tests for view component', () => {
    let componentFixture: ComponentFixture<GridComponent>;
    beforeEach(done => {
        TestBed
            .resetTestingModule()
            .configureTestingModule({
                providers: [
                    {provide: ICellPublisher, useValue: {publish: () => {}} as ICellPublisher},
                    {provide: IViewRowsProvider, useValue: {getViewRows: () => Observable.of([[30, 70]])} as IViewRowsProvider}
                ],
                declarations: [GridComponent]
            });
        componentFixture = TestBed.createComponent(GridComponent);
        componentFixture.autoDetectChanges();
        componentFixture.whenStable().then(() => done()).catch(err => console.log(err));
    });

    it('resolve cmponent ok ok', () => {
        expect(componentFixture).not.toBeUndefined();
        expect(componentFixture.isStable()).toBeTruthy();
    });
});