import { ComponentFactoryResolver, Injectable } from "@angular/core";
import { IGrid } from "coreContracts/grid/IGrid";
import { IGridFactory } from "coreContracts/grid/IGridFactory";
import { IGridParameters } from "coreContracts/grid/IGridParameters";
import { Grid } from "./grid";

@Injectable()
class GridFactory implements IGridFactory{
    constructor(private cfr: ComponentFactoryResolver) {

    }
    public create(params: IGridParameters): IGrid {
        return new Grid(params, this.cfr);
    }
}

export {GridFactory};