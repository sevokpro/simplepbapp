import { ComponentFactoryResolver, ComponentRef, Injector, ReflectiveInjector, ViewContainerRef } from "@angular/core";
import { isNullOrUndefined } from "@itexpert-dev/tiny-helpers";
import { GridRepeatMode } from "coreContracts/grid/GridRepeatMode";
import { IGrid } from "coreContracts/grid/IGrid";
import { IGridCell } from "coreContracts/grid/IGridICell";
import { IGridParameters, IGridRowsCollection } from "coreContracts/grid/IGridParameters";
import { Observable } from "rxjs/Observable";
import { ReplaySubject } from "rxjs/ReplaySubject";
import { Subject } from "rxjs/Subject";
import { Subscription } from "rxjs/Subscription";
import { ICellPublisher } from "./common/ICellPublisher";
import { IViewRowsProvider } from "./common/IViewRowsProvider";
import { GridComponent } from "./grid.component";

type cellRowId = number;
type cellColumnId = number;

interface IGridCellsMapElements{
    publisherSubject: Subject<ViewContainerRef>;
    lastMulticast?: Subscription;
}

class Grid extends IGrid{
    private viewRows: Observable<IGridRowsCollection>;
    private aliases: Map<string, IGridCell>;
    constructor(params: IGridParameters, private cfr: ComponentFactoryResolver) {
        super();
        this.aliases = params.aliases;
        let gridRows: IGridRowsCollection = isNullOrUndefined(params.rows) ? null : [...params.rows];
        if ( params.rowsRepeatMode === GridRepeatMode.repeat ) {
            while (gridRows.length <= params.rowsCount) {
                gridRows = [...gridRows, ...params.rows];
            }
            gridRows.length = params.rowsCount;
        }

        if (!isNullOrUndefined(gridRows)) {
            this.viewRows = Observable.from(gridRows).toArray();
        }else {
            this.viewRows = Observable.empty();
        }
    }
    private gridCellsMap: Map<cellRowId, Map<cellColumnId, IGridCellsMapElements>> = new Map();
    private tryGetCellReplayCell(cell: IGridCell): IGridCellsMapElements {
        if (!this.gridCellsMap.has(cell.row)) {
            this.gridCellsMap.set(cell.row, new Map());
        }
        const cellRow = this.gridCellsMap.get(cell.row);
        if (!cellRow.has(cell.column)) {
            cellRow.set(cell.column, {
                publisherSubject: new ReplaySubject(1)
            });
        }
        return cellRow.get(cell.column);
    }
    private publishCell(cell: IGridCell, vcr: Observable<ViewContainerRef>) {
        const currentCell = this.tryGetCellReplayCell(cell);
        vcr.multicast(currentCell.publisherSubject).connect();
    }

    public render(viewContainerRef: ViewContainerRef, injector: Injector): ComponentRef<any> {
        const componentInstance = this.cfr.resolveComponentFactory<GridComponent>(GridComponent);
        const extendInjector: Injector = ReflectiveInjector.resolveAndCreate(
            [
                {provide: ICellPublisher, useValue: {publish: (cell, vcr) => this.publishCell(cell, vcr) } as ICellPublisher},
                {provide: IViewRowsProvider, useValue: {getViewRows: () => this.viewRows} as IViewRowsProvider}
            ],
            injector
        );
        const element = viewContainerRef
            .createComponent(
                componentInstance,
                0,
                extendInjector);
        return element;
    }

    public getCell(cell: IGridCell): Observable<ViewContainerRef> {
        return this.tryGetCellReplayCell(cell).publisherSubject.asObservable();
    }

    public getCellByAlias(alias: string): Observable<ViewContainerRef> {
        return this.getCell( this.aliases.get(alias) );
    }
}

export {Grid};