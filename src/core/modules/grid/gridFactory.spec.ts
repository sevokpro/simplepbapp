import { TestBed } from "@angular/core/testing";
import { GridRepeatMode } from "coreContracts/grid/GridRepeatMode";
import { IGrid } from "coreContracts/grid/IGrid";
import { GridFactory } from "./gridFactory";

describe('tests for grid factory', () => {
    let gridFactory: GridFactory;
    const simpleGridParams = {rows: [], rowsCount: 5, rowsRepeatMode: GridRepeatMode.noRepeat};

    beforeEach(() => {
        TestBed.resetTestingModule();
        TestBed.configureTestingModule({
            providers: [GridFactory]
        });
        gridFactory = TestBed.get(GridFactory);
    });

    it('expect that gridFactory not undefined', () => {
        expect(gridFactory).not.toBeUndefined();
    });

    it('expect that factory method not throw error', () => {
        expect(() => gridFactory.create(simpleGridParams)).not.toThrowError();
    });

    it('epxect that factory method return right instance', () => {
        expect(gridFactory.create(simpleGridParams) instanceof IGrid).toEqual(true);
    });
});