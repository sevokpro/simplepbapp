import { ViewContainerRef } from "@angular/core";
import { IGridCell } from "coreContracts/grid/IGridICell";
import { Observable } from "rxjs/Observable";

abstract class ICellPublisher{
    public abstract publish(cell: IGridCell, vcr: Observable<ViewContainerRef>);
}

export {ICellPublisher};