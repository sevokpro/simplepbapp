import { IGridRowsCollection } from "coreContracts/grid/IGridParameters";
import { Observable } from "rxjs/Observable";

abstract class IViewRowsProvider{
    public abstract getViewRows(): Observable<IGridRowsCollection>;
}

export {IViewRowsProvider};