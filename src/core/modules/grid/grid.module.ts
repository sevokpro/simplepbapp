import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { GridComponent } from "./grid.component";

@NgModule({
    imports: [CommonModule],
    declarations: [GridComponent],
    entryComponents: [GridComponent]
})
class GridModule{}

export {GridModule};