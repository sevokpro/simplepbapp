import { CommonModule } from "@angular/common";
import { Component, ComponentFactoryResolver, Injector, NgModule, ViewContainerRef } from "@angular/core";
import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { BrowserModule } from "@angular/platform-browser";
import { GridRepeatMode } from "coreContracts/grid/GridRepeatMode";
import { IGridCell } from "coreContracts/grid/IGridICell";
import { IGridParameters } from "coreContracts/grid/IGridParameters";
import { Observable } from "rxjs/Observable";
import { Subject } from "rxjs/Subject";
import { Grid } from "./grid";
import { GridComponent } from "./grid.component";

const simpleGridParams: IGridParameters = {
    rows: [[30, 40], [50, 20]],
    rowsRepeatMode: GridRepeatMode.noRepeat
};

@Component({
    template: '',
})
class TestEnvComponent{
    constructor(
        public injector: Injector,
        public viewContainerReference: ViewContainerRef
    ) {}
}

@NgModule({
    imports: [BrowserModule, CommonModule],
    declarations: [GridComponent, TestEnvComponent],
    entryComponents: [GridComponent]
})
class TestEnvModule{}


describe('tests for grid service', () => {
    describe('check that create instance correct', () => {
        it('create simple instance without errors', () => {
            const cfr = TestBed.resetTestingModule().configureTestingModule({}).get(ComponentFactoryResolver);
            expect( () =>  new Grid(simpleGridParams, cfr)).not.toThrowError();
        });
    });

    describe('tests for grid functional', () => {
        let simleGrid: Grid;
        let envComponent: ComponentFixture<TestEnvComponent>;
        const firstCell: IGridCell = {row: 0, column: 0};

        beforeEach(done => {

            TestBed.resetTestingModule();
            TestBed.configureTestingModule({
                imports: [TestEnvModule]
            });

            simleGrid = new Grid(simpleGridParams, TestBed.get(ComponentFactoryResolver));
            envComponent = TestBed.createComponent(TestEnvComponent);
            envComponent.whenStable().then(() => done()).catch(err => console.log(err));
        });


        it('get cell dont throw error', () => {
            expect(() => simleGrid.getCell(firstCell)).not.toThrowError();
        });

        it('get cell must return observable', () => {
            expect(simleGrid.getCell(firstCell) instanceof Observable).toBeTruthy();
        });

        it('get cell must return value in 1st second', async(() => {
            simleGrid.render(envComponent.componentInstance.viewContainerReference, envComponent.componentInstance.injector);
            envComponent.detectChanges();
            Observable
                .interval(1e3)
                .mapTo('timer win')
                .race(simleGrid
                    .getCell(firstCell)
                    .first()
                ).subscribe(
                    next => {
                        expect(next).not.toBe('timer win');
                    });
        }));

        it('get cell must return instance of view container ref', async(() => {
            simleGrid.render(envComponent.componentInstance.viewContainerReference, envComponent.componentInstance.injector);
            envComponent.detectChanges();
            simleGrid.getCell(firstCell)
                .race(Observable.timer(1e3).mapTo('timer win'))
                .subscribe(
                    next => {
                        expect(next.constructor.toString()).toContain('ViewContainerRef');
                    }
                );
        }));
    });


});