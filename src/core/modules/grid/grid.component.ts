import { AfterViewInit, Component, QueryList, ViewChildren, ViewContainerRef } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { Observer } from "rxjs/Observer";
import { ICellPublisher } from "./common/ICellPublisher";
import { IViewRowsProvider } from "./common/IViewRowsProvider";

interface IFormattedResult{
    rowIndex: number;
    columnIndex: number;
    container: ViewContainerRef;
}

type IBootstrapGrid = Array<Array<{bootstrapWidth: Array<string>}>>;

@Component({
    templateUrl: './grid.component.template.pug',
})
class GridComponent implements AfterViewInit{
    @ViewChildren('cell', {read: ViewContainerRef})
    private cell: QueryList<any>;
    public viewRows: Observable<IBootstrapGrid> = this.viewRowsProvider
        .getViewRows()
        .map(next => {
            const result = next.map(row =>
                row.map(cell => {
                    return {
                        bootstrapWidth: [`col-${Math.round(cell * 0.12)}`]
                    };
                })
            );
            return result;
        });

    constructor(
        private cellPublisher: ICellPublisher,
        private viewRowsProvider: IViewRowsProvider
    ) {
    }

    public ngAfterViewInit(): void {
        this.cell
            .changes
            .startWith(...this.cell.toArray())
            .map(next => {
                const result: IFormattedResult = {
                    rowIndex: parseInt(next.element.nativeElement.attributes.rowIndex.value),
                    columnIndex: parseInt(next.element.nativeElement.attributes.columnindex.value),
                    container: next
                };
                return result;
            })
            .subscribe(
                next => {
                    this.cellPublisher.publish({row: next.rowIndex, column: next.columnIndex}, Observable.create((observer: Observer<ViewContainerRef>) => {
                        observer.next(next.container);
                    }));
                },
                err => console.warn(err),
                () => console.log('complete')
            );
    }
}

export {GridComponent};