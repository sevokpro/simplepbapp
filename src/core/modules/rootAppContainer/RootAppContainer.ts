import { Injector, ViewContainerRef } from "@angular/core";
import { isNullOrUndefined } from "@itexpert-dev/tiny-helpers";
import { IRootAppContainer } from "coreContracts/rootAppContainer";

class RootAppContainer extends IRootAppContainer {
    private injectorField: Injector;
    private viewContainerRefField: ViewContainerRef;

    public get viewContainerRef(): ViewContainerRef {
        return this.viewContainerRefField;
    }

    public set viewContainerRef(viewContainerRef: ViewContainerRef) {
        if (isNullOrUndefined(this.viewContainerRef))
            this.viewContainerRefField = viewContainerRef;
    }

    public get injector(): Injector {
        return this.injectorField;
    }

    public set injector(injector: Injector) {
        if (isNullOrUndefined(this.injector))
            this.injectorField = injector;
    }
}

export { RootAppContainer };