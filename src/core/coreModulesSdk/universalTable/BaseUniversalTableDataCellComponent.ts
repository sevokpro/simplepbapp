import { IUniversalTableCellPosition } from "coreContracts/universalTable/IUniversalTableCellPosition";
import { IUniversalTableCtrl } from "coreContracts/universalTable/IUniversalTableCtrl";
import { BaseUniversalTableComponent } from "./BaseUniversalTableComponent";

abstract class BaseUniversalTableCellComponent extends BaseUniversalTableComponent {
    protected rowIndex: number;
    protected cellIndex: number;
    public cellData: any;

    protected viewValue: any;

    public constructor(
        tableCtrl: IUniversalTableCtrl,
        cellPosition: IUniversalTableCellPosition
    ) {
        super(
            tableCtrl
        );
        this.rowIndex = cellPosition.rowIndex;
        this.cellIndex = cellPosition.cellIndex;
    }
}

export { BaseUniversalTableCellComponent };