import { IUniversalTableCtrl } from "coreContracts/universalTable/IUniversalTableCtrl";
import { BaseUniversalTableComponent } from "./BaseUniversalTableComponent";

abstract class BaseUniversalTableContainerComponent extends BaseUniversalTableComponent {
    public constructor(
        tableCtrl: IUniversalTableCtrl
    ) {
        super(tableCtrl);
    }
}

export { BaseUniversalTableContainerComponent };