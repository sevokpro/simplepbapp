import { IUniversalTableCtrl } from "coreContracts/universalTable/IUniversalTableCtrl";

class BaseUniversalTableComponent {
    protected tableCtrl: IUniversalTableCtrl;
    public constructor(
        tableCtrl: IUniversalTableCtrl
    ) {
        this.tableCtrl = tableCtrl;
    }
}

export { BaseUniversalTableComponent };