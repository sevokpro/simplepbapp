import { IUniversalTableConfigurator } from "coreContracts/universalTable/IUniversalTableConfigurator";
import { IUniversalTableCtrl } from "coreContracts/universalTable/IUniversalTableCtrl";
import { BaseUniversalTableComponent } from "./BaseUniversalTableComponent";

abstract class BaseUniversalTablePaginator extends BaseUniversalTableComponent {
    public offset: number;
    public count: number;
    public limitSteps: Array<number>;
    public limit: number;
    public page: number;

    public constructor(
        tableCtrl: IUniversalTableCtrl,
        universalTableConfig: IUniversalTableConfigurator
    ) {
        super(tableCtrl);
        this.limitSteps = universalTableConfig.getDefaultLimitSteps();
        let catchFirstMessage = true;
        const subscriptionOnChangeState = tableCtrl.stateStream.filter(next => {
            if (catchFirstMessage === true) {
                catchFirstMessage = false;
                this.limit = next.currentState.limit;
                this.tableCtrl.initParams.dataProvider.getCount({ filter: next.currentState.filter }).subscribe(
                    next => {
                        this.count = next;
                    }
                );
                this.page = next.currentState.offset / next.currentState.limit;
                return false;
            }
            return true;
        });

        subscriptionOnChangeState
            .filter(next => next.difference.hasOwnProperty('filter'))
            .pluck('currentState')
            .pluck('filter')
            .subscribe(
            next => {
                this.tableCtrl.initParams.dataProvider.getCount({ filter: next as any }).subscribe(
                    next => {
                        this.count = next;
                    }
                );
            }
            );

        subscriptionOnChangeState
            .filter(next => {
                if (next.difference.hasOwnProperty('limit')) {
                    return true;
                }
                return false;
            })
            .pluck('currentState')
            .pluck('limit')
            .filter(next => next !== this.limit)
            .subscribe(
            next => {
                this.limit = next as number;
            }
            );
    }

    public changeLimit(newValue: string) {
        const newLimit = parseInt(newValue);
        this.limit = newLimit;
        this.tableCtrl.updateState({
            limit: newLimit
        });
    }
    public changePage(newPage: number) {
        this.page = newPage;
        this.tableCtrl.updateState({ offset: (this.page - 1) * this.limit });
    }
}

export { BaseUniversalTablePaginator };