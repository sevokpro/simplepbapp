import { isNull, isNullOrUndefined } from "@itexpert-dev/tiny-helpers";
import { IUniversalTableCtrl } from "coreContracts/universalTable/IUniversalTableCtrl";
import { IUniversalTableHeader } from "coreContracts/universalTable/IUniversalTableDataProvider";
import { IUniversalTableViewRow } from "coreContracts/universalTable/IUniversalTableViewRow";
import { Observable } from "rxjs";
import { BaseUniversalTableComponent } from "./BaseUniversalTableComponent";

abstract class BaseUniversalTableGridComponent extends BaseUniversalTableComponent {
    public headers: Observable<Array<IUniversalTableHeader>>;
    public rows: Observable<Array<IUniversalTableViewRow>>;

    public constructor(tableCtrl: IUniversalTableCtrl) {
        super(tableCtrl);
        this.headers = this.tableCtrl.viewScope.headers.map(next => next.headers);
        this.rows = this.tableCtrl.viewScope.rows.map(next => {
            if (isNull(next)) {
                return [];
            }
            const result: Array<IUniversalTableViewRow> = [];
            for (const row of next.data) {
                const resultRow: IUniversalTableViewRow = {
                    cells: []
                };
                for (const field of next.viewFields) {
                    let dataField;
                    if (row.has(field)) {
                        dataField = row.get(field);
                    }
                    if (isNullOrUndefined(dataField)) {
                        dataField = {
                            type: 'String',
                            value: ''
                        };
                    }
                    resultRow.cells.push({
                        value: dataField.value,
                        type: dataField.type
                    });
                }
                result.push(resultRow);
            }
            return result;
        });
    }
}
export { BaseUniversalTableGridComponent };