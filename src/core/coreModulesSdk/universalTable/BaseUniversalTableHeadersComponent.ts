import { IUniversalTableCtrl } from "coreContracts/universalTable/IUniversalTableCtrl";
import { IUniversalTableHeaderData } from "coreContracts/universalTable/IUniversalTableHeaderData";
import { IUniversalTableViewHeader } from "coreContracts/universalTable/IUniversalTableViewHeader";
import { BaseUniversalTableComponent } from "./BaseUniversalTableComponent";

class BaseUniversalTableHeadersComponent extends BaseUniversalTableComponent {
    protected headerIndex: number;
    public headerData: IUniversalTableViewHeader;

    public constructor(tableCtrl: IUniversalTableCtrl, headerData: IUniversalTableHeaderData) {
        super(tableCtrl);
        this.headerData = headerData.headerData;
        this.headerIndex = headerData.headerIndex;
    }
}

export { BaseUniversalTableHeadersComponent };