import { Injector } from "@angular/core";
import { TestBed } from "@angular/core/testing";
import { Router } from "@angular/router";
import { RouterTestingModule } from "@angular/router/testing";
import { CoreModule } from "./core.module";

describe('test for core module', () => {
    it('expect that core module create without errors', () => {
        expect(() => {
            TestBed
                .resetTestingModule()
                .configureTestingModule({
                    imports: [CoreModule, RouterTestingModule]
                })
                .get(Injector);
        }).not.toThrowError();
    });

});