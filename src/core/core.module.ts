import { NgModule } from "@angular/core";
import { IAdvancedFormConfigurator } from "coreContracts/advancedForm/IAdvancedFormConfigurator";
import { IAdvancedFormFactory } from "coreContracts/advancedForm/IAdvancedFormFactory";
import { IAdvancedFormDataProviderFactory } from "coreContracts/advancedForm/IAdvencedFormDataProviderFactory";
import { IBackendSource } from "coreContracts/backendSource";
import { IButtonDrawerFactory } from "coreContracts/buttonDrawer/IButtonDrawerFactory";
import { IChat } from "coreContracts/chat";
import { IEntityTabsFactory } from "coreContracts/entityTabs";
import { IGridFactory } from "coreContracts/grid/IGridFactory";
import { IIndexedDbConfigurator } from "coreContracts/indexedDbSource/IIndexedDbConfigurator";
import { IIndexedDbSource } from "coreContracts/indexedDbSource/IIndexedDbSource";
import { INotify } from "coreContracts/notify";
import { IPageBuilderComponentRegistry } from "coreContracts/pageBuilder/IPageBuilderComponentRegistry";
import { IPageBuilderConstructorFactory } from "coreContracts/pageBuilder/IPageBuilderConstructorFactory";
import { IPageBuilderFactory } from "coreContracts/pageBuilder/IPageBuilderFactory";
import { IPageBuilderVariableRegistry } from "coreContracts/pageBuilder/IPageBuilderVariableRegister";
import { IPageBuilderSwitchContainerEventsFactory } from "coreContracts/pageBuilder/renderableComponentsEvents/IPageBuilderSwitchContainerEventsFactory";
import { IRootAppContainer } from "coreContracts/rootAppContainer";
import { ITabsDrawerFactory } from "coreContracts/tabsDrawer/ITabsDrawerFactory";
import { IUniversalTableConfigurator } from "coreContracts/universalTable/IUniversalTableConfigurator";
import { IUniversalTableDataProviderFactory } from "coreContracts/universalTable/IUniversalTableDataProviderFactory";
import { IUniversalTableCtrlFactory } from "coreContracts/universalTable/IUniversalTableService";
import { IWindow, IWindowUnitsFactory } from "coreContracts/window";
import { AdvancedFormConfigurator } from "src/core/modules/advancedForm/advancedFormConfiguration";
import { AppMenuModule } from "src/core/modules/appMenu/appMenuModule";
import { IndexedDbConfigurator } from "src/core/modules/indexedDbSource/indexedDbConfigurator";
import { PageBuilderConstructorFactory } from "src/core/modules/pageBuilder/pageBuilderConstructorFactory";
import { WindowUnitsFactory } from "src/core/modules/window/windowUnitsFactory";
import { AdvancedFormDataProviderFactory } from "./modules/advancedForm/advancedFormDataProviderFactory";
import { AdvancedFormFactory } from "./modules/advancedForm/advancedFormFactory";
import { BackendSource } from "./modules/BackendSource/BackendSource.service";
import { ButtonDrawerModule } from "./modules/buttonDrawer/buttonDrawer.module";
import { ButtonDrawerFactory } from "./modules/buttonDrawer/buttonDrawerFactory";
import { ChatAppModule, ChatManager } from "./modules/chat/chatApp.module";
import { EntityTabComponentsContainerModule } from "./modules/entityTabs/EntityTabComponentsContainerModule";
import { EntityTabsFactory } from "./modules/entityTabs/EntityTabsFactory";
import { GridModule } from "./modules/grid/grid.module";
import { GridFactory } from "./modules/grid/gridFactory";
import { IndexedDbSource } from "./modules/indexedDbSource/indexedDbSource";
import { NotifyCommonServicesModule } from "./modules/notify/NotifyCommonServicesModule";
import { NotifyComponentsContainerModule } from "./modules/notify/NotifyComponentsContainerModule";
import { NotifyControllerFactory } from "./modules/notify/services/notifyControllerFactory";
import { PageBuilderModule } from "./modules/pageBuilder/pageBuilder.module";
import { PageBuilderComponentRegistry } from "./modules/pageBuilder/pageBuilderComponentRegistry";
import { PageBuilderFactory } from "./modules/pageBuilder/pageBuilderFactory";
import { PageBuilderVariableRegister } from "./modules/pageBuilder/pageBuilderVariableRegister";
import { SwitchContainerEventsFactory } from "./modules/pageBuilder/renderableComponents/switchContainer/switchContainerEventsFactory";
import { RootAppContainer } from "./modules/rootAppContainer/RootAppContainer";
import { TabsDrawerModule } from "./modules/tabsDrawer/tabsDrawer.module";
import { TabsDrawerFactory } from "./modules/tabsDrawer/tabsDrawerFactory";
import { UniversalTableConfigurator } from "./modules/universalTable/universalTableConfigurator/universalTableConfigurator";
import { UniversalTableCtrlFactory } from "./modules/universalTable/universalTableCtrlFactory/universalTableCtrlFactory";
import { UniversalTableDataProviderFactory } from "./modules/universalTable/universalTableDataProviderFactory";
import { Window } from "./modules/window/window.service";

@NgModule({
    imports: [
        NotifyComponentsContainerModule,
        EntityTabComponentsContainerModule,
        AppMenuModule,
        NotifyCommonServicesModule,
        ChatAppModule,
        GridModule,
        TabsDrawerModule,
        ButtonDrawerModule,
        PageBuilderModule
    ],
    providers: [
        { provide: IChat, useClass: ChatManager },

        { provide: IBackendSource, useClass: BackendSource },

        { provide: IWindow, useClass: Window },
        { provide: IWindowUnitsFactory, useClass: WindowUnitsFactory},

        { provide: IAdvancedFormFactory, useClass: AdvancedFormFactory },
        { provide: IAdvancedFormConfigurator, useClass: AdvancedFormConfigurator },
        { provide: IAdvancedFormDataProviderFactory, useClass: AdvancedFormDataProviderFactory },

        { provide: IEntityTabsFactory, useClass: EntityTabsFactory },

        { provide: IUniversalTableCtrlFactory, useClass: UniversalTableCtrlFactory },
        { provide: IUniversalTableConfigurator, useClass: UniversalTableConfigurator },
        { provide: IUniversalTableDataProviderFactory, useClass: UniversalTableDataProviderFactory },

        { provide: IRootAppContainer, useClass: RootAppContainer },

        { provide: INotify, useClass: NotifyControllerFactory },

        { provide: IButtonDrawerFactory, useClass: ButtonDrawerFactory },

        {provide: IIndexedDbConfigurator, useClass: IndexedDbConfigurator},
        {provide: IIndexedDbSource, useClass: IndexedDbSource},

        {provide: IGridFactory, useClass: GridFactory},

        {provide: ITabsDrawerFactory, useClass: TabsDrawerFactory},

        {provide: IPageBuilderFactory, useClass: PageBuilderFactory},
        {provide: IPageBuilderComponentRegistry, useClass: PageBuilderComponentRegistry},
        {provide: IPageBuilderVariableRegistry, useClass: PageBuilderVariableRegister},
        {provide: IPageBuilderSwitchContainerEventsFactory, useClass: SwitchContainerEventsFactory},
        {provide: IPageBuilderConstructorFactory, useClass: PageBuilderConstructorFactory}
    ]
})
class CoreModule {
    constructor(

    ) {

    }
}

export { CoreModule };