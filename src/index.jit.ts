import "./vendor/index";

import { platformBrowserDynamic } from "@angular/platform-browser-dynamic";
import { BootstrapModule } from "./bootstrap.module";

platformBrowserDynamic().bootstrapModule(BootstrapModule).then();