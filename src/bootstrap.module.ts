import { CommonModule } from "@angular/common";
import { Component, NgModule, ViewContainerRef} from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { RouterModule } from "@angular/router";
import { MainAppModule } from "./app/app.module";
import { CoreModule } from "./core/core.module";

@Component({
    template: ``
})
export class AppContainer {
    constructor(
        public viewContainerRef: ViewContainerRef
    ){}
}

@NgModule({
    imports: [
        BrowserModule,
        CommonModule,
        CoreModule,
        MainAppModule,
        RouterModule.forRoot([])
    ],
    declarations: [AppContainer],
    bootstrap: [AppContainer],
})
class BootstrapModule {
    constructor() {}
}

export { BootstrapModule };