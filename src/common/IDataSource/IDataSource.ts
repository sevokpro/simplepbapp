import { Observable } from "rxjs";
import { IDataSourceCompositeRequest } from "./compositeQuery/request/IDataSourceCompositeRequest";
import { IDataSourceCompositeResponse } from "./compositeQuery/response/IDataSourceCompositeResponse";
import { IDataSourceCreateRequest } from "./create/request/IDataSourceCreateRequest";
import { IDataSourceCreateResponse } from "./create/response/IDataSourceCreateResponse";
import { IDataSourceDeleteRequest } from "./delete/request/IDataSourceDeleteRequest";
import { IDataSourceDeleteResponse } from "./delete/response/IDataSourceDeleteResponse";
import { IDataSourceMetadataRequest } from "./metadata/request/IDataSourceMetadataRequest";
import { IDataSourceMetadataResponse } from "./metadata/response/IDataSourceMetadataResponse";
import { IDataSourceReadRequest } from "./read/request/IDataSourceReadRequest";
import { IDataSourceReadRequestFilterNode } from "./read/request/IDataSourceReadRequestFilterBlock";
import { IDataSourceReadResponse } from "./read/response/IDataSourceReadResponse";
import { IDataSourceUpdateRequest } from "./update/request/IDataSourceUpdateRequest";
import { IDataSourceUpdateResponse } from "./update/response/IDataSourceUpdateResponse";

interface IEntityProperty {
    value: any;
    type?: string;
}

interface IEntityUuid {
    uuid: string;
}

interface IMetadataProperty {
    type?: string;
    reference?: string;
    compositeType?: Array<string>;
}

type IDataSourceAvailableEntityResponse = Array<IDataSourceAvailableEntity>;
interface IDataSourceAvailableEntity {
    entityName: string;
    verboseName: string;
}

abstract class IDataSource {
    public abstract read(request: IDataSourceReadRequest): Observable<IDataSourceReadResponse>;

    public abstract create(request: IDataSourceCreateRequest): Observable<IDataSourceCreateResponse>;
    public abstract update(request: IDataSourceUpdateRequest): Observable<IDataSourceUpdateResponse>;
    public abstract delete(request: IDataSourceDeleteRequest): Observable<IDataSourceDeleteResponse>;

    /**
     * @description composite query for create/update/delete entities in one query
     * @param request
     */
    public abstract compositeQuery(request: IDataSourceCompositeRequest): Observable<IDataSourceCompositeResponse>;

    /**
     * @description method for return entity metadata
     * @param request
     */
    public abstract getMetadata(request: IDataSourceMetadataRequest): Observable<IDataSourceMetadataResponse>;

    /**
     * @description method for get available entities into repository
     */
    public abstract getAvailableEntities(): Observable<IDataSourceAvailableEntityResponse>;

    public abstract count(request: { entity: string, filter?: IDataSourceReadRequestFilterNode }): Observable<number>;
}



export { IDataSource, IEntityProperty, IEntityUuid, IMetadataProperty, IDataSourceAvailableEntity, IDataSourceAvailableEntityResponse };