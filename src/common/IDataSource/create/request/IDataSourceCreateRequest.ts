import { IEntityProperty } from "../../IDataSource";

interface IDataSourceCreateRequest {
    entities: Array<{
        entityName: string,
        entity: Map<string, IEntityProperty>
    }>;
}
export { IDataSourceCreateRequest };