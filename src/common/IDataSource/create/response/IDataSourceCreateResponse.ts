import { IEntityProperty, IEntityUuid } from "../../IDataSource";
interface IDataSourceCreateResponse {
    entities: Array<{ entityUuid: IEntityUuid, entity: Map<string, IEntityProperty> }>;
}

export { IDataSourceCreateResponse };