import { IEntityUuid } from "../../IDataSource";
interface IDataSourceDeleteRequest {
    entities: Array<{
        entityName: string
        entityUuid: IEntityUuid
    }>;
}

export { IDataSourceDeleteRequest };