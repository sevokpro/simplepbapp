interface IDataSourceDeleteResponse {
    entities: Array<{ result: 'OK' | 'BAD' }>;
}

export { IDataSourceDeleteResponse };