import { IDataSourceCreateResponse } from "../../create/response/IDataSourceCreateResponse";
import { IDataSourceDeleteResponse } from "../../delete/response/IDataSourceDeleteResponse";
import { IDataSourceUpdateResponse } from "../../update/response/IDataSourceUpdateResponse";

interface IDataSourceCompositeResponse {
    create: IDataSourceCreateResponse;
    update: IDataSourceUpdateResponse;
    delete: IDataSourceDeleteResponse;
}

export { IDataSourceCompositeResponse };