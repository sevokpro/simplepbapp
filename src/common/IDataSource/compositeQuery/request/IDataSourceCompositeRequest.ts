import { IDataSourceCreateRequest } from "../../create/request/IDataSourceCreateRequest";
import { IDataSourceDeleteRequest } from "../../delete/request/IDataSourceDeleteRequest";
import { IDataSourceUpdateRequest } from "../../update/request/IDataSourceUpdateRequest";

interface IDataSourceCompositeRequest {
    create: IDataSourceCreateRequest;
    update: IDataSourceUpdateRequest;
    delete: IDataSourceDeleteRequest;
}

export { IDataSourceCompositeRequest };