import { IEntityProperty, IEntityUuid } from "../../IDataSource";
interface IDataSourceUpdateResponse {
    entities: Array<{ entityUuid: IEntityUuid, entity: Map<string, IEntityProperty> }>;
}

export { IDataSourceUpdateResponse };