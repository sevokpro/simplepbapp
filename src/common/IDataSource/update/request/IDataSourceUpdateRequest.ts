import { IEntityProperty, IEntityUuid } from "../../IDataSource";
interface IDataSourceUpdateRequest {
    entities: Array<{
        entityUuid: IEntityUuid,
        entityName: string,
        entity: Map<string, IEntityProperty>
    }>;
}
export { IDataSourceUpdateRequest };