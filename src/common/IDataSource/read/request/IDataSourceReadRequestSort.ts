declare type IDataSourceReadRequestSort = Array<{ field: string, direction: 'asc' | 'desc' }>;

export { IDataSourceReadRequestSort };