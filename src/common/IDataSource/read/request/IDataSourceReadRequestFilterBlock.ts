import { IDataSourceReadRequestFilterComparsion } from "./IDataSourceReadRequestFilterBaseField";

abstract class IDataSourceReadRequestFilterNode {
    public abstract operator: 'and' | 'or';
    public abstract nodes: Array<IDataSourceReadRequestFilterComparsion | IDataSourceReadRequestFilterNode>;
    public abstract isNegative?: boolean;
}

export { IDataSourceReadRequestFilterNode };