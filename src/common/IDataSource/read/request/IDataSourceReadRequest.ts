import { IDataSourceReadRequestFilterNode } from "./IDataSourceReadRequestFilterBlock";
import { IDataSourceReadRequestSort } from "./IDataSourceReadRequestSort";

interface IDataSourceReadRequest {
    entity: string;
    limit?: number;
    offset?: number;
    filter?: IDataSourceReadRequestFilterNode;
    fields?: Array<string>;
    withTotalResult?: boolean;
    sort?: IDataSourceReadRequestSort;
}

export {
    IDataSourceReadRequest
};