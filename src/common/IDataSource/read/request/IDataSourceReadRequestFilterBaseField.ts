abstract class IDataSourceReadRequestFilterComparsion {
    public abstract field: string;
    public abstract operator: string;
    public abstract value: any;
    public abstract isNegative?: boolean;
}

const options = {
    equal: 'equal',
    greaterThen: 'greaterThen',
    lessThen: 'lessThen',
    lessOrEqual: 'lessOrEqual',
    greaterOrEqual: 'greaterOrEqual',
    isEmpty: 'isEmpty',
    in: 'in',
    like: 'like',
};

const moreOperators = {
    more: options.greaterThen,
    moreOrEqual: options.greaterOrEqual
};

const baseOperators = {
    equal: options.equal,
    isEmpty: options.isEmpty
};

const numberOperators = {...baseOperators, ...moreOperators};
const dateOperators = {...baseOperators, ...moreOperators};
const stringOperators = {...baseOperators, like: options.like};
const referenceOperators = {...baseOperators};


class DataSourceReadRequestFilterOperators {
    public static number = numberOperators;
    public static date = dateOperators;
    public static string = stringOperators;
    public static reference = referenceOperators;
}

export { IDataSourceReadRequestFilterComparsion, DataSourceReadRequestFilterOperators };