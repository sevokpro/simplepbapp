import { IEntityProperty } from "../../IDataSource";

interface IDataSourceReadResponseEntity {
    uuid: string;
    displayName: string;
    entity: Map<string, IEntityProperty>;
}

export { IDataSourceReadResponseEntity };