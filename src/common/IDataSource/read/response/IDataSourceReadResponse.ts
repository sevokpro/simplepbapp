import { IDataSourceReadResponseEntity } from "./IDataSourceReadResponseEntity";

interface IDataSourceReadResponse {
    entities: Array<IDataSourceReadResponseEntity>;
}

export { IDataSourceReadResponse };