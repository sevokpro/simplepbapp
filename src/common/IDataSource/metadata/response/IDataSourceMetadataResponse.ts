import { IMetadataProperty } from "../../IDataSource";
interface IDataSourceMetadataResponse {
    properties: Map<string, IMetadataProperty>;
}

export { IDataSourceMetadataResponse };