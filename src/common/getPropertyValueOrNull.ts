function getPropertyValueOrNull<TObj>(obj: TObj, fun: (obj: TObj) => any) {
    try {
        return fun(obj);
    }catch (err) {
        return null;
    }
}

export {getPropertyValueOrNull};