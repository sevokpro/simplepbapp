"use strict";
let originConfig = require('./../../webpack.config.js')({
    MODE: 'dev'
});
module.exports = function (config) {
    config.set({
        frameworks: ['jasmine'],

        files: [
            './main.ts'
        ],

        reporters: ['progress', 'kjhtml'],
        autoWatch: true,
        browsers: ['Chrome'],

        preprocessors: {'**/*.ts': ['webpack', 'sourcemap']},

        webpack: {
            resolve: originConfig.resolve,
            module: originConfig.module,
            devtool: originConfig.devtool
        },
        mime: {
            'text/x-typescript': ['ts','tsx']
        },
        singleRun: false
    })
};