let HtmlWebpackPlugin = require('html-webpack-plugin');
let webpack = require('webpack');
let path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const AotPlugin = require('@ngtools/webpack').AotPlugin;
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = (envOptions) => {
    if (!envOptions.hasOwnProperty('MODE')) {
        envOptions.MODE = 'dev'
    }
    if (!envOptions.hasOwnProperty('BUNDLE_ANALYZER')) {
        envOptions.BUNDLE_ANALYZER = 'false'
    }

    let config = {
        entry: {
            index: undefined,
            core: `${__dirname}/src/core/core.module.ts`,
            client: `${__dirname}/src/app/app.module.ts`,
            libs: [
                'core-js/es7/reflect',
                'zone.js',
                '@angular/core',
                '@angular/common',
                '@angular/forms',
                '@angular/http',
                '@angular/router'
            ]
        },
        devtool: `source-map`,
        resolve: {
            extensions: [`.tsx`, `.ts`, `.js`],
            modules: [
                path.resolve(__dirname, "node_modules"),
                path.resolve(__dirname, "./")
            ],
            alias: {
                "coreContracts": path.resolve(__dirname, "src/core/contracts/"),
                "coreModulesSdk": path.resolve(__dirname, "src/core/coreModulesSdk/")
            }
        },
        module: {
            rules: [{
                test: /\.pug$/,
                use: [
                    `raw-loader`,
                    `pug-html-loader?basedir=${__dirname}`
                ]
            }, {
                test: /\.styl$/,
                use: [
                    'raw-loader', 'stylus-loader'
                ]
            }, {
                test: /\.css$/,
                use: [
                    `style-loader`,
                    `css-loader`
                ]
            }, {
                test: /\.html$/,
                use: `html-loader`
            }, {
                test: /\.(ttf|eot|woff(2)?)(\?[a-z0-9=&.]+)?$/,
                use: `file-loader`
            }, {
                test: /\.(jpe?g|png|gif|svg)$/i,
                use: `file-loader`
            }, {
                test: /\.md$/,
                use: [{
                    loader: 'html-loader'
                }, {
                    loader: 'markdown-loader'
                }]
            }]
        },
        plugins: [
            new CopyWebpackPlugin([{
                from: `${__dirname}/src/static/**/*`
            }]),
            new HtmlWebpackPlugin({
                template: `${__dirname}/src/index.pug`
            }),
            new webpack.optimize.CommonsChunkPlugin({
                names: ['index', 'core', 'client', 'libs']
            })
        ],
        devServer: {
            inline: true,
            historyApiFallback: true,
            proxy: {
                '/api': {
                    target: 'http://localmail.itexpert.ru:6001',
                    secure: false,
                    changeOrigin: true
                }
            },
            hot: true
        }
    };

    if (envOptions.BUNDLE_ANALYZER === 'true') {
        config.plugins.push(
            new BundleAnalyzerPlugin({
                analyzerMode: 'server',
                analyzerHost: '127.0.0.1',
                analyzerPort: 8888,
                openAnalyzer: false
            })
        )
    }

    if (envOptions.MODE === `dev`) {
        config.entry.index = `./src/index.jit.ts`;

        config.module.rules.push();

        config.module.rules.push({
            test: /\.ts$/,
            use: [
                `awesome-typescript-loader`,
                `angular-router-loader`,
                `angular2-template-loader`
            ],
            exclude: `/node_modules/`
        });

        config.output = {
            filename: `[name].js`,
            path: `${__dirname}/serve`,
            publicPath: `/`
        };

        config.plugins.push(new webpack.ContextReplacementPlugin(
            /angular(\\|\/)core(\\|\/)@angular/,
            path.resolve(__dirname, 'src')
        ));
        config.plugins.push(new webpack.HotModuleReplacementPlugin())

    }

    if (envOptions.MODE === `prod`) {
        config.entry.index = './src/index.aot.ts';

        config.module.rules.push({
            test: /\.ts$/,
            loaders: [
                '@ngtools/webpack'
            ]
        });

        config.plugins.push(new AotPlugin({
            tsConfigPath: './tsconfig.json',
            entryModule: 'src/bootstrap.module#BootstrapModule'
        }));

        config.plugins.push(new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false,
                drop_console: false
            },
            sourceMap: true
        }));

        config.output = {
            filename: `[name][hash].js`,
            path: `${__dirname}/dist`,
            publicPath: `/`
        }
    }

    return config
}