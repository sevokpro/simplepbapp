Запуск проекта:
* npm install && npm start

Настройка выполнения линтера при сохранении:

Webstorm:
* File > Settings > Tools > FileWatchers
* Импорт настроек ( дверка с зеленой стрелочкой, указывающая внутрь )
* путь к настройке -> `${текущий_проект}/utils/webstorm/watchers/tsLinterWatcher/watchers.xml`

Остальные IDE: необходимо написать нативный скрипт - вотчер