FROM alpine:latest

RUN apk add --update nginx
RUN mkdir -p /run/nginx

COPY nginx.conf /etc/nginx/
COPY ./dist/ /usr/share/nginx/html/

CMD ["nginx", "-g", "daemon off;"]